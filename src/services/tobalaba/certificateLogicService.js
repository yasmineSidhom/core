const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));
//const web3 = new Web3(new Web3.providers.WebsocketProvider('ws://127.0.0.1:8546'));

var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');
var fetch =require('node-fetch');
var contract = require("truffle-contract");
var ethers = require('ethers');

let CertificateLogic_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/CertificateLogic.json');
const abiCertificateLogic = CertificateLogic_artefact.abi;
const addressCertificateLogic = CertificateLogic_artefact.networks[401697].address;
const myContractCertificateLogic = new web3.eth.Contract(abiCertificateLogic, addressCertificateLogic);

let AssetProducingRegistry_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/AssetProducingRegistryLogic.json');
const abiAssetProducing = AssetProducingRegistry_artefact.abi;
const addressAssetProducing = AssetProducingRegistry_artefact.networks[401697].address;
const myContractAssetProducing = new web3.eth.Contract(abiAssetProducing, addressAssetProducing);


let CertificateDB_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/CertificateDB.json');
const abiCertificateDb = CertificateDB_artefact.abi;
const addressCertificateDb = CertificateDB_artefact.networks[401697].address;
const myContractCertificateDb = new web3.eth.Contract(abiCertificateDb, addressCertificateDb);

var Contract = new ethers.Contract(addressCertificateDb, abiCertificateDb, new  ethers.providers.JsonRpcProvider('http://127.0.0.1:8545'));

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
//var privateKey = require('./privateKey').privateKeyProducer();

module.exports = {

  ////************************************************* CREATE AND TRANSFER  CERTIFICATE ********************************************************************************///////

  //Create a certificate for asset owner with Logic Contract
  createCertificateForAssetOwner:function(needEndPoint=true){
    const createCertificateForAssetOwner = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        console.log('createCertificateForAssetOwner starts with data :');
        console.log(data);
        var assetId=data._assetId;   //uint
        var powerInW=data._powerInW; //uint
        var productionDate=data._productionDate; //uint
        var info=data._info;                     //bytes32
        var sender=data._sender;     //address

        var openDoorDataPart = myContractCertificateLogic.methods.createCertificateForAssetOwner(
          assetId,
          powerInW,
          productionDate,
          web3.utils.fromAscii(info)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('createCertificateForAssetOwner nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        console.log(JSON.stringify(serializedTx));
        console.log(JSON.stringify(serializedTx.toString('hex')));
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        console.log('createCertificateForAssetOwner before web3.eth.abi.decodeLog');
        console.log(JSON.stringify(ret));
        console.log(JSON.stringify(ret.logs));
        console.log(ret.logs[0]);
        const result = await web3.eth.abi.decodeLog([{
          type: 'uint',
          name: '_certificateId',
          indexed: true
        },{
          type: 'uint',
          name: '_powerInW',
        },{
          type: 'address',
          name: '_owner',
        },{
          type: 'address',
          name: '_escrow',
        },{
          type: 'uint',
          name: '_productionDate',
        }], ret.logs[0].data, ret.logs[0].topics[1]);
        ret.logs[0]["result"]=result;

        console.log('createCertificateForAssetOwner finished');
        console.log(ret);
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return createCertificateForAssetOwner;
    }
    this.use('/createCertificateForAssetOwner', createCertificateForAssetOwner);
  },

  //Create a certificate with Logic Contract
  createCertificate:function(needEndPoint=true){
    const createCertificate = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId;                //uint
        var owner=data._owner;                    //address
        var powerInW=data._powerInW;              //uint
        var productionDate=data._productionDate;  //uint
        var info=data._info;                      //bytes32
        var sender=data._sender;                  //address

        var openDoorDataPart = myContractCertificateLogic.methods.createCertificate(
          assetId,
          owner,
          powerInW,
          productionDate,
          web3.utils.fromAscii(info)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('createCertificate nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        const result = await web3.eth.abi.decodeLog([{
          type: 'uint',
          name: '_certificateId',
          indexed: true
        },{
          type: 'uint',
          name: '_powerInW',
        },{
          type: 'address',
          name: '_owner',
        },{
          type: 'address',
          name: '_escrow',
        },{
          type: 'uint',
          name: '_productionDate',
        },{
          type: 'bytes32',
          name: '_info',
        }], ret.logs[0].data, ret.logs[0].topics[1]);
        ret.logs[0]["result"]=result;

        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return createCertificate;
    }
    this.use('/createCertificate', createCertificate);
  },

  //set an txHash of a certificate with Logic Contract
  setCertificateTxHash:function(needEndPoint=true){
    const setCertificateTxHash = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var certificateId=data._certificateId; //uint
        var txHash=data._txHash;               //bytes32
        var sender=data._sender;               //address

        var openDoorDataPart = myContractCertificateLogic.methods.setCertificateTxHash(
          certificateId,
          txHash
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setCertificateTxHash;
    }
    this.use('/setCertificateTxHash', setCertificateTxHash);
  },

  //set an txHash of a certificate with Logic Contract
  setCertificateHash:function(needEndPoint=true){
    const setCertificateHash = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var certificateId=data._certificateId; //uint
        var sender=data._sender;               //address

        var openDoorDataPart = myContractCertificateLogic.methods.setCertificateHash(
          certificateId
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setCertificateHash;
    }
    this.use('/setCertificateHash', setCertificateHash);
  },

  //Get the certificate details with logic certificate contract
  getCertificate:function(needEndPoint=true){
    var getCertificate={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var certificateId=data._certificateId;
        var sender=data._sender;   //address

        const result= await myContractCertificateLogic.methods.getCertificate(certificateId).call({ from:sender });
        console.log(JSON.stringify(result));
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        response._certificateId = certificateId;
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getCertificate;
    }
    this.use('/getCertificate',getCertificate);
  },

  //Get the address owner of a certificate with logic certificate contract
  getCertificateOwner:function(needEndPoint=true){
    var getCertificateOwner={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        //var account=data.account
        var certificateId=data._certificateId;
        var sender=data._sender;   //address

        const result= await myContractCertificateLogic.methods.getCertificateOwner(certificateId).call({ from:sender });
        console.log(JSON.stringify(result));
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getCertificateOwner;
    }
    this.use('/getCertificateOwner',getCertificateOwner);
  },

  //Transfer certificate to another one with logic certificate contract
  changeCertificateOwner:function(needEndPoint=true){
    const changeCertificateOwner = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        //var account=data.account
        var certificateId=data._certificateId;
        var newOwner=data._newOwner;
        var sender=data._sender;   //address

        var openDoorDataPart = myContractCertificateLogic.methods.changeCertificateOwner(
          certificateId,
          newOwner
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('changeCertificateOwner nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return changeCertificateOwner;
    }
    this.use('/changeCertificateOwner', changeCertificateOwner);
  },

  //get Certificate List Length with logic certificate contract
  getCertificateListLength:function(needEndPoint=true){
    var getCertificateListLength={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;   //address

        const result= await myContractCertificateLogic.methods.getCertificateListLength().call({ from:sender });
        console.log(JSON.stringify(result));
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getCertificateListLength;
    }
    this.use('/getCertificateListLength',getCertificateListLength);
  },

  //is Certificate Retired with logic certificate contract
  isRetired:function(needEndPoint=true){
    var isRetired={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var certificateId=data._certificateId;
        var sender=data._sender;   //address

        const result= await myContractCertificateLogic.methods.isRetired(certificateId).call({ from:sender });
        console.log("Is certificate retired: "+result);
        var reponse=JSON.stringify(result);
        return Promise.resolve(reponse);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return isRetired;
    }
    this.use('/isRetired',isRetired);
  },

  //retire Certificate with logic certificate contract
  retireCertificate:function(needEndPoint=true){
    const retireCertificate = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var certificateId=data._certificateId;
        var sender=data._sender;   //address

        var openDoorDataPart = myContractCertificateLogic.methods.retireCertificate(
          certificateId
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('retireCertificate nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return retireCertificate;
    }
    this.use('/retireCertificate', retireCertificate);
  },

  //transfer Ownership By Escrow with logic certificate contract
  transferOwnershipByEscrow:function(needEndPoint=true){
    const transferOwnershipByEscrow = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var certificateId=data._certificateId;//uint
        var newOwner=data._newOwner;          //address
        var sender=data._sender;              //address

        var openDoorDataPart = myContractCertificateLogic.methods.transferOwnershipByEscrow(
          certificateId,
          newOwner
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('transferOwnershipByEscrow nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCertificateLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return transferOwnershipByEscrow;
    }
    this.use('/transferOwnershipByEscrow', transferOwnershipByEscrow);
  },

  getAllCertificates:function(needEndPoint=true){
    var getAllCertificates={
      async find(params){},
      async get(params){},
      async create (data, params){
        var owner=data._owner;             //address
        var sender=data._sender;           //address
        var certificates=[];        //address
        var prodAssets=[];

        console.log("getAllCertificates sender "+sender)
        console.log("getAllCertificates owner "+owner)

        const certificatesListLength = await myContractCertificateLogic.methods.getCertificateListLength().call({ from:sender });
        console.log("getAllCertificates certificateLogicService ListLength: "+certificatesListLength);
        //Use the offchain data, later we will use the data on chain, checking the roles
        const users = await fetch(process.env.API_URL + "/getAllUsers", {
          method: "POST",
          body: JSON.stringify({
            "_sender": sender
          }),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(handleErrors)
        .then(res => res.json())
        .then(data => {
          return data
        })
        .catch(error => {
          console.log("error in getAllUsers in getAllCertificates : ");
          console.log(error);
        });

        const prodAssetsListLength = await myContractAssetProducing.methods.getAssetListLength().call({ from:sender });

        for (i = 0; i < prodAssetsListLength; i++) {
          //Get each producing asset from the blockchain (but not the demo assets...)
          const prodAssetLocation = await myContractAssetProducing.methods.getAssetLocation(i).call({ from:sender });
          prodAssetLocation[0]=web3.utils.toAscii(prodAssetLocation[0]).replace(/\0/g, '');
          prodAssetLocation[1]=web3.utils.toAscii(prodAssetLocation[1]).replace(/\0/g, '');
          prodAssetLocation[2]=web3.utils.toAscii(prodAssetLocation[2]).replace(/\0/g, '');
          prodAssetLocation[3]=web3.utils.toAscii(prodAssetLocation[3]).replace(/\0/g, '');
          prodAssetLocation[4]=web3.utils.toAscii(prodAssetLocation[4]).replace(/\0/g, '');
          prodAssetLocation[5]=web3.utils.toAscii(prodAssetLocation[5]).replace(/\0/g, '');
          prodAssetLocation[6]=web3.utils.toAscii(prodAssetLocation[6]).replace(/\0/g, '');
          prodAssetLocation[7]=web3.utils.toAscii(prodAssetLocation[7]).replace(/\0/g, '');

          prodAssets.push({
            id: i,
            name: (prodAssetLocation[5]+" "+prodAssetLocation[4]).trim()
          });
        }



        const currentUser = users.find(user => {
          return user.address === owner;
        })

        console.log("getAllCertificates currentUser "+currentUser)
        //const isAdmin = currentUser && currentUser.roles <= 1 ? true : false;
        const isAdmin = currentUser && currentUser.organization === "ENGIE" ? true : false;

        console.log("getAllCertificates isAdmin "+isAdmin)

        for(i=0;i<certificatesListLength;i++){
          var certificate= await myContractCertificateLogic.methods.getCertificate(i).call({ from:sender });
          certificate._certificateId = i;

          const certificateOwner = users.find(user => {
            return user.address ? certificate[1] === user.address : false;
          })


          certificate._ownerName = certificateOwner ? certificateOwner.organization : 'Unknown';

          const certificateResource = prodAssets.find(prodAsset => {
            return parseInt(certificate[0]) === parseInt(prodAsset.id);
          })

          certificate._resourceName = certificateResource ? certificateResource.name : 'Unknown';

          if(isAdmin || (owner && certificate[1] === owne && certificate._retired === false)) {
            certificates.push(certificate);
          }
        }
        console.log("getAllCertificates certificates.length "+certificates.length)
        console.log("getAllCertificates certificateLogicService finished");

        return Promise.resolve(certificates);
      }
    };
    if(needEndPoint==false){
      return getAllCertificates;
    }
    this.use('/getAllCertificates',getAllCertificates);
  },

  getAllCertificatesFromServer:function(needEndPoint=true){
    var getAllCertificatesFromServer={
      async find(params){},
      async get(params){},
      async create (data, params){
        console.log(data);
        //var owner=data._owner;             //address
        var sender=data._sender;             //address

        //fetch Data from server
        var fetchCertificate = await fetch(process.env.API_URL + "/getAllCertificates", {
          method: "POST",
          body: JSON.stringify({
            "_owner": "0x0051CcCBCFF51788dDa9614b12dcea0D57208C03",
            "_sender": "0x0051CcCBCFF51788dDa9614b12dcea0D57208C03"
          }),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(function(response) {
          /* response.status     //=> number 100–599
          response.statusText //=> String
          response.headers    //=> Headers
          response.url        //=> String */

          return response.text();
        }, function(error) {
          error.message; //=> String
        });

        fetchCertificate = JSON.parse(fetchCertificate);
        console.log("fetchCertificate: "+fetchCertificate);

        for(j=0;j<fetchCertificate.length;j++)
        {
          openDoorDataPart = await myContractCertificateLogic.methods.retrieveCertificate(
            parseInt(fetchCertificate[j]._assetId,10),
            fetchCertificate[j]._owner,
            parseInt(fetchCertificate[j]._powerInW,10),
            fetchCertificate[j]._retired,
            fetchCertificate[j]._dataLog,
            parseInt(fetchCertificate[j]._coSaved,10),
            fetchCertificate[j]._escrow,
            parseInt(fetchCertificate[j]._creationTime,10),
            fetchCertificate[j]._txHash,
            parseInt(fetchCertificate[j]._productionDate,10),
            fetchCertificate[j]._info
          ).encodeABI();
          console.log(openDoorDataPart);

          nonce= await web3.eth.getTransactionCount(sender);
          console.log(nonce);

          rawTx = {
            nonce: web3.utils.toHex(nonce),
            value: 0x00,
            gasPrice: web3.utils.toHex(154140000),
            gasLimit: web3.utils.toHex(1000000),
            data: openDoorDataPart,
            to: addressCertificateLogic,
            from: sender
          };

          tx = new Tx(rawTx);
          await tx.sign(privateKey.returnPrivateKey(sender));
          serializedTx = await tx.serialize();
          ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
          console.log("Receipt: "+JSON.stringify(ret));
          console.log(j);
        }
        console.log("Succed recover for certificates");
      }
    };
    if(needEndPoint==false){
      return getAllCertificatesFromServer;
    }
    this.use('/getAllCertificatesFromServer',getAllCertificatesFromServer);
  },

  getAllCertificatesFromCertificateDB:function(needEndPoint=true){
    var getAllCertificatesFromCertificateDB={
      async find(params){},
      async get(params){},
      async create (data, params){
        console.log(data);
        var sender = data._sender;           //address
        var listCertificates = await Contract.getAllCertificates();

        listCertificates.map(x=>{
          x[0] = x[0].toNumber();
          x[2] = x[2].toNumber();
          x[5] = x[5].toNumber();
          x[7] = x[7].toNumber();
          x[9] = x[9].toNumber();
        });

        return Promise.resolve(listCertificates);

      }
    };
    if(needEndPoint==false){
      return getAllCertificatesFromCertificateDB;
    }
    this.use('/getAllCertificatesFromCertificateDB',getAllCertificatesFromCertificateDB);
  },

  //Get the listLength without call a function from CertificateLOgic
  getSmartContractVersion:function(needEndPoint=true){
    var getSmartContractVersion={
      async find(params){},
      async get(params){},
      async create (data, params){
        console.log(data);
        var sender = data._sender;           //address

        //Put the CertificatesLogic's address
        var listCertificates = await myContractCertificateLogic.methods.getSmartContractVersion().call({ from:"0x002cbE31Ed26DA610f742f1EE2B957229e57a8e4" });

        return Promise.resolve(listCertificates);

      }
    };
    if(needEndPoint==false){
      return getSmartContractVersion;
    }
    this.use('/getSmartContractVersion',getSmartContractVersion);
  },

};
