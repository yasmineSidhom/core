const Web3 = require('web3');
const functionPrivateKey=require('./privateKey');

//var web3 = new Web3(new Web3.providers.HttpProvider('http://52.232.116.178:8545'));
//var web3 = new Web3(new Web3.providers.HttpProvider("http://185.183.159.109:8545"));
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));
//var web3 = new Web3(Web3.givenProvider || 'ws://52.232.116.178:8546');

//Define accounts
//all accounts in the blockchain
//var accountProducer="0x002cbE31Ed26DA610f742f1EE2B957229e57a8e4";
//var accountConsumer="0x002A0494aaF7462a17A94ec50cAF23a89Dd63Dc8";
//var accounts=[accountProducer,accountConsumer];
var accounts;

// Get the initial account balance so it can be displayed.
web3.eth.getAccounts(function(err, accs) {
  if (err != null) {
    console.log('There was an error fetching your accounts.');
    return;
  }
  if (accs.length == 0) {
    console.log("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
    return;
  }
  accounts = accs;
  accountProducer = accounts[0];
  var balanceProducer;
  accountConsumer=accounts[1];
  var balanceConsumer;
  console.log("Info:");
  console.log("account 0 of producer: "+accountProducer);
  console.log("account 1 of consumer: "+accountConsumer);
  console.log("All accounts: "+accounts);


  balanceProducer=web3.eth.getBalance(accountProducer).then(function(result){
    console.log("Balance of account 0: "+result*0.000000000000000001+" ether");
    return result*0.000000000000000001;
  });

  balanceConsumer=web3.eth.getBalance(accountConsumer).then(function(result){
    console.log("Balance of account 1: "+result*0.000000000000000001+" ether");
    return result*0.000000000000000001;
  });
  //web3.eth.sendTransaction({from:accountConsumer, to:accountProducer, value: 200000000000000});
});

web3.eth.getBlockNumber(function(err,accs){
  console.log("Nombre de blocs: "+accs);
});

module.exports = {
  ///*********************************************************************GET ALL ACCOUNT ********************************************************************************/////
  
  //Get all accounts
  getAllAccounts:function(needEndPoint=true){
    var compte={
      async find(params){ 
        return Promise.resolve(accounts);
        //callback(null,this.accounts);
      },
      async get(id, params){
      //callback(null,accounts);
        return Promise.resolve(web3.eth.getAccounts(function(err, accs) {
          if (err != null) {
            console.log('There was an error fetching your accounts.');
            return;
          }
          if (accs.length == 0) {
            console.log("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
            return;
          }
          accounts = accs;
          return accounts;
        }));
      },
      async create(data, params) {},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint == false){
      return compte;
    }
    this.use('/accounts',compte);
  },

  //Create Account to get public and private key
  createAccount:function(needEndPoint=true){
    var createAccount={
      async find(params){ 
        return Promise.resolve(web3.eth.accounts.create());
        //callback(null,this.accounts);
      },
      async get(id, params){},
      async create(data, params) {},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return createAccount;
    }
    this.use('/createAccount',createAccount);
  },

  //Encrypt a private key to the web3 keystore v3 standard.
  encrypt:function(needEndPoint=true){
    var encrypt={
      async find(params){ },
      async get(id, params){},
      async create(data, params) {
        var privateKey=data._privateKey;
        var password=data._password;
        const keyStore = await web3.eth.accounts.encrypt(privateKey,password);
        await functionPrivateKey.addKeyStorePassword(keyStore,password);
        return Promise.resolve(keyStore);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return encrypt;
    }
    this.use('/encrypt',encrypt);
  },

  //Decrypt a keystore v3 JSON and creates the account
  decrypt:function(needEndPoint=true){
    var decrypt={
      async find(params){ },
      async get(id, params){},
      async create(data, params) {
        var keystoreJsonV3 = data._keystoreJsonV3;
        var password = data._password;
        return Promise.resolve(web3.eth.accounts.decrypt(keystoreJsonV3,password));
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return decrypt;
    }
    this.use('/decrypt',decrypt);
  },
};
