const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');

let AssetProducingRegistry_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/AssetProducingRegistryLogic.json');
const abiAssetProducing = AssetProducingRegistry_artefact.abi;
const addressAssetProducing = AssetProducingRegistry_artefact.networks[401697].address;
const myContractAssetProducing = new web3.eth.Contract(abiAssetProducing, addressAssetProducing);

//var privateKey = require('./privateKey').privateKeyProducer();
//console.log(privateKey);
module.exports=
{
  /////************************************************************* CREATE ASSET ****************************************************************************************///////
  //Create asset producing registry
  createAssetProducing:function(needEndPoint=true){
    const createAssetProducing = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;

        var openDoorDataPart = myContractAssetProducing.methods.createAsset().encodeABI();
        console.log('createAssetProducing openDoorDataPart');
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('createAssetProducing nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return createAssetProducing;
    }
    this.use('/createAsset/producer', createAssetProducing);
  },

  //Asset producing registry
  initGeneralAssetProducing:function(needEndPoint=true){
    const initGeneralAssetProducing = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId;                      //uint
        var smartMeter=data._smartMeter;                //address
        var owner=data._owner;                          //address
        var operationalSince=data._operationalSince;    //uint
        var active=data._active;                        //bool
        var sender=data._sender;                        //adddress

        var openDoorDataPart = myContractAssetProducing.methods.initGeneral(
          assetId,
          smartMeter,
          owner,
          operationalSince,
          active
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('initGeneralAssetProducing nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {
        var assetId=data._id;                                 //uint
        var smartMeter=data._smartMeter;                //address
        var owner=data._owner;                          //address
        var operationalSince=data._operationalSince;    //uint
        var active=data._active;                        //bool
        var sender=data._sender;                        //adddress

        var openDoorDataPart = myContractAssetProducing.methods.initGeneral(
          parseInt(assetId,10),
          smartMeter,
          owner,
          operationalSince,
          active
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return initGeneralAssetProducing;
    }
    this.use('/initGeneralAsset/producer', initGeneralAssetProducing);
  },

  //init producing properties with the smart contract assetProducingRegistryLogic
  assetInitProducingProperties:function(needEndPoint=true){
    const assetInitProducingProperties = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId ;                               //uint
        var assetType=data._assetType ;                           //AssetType
        var capacityWh=data._capacityWh ;                         //uint
        var registryCompliance=data._registryCompliance ;         //Compliance
        var otherGreenAttributes=data._otherGreenAttributes ;     //bytes32
        var typeOfPublicSupport=data._typeOfPublicSupport ;       //bytes32
        var sender=data._sender;                                  //Address

        var openDoorDataPart = myContractAssetProducing.methods.initProducingProperties(
          assetId,
          assetType,
          capacityWh,
          registryCompliance,
          web3.utils.fromAscii(otherGreenAttributes),
          web3.utils.fromAscii(typeOfPublicSupport)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('assetInitProducingProperties nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {
        var assetId=data._id ;                                          //uint
        var assetType=data._assetType ;                           //AssetType
        var capacityWh=data._capacityWh ;                         //uint
        var registryCompliance=data._registryCompliance ;         //Compliance
        var otherGreenAttributes=data._otherGreenAttributes ;     //bytes32
        var typeOfPublicSupport=data._typeOfPublicSupport ;       //bytes32
        var sender=data._sender;                                  //Address

        var openDoorDataPart = myContractAssetProducing.methods.initProducingProperties(
          parseInt(assetId,10),
          assetType,
          capacityWh,
          registryCompliance,
          web3.utils.fromAscii(otherGreenAttributes),
          web3.utils.fromAscii(typeOfPublicSupport)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return assetInitProducingProperties;
    }
    this.use('/assetInitProperties/producer', assetInitProducingProperties);
  },

  //init Location producing properties with the smart contract assetProducingRegistryLogic
  assetInitLocationProducing:function(needEndPoint=true){
    const assetInitLocationProducing = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId= data._assetId ;            //uint
        var country= data._country ;            //bytes32
        var region= data._region ;              //bytes32
        var zip= data._zip ;                    //bytes32
        var city= data._city ;                  //bytes32
        var street= data._street ;              //bytes32
        var houseNumber= data._houseNumber ;    //bytes32
        var gpsLatitude= data._gpsLatitude ;    //bytes32
        var gpsLongitude= data._gpsLongitude ;  //bytes32
        var sender=data._sender;                //address

        var openDoorDataPart = myContractAssetProducing.methods.initLocation(
          parseInt(assetId, 10),
          web3.utils.fromAscii(country),
          web3.utils.fromAscii(region),
          web3.utils.fromAscii(zip),
          web3.utils.fromAscii(city),
          web3.utils.fromAscii(street),
          web3.utils.fromAscii(houseNumber),
          web3.utils.fromAscii(gpsLatitude),
          web3.utils.fromAscii(gpsLongitude)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('assetInitLocationProducing nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {
        var assetId= data._id ;                       //uint
        var country= data._country ;            //bytes32
        var region= data._region ;              //bytes32
        var zip= data._zip ;                    //bytes32
        var city= data._city ;                  //bytes32
        var street= data._street ;              //bytes32
        var houseNumber= data._houseNumber ;    //bytes32
        var gpsLatitude= data._gpsLatitude ;    //bytes32
        var gpsLongitude= data._gpsLongitude ;  //bytes32
        var sender=data._sender;                //address

        var openDoorDataPart = myContractAssetProducing.methods.initLocation(
          parseInt(assetId, 10),
          web3.utils.fromAscii(country),
          web3.utils.fromAscii(region),
          web3.utils.fromAscii(zip),
          web3.utils.fromAscii(city),
          web3.utils.fromAscii(street),
          web3.utils.fromAscii(houseNumber),
          web3.utils.fromAscii(gpsLatitude),
          web3.utils.fromAscii(gpsLongitude)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return assetInitLocationProducing;
    }
    this.use('/assetInitLocation/producer', assetInitLocationProducing);
  },

  //asset Save Smart Meter Read from producing registry contract
  assetSaveSmartMeterReadProducing:function(needEndPoint=true){
    const assetSaveSmartMeterReadProducing = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        console.log('assetSaveSmartMeterReadProducing start');
        var assetId = data._assetId;                                        //uint
        var newMeterRead= data._newMeterRead;                               //uint
        var smartMeterDown= data._smartMeterDown;                           //bool
        var lastSmartMeterReadFileHash =  web3.utils.fromAscii(data._lastSmartMeterReadFileHash); //bytes32
        var CO2OffsetMeterRead= data._CO2OffsetMeterRead;                   //uint
        var CO2OffsetServiceDown = data._CO2OffsetServiceDown;              //bool
        var sender=data._sender;                                            //address
        console.log('assetSaveSmartMeterReadProducing data :');
        console.log(data);

        var openDoorDataPart = myContractAssetProducing.methods.saveSmartMeterRead(
          assetId,
          newMeterRead,
          smartMeterDown,
          lastSmartMeterReadFileHash,
          CO2OffsetMeterRead,
          CO2OffsetServiceDown
        ).encodeABI();
        //console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('assetSaveSmartMeterReadProducing nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        console.log('assetSaveSmartMeterReadProducing finished');
        return Promise.resolve(ret);

      },
      async update(id, data, params) {
        var assetId = data._id;                                                   //uint
        var newMeterRead= data._newMeterRead;                               //uint
        var smartMeterDown= data._smartMeterDown;                           //bool
        var lastSmartMeterReadFileHash =  data._lastSmartMeterReadFileHash; //bytes32
        var CO2OffsetMeterRead= data._CO2OffsetMeterRead;                   //uint
        var CO2OffsetServiceDown = data._CO2OffsetServiceDown;              //bool
        var sender=data._sender;                                            //address

        var openDoorDataPart = myContractAssetProducing.methods.saveSmartMeterRead(
          parseInt(assetId,10),
          newMeterRead,
          smartMeterDown,
          lastSmartMeterReadFileHash,
          CO2OffsetMeterRead,
          CO2OffsetServiceDown
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return assetSaveSmartMeterReadProducing;
    }
    this.use('/assetSaveSmartMeterRead/producer', assetSaveSmartMeterReadProducing);
  },

  //update Smart Meter from producing registry contract
  updateSmartMeterProducing:function(needEndPoint=true){
    const updateSmartMeterProducing = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId = data._assetId;                                        //uint
        var newSmartMeter= data._newSmartMeter;                             //address
        var sender=data._sender;                                            //address

        var openDoorDataPart = myContractAssetProducing.methods.updateSmartMeter(
          assetId,
          newSmartMeter,
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log('updateSmartMeterProducing nonce :'+nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetProducing,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return updateSmartMeterProducing;
    }
    this.use('/updateSmartMeter/producer', updateSmartMeterProducing);
  },


  //*****************************************Get Iinformation Of Asset****************************************//
  //Get Asset producing registry
  getAssetProducing:function(needEndPoint=true){
    var getAssetProducing={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;   //address

        const result= await myContractAssetProducing.methods.getAssetGeneral(assetId).call({ from:sender });
        console.log('getAssetProducing');
        return Promise.resolve(result);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetProducing;
    }
    this.use('/getAsset/producer',getAssetProducing);
  },

  //get Asset Producing Properties producing registry
  getAssetProducingPropertiesProducing:function(needEndPoint=true){
    var getAssetProducingPropertiesProducing={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;   //address

        const result= await myContractAssetProducing.methods.getAssetProducingProperties(assetId).call({ from:sender });
        result[6]=web3.utils.toAscii(result[6]).replace(/\0/g, '');
        result[7]=web3.utils.toAscii(result[7]).replace(/\0/g, '');
        result['otherGreenAttributes']=web3.utils.toAscii(result['otherGreenAttributes']).replace(/\0/g, '');
        result['typeOfPublicSupport']=web3.utils.toAscii(result['typeOfPublicSupport']).replace(/\0/g, '');
        console.log('getAssetProducingPropertiesProducing:'+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetProducingPropertiesProducing;
    }
    this.use('/getAssetProperties/producer',getAssetProducingPropertiesProducing);
  },

  //get Asset Location
  getAssetLocationProducing:function(needEndPoint=true){
    var getAssetLocationProducing={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;   //address

        const result= await myContractAssetProducing.methods.getAssetLocation(assetId).call({ from:sender });
        console.log('getAssetLocationProducing:'+JSON.stringify(result));
        result[0]=web3.utils.toAscii(result[0]).replace(/\0/g, '');
        result[1]=web3.utils.toAscii(result[1]).replace(/\0/g, '');
        result[2]=web3.utils.toAscii(result[2]).replace(/\0/g, '');
        result[3]=web3.utils.toAscii(result[3]).replace(/\0/g, '');
        result[4]=web3.utils.toAscii(result[4]).replace(/\0/g, '');
        result[5]=web3.utils.toAscii(result[5]).replace(/\0/g, '');
        result[6]=web3.utils.toAscii(result[6]).replace(/\0/g, '');
        result[7]=web3.utils.toAscii(result[7]).replace(/\0/g, '');
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetLocationProducing;
    }
    this.use('/getAssetLocation/producer',getAssetLocationProducing);
  },

  //getCoSaved asset
  getAssetCoSavedProducing:function(needEndPoint=true){
    var getAssetCoSavedProducing={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var wh=data._wh;           //uint
        var sender=data._sender;   //address

        const result= await myContractAssetProducing.methods.getCoSaved(assetId, wh).call({ from:sender });
        console.log('getAssetCoSavedProducing: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetCoSavedProducing;
    }
    this.use('/getAssetCoSaved/producer',getAssetCoSavedProducing);
  },

  //getAssetDataLog asset
  getAssetDataLogProducing:function(needEndPoint=true){
    var getAssetDataLogProducing={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;   //address

        const result= await myContractAssetProducing.methods.getAssetDataLog(assetId).call({ from:sender });
        console.log('getAssetDataLogProducing: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetDataLogProducing;
    }
    this.use('/getAssetDataLog/producer',getAssetDataLogProducing);
  },

  //get asset list lenght producing registry
  getAssetListLengthProducing:function(needEndPoint=true){
    var getAssetListLengthProducing={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;//address

        const result= await myContractAssetProducing.methods.getAssetListLength().call({ from:sender });
        console.log('getAssetListLengthProducing: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetListLengthProducing;
    }
    this.use('/getAssetListLength/producer',getAssetListLengthProducing);
  },

  //Get past data smart meter
  getPastDataSmartMeter: function(needEndPoint=true){
    var getPastDataSmartMeter={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender = data._sender;      //address
        var assetId = data._AssetId;

        //ABI and address are from Olivier sever
        const abiAssetProducing = [ { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_newSmartMeter", "type": "address" } ], "name": "updateSmartMeter", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getAssetLocation", "outputs": [ { "name": "country", "type": "bytes32" }, { "name": "region", "type": "bytes32" }, { "name": "zip", "type": "bytes32" }, { "name": "city", "type": "bytes32" }, { "name": "street", "type": "bytes32" }, { "name": "houseNumber", "type": "bytes32" }, { "name": "gpsLatitude", "type": "bytes32" }, { "name": "gpsLongitude", "type": "bytes32" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_dbAddress", "type": "address" } ], "name": "init", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newLogic", "type": "address" } ], "name": "update", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "createAsset", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "db", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_country", "type": "bytes32" }, { "name": "_region", "type": "bytes32" }, { "name": "_zip", "type": "bytes32" }, { "name": "_city", "type": "bytes32" }, { "name": "_street", "type": "bytes32" }, { "name": "_houseNumber", "type": "bytes32" }, { "name": "_gpsLatitude", "type": "bytes32" }, { "name": "_gpsLongitude", "type": "bytes32" } ], "name": "initLocation", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "getAssetListLength", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "cooContract", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_role", "type": "uint8" }, { "name": "_caller", "type": "address" } ], "name": "isRole", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getActive", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_active", "type": "bool" } ], "name": "setActive", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getAssetDataLog", "outputs": [ { "name": "datalog", "type": "bytes32" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [ { "name": "_cooContract", "type": "address" } ], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_assetId", "type": "uint256" }, { "indexed": false, "name": "_oldMeterRead", "type": "uint256" }, { "indexed": false, "name": "_newMeterRead", "type": "uint256" }, { "indexed": false, "name": "_smartMeterDown", "type": "bool" }, { "indexed": false, "name": "_certificatesCreatedForWh", "type": "uint256" }, { "indexed": false, "name": "_oldCO2OffsetReading", "type": "uint256" }, { "indexed": false, "name": "_newCO2OffsetReading", "type": "uint256" }, { "indexed": false, "name": "_serviceDown", "type": "bool" } ], "name": "LogNewMeterRead", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "sender", "type": "address" }, { "indexed": true, "name": "_assetId", "type": "uint256" } ], "name": "LogAssetCreated", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_assetId", "type": "uint256" } ], "name": "LogAssetFullyInitialized", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_assetId", "type": "uint256" } ], "name": "LogAssetSetActive", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "_assetId", "type": "uint256" } ], "name": "LogAssetSetInactive", "type": "event" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_smartMeter", "type": "address" }, { "name": "_owner", "type": "address" }, { "name": "_operationalSince", "type": "uint256" }, { "name": "_active", "type": "bool" } ], "name": "initGeneral", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_assetType", "type": "uint8" }, { "name": "_capacityWh", "type": "uint256" }, { "name": "_registryCompliance", "type": "uint8" }, { "name": "_otherGreenAttributes", "type": "bytes32" }, { "name": "_typeOfPublicSupport", "type": "bytes32" } ], "name": "initProducingProperties", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_newMeterRead", "type": "uint256" }, { "name": "_smartMeterDown", "type": "bool" }, { "name": "_lastSmartMeterReadFileHash", "type": "bytes32" }, { "name": "_CO2OffsetMeterRead", "type": "uint256" }, { "name": "_CO2OffsetServiceDown", "type": "bool" } ], "name": "saveSmartMeterRead", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_co2", "type": "uint256" } ], "name": "setCO2UsedForCertificate", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_whUsed", "type": "uint256" } ], "name": "useWhForCertificate", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getAssetGeneral", "outputs": [ { "name": "_smartMeter", "type": "address" }, { "name": "_owner", "type": "address" }, { "name": "_operationalSince", "type": "uint256" }, { "name": "_lastSmartMeterReadWh", "type": "uint256" }, { "name": "_active", "type": "bool" }, { "name": "_lastSmartMeterReadFileHash", "type": "bytes32" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getAssetProducingProperties", "outputs": [ { "name": "assetType", "type": "uint256" }, { "name": "capacityWh", "type": "uint256" }, { "name": "certificatesCreatedForWh", "type": "uint256" }, { "name": "lastSmartMeterCO2OffsetRead", "type": "uint256" }, { "name": "cO2UsedForCertificate", "type": "uint256" }, { "name": "registryCompliance", "type": "uint256" }, { "name": "otherGreenAttributes", "type": "bytes32" }, { "name": "typeOfPublicSupport", "type": "bytes32" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getAssetType", "outputs": [ { "name": "", "type": "uint8" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getCompliance", "outputs": [ { "name": "c", "type": "uint8" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" } ], "name": "getCo2UsedForCertificate", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_assetId", "type": "uint256" }, { "name": "_wh", "type": "uint256" } ], "name": "getCoSaved", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
        const addressAssetProducing = "0xdd6fa25888d5f5d9ab161adfa317d14a13ca38f3";
        const myContractAssetProducing = new web3.eth.Contract(abiAssetProducing, addressAssetProducing);

        const filter = {
          filter: {
            _assetId: [parseInt(assetId,10)],
          },
          fromBlock: 0,
          toBlock: 'latest'};
        const events = await myContractAssetProducing.getPastEvents("LogNewMeterRead",filter);
        const map1 = events.map(x => x.returnValues._oldMeterRead);
        //console.log(events);
        map1.push(events[events.length-1].returnValues._newMeterRead);

        return Promise.resolve(map1);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getPastDataSmartMeter;
    }
    this.use('/getPastDataSmartMeter/producer',getPastDataSmartMeter);
  },

};
