const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

var fetch =require('node-fetch');
var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');

let UserLogic_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/UserLogic.json');
const abiUserLogic = UserLogic_artefact.abi;
const addressUserLogic = UserLogic_artefact.networks[401697].address;
const myContractUserLogic = new web3.eth.Contract(abiUserLogic, addressUserLogic);

//var privateKey = require('./privateKey').privateKeyProducer();
const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

module.exports ={
  /////**************************************************** SET USER INFORMATION ******************************************************************************///////

  //set User with contract logistic
  setUser:function(needEndPoint=true){
    const setUser = {
      async find(params){},
      async get(id, params) {},
      async create(data, params) {
        //var account=data.account
        var user = data._user;             //Address
        var firstName = data._firstName;
        var surname = data._surname;
        var organization = data._organization;
        var street = data._street;
        var number = data._number;
        var zip = data._zip;
        var city = data._city;
        var country = data._country;
        var state = data._state;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.setUser(
          user,
          web3.utils.fromAscii(firstName),
          web3.utils.fromAscii(surname),
          web3.utils.fromAscii(organization),
          web3.utils.fromAscii(street),
          web3.utils.fromAscii(number),
          web3.utils.fromAscii(zip),
          web3.utils.fromAscii(city),
          web3.utils.fromAscii(country),
          web3.utils.fromAscii(state)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setUser;
    }
    this.use('/setUser', setUser);
  },

  //get Full User with contract logistic
  getFullUser:function(needEndPoint=true){
    var getFullUser={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var user=data._user;
        var sender=data._sender;

        const result= await myContractUserLogic.methods.getFullUser(user).call({ from:sender });
        result[0]=web3.utils.toAscii(result[0]).replace(/\0/g, '');
        result[1]=web3.utils.toAscii(result[1]).replace(/\0/g, '');
        result[2]=web3.utils.toAscii(result[2]).replace(/\0/g, '');
        result[3]=web3.utils.toAscii(result[3]).replace(/\0/g, '');
        result[4]=web3.utils.toAscii(result[4]).replace(/\0/g, '');
        result[5]=web3.utils.toAscii(result[5]).replace(/\0/g, '');
        result[6]=web3.utils.toAscii(result[6]).replace(/\0/g, '');
        result[7]=web3.utils.toAscii(result[7]).replace(/\0/g, '');
        result[8]=web3.utils.toAscii(result[8]).replace(/\0/g, '');
        result["firstName"]=web3.utils.toAscii(result["firstName"]).replace(/\0/g, '');
        result["surname"]=web3.utils.toAscii(result["surname"]).replace(/\0/g, '');
        result["organization"]=web3.utils.toAscii(result["organization"]).replace(/\0/g, '');
        result["street"]=web3.utils.toAscii(result["street"]).replace(/\0/g, '');
        result["number"]=web3.utils.toAscii(result["number"]).replace(/\0/g, '');
        result["zip"]=web3.utils.toAscii(result["zip"]).replace(/\0/g, '');
        result["city"]=web3.utils.toAscii(result["city"]).replace(/\0/g, '');
        result["country"]=web3.utils.toAscii(result["country"]).replace(/\0/g, '');
        result["state"]=web3.utils.toAscii(result["state"]).replace(/\0/g, '');
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getFullUser;
    }
    this.use('/getFullUser',getFullUser);
  },

  //set role with userlogic contract
  setRoles:function(needEndPoint=true){
    const setRoles = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var user=data._user;     //Address
        var rights=data._rights;  //Unit
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.setRoles(
          user,
          parseInt(rights,10)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setRoles;
    }
    this.use('/setRoles', setRoles);
  },

  //set info Organization of an account with userlogic contract
  setOrganizationAddress:function(needEndPoint=true){
    const setOrganizationAddress = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var user=data._user;            //address
        var street=data._street;         //bytes32
        var number=data._number;        //bytes32
        var zip=data._zip;              //bytes32
        var city=data._city;            //bytes32
        var country=data._country;      //bytes32
        var state=data._state;          //bytes32
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.setOrganizationAddress(
          user,
          web3.utils.fromAscii(street),
          parseInt(number,10),
          web3.utils.fromAscii(zip),
          web3.utils.fromAscii(city),
          web3.utils.fromAscii(country),
          web3.utils.fromAscii(state)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setOrganizationAddress;
    }
    this.use('/setOrganizationAddress', setOrganizationAddress);
  },

  //set the Organization name of an account with userlogic contract
  setOrganization:function(needEndPoint=true){
    const setOrganization = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var user = data._user;                 //address
        var organization = data._organization;  //bytes32
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.setOrganization(
          user,
          web3.utils.fromAscii(organization)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setOrganization;
    }
    this.use('/setOrganization', setOrganization);
  },

  //set UserName with userlogic contract
  setUserName:function(needEndPoint=true){
    const setUserName = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var user=data._user;              //address
        var firstname=data. _firstName;   //bytes32
        var surname=data._surname;         //bytes32
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.setUserName(
          user,
          web3.utils.fromAscii(firstname),
          web3.utils.fromAscii(surname)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setUserName;
    }
    this.use('/setUserName', setUserName);
  },

  ///****************************************ADD ROLE FOR ACCOUNT **********************************************************************/////

  //add addAdminRole role with userlogic contract
  addAdminRole:function(needEndPoint=true){
    const addAdminRole = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var roleManagementRole=data._roleManagementRole;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.addAdminRole(
          address,
          parseInt(roleManagementRole,10)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return addAdminRole;
    }
    this.use('/addAdminRole', addAdminRole);
  },

  //add assetManager role with userlogic contract
  addAssetManager:function(needEndPoint=true){
    const addAssetManager = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.addAssetManagerRole(
          address
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return addAssetManager;
    }
    this.use('/addAssetManager', addAssetManager);
  },

  //add addMatcherRole role with userlogic contract
  addMatcherRole:function(needEndPoint=true){
    const addMatcherRole = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.addMatcherRole(
          address
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return addMatcherRole;
    }
    this.use('/addMatcherRole', addMatcherRole);
  },

  //add addTraderRole role with userlogic contract
  addTraderRole:function(needEndPoint=true){
    const addTraderRole = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.addTraderRole(
          address
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return addTraderRole;
    }
    this.use('/addTraderRole', addTraderRole);
  },

  ///******************************************************** REMOVE ROLE FOR AN ACCOUNT ************************************************************************/

  //Deactivate User role with userlogic contract
  deactivateUser:function(needEndPoint=true){
    const deactivateUser = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.deactivateUser(
          address
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return deactivateUser;
    }
    this.use('/deactivateUser', deactivateUser);
  },

  //remove Admin Role  role with userlogic contract
  removeAdminRole:function(needEndPoint=true){
    const removeAdminRole = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var roleManagementRole=data._roleManagementRole;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.removeAdminRole(
          address,
          parseInt(roleManagementRole,10)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return removeAdminRole;
    }
    this.use('/removeAdminRole', removeAdminRole);
  },

  //remove Asset Manager Role with userlogic contract
  removeAssetManagerRole:function(needEndPoint=true){
    const removeAssetManagerRole = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.removeAssetManagerRole(
          address
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return removeAssetManagerRole;
    }
    this.use('/removeAssetManagerRole', removeAssetManagerRole);
  },

  //remove Trader Role with userlogic contract
  removeTraderRole:function(needEndPoint=true){
    const removeTraderRole = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        var openDoorDataPart = myContractUserLogic.methods.removeTraderRole(
          address
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce = await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressUserLogic,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return removeTraderRole;
    }
    this.use('/removeTraderRole', removeTraderRole);
  },

  ///******************************************************* GET ROLE OF AN USER***************************************************************************************/

  //get Roles Rights with userlogic contract
  getRolesRights:function(needEndPoint=true){
    var getRolesRights={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        const result= await myContractUserLogic.methods.getRolesRights(address).call({ from:sender });
        console.log(JSON.stringify(result));
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getRolesRights;
    }
    this.use('/getRolesRights',getRolesRights);
  },

  ////***********************************************************CHECK ADDRESS IF EXIST *****************************************************************************/

  //does User Exist with userlogic contract
  doesUserExist:function(needEndPoint=true){
    var doesUserExist={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var address=data._address;
        var sender=data._sender;

        const result= await myContractUserLogic.methods.doesUserExist(address).call({ from:sender });
        console.log(JSON.stringify(result));
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return doesUserExist;
    }
    this.use('/doesUserExist',doesUserExist);
  },

  //get list length user address
  getUserListLength:function(needEndPoint=true){
    var getUserListLength={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;
        const result= await myContractUserLogic.methods.getUserListLength().call({ from:sender });
        console.log(JSON.stringify(result));
        var response=JSON.stringify(result);
        response=JSON.parse(response);
        return Promise.resolve(response);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getUserListLength;
    }
    this.use('/getUserListLength',getUserListLength);
  },

  //get All User with userlogic contract
  getAllUserAddress:function(needEndPoint=true){
    var getAllUserAddress={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;
        var tabUser=[];
        var user;

        const listLength= await myContractUserLogic.methods.getUserListLength().call({ from:sender });

        for(i=0;i<listLength;i++){
          user= await myContractUserLogic.methods.getUserAddress(i).call({ from:sender });
          tabUser.push(user);
        }
        return Promise.resolve(tabUser);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAllUserAddress;
    }
    this.use('/getAllUserAddress',getAllUserAddress);
  },

  //get All User with userlogic contract
  getAllUsers:function(needEndPoint=true){
    var getAllUsers={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;

        const usersAddresses = await fetch(process.env.API_URL + "/getAllUserAddress", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(handleErrors)
        .then(res => res.json())
        .then(data => {
          return data
        })
        .catch(error => {
          console.log("error in getAllUsers in getAllUserAddress : ");
          console.log(error);
        });
        const users = [];

        for (let address of usersAddresses) {
          const userTemp = await myContractUserLogic.methods.getFullUser(address).call({ from:sender });


          userTemp[0]=web3.utils.toAscii(userTemp[0]).replace(/\0/g, '');
          userTemp[1]=web3.utils.toAscii(userTemp[1]).replace(/\0/g, '');
          userTemp[2]=web3.utils.toAscii(userTemp[2]).replace(/\0/g, '');
          userTemp[3]=web3.utils.toAscii(userTemp[3]).replace(/\0/g, '');
          userTemp[4]=web3.utils.toAscii(userTemp[4]).replace(/\0/g, '');
          userTemp[5]=web3.utils.toAscii(userTemp[5]).replace(/\0/g, '');
          userTemp[6]=web3.utils.toAscii(userTemp[6]).replace(/\0/g, '');
          userTemp[7]=web3.utils.toAscii(userTemp[7]).replace(/\0/g, '');
          userTemp[8]=web3.utils.toAscii(userTemp[8]).replace(/\0/g, '');
          userTemp["address"]=address;
          userTemp["firstName"]=web3.utils.toAscii(userTemp["firstName"]).replace(/\0/g, '');
          userTemp["surname"]=web3.utils.toAscii(userTemp["surname"]).replace(/\0/g, '');
          userTemp["organization"]=web3.utils.toAscii(userTemp["organization"]).replace(/\0/g, '');
          userTemp["street"]=web3.utils.toAscii(userTemp["street"]).replace(/\0/g, '');
          userTemp["number"]=web3.utils.toAscii(userTemp["number"]).replace(/\0/g, '');
          userTemp["zip"]=web3.utils.toAscii(userTemp["zip"]).replace(/\0/g, '');
          userTemp["city"]=web3.utils.toAscii(userTemp["city"]).replace(/\0/g, '');
          userTemp["country"]=web3.utils.toAscii(userTemp["country"]).replace(/\0/g, '');
          userTemp["state"]=web3.utils.toAscii(userTemp["state"]).replace(/\0/g, '');

          users.push(userTemp)
        }

        return Promise.resolve(users);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAllUsers;
    }
    this.use('/getAllUsers', getAllUsers);
  }
};
