const rp=require('request-promise');

module.exports = {
  //emissionCO2 History
  emissionCO2History:function(needEndPoint=true){
    var emissionCO2History={
      async find(params){
        var ladate=new Date();
        var year = ladate.getFullYear();

        var options = {
          uri: 'https://opendata.rte-france.com/api/records/1.0/search/?dataset=eco2mix_national_tr&rows=-1&sort=-date_heure&refine.date_heure='+year.toString(),
          json: true // Automatically parses the JSON string in the response
        };

        var tab=[];

        return Promise.resolve( 
          rp(options)
            .then(function (res) {
              var list = res.records.length;
              var somme=0;
              for(a=0,i=0;i<list;i++,a++){
                if(a==96){
                  tab.push({
                    date:res.records[i-1].fields.date,
                    CO2:somme/96
                  });
                  somme=0;
                  a=0;
                }
                else{
                  somme += res.records[i].fields.taux_co2;
                }
              }
              return tab;
            })
            .catch(function (err) {
              return err;
            }));
          
      },
      async get(id, params){       
          
      },
      async create(data, params){
        
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return emissionCO2History;
    }
    this.use('/emissionCO2History',emissionCO2History);
  },
};