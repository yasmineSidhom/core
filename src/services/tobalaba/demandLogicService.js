//const contract 	= require('truffle-contract');
//const Web3 = require('web3');
//var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));
  
const createDatabaseFunction=require('../../db/createTable');
const createDemandFunction=require('../../db/addElement');
const queryDemandFunction=require('../../db/queryDemand');
const assetProducingService=require('./assetProducingRegistryLogicService');
const certificateService=require('./certificateLogicService');
const demandLogicService=require('./demandLogicService');

module.exports = {
  //****************************************** Demand ************************************************//
  //createDemand 
  createDemand:function(needEndPoint=true){
    var createDemand={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var demandMask = data._demandMask;   //int
        var created = data._created;         //int
        var enabled = data._enabled;         //bool
        createDatabaseFunction();
        return Promise.resolve(createDemandFunction.demand(demandMask,created,enabled));
      } 
    };
    if(needEndPoint==false){
      return createDemand;
    }
    this.use('/createDemand',createDemand);
  },
  
  //createDemandGeneralInfo 
  createDemandGeneralInfo:function(needEndPoint=true){
    var createDemandGeneralInfo={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){  
        var originator= data._originator; //address
        var buyer= data._buyer;           //address
        var startTime= data._startTime; //int
        var endTime=data._endTime;      //int
        var timeframe= data._timeframe;  //int
        var pricePerCertifiedKWh=data._pricePerCertifiedKWh;  //int
        var currency=data._currency;             //int 
  
        return Promise.resolve(createDemandFunction.demandGeneralInfo(originator,buyer,startTime,endTime,timeframe,pricePerCertifiedKWh,currency));
      } 
    };
    if(needEndPoint==false){
      return createDemandGeneralInfo;
    }
    this.use('/createDemandGeneralInfo',createDemandGeneralInfo);
  },
  
  //createDemandCoupling 
  createDemandCoupling:function(needEndPoint=true){
    var createDemandCoupling={
      find: function(params,callback){
        createDatabaseFunction();
        createDemandFunction();
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){ 
        var producingAssets= data._producingAssets;       //int
        var consumingAssets= data._consumingAssets;     //int     
        return Promise.resolve(createDemandFunction.demandCoupling(producingAssets,consumingAssets));                
      } 
    };
    if(needEndPoint==false){
      return createDemandCoupling;
    }
    this.use('/createDemandCoupling',createDemandCoupling);
  },
  
  //demandPriceDriving 
  demandPriceDriving:function(needEndPoint=true){
    var demandPriceDriving={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var registryCompliance= data._registryCompliance;       //int
        var assettype= data._assettype;                         //int
        var minCO2Offset= data._minCO2Offset;                   //int
        var otherGreenAttributes= data._otherGreenAttributes;   //bytes32
        var typeOfPublicSupport= data._typeOfPublicSupport;     //bytes32
        var isInitialized= data._isInitialized;                 //bool 
        return Promise.resolve(createDemandFunction.demandPriceDriving(registryCompliance,assettype,minCO2Offset,otherGreenAttributes,typeOfPublicSupport,isInitialized));
      } 
    };
    if(needEndPoint==false){
      return demandPriceDriving;
    }
    this.use('/demandPriceDriving',demandPriceDriving);
  },
  
  //demandLocation 
  demandLocation:function(needEndPoint=true){
    var demandLocation={
      find: function(params,callback){
        createDatabaseFunction();
        createDemandFunction();
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var country= data._country;            //bytes32
        var region= data._region;              //bytes32
        var city= data._city;                  //bytes32
        var street= data._street;              //bytes32
        var zip= data._zip;                    //bytes32
        var houseNumber= data._houseNumber;    //bytes32
        var gpsLatitude= data._gpsLatitude;    //bytes32
        var gpsLongitude= data._gpsLongitude;  //bytes32
        var exists= data._exists;              //bool
        return Promise.resolve(createDemandFunction.demandLocation(country,region,city,street,zip,houseNumber,gpsLatitude,gpsLongitude,exists));  
      } 
    };
    if(needEndPoint==false){
      return demandLocation;
    }
    this.use('/demandLocation',demandLocation);
  },
  
  //demandMatcherProperties 
  demandMatcherProperties:function(needEndPoint=true){
    var demandMatcherProperties={
      find: function(params,callback){
        createDatabaseFunction();
        createDemandFunction();
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){ 
        var targetWhPerperiod= data._targetWhPerperiod;                //int
        var currentWhPerperiod= data._currentWhPerperiod;              //int
        var certInCurrentperiod= data._certInCurrentperiod;            //int
        var productionLastSetInPeriod= data._productionLastSetInPeriod;//int
        var matcher= data._matcher;                                    //address   
        return Promise.resolve(createDemandFunction.demandMatcherProperties(targetWhPerperiod,currentWhPerperiod,certInCurrentperiod,productionLastSetInPeriod,matcher));  
      } 
    };
    if(needEndPoint==false){
      return demandMatcherProperties;
    }
    this.use('/demandMatcherProperties',demandMatcherProperties);
  },
  
  //QueryDemand in order to get all demand information
  QueryDemand:function(needEndPoint=true){
    var QueryDemand={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){   
        var id=data._id;
        return Promise.resolve(queryDemandFunction.infodemand(id));                  
      } 
    };
    if(needEndPoint==false){
      return QueryDemand;
    }
    this.use('/QueryDemand',QueryDemand);
  },

  //demandGetCurrentPeriod 
  demandGetCurrentPeriod:function(needEndPoint=true){
    var demandGetCurrentPeriod={
      find: function(params,callback){
        createDatabaseFunction();
        createDemandFunction();
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){  
        var demandId = data._id  ;
  
        var timeframe;
        var created;
  
        module.exports.QueryDemand(false).create(demandId,null,null).then(function(res){
          timeframe=res[0].timeframe;
          created=res[0].created;
        });
  
        if ( timeframe == 0) //if equal yearly
        {
          return Promise.resolve( 
            ( Date.now() - created ) / (60*60*24*365) 
          );
        }
        if ( timeframe == 1) //if equal monthly
        {
          return Promise.resolve( 
            ( Date.now() - created ) / (60*60*24*30) 
          );
        }
        if ( timeframe == 2) //if equal daily
        {
          return Promise.resolve( 
            ( Date.now() - created ) / (60*60*24) 
          );
        }  
        if ( timeframe == 3) //if equal hourly
        {
          return Promise.resolve( 
            ( Date.now() - created ) / (60*60) 
          );
        }      
  
      } 
    };
    if(needEndPoint==false){
      return demandGetCurrentPeriod;
    }
    this.use('/demandGetCurrentPeriod',demandGetCurrentPeriod);
  },

  //****************************************All Matching Services ***************************//
  //checkDemandGeneral
  checkDemandGeneral:function(needEndPoint=true){
    var checkDemandGeneral={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var sender = data._sender;  
        var demandId = data._demandId; 
        var prodAssetId = data._prodAssetId; 
        var id={
          _id:demandId
        };
  
        var dataDemand={
          originator:'',
          buyer:'',
          startTime:0,
          endTime:0,
          demandMask:0
        };
  
        var assetProducing={
          _sender:sender,
          _assetId:prodAssetId
        };
  
        var dataAsset={
          smartMeter:"",
          owner:"",
          operationalSince:0,
          lastSmartMeterReadWh:0,
          active:false,
          lastSmartMeterReadFileHash:""
        };
        
        module.exports.QueryDemand(false).create(id,null,null).then(function(res){
          dataDemand.originator = res[0].originator;
          dataDemand.buyer = res[0].buyer;
          dataDemand.startTime = res[0].startTime;
          dataDemand.endTime = res[0].endTime;
          dataDemand.demandMask = res[0].demandMask;
          console.log(dataDemand);
        });
  
        assetProducingService.getAsset(false).create(assetProducing,null,null).then(function(res){
          dataAsset.smartMeter= res[0];
          dataAsset.owner= res[1];
          dataAsset.operationalSince= res[2].toNumber();
          dataAsset.lastSmartMeterReadWh= res[3].toNumber();
          dataAsset.active= res[4];
          dataAsset.lastSmartMeterReadFileHash= res[5];
          console.log(dataAsset);
        });
  
        //Verify if it is the owner fron asset and demand
        if(dataDemand.originator != dataAsset.owner){
          return Promise.resolve(["",false,"Wrong originator"]);
        }
  
        //Verify the time
        else if(!(dataDemand.startTime <= Date.now() && dataDemand.endTime >= Date.now())){
          return Promise.resolve(["",false,"Wrong time"]);
        }
  
        else{
          return Promise.resolve([dataDemand.buyer,true,"everything ok"]);
        } 
  
      } 
    };
    if(needEndPoint == false){
      return checkDemandGeneral;
    }
    this.use('/checkDemandGeneral',checkDemandGeneral);
  },
  
  //checkPriceDriving
  checkPriceDriving:function(needEndPoint=true){
    var checkPriceDriving={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var sender=data._sender;  
        var demandId=data._demandId; 
        var wCreated=data._wCreated; 
        var prodAssetId=data._prodAssetId; 
        var co2Saved=data._co2Saved; 
  
        var id={
          _id:demandId
        };
  
        var assetProducing={
          _sender:sender,
          _assetId:prodAssetId
        };
  
        var dataDemandPriceDriving={
          typeOfPublicSupport: "",
          otherGreenAttributes: "",
          minCO2OffsetDemand:0,
          compliance:0,
          assetType:null,
          country: "",
          region: "",
          city: "",
          street: "",
          zip: "",
          houseNumber: "",
          gpsLatitude: "",
          gpsLongitude: "",
        };
  
        var dataAsset={
          assetType:null,
          capacityWh:null,
          certificatesCreatedForWh:null,
          lastSmartMeterCO2OffsetRead:null,
          cO2UsedForCertificate:null,
          registryCompliance:null,
          otherGreenAttributes:"",
          typeOfPublicSupport:"",
          country: "",
          region: "",
          city: "",
          street: "",
          zip: "",
          houseNumber: "",
          gpsLatitude: "",
          gpsLongitude: ""
        };
  
        if(wCreated == 0) return Promise.resolve([false, "no energy created"]);
  
        module.exports.QueryDemand(false).create(id,null,null).then(function(res){
          dataDemandPriceDriving.typeOfPublicSupport=res[0].typeOfPublicSupport;
          dataDemandPriceDriving.otherGreenAttributes=res[0].otherGreenAttributes;
          dataDemandPriceDriving.minCO2OffsetDemand = res[0].minCO2Offset;
          dataDemandPriceDriving.compliance = res[0].registryCompliance;
          dataDemandPriceDriving.assetType = res[0].assettype;
          dataDemandPriceDriving.country=  res[0].country;
          dataDemandPriceDriving.region=  res[0].region;
          dataDemandPriceDriving.city=  res[0].city;
          dataDemandPriceDriving.street=  res[0].street;
          dataDemandPriceDriving.zip=  res[0].zip;
          dataDemandPriceDriving.houseNumber=  res[0].houseNumber;
          dataDemandPriceDriving.gpsLatitude=  res[0].gpsLatitude;
          dataDemandPriceDriving.gpsLongitude=  res[0].gpsLongitude;
          console.log(dataDemandPriceDriving);
        });
        
        assetProducingService.getAssetProducingProperties(false).create(assetProducing,null,null).then(function(res){
          dataAsset.assetType= res[0].toNumber();
          dataAsset.capacityWh=res[1].toNumber();
          dataAsset.certificatesCreatedForWh=res[2].toNumber();
          dataAsset.lastSmartMeterCO2OffsetRead=res[3].toNumber();
          dataAsset.cO2UsedForCertificate=res[4].toNumber();
          dataAsset.registryCompliance=res[5].toNumber();
          dataAsset.otherGreenAttributes=res[6];
          dataAsset.typeOfPublicSupport=res[7];
          console.log(dataAsset);
        });
        
        assetProducingService.getAssetLocation(false).create(assetProducing,null,null).then(function(res){
          dataAsset.country= res[0];
          dataAsset.region= res[1];
          dataAsset.city= res[2];
          dataAsset.street= res[3];
          dataAsset.zip= res[4];
          dataAsset.houseNumber= res[5];
          dataAsset.gpsLatitude= res[6];
          dataAsset.gpsLongitude= res[7];
          console.log(dataAsset);
        });
          
        if(dataDemandPriceDriving.assetType != dataAsset.assetType){
          return Promise.resolve([false, "wrong asset type"]);
        }
        if(dataDemandPriceDriving.country != dataAsset.country ){
          return Promise.resolve([false, "wrong country"]);
        }
        if(dataDemandPriceDriving.region != dataAsset.region ){
          return Promise.resolve([false, "wrong region"]);
        }
        if(dataDemandPriceDriving.compliance != dataAsset.registryCompliance ){
          return Promise.resolve([false, "wrong compliance"]);  
        }
  
        //Condition _co2Saved for checkPriceDriving service
        if(co2Saved == 0){
          co2Saved = getAssetCoSaved.create({
            _prodAssetId:prodAssetId, 
            _wCreated :wCreated ,
            _sender :sender}, null,null); 
        }
        
        var co2PerWh = ((co2Saved * 1000) / wCreated)/10;
  
        if(co2PerWh < dataDemandPriceDriving.minCO2OffsetDemand)
          return Promise.resolve([false, "wrong CO2"]);
        
        if(dataDemandPriceDriving.otherGreenAttributes != dataAsset.otherGreenAttributes ){
          return Promise.resolve([false, "wrong otherGreenAttributes"]);  
        }
  
        if(dataDemandPriceDriving.typeOfPublicSupport != dataAsset.typeOfPublicSupport ){
          return Promise.resolve([false, "wrong typeOfPublicSupport"]);  
        }
        else{
          return Promise.resolve([true,"everything OK"]);
        }
        
      } 
    };
    if(needEndPoint==false){
      return checkPriceDriving;
    }
    this.use('/checkPriceDriving',checkPriceDriving);
  },
  
  //checkMatcher
  checkMatcher:function(needEndPoint=true){
    var checkMatcher={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,"Insert");
      },
      create :function(data, params,callback){
        var sender=data._sender;  
        var demandId=data._demandId; 
        var wCreated=data._wCreated; 
    
        var id={
          _id:demandId
        };
    
        var dataDemandPriceDriving={
          targetWhPerperiod:0, 
          currentWhPerPeriod:0, 
          certInCurrentPeriod:0, 
          productionLastSetInPeriod:0, 
          matcher:"",
          typeOfPublicSupport: "",
          otherGreenAttributes: "",
          minCO2OffsetDemand:0,
          compliance:0,
          assetType:null,
          country: "",
          region: "",
          city: "",
          street: "",
          zip: "",
          houseNumber: "",
          gpsLatitude: "",
          gpsLongitude: "",
        };       
  
        module.exports.QueryDemand(false).create(id,null,null).then(function(res){
          dataDemandPriceDriving.targetWhPerperiod = res[0].targetWhPerperiod; 
          dataDemandPriceDriving.currentWhPerPeriod = res[0].currentWhPerperiod; 
          dataDemandPriceDriving.certInCurrentPeriod = res[0].certInCurrentperiod; 
          dataDemandPriceDriving.productionLastSetInPeriod = res[0].productionLastSetInPeriod; 
          dataDemandPriceDriving.matcher = res[0].matcher;
          dataDemandPriceDriving.typeOfPublicSupport = res[0].typeOfPublicSupport;
          dataDemandPriceDriving.otherGreenAttributes = res[0].otherGreenAttributes;
          dataDemandPriceDriving.minCO2OffsetDemand = res[0].minCO2Offset;
          dataDemandPriceDriving.compliance = res[0].registryCompliance;
          dataDemandPriceDriving.assetType = res[0].assettype;
          dataDemandPriceDriving.country = res[0].country;
          dataDemandPriceDriving.region = res[0].region;
          dataDemandPriceDriving.city = res[0].city;
          dataDemandPriceDriving.street = res[0].street;
          dataDemandPriceDriving.zip = res[0].zip;
          dataDemandPriceDriving.houseNumber = res[0].houseNumber;
          dataDemandPriceDriving.gpsLatitude = res[0].gpsLatitude;
          dataDemandPriceDriving.gpsLongitude = res[0].gpsLongitude;
          console.log(dataDemandPriceDriving);
        });
        if (dataDemandPriceDriving.matcher != sender){
          return Promise.resolve([0,0,0, false,"wrong matcher"]);
        }
  
        var currentPeriod; 
        module.exports.demandGetCurrentPeriod(false).create(_demandId,null,null).then(function(res){
          console.log("Return created time of demand: "+res);
          currentPeriod=res;
        });
  
        if (currentPeriod == dataDemandPriceDriving.productionLastSetInPeriod) {
          if(dataDemandPriceDriving.currentWhPerPeriod + _wCreated > whAmountPerPeriod){
            return Promise.resolve([0,0,0, false, "too much whPerPeriode"]); 
          }  
        }            
        else {
          if(_wCreated > whAmountPerPeriod) {
            return Promise.resolve([0,0,0, false, "too much whPerPeriode and currentPeriod != productionLastSetInPeriod"]);
          }
          dataDemandPriceDriving.productionLastSetInPeriod = currentPeriod;
          dataDemandPriceDriving.currentWhPerPeriod = 0;
          dataDemandPriceDriving.certInCurrentPeriod = 0;
        }
        dataDemandPriceDriving.currentWhPerPeriod += _wCreated;
        dataDemandPriceDriving.certInCurrentPeriod += 1;
        return Promise.resolve([dataDemandPriceDriving.currentWhPerPeriod, dataDemandPriceDriving.certInCurrentPeriod, currentPeriod, true, "everything OK"]);
  
      } 
    };
    if(needEndPoint==false){
      return checkMatcher;
    }
    this.use('/checkMatcher',checkMatcher);
  },
  
  //checkDemandCoupling
  checkDemandCoupling:function(needEndPoint=true){
    var checkDemandCoupling={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var sender=data._sender;  
        var demandId=data._demandId; 
        var wCreated=data._wCreated; 
        var prodAssetId=data._prodAssetId; 
        var co2Saved=data._co2Saved; 
  
      } 
    };
    if(needEndPoint==false){
      return checkDemandCoupling;
    }
    this.use('/checkDemandCoupling',checkDemandCoupling);
  },

  //Matching Algo
  matching:function(needEndPoint=true){
    var matching={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var sender=data._sender;  
        var demandId=data._demandId;
        var wCreated=data._wCreated;  
        var co2Saved=data._co2Saved;
        var owner=data._owner;
        var powerInWCertificate1 =data._powerInWCertificate1;
        var powerInWCertificate2 =data._powerInWCertificate2;    
        var assetChoosen ={
          assetOneID : data._assetOneID,
          assetTwoID : data._assetTwoID
        };

        var objectDemand={
          object1:{
            _sender: sender,
            _demandId: demandId,
            _prodAssetId: assetChoosen.assetOneID,
            _wCreated: wCreated,
            _co2Saved: co2Saved
          },
          object2:{
            _sender : sender,
            _demandId :demandId,
            _prodAssetId :assetChoosen.assetTwoID,
            _wCreated: wCreated,
            _co2Saved: co2Saved
          },
        };
        
        var objectCertificate={
          object1:{
            _assetId: assetChoosen.assetOneID,  
            _owner: owner,                                              
            _powerInW: powerInWCertificate1,                                   
            _sender: sender
          },
          object2:{
            _assetId: assetChoosen.assetTwoID, 
            _owner: owner,                                                        
            _powerInW: powerInWCertificate2,                                   
            _sender: sender
          },
        };
        
        var check1;
        var check2;
        //Check the parameters of assetID and the demandID
        return Promise.resolve(
          module.exports.checkDemandGeneral(false).create(objectDemand.object1,null,null)
            .then(function(res){
              check=res;
              console.log("checkDemandGeneral1: " + check);
              if(check[0] == false){return false;}
              else{return true;}
            }).then(function(res){
              console.log(res);
              return module.exports.checkDemandGeneral(false).create(objectDemand.object2,null,null)
                .then(function(res){
                  check=res;
                  console.log("checkDemandGeneral2: " + check);
                  if(check[0] == false){return false;}
                  else{return true;}
                });
            }).then(function(res){
              console.log(res);
              return module.exports.checkPriceDriving(false).create(objectDemand.object1,null,null)
                .then(function(res){
                  check=res;
                  console.log("checkPriceDriving1: " + check);
                  if(check[0] == false){return false;}
                  else{return true;}
                });
            }).then(function(res){
              console.log(res);
              return module.exports.checkPriceDriving(false).create(objectDemand.object2,null,null)
                .then(function(res){
                  check=res;
                  console.log("checkPriceDriving2: " + check);
                  if(check[0] == false){return false;}
                  else{return true;}
                });
            }).then(function(res){
              console.log(res);
              return module.exports.checkMatcher(false).create(objectDemand.object1,null,null)
                .then(function(res){
                  check=res;
                  console.log("checkMatcher1: " + check);
                  if(check[3] == false){return false;}
                  else{return true;}
                });
            }).then(function(res){
              console.log(res);
              return module.exports.checkMatcher(false).create(objectDemand.object2,null,null)
                .then(function(res){
                  check=res;
                  console.log("checkMatcher2: " + check);
                  if(check[3] == false){return false;}
                  else{return true;}
                });
            }).then(function(res){
              return certificateService.createCertificateForAssetOwner(false).create(objectCertificate.object1,null,null)
                .then(function(res){
                  return res;
                });
            }).then(function(id1){
              return certificateService.createCertificateForAssetOwner(false).create(objectCertificate.object2,null,null)
                .then(function(id2){
                  var AllId = "AllCertificatID: "+id1.logs[0].args._certificateId+" and " + id2.logs[0].args._certificateId;
                  console.log(AllId);
                  return [id1.logs[0].args._certificateId,id2.logs[0].args._certificateId];
                });  
            }).then(function(res1){
              return certificateService.changeCertificateOwner(false).create({
                _certificateId:parseInt(res1[0],10),
                _newOwner: owner,
                _sender: sender  
              },null,null)
                .then(function(res){
                  console.log("Transfer certificate for: "+objectCertificate.object1._owner);
                  return res1;
                });  
            }).then(function(res2){
              return certificateService.changeCertificateOwner(false).create({
                _certificateId:parseInt(res2[1],10),
                _newOwner: owner,
                _sender: sender  
              },null,null)
                .then(function(res){
                  console.log("Transfer certificate for: "+objectCertificate.object2._owner);
                  return res2;
                });  
            })   
            .then(function(res){
              return res; 
            })
        ); 
      } 
    };
    if(needEndPoint==false){
      return matching;
    }
    this.use('/matching',matching);
  },

  //Matching For one Asset
  matchingForOneAsset:function(needEndPoint=true){
    var matchingForOneAsset={
      find: function(params,callback){
        callback(null,"Insert");
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params,callback){
        var sender=data._sender;  
        var demandId=data._demandId;
        var wCreated=data._wCreated;  
        var co2Saved=data._co2Saved;
        var owner=data._owner;
        var powerInWCertificate =data._powerInWCertificate;
        var assetID = data._assetID;

        var objectDemand={
          _sender: sender,
          _demandId: demandId,
          _prodAssetId: assetID,
          _wCreated: wCreated,
          _co2Saved: co2Saved
        };
        
        var objectCertificate={
          _assetId: assetID,  
          _owner: owner,                                              
          _powerInW: powerInWCertificate,                                   
          _sender: sender    
        };
        
        //Check parameters of assetID and the demandID
        return Promise.resolve(
          module.exports.checkDemandGeneral(false).create(objectDemand,null,null)
            .then(function(res){
              check=res;
              console.log("checkDemandGeneral: " + check);
              if(check[0] == false){
                return false;
              }
              else{
                return true;
              }
            }).then(function(res){
              console.log(res);
              if(res==false){
                return false;
              }
              else{
                return module.exports.checkPriceDriving(false).create(objectDemand,null,null)
                  .then(function(res){
                    check=res;
                    console.log("checkPriceDriving: " + check);
                    if(check[0] == false){return false;}
                    else{return true;}
                  });
              }
            }).then(function(res){
              console.log(res);
              if(res==false){
                return false;
              }
              else{
                return module.exports.checkMatcher(false).create(objectDemand,null,null)
                  .then(function(res){
                    check=res;
                    console.log("checkMatcher: " + check);
                    if(check[3] == false){return false;}
                    else{return true;}
                  }); 
              }
            }).then(function(res){
              console.log(res);
              if(res==false){
                return false;
              }
              else{
                return certificateService.createCertificateForAssetOwner(false).create(objectCertificate,null,null)
                  .then(function(res){
                    var CertificateId = "CertificatID: "+res.logs[0].args._certificateId;
                    console.log(CertificateId);
                    return [res.logs[0].args._certificateId];
                  });
              }  
            }).then(function(res1){
              console.log(res1);
              if(res1==false){
                return false;
              }
              else{
                return certificateService.changeCertificateOwner(false).create({
                  _certificateId:parseInt(res1[0],10),
                  _newOwner: owner,
                  _sender: sender  
                },null,null)
                  .then(function(res){
                    console.log("Transfer certificate for: "+objectCertificate._owner);
                    return res1;
                  });
              }  
            }).then(function(res){
              console.log("End: "+res);
              return res;
            })
        ); 
      } 
    };
    if(needEndPoint==false){
      return matchingForOneAsset;
    }
    this.use('/matchingForOneAsset',matchingForOneAsset);
  },
};