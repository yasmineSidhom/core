const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));
//const web3 = new Web3(new Web3.providers.WebsocketProvider('ws://127.0.0.1:8546'));

var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');
var fetch =require('node-fetch');

let CoO_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/CoO.json');
const abiCoO = CoO_artefact.abi;
const addressCoO = CoO_artefact.networks[401697].address;
const myContractCoO = new web3.eth.Contract(abiCoO, addressCoO);

let CertificateLogic_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/CertificateLogic.json');
const abiCertificateLogic = CertificateLogic_artefact.abi;
const addressCertificateLogic = CertificateLogic_artefact.networks[401697].address;
const myContractCertificateLogic = new web3.eth.Contract(abiCertificateLogic, addressCertificateLogic);

let CertificateDB_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/CertificateDB.json');
const abiCertificateDB = CertificateDB_artefact.abi;
const addressCertificateDB = CertificateDB_artefact.networks[401697].address;
const myContractCertificateDB = new web3.eth.Contract(abiCertificateDB, addressCertificateDB);

//Private Key
var privateKey = require('./privateKey');
//CertificatelogicService
const CertificatelogicService=require('./certificateLogicService');

module.exports = {

  retrieveCertificateWithoutHash:function(needEndPoint=true){
    var retrieveCertificateWithoutHash={
      async find(params){},
      async get(params){},
      async create (data, params){

        var sender = data._sender;                                             //address
        var owner =data._owner;                                               //address
        var newSmartContractAddress = data._newSmartContractAddress;         //address
        var abiNewSmartContract = data._abiNewSmartContract;                 //array

        //fetch Data from server
        var fetchCertificate = await fetch(process.env.API_URL + "/getAllCertificates", {
          method: "POST",
          body: JSON.stringify({
            "_owner": owner,
            "_sender": owner
          }),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(function(response) {
          /* response.status     //=> number 100–599
          response.statusText //=> String
          response.headers    //=> Headers
          response.url        //=> String */

          return response.text();
        }, function(error) {
          error.message; //=> String
        });

        fetchCertificate = JSON.parse(fetchCertificate);
        console.log("fetchCertificate: "+fetchCertificate);

        const newCertificateLogic = new web3.eth.Contract(abiNewSmartContract, newSmartContractAddress);

        //Initialize variables
        var openDoorDataPart, nonce, rawTx, tx, serializedTx, ret;

        ////Put all certificates in the new smart contract CertificateLogic
        for(j=0;j<fetchCertificate.length;j++)
        {
          openDoorDataPart = await newCertificateLogic.methods.retrieveCertificate(
            parseInt(fetchCertificate[j]._assetId,10),
            fetchCertificate[j]._owner,
            parseInt(fetchCertificate[j]._powerInW,10),
            fetchCertificate[j]._retired,
            fetchCertificate[j]._dataLog,
            parseInt(fetchCertificate[j]._coSaved,10),
            fetchCertificate[j]._escrow,
            parseInt(fetchCertificate[j]._creationTime,10),
            fetchCertificate[j]._txHash,
            parseInt(fetchCertificate[j]._productionDate,10),
            fetchCertificate[j]._info
          ).encodeABI();
          console.log(openDoorDataPart);

          nonce= await web3.eth.getTransactionCount(sender);
          console.log(nonce);

          rawTx = {
            nonce: web3.utils.toHex(nonce),
            value: 0x00,
            gasPrice: web3.utils.toHex(154140000),
            gasLimit: web3.utils.toHex(1000000),
            data: openDoorDataPart,
            to: newSmartContractAddress,
            from: sender
          };

          tx = new Tx(rawTx);

          var privateKey =require('./privateKey').privateKeyByAddress(sender);
          await tx.sign(privateKey);
          serializedTx = await tx.serialize();
          ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
          console.log("Receipt: "+JSON.stringify(ret));
          console.log(j);
        }
        console.log("Succed recover for certificates");

        /*
        //Use it if we want to hash the certificate completely and then use it to check certificate

        for(g=0;g<fetchCertificate.length;g++)
        {
          openDoorDataPart = await newCertificateLogic.methods.setCertificateHash(
            g
          ).encodeABI();
          console.log(openDoorDataPart);

          nonce= await web3.eth.getTransactionCount(sender);
          console.log(nonce);

          rawTx = {
            nonce: web3.utils.toHex(nonce),
            value: 0x00,
            gasPrice: web3.utils.toHex(154140000),
            gasLimit: web3.utils.toHex(1000000),
            data: openDoorDataPart,
            to: newSmartContractAddress,
            from: sender
          };

          tx = new Tx(rawTx);
          await tx.sign(privateKey.returnPrivateKey(sender));
          serializedTx = await tx.serialize();
          ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
          console.log("Receipt: "+JSON.stringify(ret));
          console.log(g);
        }*/

        console.log("------------------------------------------");
        console.log("SetCertificateHash OK");

        return Promise.resolve("EveryThing is good.");
      }
    };
    if(needEndPoint == false){
      return retrieveCertificateWithoutHash;
    }
    this.use('/retrieveCertificateWithoutHash',retrieveCertificateWithoutHash);
  },

  retrieveCertificate:function(needEndPoint=true){
    var retrieveCertificate={
      async find(params){},
      async get(params){},
      async create (data, params){

        var sender=data._sender;                                             //address
        var owner=data._owner;                                               //address
        var newSmartContractAddress = data._newSmartContractAddress;         //address
        var abiNewSmartContract = data._abiNewSmartContract;                 //array

        const listCertificate=await CertificatelogicService.getAllCertificates(false).create({_owner:owner, _sender:sender },null);
        console.log(listCertificate[0]);

        const listCertificateLength=await CertificatelogicService.getCertificateListLength(false).create({_sender:sender },null);
        console.log(listCertificateLength);

        const newCertificateLogic = new web3.eth.Contract(abiNewSmartContract, newSmartContractAddress);

        //Initialize variables
        var openDoorDataPart, nonce, rawTx, tx, serializedTx, ret;

        ////Put all certificates in the new smart contract CertificateLogic
        for(i=0;i<listCertificateLength;i++)
        {
          openDoorDataPart = await newCertificateLogic.methods.retrieveCertificate(
            parseInt(listCertificate[i]._assetId,10),
            listCertificate[i]._owner,
            parseInt(listCertificate[i]._powerInW,10),
            listCertificate[i]._retired,
            listCertificate[i]._dataLog,
            parseInt(listCertificate[i]._coSaved,10),
            listCertificate[i]._escrow,
            parseInt(listCertificate[i]._creationTime,10),
            listCertificate[i]._txHash,
            parseInt(listCertificate[i]._productionDate,10),
            listCertificate[i]._certificateHash
          ).encodeABI();
          console.log(openDoorDataPart);

          nonce= await web3.eth.getTransactionCount(sender);
          console.log(nonce);

          rawTx = {
            nonce: web3.utils.toHex(nonce),
            value: 0x00,
            gasPrice: web3.utils.toHex(154140000),
            gasLimit: web3.utils.toHex(1000000),
            data: openDoorDataPart,
            to: newSmartContractAddress,
            from: sender
          };

          tx = new Tx(rawTx);

          var privateKey =require('./privateKey').privateKeyByAddress(sender);
          await tx.sign(privateKey);
          serializedTx = await tx.serialize();
          ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
          console.log("Reçu: "+JSON.stringify(ret));
        }

        console.log("Succed recover for certificates");

        const listLengthNewSmartContract = await newCertificateLogic.methods.getCertificateListLength().call({from:sender});
        console.log("getCertificateListLength of New SM: "+listLengthNewSmartContract);

        return Promise.resolve("Good");
      }
    };
    if(needEndPoint == false){
      return retrieveCertificate;
    }
    this.use('/retrieveCertificate',retrieveCertificate);
  },

  updateCertificateLogic:function(needEndPoint=true){
    var updateCertificateLogic={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;                                   //address
        var newSmartContractAddress=data._newSmartContractAddress; //address

        ////Update CoO contract for CertificateLogic
        var openDoorDataPart = await myContractCoO.methods.update(
          "0x0000000000000000000000000000000000000000",
          "0x0000000000000000000000000000000000000000",
          newSmartContractAddress,
          "0x0000000000000000000000000000000000000000"
        ).encodeABI();
        console.log(openDoorDataPart);

        var nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(15414000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressCoO,
          from: sender
        };

        var tx = new Tx(rawTx);

        var privateKey =require('./privateKey').privateKeyByAddress(sender);
        await tx.sign(privateKey);
        var serializedTx=await tx.serialize();
        var ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        console.log("Reçu: "+JSON.stringify(ret));

        console.log("CertificateRegistry updated");

        const newCertificateRegistry= await myContractCoO.methods.certificateRegistry().call({from: sender});
        console.log("certificateRegistry: "+newCertificateRegistry);

        return Promise.resolve("good");
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return updateCertificateLogic;
    }
    this.use('/updateCertificateLogic',updateCertificateLogic);
  },

  addressCertificate:function(needEndPoint=true){
    var addressCertificate={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;            //address

        const cooCertificateRegistry = await myContractCoO.methods.certificateRegistry().call({from:sender});
        console.log("cooCertificateRegistry: " +cooCertificateRegistry);

        const certificateDbAddress = await myContractCertificateLogic.methods.certificateDb().call({from : sender });
        console.log("certificateDbAddress: " + certificateDbAddress);

        const certificateLogicAddress = await myContractCertificateDB.methods.owner().call({from : sender });
        console.log("certificateLogicAddress: "+ certificateLogicAddress);

        return Promise.resolve("good");
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return addressCertificate;
    }
    this.use('/addressCertificate',addressCertificate);
  },

};
