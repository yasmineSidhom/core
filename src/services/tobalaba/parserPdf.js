const pdfParser = require('../../libPdf/src/pdf2json/pdf2json.js');
const fs = require('fs');
const fetch =require('node-fetch');

//functions & variables to use later to validate the certificate with the smart contract
/*
const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

let CertificateLogic_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/CertificateLogic.json');
const abiCertificateLogic = CertificateLogic_artefact.abi;
const addressCertificateLogic = CertificateLogic_artefact.networks[401697].address;
const myContractCertificateLogic = new web3.eth.Contract(abiCertificateLogic, addressCertificateLogic);
const CertificatelogicService = require('./certificateLogicService');
*/

module.exports = {
  //uploadsPdfParser
  uploadsPdfParser:function(needEndPoint=true){
    var uploadsPdfParser={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return uploadsPdfParser;
    }
    this.use('/uploads/pdf/parser',async (req,res) => {

      console.log('starting uploadsPdfParser...')
      //New version  start
      /*
      const myResult = await fetch(process.env.API_URL + "/getAllCertificates", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: json
      })
      .then(handleErrors)
      .then(async res => {

        console.log("res :", res);
        return res.json();
      })
      .then(async certificates => {

        console.log(certificates);
        return certificates;

      })
      .catch(error => {
        console.log(error);
      });
      //New version  end


      console.log(myResult);
      */

      //fetch Data from server

      var fetchCertificate = await fetch(process.env.API_URL + "/getAllCertificates", {
        method: "POST",
        body: JSON.stringify({
          "_owner": "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
          "_sender": "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747"
        }),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
      }).then(function(response) {
        console.log('------- fetchCertificate response start');
        console.log(response);
        console.log('------- fetchCertificate response end');
        //console.log("RESPONSE: "+response);
        //console.log("RESPONSE: "+response.text());
        return response.text();
      } , function(error) {
        console.log("error: "+error);
        error.message; //=> String
      });

      fetchCertificate = JSON.parse(fetchCertificate);
      //console.log("fetchCertificate: "+fetchCertificate);


      console.log(req.files);
      if(req.files && req.files.files.mimetype == "application/pdf"){
        var file = req.files.files,
          name = file.name,
          type = file.mimetype;
        var uploadpath = __dirname + '/../../uploads/' + name;
        console.log('__dirname', __dirname);
        //var uploadpath = '../../../uploads/' + name;
        file.mv(uploadpath,function(err){
          if(err){
            console.log("File Upload Failed ", name, err);
            res.send("Error Occured!");
            res.end();
          }
          else {
            console.log("File Uploaded",name);
            pdfParser.pdf2json(uploadpath,
              function (error, pdf) {

                console.log('---- before fs.unlink');
                fs.unlink(uploadpath, function (err) {
                  if (err) throw err;
                  // if no error, file has been deleted successfully
                  console.log('File deleted!');
                });

                if (error != null) {
                  console.log('---- in error during pdf2json');
                  console.log("here");
                  console.log("error: ")
                  console.log(error);

                  res.status(404).send({
                    result:false
                  });
                  res.end();
                } else {
                  try {
                    //console.log(JSON.stringify(pdf));
                    console.log('---- in try (no error)');
                    dataCertificate={
                      powerInW:null,
                      coSaved:null,
                      assetId:null,
                      productionDate:null,
                      txHash:null
                    };

                    asset=pdf.pages[0].texts[4].text.split(" ");
                    getLength=asset.length;
                    dataCertificate.assetId=asset[getLength-2]+" "+asset[getLength-1];
                    if(dataCertificate.assetId == "(45,391546N, 2,364349E)"){ dataCertificate.assetId=0;}
                    if(dataCertificate.assetId == "(43,761234N, 2,902555E)"){dataCertificate.assetId=1;}
                    if(dataCertificate.assetId == "(42,8331477N, 0,1998515000000225E)"){dataCertificate.assetId=2;}

                    console.log('---- dataCertificate.assetId = ', dataCertificate.assetId);

                    dataCertificate.powerInW = pdf.pages[0].texts[2].text;

                    console.log('---- dataCertificate.powerInW init = ', dataCertificate.powerInW);
                    if(dataCertificate.powerInW.split(" ")[0]) {
                      dataCertificate.powerInW = parseFloat(dataCertificate.powerInW.split(" ")[0].replace(",","."))*1000;
                    }
                    console.log('---- dataCertificate.powerInW cleaned = ', dataCertificate.powerInW);

                    //console.log('---- dataCertificate.coSaved init = ', pdf.pages[0].texts[2].text);
                    console.log('---- dataCertificate.coSaved init 1 = ', pdf.pages[0].texts[2].text);
                    console.log('---- dataCertificate.coSaved init 2 = ', pdf.pages[0].texts[2].text.split(" ")[0]);
                    console.log('---- dataCertificate.coSaved init 3 = ', pdf.pages[0].texts[2].text.split(" ")[1]);
                    console.log('---- dataCertificate.coSaved init 4 = ', pdf.pages[0].texts[2].text.split(" ")[2]);
                    console.log('---- dataCertificate.coSaved init 5 = ', pdf.pages[0].texts[2].text.split(" ")[3]);
                    if(
                      pdf.pages[0].texts[2].text.split(" ")[4].split("(")[1] &&
                      !isNaN( parseFloat(pdf.pages[0].texts[2].text.split(" ")[4].split("(")[1].replace(",",".")))
                    ) {
                      //Manage older certificate with "Watt Hour" instead of "Watt-Hour"
                      dataCertificate.coSaved= parseFloat(pdf.pages[0].texts[2].text.split(" ")[4].split("(")[1].replace(",","."));
                    } else if(
                      pdf.pages[0].texts[2].text.split(" ")[3].split("(")[1] &&
                      !isNaN( parseFloat(pdf.pages[0].texts[2].text.split(" ")[3].split("(")[1].replace(",",".")))
                    ){
                      dataCertificate.coSaved= parseFloat(pdf.pages[0].texts[2].text.split(" ")[3].split("(")[1].replace(",","."));
                    } else {
                      dataCertificate.coSaved = 'undefined'
                    }
                    console.log('---- dataCertificate.coSaved cleaned = ', dataCertificate.coSaved);


                    var tps=pdf.pages[0].texts[7].text.split(" ");
                    var time= tps[0]+"T"+tps[1];
                    var productionDateTimeStamp = new Date(time).getTime();
                    dataCertificate.productionDate = productionDateTimeStamp/1000;
                    dataCertificate.txHash=pdf.pages[0].texts[13].text.split("/")[4];

                    console.log('---- dataCertificate = ', dataCertificate);

                    if(dataCertificate.powerInW == null || dataCertificate.assetId == null || dataCertificate.productionDate == null || dataCertificate.txHash == null ){
                      console.log("Your certificate is not authentic.");
                      res.status(403).send({result:false});
                      res.end();
                    }

                    const realCertificate = fetchCertificate.find(certificate => {

                      return (
                        dataCertificate.powerInW === parseInt(parseInt(certificate._powerInW)/1000) &&
                        dataCertificate.assetId === parseInt(certificate._assetId) &&
                        //dataCertificate.productionDate === parseInt(certificate._productionDate) &&
                        dataCertificate.txHash === certificate._txHash
                      );
                    });
                    console.log('---- realCertificate = ', realCertificate);

                    if (realCertificate !== undefined){
                      const fullCertificate = { ...realCertificate,  _coSaved: dataCertificate.coSaved };

                      console.log('---- fullCertificate = ', fullCertificate);
                      res.status(201).send({
                        dataCertificate:fullCertificate,
                        result:true
                      });
                    }
                    else{
                      res.status(201).send({
                        result:false
                      });
                    }

                    //This part is used to verify the hash of certificate with smart contract
                    /* myContractCertificateLogic.methods.verifydHash(
                      dataCertificate.powerInW,
                      dataCertificate.assetId,
                      dataCertificate.productionDate,
                      dataCertificate.txHash
                    ).call({ from:'0x002cbE31Ed26DA610f742f1EE2B957229e57a8e4' }).then(function(result){
                      if(result == true){
                        dataCertificate.productionDate=dataCertificate.productionDate*1000;
                        res.status(201).send({
                          dataCertificate:dataCertificate,
                          result:result
                        });
                        res.end();
                      }else{
                        //console.log("here");
                        res.status(403).send({
                          result:result
                        });
                        res.end();
                      }
                    }); */
                  }
                  catch(error){
                    console.log('---- error trying :');
                    console.log(error);
                    res.status(403).send({
                      result:false
                    });
                    res.end();
                  }

                }
              }
            );
          }
        });
      }
      else {
        console.log("The file in not a PDF.");
        res.status(403).send({
          result:false
        });
        res.end();
      }
    });
  },


};
