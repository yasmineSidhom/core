var keythereum = require("keythereum");
var keyFileProducer = {
  id: "69203928-b5d6-25e3-505f-b3747e36ff59",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "bbc22d28f8442499d8e1df9ba14f0fe2" },
    ciphertext:
      "97c958344a6406f406d793614a7c4894fe6494f63fff1c5060e5c32618e09d61",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "7caa3b0720a6a7d4b75819a19ef48df95f5012483c11c4cf3625061f0bd3cf9c"
    },
    mac: "dc60ecb3ab4dd084e975a47d9c33b0858089df1fbbd9fcf672343a2e3da94dfd"
  },
  address: "002cbe31ed26da610f742f1ee2b957229e57a8e4",
  name: "Engie",
  meta: '{"description":"","passwordHint":"engie","timestamp":1519813337440}'
};
var keyFileConsumer = {
  id: "53971ef1-c216-7ffb-9be1-999bd8771338",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "b529cbb8094368217e37d568862fcced" },
    ciphertext:
      "267d4f04720984f4b5ae6b2d1869a5313e4bd8d7df64ba6aa8a4de2914941d0e",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "5b864a1b6253d8b4f95a06752c4f0791bd8b4f18f3338aadf3cb22aca06214e9"
    },
    mac: "d470dc58ede255609edda5036f084b0b68c3090efb2991d298d7a667d602e8d5"
  },
  address: "002a0494aaf7462a17a94ec50caf23a89dd63dc8",
  name: "Consumer",
  meta:
    '{"abi":[{"constant":false,"inputs":[{"name":"_owner","type":"address"}],"name":"removeOwner","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_addr","type":"address"}],"name":"isOwner","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":true,"inputs":[],"name":"m_numOwners","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":true,"inputs":[],"name":"m_lastDay","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[],"name":"resetSpentToday","outputs":[],"type":"function"},{"constant":true,"inputs":[],"name":"m_spentToday","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[{"name":"_owner","type":"address"}],"name":"addOwner","outputs":[],"type":"function"},{"constant":true,"inputs":[],"name":"m_required","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"constant":false,"inputs":[{"name":"_h","type":"bytes32"}],"name":"confirm","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":false,"inputs":[{"name":"_newLimit","type":"uint256"}],"name":"setDailyLimit","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"},{"name":"_data","type":"bytes"}],"name":"execute","outputs":[{"name":"_r","type":"bytes32"}],"type":"function"},{"constant":false,"inputs":[{"name":"_operation","type":"bytes32"}],"name":"revoke","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_newRequired","type":"uint256"}],"name":"changeRequirement","outputs":[],"type":"function"},{"constant":true,"inputs":[{"name":"_operation","type":"bytes32"},{"name":"_owner","type":"address"}],"name":"hasConfirmed","outputs":[{"name":"","type":"bool"}],"type":"function"},{"constant":true,"inputs":[{"name":"ownerIndex","type":"uint256"}],"name":"getOwner","outputs":[{"name":"","type":"address"}],"type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"kill","outputs":[],"type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"}],"name":"changeOwner","outputs":[],"type":"function"},{"constant":true,"inputs":[],"name":"m_dailyLimit","outputs":[{"name":"","type":"uint256"}],"type":"function"},{"inputs":[{"name":"_owners","type":"address[]"},{"name":"_required","type":"uint256"},{"name":"_daylimit","type":"uint256"}],"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"operation","type":"bytes32"}],"name":"Confirmation","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"operation","type":"bytes32"}],"name":"Revoke","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"oldOwner","type":"address"},{"indexed":false,"name":"newOwner","type":"address"}],"name":"OwnerChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newOwner","type":"address"}],"name":"OwnerAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"oldOwner","type":"address"}],"name":"OwnerRemoved","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newRequirement","type":"uint256"}],"name":"RequirementChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_from","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"data","type":"bytes"},{"indexed":false,"name":"created","type":"address"}],"name":"SingleTransact","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"operation","type":"bytes32"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"data","type":"bytes"},{"indexed":false,"name":"created","type":"address"}],"name":"MultiTransact","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"operation","type":"bytes32"},{"indexed":false,"name":"initiator","type":"address"},{"indexed":false,"name":"value","type":"uint256"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"data","type":"bytes"}],"name":"ConfirmationNeeded","type":"event"}],"deleted":false,"description":"","name":"Producer","passwordHint":"","tags":["wallet"],"timestamp":1522224243126,"wallet":true}'
};

var weechainKeys = {};

/*** NEW : using the new (light) version of the Smart Contract ***/

// Owner account
weechainKeys["YASMINE_ACCOUNT"] = {};
weechainKeys["YASMINE_ACCOUNT"]["key"] = {
  address: "00b091c0dc2adde00d9481d5b0a186a7005673d4",
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: {
      iv: "f0d20b2def44eb3593da252dd14d29f2"
    },
    ciphertext:
      "af21c46073992c1e7f1bc9ca5ad2b5a690ebe5965c023b330301431ed619a81d",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "ba81f22bbf89cfed48a3d050eaa921689752d6901c7f20500a4f6b2ab177fd15"
    },
    mac: "de050e40bda9bb6edc9d0e1ae062b8977e154f9e152621b7ecf08b0d2c6749c8"
  },
  id: "d782f973-a2e6-813e-f67f-3d9595078b3d",
  meta: '{"description":"","passwordHint":"","timestamp":1549983379690}',
  name: "Yasmine_Account",
  version: 3
};
weechainKeys["YASMINE_ACCOUNT"]["pwd"] = "Engie2018";

// Guest account (set to Admin)
weechainKeys["GUEST"] = {};
weechainKeys["GUEST"]["key"] = {
  address: "00df183f519412a3e2cdb4306413f14617ff3f09",
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: {
      iv: "0aad75ba6c1e064af419523a81afd45a"
    },
    ciphertext:
      "da3cd7989af41b5c26437bec113edd694adffde2d46065401eef175b4a3bd791",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "bc611a716101e9f2a25c254d08186746621cc1549fe712e12c812d11751f605c"
    },
    mac: "533246033fa7ccc5d8773c443b12fbb169653fa3e6f97304449d978cb3369243"
  },
  id: "43703cd6-b956-1994-6eed-888f32af7553",
  meta: '{"description":"","passwordHint":"","timestamp":1549988603616}',
  name: "GUEST",
  version: 3
};
weechainKeys["GUEST"]["pwd"] = "Engie2018";

// Guest account
weechainKeys["GUEST_2"] = {};
weechainKeys["GUEST_2"]["key"] = {
  address: "0088b8a1080c0962dbe46c6b0931ecbc99933685",
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: {
      iv: "90435d64a465e178caf5125bd66b6b77"
    },
    ciphertext:
      "97280ec0334c16cebb41d1aef65efb7e865fc36972197cb9857e6f03737e15ea",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "a03693313b511d4395ce3baa4f478d4d87f303f090a1ca2ca1de3b0cdc013e0f"
    },
    mac: "08417c4cf63c81dfe4f94fec218285e5f497e3792de36eeb63cceea33f1833b9"
  },
  id: "696d027d-0989-aaf1-0f43-d77048e9d34f",
  meta: '{"description":"","passwordHint":"","timestamp":1550043965745}',
  name: "GUEST_2",
  version: 3
};
weechainKeys["GUEST_2"]["pwd"] = "Engie2018";
//*** END NEW ***/

weechainKeys["fontanelles"] = {};
weechainKeys["fontanelles"]["key"] = {
  id: "1f42ead0-de82-36de-aec3-e2839dbe8a25",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "d9ea0af693504c1257cbe402310e3d18" },
    ciphertext:
      "7ae11c3c63522e95cc09639477f64a620f9347d4c878b362357b42d7b78fd4ea",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "f5ccfd394c38257bf4905dba4f515e528b52449f048b23c7128e78c814c52ebe"
    },
    mac: "f923c61131c07ae48c1be9e0eca66138071714b3b3cf230074e3183ab9116d4a"
  },
  address: "00c3b44dbea6f103dd30d7e90d59787175fea185",
  name: "fontanelles",
  meta: '{"description":"","passwordHint":"","timestamp":1525955759983}'
};
weechainKeys["fontanelles"]["pwd"] = "fontanellesProd";

weechainKeys["eget"] = {};
weechainKeys["eget"]["key"] = {
  id: "fafac314-b310-a3cb-3cbb-e624f4043118",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "c4a53fec5b39548d1b5eb99e740e7a5c" },
    ciphertext:
      "41e0d49568c05c1c57155b60e2ed53ded2e153b8be2dd63a2ecd7e99a9110cd0",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "178d6adeabfbb4514adb7cf0bd6a658b2f20981f1d28cb79801f0c2fb8fd2471"
    },
    mac: "6abaaa1be7d1636324622d0cf740d660a78883cceea8e55460951c44f99fc2eb"
  },
  address: "004a064a9aaf2fcd9687d82fc5a1116e9325000a",
  name: "eget",
  meta: '{"description":"","passwordHint":"","timestamp":1525955588136}'
};
weechainKeys["eget"]["pwd"] = "egetProd";

weechainKeys["mareges"] = {};
weechainKeys["mareges"]["key"] = {
  id: "2cf1682f-c6af-f259-7c72-7c71d9a4fcf9",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "0c96ba9718a35b67faacdc3579208e5d" },
    ciphertext:
      "f44e6ea98106e79f988ef5765ac8a9e11b9477517d0156ded9a1991b3807d7af",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "3557aa0494bdebb7dcbe3bbe9fa353ce3dc9560dbcc1ecfb20e5fbd37ccc2fe7"
    },
    mac: "825c03e353d0a480437171000f0649898aa9141a2d2dd702d0563b2fd5f0d031"
  },
  address: "00cb17ea4dfa8578582c00a79dd92d6111c28c54",
  name: "mareges",
  meta: '{"description":"","passwordHint":"","timestamp":1525955362314}'
};
weechainKeys["mareges"]["pwd"] = "maregesProd";

weechainKeys["lida"] = {};
weechainKeys["lida"]["key"] = {
  id: "591f31af-25b2-4b92-5b09-b0c839d95013",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "1da7b89ebfa59e1dd724b8d1a2a72786" },
    ciphertext:
      "11f181e261fc64b4f9e39c0a970f8af50991dcd0a224680c567095e2f0c8539b",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "82316748a92bc45439338277be40fdfb6650628083deaf152d9d8f9daab7c21c"
    },
    mac: "60b39a0da1e83649c2e34c5b07027fadad6361fdee154e7723ee2b149942343e"
  },
  address: "00deb90c67b8953f6dc4a5db3bb2bd6ab3bc2b3e",
  name: "lida",
  meta: '{"description":"","passwordHint":"","timestamp":1525954974660}'
};
weechainKeys["lida"]["pwd"] = "lidaCons";

//laboulangere / LaBoulangere2018WEECHAIN!
// 0x00d0F0e2AF12fec0C16c6D595a9554b99B6dE045
// amendment divisible rockstar sympathy backboard gorgeous award habitual footpath purchase reborn grappling

// 0x00f7556482DC4A029Cd45F1F9887Ee4E394Be4B8
// 2018*PainConcept
// justice idiocy denim shelve bankable baking barrette squatted thermos worrier shock zeppelin

weechainKeys["painconcept"] = {};
weechainKeys["painconcept"]["key"] = {
  address: "00f7556482dc4a029cd45f1f9887ee4e394be4b8",
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: {
      iv: "26d1f958ba314c77f8303cbe284e5ebe"
    },
    ciphertext:
      "049e178e43edf49ed4dbd3a58598bf8099f8d2811e693657631e4763983c820c",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "e1f930a099ae472ad1f22b669a7fab9dc535c3eed8ec958d2da94d659bd7166f"
    },
    mac: "91f74040842e240f64727c1ee779626c650c13e48b05efd3fbf8c9fe0186ccff"
  },
  id: "243f3ba5-3914-9717-aa0e-9a73769eea14",
  meta: '{"description":"","passwordHint":"","timestamp":1538226113296}',
  name: "painconcept",
  version: 3
};
weechainKeys["painconcept"]["pwd"] = "2018*PainConcept";

weechainKeys["laboulangere"] = {};
weechainKeys["laboulangere"]["key"] = {
  address: "00d0f0e2af12fec0c16c6d595a9554b99b6de045",
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: {
      iv: "efb979b0ef81cfd33d98c1c576d322d5"
    },
    ciphertext:
      "8e1cc2978d99a0410a327fc61caaf0853ae59eeffc4e6c95d66b6e75b5874944",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "b07626e8409a7e5a0b16e39d360b90611ad7d83f62104fd581754dd05efe6649"
    },
    mac: "689ac79a40812d6b8122628c1a1ccd7aa7936f9a0828e0c9d4ed4748c3cce12f"
  },
  id: "c7f90df4-3f18-4d0b-c5e7-fa309f0178c1",
  meta: '{"description":"","passwordHint":"","timestamp":1538224586006}',
  name: "laboulangere",
  version: 3
};
weechainKeys["laboulangere"]["pwd"] = "LaBoulangere2018WEECHAIN!";

weechainKeys["airproducts"] = {};
weechainKeys["airproducts"]["key"] = {
  id: "64384fd4-dd7f-522a-2737-ce866ba59309",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "2222533351672acfc230311541ad240b" },
    ciphertext:
      "2415159341e48b581eadddebf832c45a4a4e244cb92d776a1c69ec52eaa5b728",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "36d4183ee2f5402b17701d07c5d9da0a8e59dc146414818e8b9acbc52b26ad76"
    },
    mac: "d814d94adf8d7bf58ea09c060d84c485b7e1c4444218375f1e6d293f850c7f30"
  },
  address: "0051cccbcff51788dda9614b12dcea0d57208c03",
  name: "airproducts",
  meta: '{"description":"","passwordHint":"","timestamp":1525959112957}'
};
weechainKeys["airproducts"]["pwd"] = "airproductsAccount";

weechainKeys["engie"] = {};
weechainKeys["engie"]["key"] = {
  id: "fdadd190-ef5c-6daa-66bc-336536ac8299",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "0acbeb3aaf770cffb67e9d18a843801d" },
    ciphertext:
      "632bb911e5879bf02b9ea4b259a7b751656959f62d57217246992a4fe6e2372e",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "282d3b2592c1974213bc10ef4f911124c0db7c0cff9971b1fb11bdcdcdbaa99e"
    },
    mac: "a456ae862328c0ec3cc0b47916605a38543f6935ea83a8428df85fd7605a2f75"
  },
  address: "00f7e3b7f261b652e326d030decc189a06541abc",
  name: "engie",
  meta: '{"description":"","passwordHint":"","timestamp":1525958977355}'
};
weechainKeys["engie"]["pwd"] = "engieAccount";

weechainKeys["weechain_coo_owner"] = {};
weechainKeys["weechain_coo_owner"]["key"] = {
  id: "11115790-b311-5f08-804b-5ecf3b8b393e",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "a63acdb316d1f132ef04faec94140a94" },
    ciphertext:
      "fcce512dae49ad8b459beed9bd6e4785e8207a440b2536c926d669d11e96f3b0",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "6bb4025ca8c30bfaebf3017303cf082da6d6029e0fea9b74d85b08b4ab2d372a"
    },
    mac: "757bf32b084503827bb586f92ac2ab0481ca95b556d9312f2652c319461bd054"
  },
  address: "0029ab4dfcef587bb80e8b66d89ae44f4827f747",
  name: "WEECHAIN_PROD",
  meta:
    '{"description":"","passwordHint":"classic, free 4 prod","timestamp":1522936321859}'
};
weechainKeys["weechain_coo_owner"]["pwd"] = "laure2012*WCHPROD";

weechainKeys["smartmeter_prod1"] = {};
weechainKeys["smartmeter_prod1"]["key"] = {
  id: "d7c7838d-097e-1fd7-0c4c-2f14bf21ea62",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "168def40489c8362d4a51422aec852fe" },
    ciphertext:
      "161638dad4ab55c2c062860fef7859a678ef6288def0263ab1cdb255701ed1df",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "325b014c8504c8e3ec13f452474a3017e09ea0f936db7efb79385d7f4ebc66cc"
    },
    mac: "46831fab9248430980e0a14ab4e98009e122c180723fd29d3824cf0aaa35671e"
  },
  address: "008cab3fe4acdf4d7cb6b246ef16a2364b993527",
  name: "smartmeter_prod1",
  meta: '{"description":"","passwordHint":"","timestamp":1524319409874}'
};
weechainKeys["smartmeter_prod1"]["pwd"] = "laure2012*WCHPROD";

weechainKeys["smartmeter_cons1"] = {};
weechainKeys["smartmeter_cons1"]["key"] = {
  id: "6f82a6a3-6b49-179a-cd58-c5700432ca3c",
  version: 3,
  crypto: {
    cipher: "aes-128-ctr",
    cipherparams: { iv: "6273fea8ca3c09ed0d62860e4fc7db3e" },
    ciphertext:
      "09585efabb4b1e62dae6d9c177dc86bcc39df490b30255990c33a2998f7fa524",
    kdf: "pbkdf2",
    kdfparams: {
      c: 10240,
      dklen: 32,
      prf: "hmac-sha256",
      salt: "8c738a39f6ab728286c354c17805e54177f5db80180d8c82b3d7ca536fa1babc"
    },
    mac: "2363ebc8df79350501e7ffddf6aab2967be71113556e2c5443088fd301a60af8"
  },
  address: "0081288db2ef8bd44b4f58653c325a90ddd9b506",
  name: "smartmeter_cons1",
  meta: '{"description":"","passwordHint":"","timestamp":1524319965879}'
};
weechainKeys["smartmeter_cons1"]["pwd"] = "weechain*SMC1";

weechainKeys['plt_coindre'] = {};
weechainKeys['plt_coindre']['key'] = {
    "address": "00644e4a9b3ac9ab4544314c085b525c80311ed3",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "f86a68e1937b99e60c970ef54086505e"
        },
        "ciphertext": "6cacc6d925413bccd485d979e018a8a8e1d887f161abe717033e032490b6fdf5",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "ae9528115ee251aac7da016dedb039e99f0074a2488494c19a5d4778e76ef290"
        },
        "mac": "c7ee85565390ba58758dca4534c14496714650af08a0de07a46463940a32c5c0"
    },
    "id": "935e4141-aa93-0ab7-e6cd-599d7c9a7905",
    "meta": "{\"description\":\"\",\"passwordHint\":\"c0indr3\",\"timestamp\":1550136736864}",
    "name": "Coindre",
    "version": 3
}
weechainKeys['plt_coindre']['pwd'] = 'c0indr3';


weechainKeys['plt_soulom_bc'] = {};
weechainKeys['plt_soulom_bc']['key'] = {
    "address": "004888bac67f185af581d4f3ca5bb665e6fd9c52",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "c29177cfe345ba71fc1d43e422012945"
        },
        "ciphertext": "c1e86bb17a98419a317c6f1b8b56f71964089b1db2015743d2566f6d55a93dba",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "521448b0aac9731729a8b4ec472cd0f37ae56f91f2890727dca9be68a784ded9"
        },
        "mac": "3558cae2c41ef15cba7e469d408e8aab92dcd27248c1876bd59450b4394e7552"
    },
    "id": "860471d2-ce78-aa08-e5ed-4610f4e9d2e4",
    "meta": "{\"description\":\"\",\"passwordHint\":\"s0ul0mBC\",\"timestamp\":1550137055072}",
    "name": "Soulom BC",
    "version": 3
}
weechainKeys['plt_soulom_bc']['pwd'] = 's0ul0mBC';


weechainKeys['plt_soulom_hc'] = {};
weechainKeys['plt_soulom_hc']['key'] = {
    "address": "0040a818292b8cf7edd60a99cd7baccecf7f4f23",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "29d3ecb5e3a072fa2d2440e9bbeda83f"
        },
        "ciphertext": "17775ee69263ce6912eb14fdf48debbc81a94f57f86395e916e72df89b11d729",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "e474e7b9afa51f13d4525aa47def9291fc7ebe4cbcc52cf65f5f8d264441371c"
        },
        "mac": "e3b933cb14db5fb790544d046a6af66dc82aab2562c9f72278750ad7b9c08161"
    },
    "id": "ae6c070d-9c03-7f7a-a638-d841fea0fa1b",
    "meta": "{\"description\":\"\",\"passwordHint\":\"s0ul0mHC\",\"timestamp\":1550136906177}",
    "name": "Soulom HC",
    "version": 3
}
weechainKeys['plt_soulom_hc']['pwd'] = 's0ul0mHC';


weechainKeys['laboulangere_norpain'] = {};
weechainKeys['laboulangere_norpain']['key'] = {
    "address": "007b389338a2ebcb414b314f335cbd02f829d865",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "04755c8df0a10c5d4beec2bfc717e70a"
        },
        "ciphertext": "a84703ff71ae1f96eb206241dc757971bec48a49c7d7446171498fb51b51a01a",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "893e8d0d7ecc9e6fe7a6c43a25a4fd4182d04edc5e6e9fde1edeb9c8b907476d"
        },
        "mac": "2331339305795cab55f9cdc1aea82749ac2e6f6eca7b5d85a98ba1703e8f6fcb"
    },
    "id": "70b90d52-1584-cfa7-a9b0-2e8dea013c56",
    "meta": "{\"description\":\"\",\"passwordHint\":\"n0rP4in\",\"timestamp\":1550138640208}",
    "name": "NORPAIN",
    "version": 3
}
weechainKeys['laboulangere_norpain']['pwd'] = 'n0rP4in';


weechainKeys['laboulangere_ouest_boulangere_prod'] = {};
weechainKeys['laboulangere_ouest_boulangere_prod']['key'] = {
    "address": "00b98f3a71b13bf24c486463ef8733188a813b9e",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "a2b353c2396f34fa64b86c60e68631c5"
        },
        "ciphertext": "a8dea1f2ef1ef852a1288a27061e1d63416add87cf33d04dda84831aca4434bd",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "b9b1d0d0b143be6d2af39e54637f8105a4bc10dce8949fa56376e6c6f8d8e1aa"
        },
        "mac": "0c0094896123b6c3b6457b47ad3e625168b30397095b4754ab6407543786c1c9"
    },
    "id": "b80276d3-1186-4048-8f78-b11bd5504484",
    "meta": "{\"description\":\"\",\"passwordHint\":\"OuestBoul4ngereProd\",\"timestamp\":1550132212157}",
    "name": "OUEST BOULANGERE Production",
    "version": 3
}
weechainKeys['laboulangere_ouest_boulangere_prod']['pwd'] = 'OuestBoul4ngereProd';

weechainKeys['laboulangere_ouest_boulangere_logistique'] = {};
weechainKeys['laboulangere_ouest_boulangere_logistique']['key'] = {
    "address": "00a7c74ece30b8d92d866b387cce4e9568692e4f",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "603a529d655f9291b14c33d5a98a7585"
        },
        "ciphertext": "38343ba034f6ac07f7bbcc9da5e0755f97158033c20b6ce5c0c652c4056b8047",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "78e193ea08f506a463a6c2f2789afa29de9a4e4cbc2b95d0ceb967eb1a62f479"
        },
        "mac": "cb738ae19cee16842cd82f7cfd71cef84385a91e62ab046e04478fa671ae224a"
    },
    "id": "6cb0a585-c0e2-1728-91f5-04d49e8c32b0",
    "meta": "{\"description\":\"\",\"passwordHint\":\"\",\"timestamp\":1550132451500}",
    "name": "OUEST BOULANGERE Logistique",
    "version": 3
}
weechainKeys['laboulangere_ouest_boulangere_logistique']['pwd'] = 'OuestBoul4ngereLog';


weechainKeys['laboulangere_beaune_brioche'] = {};
weechainKeys['laboulangere_beaune_brioche']['key'] = {
    "address": "00579ee0f82ad4c38f6113bec58ff7d30dd2393a",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "9bbdc78b7bdf9876334dd60d0e2e61d9"
        },
        "ciphertext": "cbadee48281c7e0ef42f5530d330c5bbb145bd61c07a941a1ad3006408faf19d",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "131ee04bd1b7b166e4a8c89af545abacb2ada96947412b9f6289b48ee39cbf3b"
        },
        "mac": "071132193ebd9f4ca6e572be0a599d12cedda246dee2b4090491efa96d66a8be"
    },
    "id": "e3a269d9-8213-852d-9c29-67d92f1227f2",
    "meta": "{\"description\":\"\",\"passwordHint\":\"Be4uneBrioche\",\"timestamp\":1550132688831}",
    "name": "BEAUNE BRIOCHE",
    "version": 3
}
weechainKeys['laboulangere_beaune_brioche']['pwd'] = 'OuestBoul4ngereLog';


weechainKeys['laboulangere_viennoiserie_ligerienne'] = {};
weechainKeys['laboulangere_viennoiserie_ligerienne']['key'] = {
    "address": "00ddc548b42c13667cf2db1911f69744b11d11a5",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "4094f3a59bb0778ce111e916c4d7df01"
        },
        "ciphertext": "6a400daa5bdc0d8493627ee1af9951f10c51ddb1c0b81a98592f2b867c8a2500",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "faf4268be7158c4a5ff0be35a88fdef2f2a698bde6e03389a9ccb23b544396c9"
        },
        "mac": "af3f66e6e61956b2acf1e61840d6a7d3ed640b1592d4b94382b2f884bb363684"
    },
    "id": "7142adbd-7bf2-a372-2e04-316b0d3ed71d",
    "meta": "{\"description\":\"\",\"passwordHint\":\"Vi3nnois3ri3Lig3ri3nn3\",\"timestamp\":1550132851147}",
    "name": "VIENNOISERIE LIGERIENNE",
    "version": 3
}
weechainKeys['laboulangere_viennoiserie_ligerienne']['pwd'] = 'Vi3nnois3ri3Lig3ri3nn3';


weechainKeys['laboulangere_panorient'] = {};
weechainKeys['laboulangere_panorient']['key'] = {
    "address": "003e9184a21f3e9b3e58c5d58b53b5e2b98206ed",
    "crypto": {
        "cipher": "aes-128-ctr",
        "cipherparams": {
            "iv": "ce4f14c8adf909f20093e02042fdc1d0"
        },
        "ciphertext": "07416dc94f90bf18fea617be6f48f0bee5d6648d688496a0bab28bf33c5241a8",
        "kdf": "pbkdf2",
        "kdfparams": {
            "c": 10240,
            "dklen": 32,
            "prf": "hmac-sha256",
            "salt": "f871454e06bf139bd5ce0bcb66f0c7a9f68235778a57ddac74e01cc806fedd4f"
        },
        "mac": "96b1de0d6bf7b162e875f41cd032ae458f97d9bf263aa11d7f46f74fb8dc3b23"
    },
    "id": "3972ed11-4344-6545-b3a3-c3c53f2c66f8",
    "meta": "{\"description\":\"\",\"passwordHint\":\"p4nori3nt\",\"timestamp\":1550134365685}",
    "name": "PANORIENT",
    "version": 3
}
weechainKeys['laboulangere_panorient']['pwd'] = 'p4nori3nt';
/*
barilla / 2018#Barilla
0x00cEa80A19a0fC8D2137c6d5576e430E1F5a677E
frigidity book florist skimmed dizziness salami bolster sinner upcoming erased transpire unstopped
*/
exports.privateKeyProducer = function() {
  return keythereum.recover("engie", keyFileProducer);
};

exports.privateKeyConsumer = function() {
  return keythereum.recover("airproducts", keyFileConsumer);
};

exports.privateKeyByAccount = function(account) {
  return keythereum.recover(
    weechainKeys[account]["pwd"],
    weechainKeys[account]["key"]
  );
};

exports.privateKeyByAddress = function(address) {
  const weechainAccounts = Object.keys(weechainKeys);
  const account = weechainAccounts.find(weechainAccount => {
    return (
      "0x" + weechainKeys[weechainAccount].key.address === address.toLowerCase()
    );
  });

  return keythereum.recover(
    weechainKeys[account]["pwd"],
    weechainKeys[account]["key"]
  );
};

/**** YS added the following - Retrieving keys from DB ***/

async function privateKeyByAddress2(address) {
  adr = address.slice(2).toLowerCase();
  var key_result = require("../../routes/dbService")
    .getKey(adr)
    .then(function(result) {
      return result;
    });

  var key = await Promise.resolve(key_result);

  Key_structure = key[0].toObject().Key;
  var meta = JSON.stringify(key[0].toObject().Key.meta);
  meta_1 = remove_character(meta, 48);
  meta_2 = remove_character(meta_1, meta_1.length - 2);
  Key_structure.meta = meta_2;
  Key_structure.crypto.kdfparams.c = parseInt(
    key[0].toObject().Key.crypto.kdfparams.c
  );
  Key_structure.crypto.kdfparams.dklen = parseInt(
    key[0].toObject().Key.crypto.kdfparams.dklen
  );
  Key_structure.version = parseInt(key[0].toObject().Key.version);

  return keythereum.recover(key[0].pwd, Key_structure);
}

module.exports.privateKeyByAddress2 = privateKeyByAddress2;

// Key structure, will be filled with data retrieved from DB
let Key_structure = {
  address: "",
  crypto: {
    cipher: "",
    cipherparams: {
      iv: ""
    },
    ciphertext: "",
    kdf: "",
    kdfparams: {
      c: 0,
      dklen: 0,
      prf: "",
      salt: ""
    },
    mac: ""
  },
  id: "",
  meta: '{"description":"","passwordHint":"","timestamp":0}',
  name: "",
  version: 0
};

// Help function
function remove_character(str, char_pos) {
  part1 = str.substring(0, char_pos);
  part2 = str.substring(char_pos + 1, str.length);
  return part1 + part2;
}
