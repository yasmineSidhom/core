const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');

let AssetConsumingRegistry_artefact = require('../../../'+process.env.BUILD_FOLDER+'/contracts/AssetConsumingRegistryLogic.json');
const abiAssetConsuming = AssetConsumingRegistry_artefact.abi;
const addressAssetConsuming = AssetConsumingRegistry_artefact.networks[401697].address;
const myContractAssetConsuming = new web3.eth.Contract(abiAssetConsuming, addressAssetConsuming);

//var privateKey=require('./privateKey').privateKeyConsumer();
//console.log(privateKey);
module.exports = {

  //Create asset consuming registry
  createAssetConsuming:function(needEndPoint=true){
    const createAssetConsuming = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;

        var openDoorDataPart = myContractAssetConsuming.methods.createAsset().encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return createAssetConsuming;
    }
    this.use('/createAsset/consumer', createAssetConsuming);
  },

  //Init General Asset consuming registry
  initGeneralAssetConsuming:function(needEndPoint=true){
    const initGeneralAssetConsuming = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId = data._assetId;                     //uint
        var smartMeter = data._smartMeter;               //address
        var owner = data._owner;                         //address
        var operationalSince = data._operationalSince;   //uint
        var capacityWh = data._capacityWh;               //uint
        var maxCapacitySet = data._maxCapacitySet;       //bool
        var active=  data._active;                       //bool
        var sender=data._sender;                         //adddress

        var openDoorDataPart = myContractAssetConsuming.methods.initGeneral(
          assetId,
          smartMeter,
          owner,
          operationalSince,
          capacityWh,
          maxCapacitySet,
          active,
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };
        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async update(id, data, params) {
        var assetId = id;                                //uint
        var smartMeter = data._smartMeter;               //address
        var owner = data._owner;                         //address
        var operationalSince = data._operationalSince;   //uint
        var capacityWh = data._capacityWh;               //uint
        var maxCapacitySet = data._maxCapacitySet;       //bool
        var active=  data._active;                       //bool
        var sender=data._sender;                         //adddress

        var openDoorDataPart = myContractAssetConsuming.methods.initGeneral(
          parseInt(assetId,10),
          smartMeter,
          owner,
          operationalSince,
          capacityWh,
          maxCapacitySet,
          active,
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };
        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return initGeneralAssetConsuming;
    }
    this.use('/initGeneralAsset/consumer', initGeneralAssetConsuming);
  },

  //init Location producing properties with the smart contract assetProducingRegistryLogic
  assetInitLocationConsuming:function(needEndPoint=true){
    const assetInitLocationConsuming = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId= data._assetId ;            //uint
        var country= data._country ;            //bytes32
        var region= data._region ;              //bytes32
        var zip= data._zip ;                    //bytes32
        var city= data._city ;                  //bytes32
        var street= data._street ;              //bytes32
        var houseNumber= data._houseNumber ;    //bytes32
        var gpsLatitude= data._gpsLatitude ;    //bytes32
        var gpsLongitude= data._gpsLongitude ;  //bytes32
        var sender=data._sender;                //address

        var openDoorDataPart = myContractAssetConsuming.methods.initLocation(
          parseInt(assetId, 10),
          web3.utils.fromAscii(country),
          web3.utils.fromAscii(region),
          web3.utils.fromAscii(zip),
          web3.utils.fromAscii(city),
          web3.utils.fromAscii(street),
          web3.utils.fromAscii(houseNumber),
          web3.utils.fromAscii(gpsLatitude),
          web3.utils.fromAscii(gpsLongitude)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {
        var assetId= id ;            //uint
        var country= data._country ;            //bytes32
        var region= data._region ;              //bytes32
        var zip= data._zip ;                    //bytes32
        var city= data._city ;                  //bytes32
        var street= data._street ;              //bytes32
        var houseNumber= data._houseNumber ;    //bytes32
        var gpsLatitude= data._gpsLatitude ;    //bytes32
        var gpsLongitude= data._gpsLongitude ;  //bytes32
        var sender=data._sender;                //address

        var openDoorDataPart = myContractAssetConsuming.methods.initLocation(
          parseInt(assetId, 10),
          web3.utils.fromAscii(country),
          web3.utils.fromAscii(region),
          web3.utils.fromAscii(zip),
          web3.utils.fromAscii(city),
          web3.utils.fromAscii(street),
          web3.utils.fromAscii(houseNumber),
          web3.utils.fromAscii(gpsLatitude),
          web3.utils.fromAscii(gpsLongitude)
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };

        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return assetInitLocationConsuming;
    }
    this.use('/assetInitLocation/consumer', assetInitLocationConsuming);
  },

  //Save Smart Meter Read consuming registry
  assetSaveSmartMeterReadConsuming:function(needEndPoint=true){
    const assetSaveSmartMeterReadConsuming = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId = data._assetId;                                       //uint
        var newMeterRead = data._newMeterRead;                             //uint
        var lastSmartMeterReadFileHash = data._lastSmartMeterReadFileHash; //bytes32
        var smartMeterDown = data._smartMeterDown;                         //bool
        var sender = data._sender;                                         //adddress

        var openDoorDataPart = myContractAssetConsuming.methods.saveSmartMeterRead(
          assetId,
          newMeterRead,
          web3.utils.fromAscii(lastSmartMeterReadFileHash),
          smartMeterDown,
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };
        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async update(id, data, params) {
        var assetId = id;                                                  //uint
        var newMeterRead = data._newMeterRead;                             //uint
        var lastSmartMeterReadFileHash = data._lastSmartMeterReadFileHash; //bytes32
        var smartMeterDown = data._smartMeterDown;                         //bool
        var sender = data._sender;                                         //adddress

        var openDoorDataPart = myContractAssetConsuming.methods.saveSmartMeterRead(
          parseInt(assetId, 10),
          newMeterRead,
          web3.utils.fromAscii(lastSmartMeterReadFileHash),
          smartMeterDown,
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };
        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return assetSaveSmartMeterReadConsuming;
    }
    this.use('/saveSmartMeterRead/consumer', assetSaveSmartMeterReadConsuming);
  },

  //set Consumption For Periode from consuming registry contract
  setConsumptionForPeriode:function(needEndPoint=true){
    const setConsumptionForPeriode = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId = data._assetId;             //uint
        var consumed = data._consumed;           //uint
        var sender=data._sender;                 //address

        var openDoorDataPart = myContractAssetConsuming.methods.setConsumptionForPeriode(
          assetId,
          consumed
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {
        var assetId = id;                        //uint
        var consumed = data._consumed;           //uint
        var sender=data._sender;                 //address

        var openDoorDataPart = myContractAssetConsuming.methods.setConsumptionForPeriode(
          parseInt(assetId,10),
          consumed
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };

        var tx = new Tx(rawTx);
        await tx.sign(privateKey.returnPrivateKey(sender));
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);
      },
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return setConsumptionForPeriode;
    }
    this.use('/setConsumptionForPeriode/consumer', setConsumptionForPeriode);
  },

  //update Smart Meter from consuming registry contract
  updateSmartMeterConsuming:function(needEndPoint=true){
    const updateSmartMeterConsuming = {
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId = data._assetId;                                        //uint
        var newSmartMeter= data._newSmartMeter;                             //address
        var sender=data._sender;                                            //address

        var openDoorDataPart = myContractAssetConsuming.methods.updateSmartMeter(
          assetId,
          newSmartMeter,
        ).encodeABI();
        console.log(openDoorDataPart);

        const nonce= await web3.eth.getTransactionCount(sender);
        console.log(nonce);

        var rawTx = {
          nonce: web3.utils.toHex(nonce),
          value: 0x00,
          gasPrice: web3.utils.toHex(4000000000),
          gasLimit: web3.utils.toHex(1000000),
          data: openDoorDataPart,
          to: addressAssetConsuming,
          from: sender
        };

        var tx = new Tx(rawTx);
        var privateKey =require('./privateKey').privateKeyByAddress(data._sender);
        await tx.sign(privateKey);
        const serializedTx=await tx.serialize();
        const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));
        return Promise.resolve(ret);

      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return updateSmartMeterConsuming;
    }
    this.use('/updateSmartMeter/consumer', updateSmartMeterConsuming);
  },

  ///************************************************************************************GET INFORMATION CONSUMER********************************************************///

  //get Asset General consuming registry
  getAssetGeneralConsuming:function(needEndPoint=true){
    var getAssetGeneralConsuming={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId;//address
        var sender=data._sender;//address

        const result= await myContractAssetConsuming.methods.getAssetGeneral(assetId).call({ from:sender });
        console.log('getAssetGeneralConsuming: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetGeneralConsuming;
    }
    this.use('/getAssetGeneral/consumer',getAssetGeneralConsuming);
  },

  //get Asset General consuming registry
  getConsumingProperies:function(needEndPoint=true){
    var getConsumingProperies={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;//address

        const result= await myContractAssetConsuming.methods.getConsumingProperies(assetId).call({ from:sender });
        console.log('getConsumingProperies: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getConsumingProperies;
    }
    this.use('/getConsumingProperies/consumer',getConsumingProperies);
  },

  //get Asset Location
  getAssetLocationConsuming:function(needEndPoint=true){
    var getAssetLocationConsuming={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;   //address

        const result= await myContractAssetConsuming.methods.getAssetLocation(assetId).call({ from:sender });
        console.log('getAssetLocationConsuming:'+JSON.stringify(result));
        result[0]=web3.utils.toAscii(result[0]).replace(/\0/g, '');
        result[1]=web3.utils.toAscii(result[1]).replace(/\0/g, '');
        result[2]=web3.utils.toAscii(result[2]).replace(/\0/g, '');
        result[3]=web3.utils.toAscii(result[3]).replace(/\0/g, '');
        result[4]=web3.utils.toAscii(result[4]).replace(/\0/g, '');
        result[5]=web3.utils.toAscii(result[5]).replace(/\0/g, '');
        result[6]=web3.utils.toAscii(result[6]).replace(/\0/g, '');
        result[7]=web3.utils.toAscii(result[7]).replace(/\0/g, '');
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetLocationConsuming;
    }
    this.use('/getAssetLocation/consumer',getAssetLocationConsuming);
  },

  //getAssetDataLog asset
  getAssetDataLogConsuming:function(needEndPoint=true){
    var getAssetDataLogConsuming={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var assetId=data._assetId; //uint
        var sender=data._sender;   //address

        const result= await myContractAssetConsuming.methods.getAssetDataLog(assetId).call({ from:sender });
        console.log('getAssetDataLogConsuming: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetDataLogConsuming;
    }
    this.use('/getAssetDataLog/consumer',getAssetDataLogConsuming);
  },

  //get asset list lenght consuming registry
  getAssetListLengthConsuming:function(needEndPoint=true){
    var getAssetListLengthConsuming={
      async find(params) {},
      async get(id, params) {},
      async create(data, params) {
        var sender=data._sender;//address

        const result= await myContractAssetConsuming.methods.getAssetListLength().call({ from:sender });
        console.log('getAssetListLengthConsuming: '+JSON.stringify(result));
        return Promise.resolve(result);
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getAssetListLengthConsuming;
    }
    this.use('/getAssetListLength/consumer',getAssetListLengthConsuming);
  }

};
