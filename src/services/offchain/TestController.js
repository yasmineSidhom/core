const consumingSites = require('../../db/json/consumingSites.json');
const producingAssets = require('../../db/json/producingAssets.json');
const lida30Days = require('../../db/json/lida30Days.json');
const lidaYearToDate = require('../../db/json/lidaYearToDate.json');
const transportCertificates = require('../../db/json/transportCertificates.json');
const co2 = require('../../db/json/co2.json');

//DB Schemas
const Test = require('../../db/Test');

const fs = require('fs');
const path = require('path');
const moment = require('moment');
const fetch = require('node-fetch');

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

module.exports = {

  myTest:function(needEndPoint=true){
    var myTest={
      async find(params){
        console.log("myTest.find", params)
        return "Find is working";
      },
      async get(id, params){
        console.log("myTest.get", id, params)
        return "Get is working";
      },
      async create(data, params){
        console.log("myTest.create", data, params)
        return "Create is working";
      },
      async update(id, data, params) {
        console.log("myTest.update", id, data, params)
        return "update is working";
      },
      async patch(id, data, params) {
        console.log("myTest.patch", id, data, params)
        return "patch is working";
      },
      async remove(id, params) {
        console.log("myTest.remove", id, params)
        return "remove is working";
      },
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return myTest;
    }
    this.use('/mytest',async (req,res) => {
      myTest.find(req,res)
    });
    this.use('/mytest/create',async (req,res) => {
      myTest.create(req,res)
    });
  },

  createTest:function(needEndPoint=true){
    var createTest={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return createTest;
    }
    this.use('/test/create',async (req,res) => {
      Test.create({
              name : req.body.name
          },
          function (err, test) {
              if (err) return res.status(500).send("There was a problem adding the information to the database.");
              res.status(200).send(test);
          });
    });
  },


  getTests:function(needEndPoint=true){
    var getTests={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getTests;
    }
    this.use('/test',async (req,res) => {
      Test.find({}, function (err, tests) {
          if (err) return res.status(500).send("There was a problem finding the tests.");
          res.status(200).send(tests);
      });
    });
  },

};
