//const consumingSitesData = require('../../db/json/consumingSites.json');
//const producingAssetsData = require('../../db/json/producingAssets.json');
const weechain_conso130Days = require('../../db/json/weechain_conso130Days.json');
const weechain_conso1YearToDate = require('../../db/json/weechain_conso1YearToDate.json');
const barilla30Days = require('../../db/json/barilla30Days.json');
const barillaYearToDate = require('../../db/json/barillaYearToDate.json');
const laboulangere30Days = require('../../db/json/laboulangere30Days.json');
const laboulangereYearToDate = require('../../db/json/laboulangereYearToDate.json');
const laBoulangere30Days = require('../../db/json/laBoulangere30Days.json');
const laBoulangereYearToDate = require('../../db/json/laBoulangereYearToDate.json');
const transportCertificates = require('../../db/json/transportCertificates.json');

const fs = require('fs');
const path = require('path');
const moment = require('moment');
const fetch = require('node-fetch');

const co2 = JSON.parse(fs.readFileSync(require.resolve('../../db/json/co2.json'), 'utf8'))

const lida_rte_030Days = JSON.parse(fs.readFileSync(require.resolve('../../db/json/lida_rte_030Days.json'), 'utf8'))
const lida_rte_0YearToDate = JSON.parse(fs.readFileSync(require.resolve('../../db/json/lida_rte_0YearToDate.json'), 'utf8'))
const lida_rte_130Days = JSON.parse(fs.readFileSync(require.resolve('../../db/json/lida_rte_130Days.json'), 'utf8'))
const lida_rte_1YearToDate = JSON.parse(fs.readFileSync(require.resolve('../../db/json/lida_rte_1YearToDate.json'), 'utf8'))
const laboulangere_230Days = JSON.parse(fs.readFileSync(require.resolve('../../db/json/laboulangere_230Days.json'), 'utf8'))
const laboulangere_2YearToDate = JSON.parse(fs.readFileSync(require.resolve('../../db/json/laboulangere_2YearToDate.json'), 'utf8'))

const auth0ApiToken = require('../../db/json/auth0ApiToken.json');




const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}


const readJson = (path, cb) => {
  fs.readFile(require.resolve(path), (err, data) => {
    if (err)
      cb(err)
    else
      cb(null, JSON.parse(data))
  })
}

const getDates = (startDate, endDate) => {
  var dates = [],
      currentDate = startDate,
      addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
  while (currentDate < endDate) {
    dates.push(currentDate);
    currentDate = addDays.call(currentDate, 1);
  }
  return dates;
};

const getAssets = async () => {

  let myConsumingSites = JSON.parse(fs.readFileSync(require.resolve('../../db/json/consumingSites.json'), 'utf8'))
  let myProducingAssets = JSON.parse(fs.readFileSync(require.resolve('../../db/json/producingAssets.json'), 'utf8'))

  return {
    producingAssets: myProducingAssets,
    consumingSites: myConsumingSites
  }


}

module.exports = {
  //getConsumingSitesOffchain

  matching:function(consumingSitesMatching, producingAssetsMatching, nbMaxIterations, currentIteration){

    //Enrich the data of the producing assets : set the number of consuming sites related to this asset
    let consumingSitesData = JSON.parse(fs.readFileSync(require.resolve('../../db/json/consumingSites.json'), 'utf8'))
    let producingAssetsData = JSON.parse(fs.readFileSync(require.resolve('../../db/json/producingAssets.json'), 'utf8'))


    /*console.log('')
    console.log('')
    console.log('')
    console.log('')*/
    if(currentIteration === 0) {
      //console.log('🔥 Starting matching... ')
    } else {
      //console.log('🔥 New matching iteration : '+currentIteration+'/'+nbMaxIterations)
    }


    producingAssetsMatchingDetailed = producingAssetsMatching.map(producingAsset => {
      if(currentIteration === 0) {
        producingAsset.productionAvailable = producingAsset.production;
      }
      /*console.log('  '+producingAsset.name)
      console.log('    production :' + Number.parseFloat(producingAsset.production/1000).toPrecision(4)+'MWh')
      console.log('    productionAvailable :' + Number.parseFloat(producingAsset.productionAvailable/1000).toPrecision(4)+'MWh')
      console.log('    energyMatched :' + Number.parseFloat(producingAsset.energyMatched/1000).toPrecision(4)+'MWh')
      */

      return producingAsset;
    })

    consumingSitesMatchingDetailed = consumingSitesMatching.map(site => {
      //Sort the producings assets by priority
      site.producingAssets.sort((assetA, assetB) => {
        return assetB.priority - assetA.priority;
      });


      /*console.log('')
      console.log('-------------------------')
      console.log('  Consumption site : '+site.name)
      console.log('  Conso: '+Number.parseFloat(site.consumption/1000).toPrecision(4))
      console.log('  Energy matched: '+Number.parseFloat(site.energyMatched/1000).toPrecision(4))*/


      //For this consuming site, we can now run the first attempt to match the energy with the renewables assets regarding the priority
      //site.energyMatched = 0;
      site.producingAssets = site.producingAssets.map( asset => {

        //For this asset (the producingAssets are sorted by priority), let's calculate the energy matched for the site
        let prodAsset = producingAssetsMatching.find(producingAsset => {
          return producingAsset.id === asset.id //get the asset production
        })
        /*console.log('   Production site : '+prodAsset.name)*/



        let energyProducedReservedForSite = prodAsset.productionAvailable  * asset.percentage / 100;
        let energyNotMatchedForSite = site.consumption - site.energyMatched;
        asset.energyMatched = (asset.energyMatched && currentIteration !== 0) ? asset.energyMatched : 0;

        if(energyNotMatchedForSite > 0){
          if( energyProducedReservedForSite < energyNotMatchedForSite ) {
            site.energyMatched      += energyProducedReservedForSite;
            prodAsset.energyMatched += energyProducedReservedForSite;
            asset.energyMatched += energyProducedReservedForSite;
          } else {
            site.energyMatched += energyNotMatchedForSite;
            prodAsset.energyMatched += energyNotMatchedForSite;
            asset.energyMatched += energyNotMatchedForSite;
          }
        }
        /*

        console.log('     production : '+Number.parseFloat(prodAsset.production/1000).toPrecision(4)+'MWh')
        console.log('     productionAvailable : '+Number.parseFloat(prodAsset.productionAvailable/1000).toPrecision(4)+'MWh')
        console.log('     prodAsset.energyMatched : '+Number.parseFloat(prodAsset.energyMatched/1000).toPrecision(4)+'MWh')
        console.log('     energyProducedReservedForSite : '+asset.percentage+'% '+Number.parseFloat(energyProducedReservedForSite/1000).toPrecision(4)+'MWh')
        console.log('     energyNotMatchedForSite : '+Number.parseFloat(energyNotMatchedForSite/1000).toPrecision(4)+'MWh')
        */


        producingAssetsMatching = producingAssetsMatching.map( loopAsset => {
          if(prodAsset.id === loopAsset.id){
            return prodAsset
          } else {
            return loopAsset
          }
        })

        return asset;
      })
      return site;
    });




    /*console.log('')
    console.log('')
    console.log('consumingSitesMatchingDetailed')*/
    consumingSitesMatchingDetailed.map(consumingSiteMatching => {
      /*console.log('')
      console.log('  ' + consumingSiteMatching.smireName+' ' + parseInt(consumingSiteMatching.consumptionPercentage*100)+'%')
      console.log('    consumption : ' + parseInt(consumingSiteMatching.consumption/100)/10+'MWh')
      console.log('    energyMatched :' + parseInt(consumingSiteMatching.energyMatched/100)/10+'MWh')
      */
      consumingSiteMatching.producingAssets.map(producingAsset => {
        let producingAssetDetails = producingAssetsData.find(pAsset => {
          return pAsset.id === producingAsset.id //get the asset production
        })
        //console.log('        '+producingAssetDetails.name+' - energyMatched : ' + parseInt(producingAsset.energyMatched/1000)+'MWh '+producingAsset.percentage+'%')


      })
    })


    //Let's remove the percentage for the consuming sites that have all of their energy already matched
    const refreshedConsumingSitesMatching = consumingSitesMatchingDetailed.map( site => {
      if(site.consumption - site.energyMatched === 0) {
        //Put the percentage of all of its prod assets to 0
        const producingAssetsAtNullPercentage = site.producingAssets.map(prodAsset => {
          prodAsset.percentage = 0;
          return prodAsset;
        })
        site.producingAssets = producingAssetsAtNullPercentage
      }
      return site;
    })

    /*
    console.log('')
    console.log('')
    console.log('    refreshedConsumingSitesMatching')
    console.log(refreshedConsumingSitesMatching)
    */

    //Let's update the percentage of each producing assets for each consuming site
    //As we have removed some sites, the proportions need to be updated
    const updatedConsumingSitesMatching = refreshedConsumingSitesMatching.map( site => {
      const newSiteProducingAssets = site.producingAssets.map(prodAsset => {

        const oldPercentage = prodAsset.percentage;
        let totalForDivision = 0;

        //Find this asset among all the refreshed consuming sites (without the one with energy already matched )
        refreshedConsumingSitesMatching.map( siteCons => {
          siteCons.producingAssets.map(prodAssetBis => {
            if(prodAssetBis.id === prodAsset.id) {
              totalForDivision+= prodAssetBis.percentage;
            }
          })

        })

        let newPercentage = 100;
        if( totalForDivision > 0) {
          newPercentage = 100*oldPercentage/totalForDivision;
        } else {
          //console.error('NtoN Error during totalForDivision...', totalForDivision, oldPercentage)
        }

        prodAsset.percentage = newPercentage;
        return prodAsset;

      })

      site.producingAssets = newSiteProducingAssets;
      return site;
    })

    //Update the available production for the next round
    producingAssetsMatchingDetailed2 = producingAssetsMatchingDetailed.map( pAsset => {
      pAsset.productionAvailable =  pAsset.production - pAsset.energyMatched
      //console.log('   Prod asset '+pAsset.name+' prod ='+pAsset.production+' matched ='+pAsset.energyMatched+' )')
      return pAsset
    })

    /*console.log('')
    console.log('')
    console.log('    producingAssetsMatchingDetailed2')
    console.log(producingAssetsMatchingDetailed2)
    */


    /*console.log('')
    console.log('')
    console.log('    updatedConsumingSitesMatching')
    console.log(updatedConsumingSitesMatching)
    */
    //Get the total needed equivalent to 100%
    //Apply this total to each consuming site
    let energyWaitingToMatch = 0;

    updatedConsumingSitesMatching.map( cs => {
      energyWaitingToMatch += (cs.consumption - cs.energyMatched)
    })

    let energyStillAvailable = 0;

    producingAssetsMatchingDetailed2.map( p => {
      energyStillAvailable += (p.production - p.energyMatched)
      //console.log('Adding '+(p.production - p.energyMatched)+' from asset '+p.name+' ( prod ='+p.production+' , matched ='+p.energyMatched+' )')
    })

    let iterateAgain = true;
    if(currentIteration >= nbMaxIterations || energyWaitingToMatch === 0 || energyStillAvailable === 0) {
      iterateAgain = false;
    }
    //console.log('energyWaitingToMatch = '+energyWaitingToMatch)
    //console.log('energyStillAvailable = '+energyStillAvailable)

    if(iterateAgain) {
      return module.exports.matching(updatedConsumingSitesMatching, producingAssetsMatchingDetailed2, nbMaxIterations, currentIteration + 1)
    } else {
      return {consumingSites:updatedConsumingSitesMatching, producingAssets:producingAssetsMatchingDetailed2}
    }
  },


  getConsumingSitesOffchain:function(needEndPoint=true){
    var getConsumingSitesOffchain={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getConsumingSitesOffchain;
    }
    this.use('/getConsumingSitesOffchain',async (req,res) => {


      readJson('../../db/json/consumingSites.json', (err, consumingSitesOffchain) => {
        /*console.log('consumingSitesOffchain')
        consumingSitesOffchain.map(cs => {
          console.log(cs.id +'. '+cs.name+' ('+cs.smireName+' '+(parseInt(cs.consumptionPercentage*100, 10))+'%)')
          console.log(cs.producingAssets)
        })*/
        res.status(201).send({
          consumingSites:consumingSitesOffchain
        });
      });

    });
  },

  getConsumingSiteOffchain:function(needEndPoint=true){
    var getConsumingSiteOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getConsumingSiteOffchain;
    }

    this.use('/getConsumingSiteOffchain',async (req, res, next) => {

      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      const consumingSite = consumingSitesData.find(site => {
        return parseInt(req.body.assetId) === parseInt(site.id);
      })
      if(consumingSite) {
        res.status(201).send(
          consumingSite
        );
      } else {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

    });
  },

  getConsumingSiteLightTimeSeriesOffchain:function(needEndPoint=true){
    var getConsumingSiteLightTimeSeriesOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getConsumingSiteLightTimeSeriesOffchain;
    }

    this.use('/getConsumingSiteLightTimeSeriesOffchain',async (req, res, next) => {
      //check the data
      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;

      console.log(consumingSitesData)
      console.log(req.body)
      console.log(req.body.asset)

      const consumingAsset = consumingSitesData.find(site => {
        return parseInt(req.body.asset.id) === parseInt(site.id);
      })

      const start = req.body.start;
      const end = req.body.end;

      if(!consumingAsset) {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

      //Then we build the query
      let dayCount = 30;
      sitesInUrlSmire = consumingAsset.smireName;

      let username = 'system.weechain';
      let password = '3OetAPO8D03XIYs2WRuj';

      let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
      let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

      let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

      const response = await fetch(url, {method:'GET'});

      const json = await response.json();
      json.labels = [consumingAsset.name];
      res.status(201).send({
        data:json
      });
    });
  },

  getConsumingSiteTimeSeriesOffchain:function(needEndPoint=true){
    var getConsumingSiteTimeSeriesOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getConsumingSiteTimeSeriesOffchain;
    }

    this.use('/getConsumingSiteTimeSeriesOffchain',async (req, res, next) => {
      //check the data
      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      const consumingAsset = consumingSitesData.find(site => {
        return parseInt(req.body.asset.id) === parseInt(site.id);
      })

      const start = req.body.start;
      const end = req.body.end;

      if(!consumingAsset) {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

      //First, we get the producing assets related to this consuming asset

      const prodAssets = consumingAsset.producingAssets.map(producingAsset => {
        const tempAsset = producingAssetsData.find(prodAsset => {
          return producingAsset.id === prodAsset.id
        })
        return { ...tempAsset,  priority: producingAsset.priority,  percentage: producingAsset.percentage };
      })
      prodAssets.sort((assetA, assetB) => {
        return(assetB.priority -  assetA.priority )
      })

      //Then we build the query
      let dayCount = 30;
      let sitesInUrlSmire = '';
      prodAssets.map(prodAsset => {
        sitesInUrlSmire += prodAsset.name+'%2C';
      })
      sitesInUrlSmire += consumingAsset.smireName;

      let username = 'system.weechain';
      let password = '3OetAPO8D03XIYs2WRuj';

      let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
      let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

      let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

      const response = await fetch(url, {method:'GET'});

      const json = await response.json();
      json.labels = prodAssets.map(prodAsset => {
        return prodAsset.displayedName
      });
      json.labels.push(consumingAsset.name);
      res.status(201).send({
        data:json
      });
    });
  },

  getAllConsumingSitesConsumptionOffchain:function(needEndPoint=true){
    var getAllConsumingSitesConsumptionOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getAllConsumingSitesConsumptionOffchain;
    }

    this.use('/getAllConsumingSitesConsumptionOffchain',async (req, res, next) => {

      const start = req.body.start;
      const end = req.body.end;

      //Then we build the query
      let dayCount = 30;
      let sitesInUrlSmire = '';
      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      consumingSitesData.map((asset, idx) => {

        if(idx < consumingSitesData.length - 1) {
          sitesInUrlSmire += asset.smireName+'%2C';
        } else {
          sitesInUrlSmire += asset.smireName;
        }
      })

      console.log('🤦🤦🤦🤦🤦🤦🤦🤦🤦🤦'+sitesInUrlSmire)

      let username = 'system.weechain';
      let password = '3OetAPO8D03XIYs2WRuj';

      let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
      let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

      let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

      const response = await fetch(url, {method:'GET'});

      const json = await response.json();
      json.labels = consumingSitesData.map(asset => {
        return asset.name
      })
      json.ids = consumingSitesData.map(asset => {
        return asset.id
      })

      res.status(201).send({
        data:json
      });
    });
  },

  getAllProducingAssestProductionOffchain:function(needEndPoint=true){
    var getAllProducingAssestProductionOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getAllProducingAssestProductionOffchain;
    }

    this.use('/getAllProducingAssestProductionOffchain',async (req, res, next) => {

      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      const start = req.body.start;
      const end = req.body.end;

      //Then we build the query
      let dayCount = 30;
      let sitesInUrlSmire = '';
      producingAssetsData.map((asset, idx) => {
        if(idx < producingAssetsData.length - 1) {
          sitesInUrlSmire += asset.name+'%2C';
        } else {
          sitesInUrlSmire += asset.name;
        }
      })

      let username = 'system.weechain';
      let password = '3OetAPO8D03XIYs2WRuj';

      let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
      let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

      let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

      const response = await fetch(url, {method:'GET'});

      const json = await response.json();
      json.labels = producingAssetsData.map(asset => {
        return asset.displayedName
      })
      json.ids = producingAssetsData.map(asset => {
        return asset.id
      })

      res.status(201).send({
        data:json
      });
    });
  },




  getConsumingSiteCalculatedDataOffchain:function(needEndPoint=true){
    var getConsumingSiteCalculatedDataOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getConsumingSiteCalculatedDataOffchain;
    }

    this.use('/getConsumingSiteCalculatedDataOffchain',async (req, res, next) => {
      //check the data

      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      const asset = consumingSitesData.find(site => {
        return parseInt(req.body.asset.id) === parseInt(site.id);
      })

      const start = req.body.start;
      const end = req.body.end;
      //console.log(start +' ----->  '+end)
      if(!asset) {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

      if(start && !moment(start, 'YYYY-MM-DD').isValid()) {
        res.status(400).send({
          error:"Bad start date format (YYYY-MM-DD)",
        });
        res.end();
      }

      if(end && !moment(end, 'YYYY-MM-DD').isValid()) {
        res.status(400).send({
          error:"Bad end date format (YYYY-MM-DD)",
        });
        res.end();
      }

      let consumptionPercentage = asset.consumptionPercentage;
      if(!consumptionPercentage) {
        consumptionPercentage = 1;
      }

      asset.producingAssets.sort((assetA, assetB) => {
        return assetB.priority - assetA.priority;
      });

      let startDate = !start ? moment().subtract(30, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
      let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

      //console.log(startDate+' ----->  '+endDate)
      let result = []
      //Patch for performance, avoid slow request on lida site
      if( parseInt(asset.id) === 0 && moment().subtract(30, 'days').format('YYYY-MM-DD') === startDate ) {
        result.data = lida_rte_030Days;
      } else if ( parseInt(asset.id) === 0 && '2018-01-01' === startDate ) {
        result.data = lida_rte_0YearToDate;
      } else if( parseInt(asset.id) === 1 && moment().subtract(30, 'days').format('YYYY-MM-DD') === startDate ) {
        console.log(" 😍⚡️⚡️⚡️ ⚡️⚡️⚡️😍 lida_rte_130Days used");
        result.data = lida_rte_130Days;
      } else if ( parseInt(asset.id) === 1 && '2018-01-01' === startDate ) {
        console.log(" 😍⚡️⚡️⚡️ ⚡️⚡️⚡️😍 lida_rte_1YearToDate used");
        result.data = lida_rte_1YearToDate;
      } else if( parseInt(asset.id) === 2 && moment().subtract(30, 'days').format('YYYY-MM-DD') === startDate ) {
        result.data = laboulangere30Days;
      } else if ( parseInt(asset.id) === 2 && '2018-01-01' === startDate ) {
        result.data = laboulangereYearToDate;
      } else if( parseInt(asset.id) === 3 && moment().subtract(30, 'days').format('YYYY-MM-DD') === startDate ) {
        result.data = barilla30Days;
      } else if ( parseInt(asset.id) === 3 && '2018-01-01' === startDate ) {
        result.data = barillaYearToDate;
      } else {

        let url = process.env.API_URL + '/getConsumingSiteTimeSeriesOffchain';

        const data = {
          "asset": asset,
          start: startDate,
          end: endDate
        };

        const json = JSON.stringify(data);
        result = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          },
          body: json
        })
          .then(handleErrors)
          .then(res => res.json())
          .then(data => {
            return data
          })
          .catch(error => {
            console.log("error in getConsumingSiteCalculatedDataOffchain : ");
            console.log(error);
          });
      }

      const reqSiteTimeSeries2 = result.data;

      let barChartLabels2 = [];
      console.log("⚡️⚡️⚡️ ⚡️⚡️⚡️⚡️⚡️⚡️ ⚡️⚡️⚡️ reqSiteTimeSeries2 : ", reqSiteTimeSeries2);
      console.log("⚡️⚡️⚡️ ⚡️⚡️⚡️⚡️⚡️⚡️ ⚡️⚡️⚡️ reqSiteTimeSeries2.index : ", reqSiteTimeSeries2.index);
      for (const curIndex of reqSiteTimeSeries2.index) {
          barChartLabels2.push(curIndex.substr(0, 10));
      }

      let barChartData2 = [];
      let totalEnergyMatchedByAsset2 = []

      reqSiteTimeSeries2.columns.map((column, index) => {
        //console.log("reqSiteTimeSeries2 column :", column);
        if(index < reqSiteTimeSeries2.columns.length - 1 ) {
          //console.log("producingAssetsData find asset with column as name", producingAssetsData);


          //We do that because of the demo accounts : several assets with the same smire name
          const currentProdPossibleAssets = producingAssetsData.filter(prodAsset => {
            return column === prodAsset.name
          })

          const currentProdAsset = currentProdPossibleAssets.find(posAsset => {
            let isTheGoodAsset = false;
            asset.producingAssets.map(relatedAsset => {
              if(relatedAsset.id === posAsset.id) isTheGoodAsset = true;
            })
            return isTheGoodAsset
          })

          //console.log("currentProdAsset", currentProdAsset);

          barChartData2.push({data: [], label: currentProdAsset.displayedName});
          totalEnergyMatchedByAsset2.push({
            energyMatched: 0,
            id:  currentProdAsset.id,
            label: currentProdAsset.displayedName
          });
        } else {
          barChartData2.push({data: [], label: asset.name+' (consumption)', type: 'line'});
        }
      })

      let totalAvoidedCo22 = 0;
      let totalConsumption2 = 0;
      let totalEnergyMatched2 = 0;

      let doughnutData2 = [];


      for (let i = 0; i < reqSiteTimeSeries2.data.length; i++) {
        for (let j = 0; j < barChartData2.length; j++) {
          let energyData = reqSiteTimeSeries2.data[i][j];
          if(!energyData || energyData === "None") {
            energyData = 0
          }
          if(j < barChartData2.length - 1) {
            barChartData2[j].data.push(parseInt(energyData, 10));
          } else {
            barChartData2[j].data.push(parseInt(energyData * consumptionPercentage, 10));
          }
        }


        let consSitesWithConsumption = consumingSitesData.map(site => {
          //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
          //reqSiteTimeSeries2
          reqSiteTimeSeries2.columns.map((siteName, index) => {
            if(siteName === site.smireName) {
              //console.log("siteName = "+siteName+" on index = "+index);
              site.consumption = parseInt(reqSiteTimeSeries2.data[i][index]*consumptionPercentage, 10);
            }
          })

          site.energyMatched = 0;
          //console.log('auth0 site', site);
          return site;
        })


        let producingAssetsWithProduction = producingAssetsData.map(prodAsset => {
          //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
          //reqSiteTimeSeries2
          reqSiteTimeSeries2.columns.map((assetName, index) => {
            if(assetName === prodAsset.name) {
              //console.log('auth0 reqSiteTimeSeries2 consumption', parseInt(reqSiteTimeSeries2.data[i][index], 10))
              prodAsset.production = parseInt(reqSiteTimeSeries2.data[i][index], 10);
            }
          })
          prodAsset.energyMatched = 0;
          //console.log('auth0 prodAsset', prodAsset);
          return prodAsset;
        })

        //console.log('🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 current date : ', barChartLabels2[i])

        const matchingResult = module.exports.matching(consSitesWithConsumption, producingAssetsWithProduction, 0, 0);
        //console.log('auth0 NtoN matchingResult : ', matchingResult);

        doughnutData2[i]= [];

        //console.log('matchingResult')
        //console.log(matchingResult)

        const assetWithConsumption = matchingResult.consumingSites.find(consSite => {
          //console.log('auth0 asset name : ', asset.smireName, consSite.smireName)
          return asset.smireName === consSite.smireName;
        })
        /*console.log('');
        console.log('');
        console.log('');
        console.log('');*/
        //console.log('matchingResult', matchingResult)
        const assetConsumption = assetWithConsumption.consumption;
        const assetEnergyMatched = assetWithConsumption.energyMatched;
        let energyMatchedForDay = 0

        assetWithConsumption.producingAssets.map((prodAsset, index) => {
          const defaultValue = (index === 0) ? 100 : 0;
          //console.log(i+' .producingAssets asset '+prodAsset.id, ( assetConsumption > 0 ) ? (prodAsset.energyMatched/assetConsumption)*100 : defaultValue)
          //console.log(barChartLabels2[i]+" - "+assetWithConsumption.smireName+' assetConsumption = '+assetConsumption+' assetEnergyMatched = '+assetEnergyMatched)
          doughnutData2[i].push(( assetConsumption > 0 ) ? (prodAsset.energyMatched/assetConsumption)*100 : defaultValue)
          if (totalEnergyMatchedByAsset2[index]) {
            totalEnergyMatchedByAsset2[index].energyMatched += prodAsset.energyMatched;
          }
          energyMatchedForDay += prodAsset.energyMatched;
        })


        doughnutData2[i].push(( assetConsumption > 0 ) ? ((assetConsumption - assetEnergyMatched)/assetConsumption)*100 : 0);
        //doughnutData2[i].push(( assetConsumption > 0 ) ? (energyMatchedForDay/assetConsumption)*100 : 0)
        //console.log(barChartLabels2[i])

        let co2DataForDay = co2.find(item => {
          return item.date === barChartLabels2[i]
        })
        /*console.log('co2 : ')
        console.log(co2DataForDay)
        console.log(barChartLabels2[i])
        console.log(co2[co2.length-4])
        console.log(co2[co2.length-3])
        console.log(co2[co2.length-2])
        console.log(co2[co2.length-1])
        */

        totalConsumption2 += assetConsumption;
        totalAvoidedCo22 += energyMatchedForDay*co2DataForDay.CO2;
        totalEnergyMatched2 += energyMatchedForDay;
      }

      //console.log('auth0 doughnutData2', doughnutData2)

      let doughnutChartLabels2 = asset.producingAssets.map(prodAsset => {
        let currentProdAsset = producingAssetsData.find(matchProdAsset => {
          return matchProdAsset.id === prodAsset.id
        })
        return currentProdAsset.displayedName;

      });

      doughnutChartLabels2.push('Energy Mix')

      let doughnutChartLastDayData2 = doughnutData2[doughnutData2.length - 1].map(data => data);


      let totalPercentage = [];
      for (var i = 0; i < doughnutData2.length; i++) {
        doughnutData2[i].map((dataPercentage, index) => {
            totalPercentage[index] = isNaN(totalPercentage[index]) ? dataPercentage : totalPercentage[index]+dataPercentage;
        })
      }

      let doughnutChartData2 = totalPercentage.map(sitePercentage => {
        return parseInt(sitePercentage*10/doughnutData2.length, 10)/10;
      });

      let graphData = {
        doughnutChartLabels: doughnutChartLabels2,
        doughnutChartData: doughnutChartData2,
        doughnutChartLastDayData: doughnutChartLastDayData2,
        doughnutData: doughnutData2,
        barChartData: barChartData2,
        barChartLabels: barChartLabels2,
        totalAvoidedCo2: totalAvoidedCo22,
        totalConsumption: totalConsumption2,
        totalEnergyMatched: totalEnergyMatched2,
        totalEnergyMatchedByAsset: totalEnergyMatchedByAsset2
      }


      res.status(201).send({
        data:graphData
      });
    });
  },


  getConsumingSiteCalculatedDataOnchain:function(needEndPoint=true){
    var getConsumingSiteCalculatedDataOnchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getConsumingSiteCalculatedDataOnchain;
    }

    this.use('/getConsumingSiteCalculatedDataOnchain',async (req, res, next) => {
      //check the data

      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      const asset = consumingSitesData.find(site => {
        return parseInt(req.body.asset.id) === parseInt(site.id);
      })

      const start = req.body.start;
      const end = req.body.end;
      //console.log(start +' ----->  '+end)
      if(!asset) {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

      if(start && !moment(start, 'YYYY-MM-DD').isValid()) {
        res.status(400).send({
          error:"Bad start date format (YYYY-MM-DD)",
        });
        res.end();
      }

      if(end && !moment(end, 'YYYY-MM-DD').isValid()) {
        res.status(400).send({
          error:"Bad end date format (YYYY-MM-DD)",
        });
        res.end();
      }

      let url = process.env.API_URL + '/getAllCertificates';

      const paramCertificates = {
        "_owner":  "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
        "_sender": "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747"
      }

      const data = { asset, start, end };
      const json = JSON.stringify(paramCertificates);

      let allCertificates =  await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: json
      })
        .then(handleErrors)
        .then(res => res.json())
        .then(result => {
          return result
        })
        .catch(error => {
          console.log(error);
        });

      let urlPa = process.env.API_URL + '/getProducingAssetsOffchain';

      const responsePa = await fetch(urlPa, {method:'GET'});
      const allPasResults = await responsePa.json();
      const allProducingAssets = allPasResults.producingAssets;

      //Get the current asset and its owner
      //Find the producingAssets of the current asset

      //_powerInW
      //_owner
      //_productionDate
      //_assetId
      //_certificateId

      allCertificates = allCertificates.filter(certificate => {
        return !certificate._retired;
      });

      allCertificates.sort((certificateA, certificateB) => {
        return certificateA._productionDate - certificateB._productionDate;
      });

      //Manage all the dates
      const minDate = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(31, 'days').format('X')
      const maxDate = end ? moment(end, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

      const minDateRange = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('X')
      const maxDateRange = end ? moment(end, 'YYYY-MM-DD').add(1, "days").set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

      //Filter to keep only the certificates that are in the range of dates
      allCertificates = allCertificates.filter((c, index) => {
        const certifDate = parseInt(c._productionDate)

        return (certifDate >= minDate && certifDate <= maxDate);
      });

      let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

      //Change the format of the date to display it in the charts
      allDates = allDates.map(date => {
        return moment(date).format('YYYY-MM-DD')
      })

      //Just to be sure to have the dates in the correct order
      allDates.sort((dateA, dateB) => {
        const unixDateA =  moment(dateA, 'YYYY-MM-DD').format('X')
        const unixDateB =  moment(dateB, 'YYYY-MM-DD').format('X')
        return unixDateA - unixDateB;
      });

      const barChartLabels = allDates;

      //Don't keep the fake / demo assets
      let allRealProducingAssets = allProducingAssets.filter(p => {
        return p.isFake !== true
      });

      allRealProducingAssets = allProducingAssets.filter(p => {
        return asset.producingAssets.find(pa => {
          return p.id === pa.id
        })
      });


      /*
      let graphData = {
        doughnutChartLabels: doughnutChartLabels2,
        doughnutChartData: doughnutChartData2,
        doughnutChartLastDayData: doughnutChartLastDayData2,
        doughnutData: doughnutData2,
        barChartData: barChartData2,
        barChartLabels: barChartLabels2,
        totalAvoidedCo2: totalAvoidedCo22,
        totalConsumption: totalConsumption2,
        totalEnergyMatched: totalEnergyMatched2,
        totalEnergyMatchedByAsset: totalEnergyMatchedByAsset2
      }
      */

      let totalAvoidedCo2 = 0;
      let totalEnergyMatched = 0;

      //We sort the producing assets by priority, used in the same order later in the charts
      allRealProducingAssets.sort((prodAssetA, prodAssetB) => {
        const prodAssetAWithPriority =  asset.producingAssets.find(p => {
          return p.id === prodAssetA.id
        })

        const prodAssetAPriority =  prodAssetAWithPriority.priority

        const prodAssetBWithPriority =  asset.producingAssets.find(p => {
          return p.id === prodAssetB.id
        })

        const prodAssetBPriority =  prodAssetBWithPriority.priority

        return prodAssetBPriority - prodAssetAPriority;
      });

      const barChartData = [];
      const doughnutData = [];
      const barChartDataMatched = [];

      const totalEnergyMatchedByAsset = []

      allRealProducingAssets.map((pA, index) => {

        const assetCertificates = allCertificates.filter(certificate => {
          //return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10) && certificate._owner === asset.owner
          if( pA.isFake !== true ) {
            return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10)
          }
          else {
            return parseInt(certificate._assetId, 10) === parseInt(pA.duplicateOfAssetId, 10)
          }

        })

        const ownerCertificates = allCertificates.filter(certificate => {
          if( pA.isFake !== true ) {
            return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10) && certificate._owner === asset.owner
          }
          else {
            return parseInt(certificate._assetId, 10) === parseInt(pA.duplicateOfAssetId, 10) && certificate._owner === asset.owner
          }
        })

        barChartData[index] = {
          data: [],
          label: pA.displayedName,
          type: 'bar'
        };


        barChartDataMatched[index] = {
          data: [],
          label: pA.displayedName,
          type: 'bar'
        };

        assetCertificates.map(c => {
          //What is the date of the certificate ?
          const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
          //6 hours added is a patch. Need to manage utc, timezone...

          //Find the index in allDates for this date
          const dateIndex = allDates.indexOf(certifDate);

          //is there already a data for this date : if no, create it, otherwise, add the value to the existing value
          if(barChartData[index].data[dateIndex]) {
            //Sum all the certificates for this asset for that day
            barChartData[index].data[dateIndex] += parseFloat(c._powerInW)/1000;
          } else {
            barChartData[index].data[dateIndex] = parseFloat(c._powerInW)/1000;
          }
        })


        let pAEnergyMatched = 0;

        ownerCertificates.map(c => {

          const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
          //6 hours added is a patch. Need to manage utc, timezone...

          //Find the index in allDates for this date
          const dateIndex = allDates.indexOf(certifDate);

          //is there already a data for this date : if no, create it, otherwise, add the value to the existing value
          if(barChartDataMatched[index].data[dateIndex]) {
            //Sum all the certificates for this asset for that day
            barChartDataMatched[index].data[dateIndex] += parseFloat(c._powerInW)/1000;
          } else {
            barChartDataMatched[index].data[dateIndex] = parseFloat(c._powerInW)/1000;
          }
          pAEnergyMatched += parseFloat(c._powerInW)/1000;
          totalEnergyMatched += parseFloat(c._powerInW)/1000;
        })


        totalEnergyMatchedByAsset.push({
          energyMatched: pAEnergyMatched,
          id:  pA.id,
          label: pA.displayedName
        });


      })

      //Now, retrieve the consumption data of the asset
      let urlConsumingSiteTimeSeries = process.env.API_URL + '/getConsumingSiteLightTimeSeriesOffchain';

      const dataConsumingSiteTimeSeries = { "asset": asset, start, end };
      const jsonConsumingSiteTimeSeries = JSON.stringify(dataConsumingSiteTimeSeries);
      const consumingSiteDailyConsumptionData = await fetch(urlConsumingSiteTimeSeries, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json; charset=utf-8' },
        body: jsonConsumingSiteTimeSeries
      })
      .then(handleErrors).then(res => res.json()) .then(data => { return data })
      .catch(error => { console.log(error); });

      const consumingSiteDailyConsumption = consumingSiteDailyConsumptionData.data;
      //console.log('consumingSiteDailyConsumption', consumingSiteDailyConsumption, asset);

      const consumptionData = [];
      const consumptionPercentage = asset.consumptionPercentage ? asset.consumptionPercentage : 1;
      let totalConsumption = 0;

      consumingSiteDailyConsumption.data.map((consumptionArray, idx) => {
        const dailyConsumption = consumptionArray[0];
        let day = consumingSiteDailyConsumption.index[idx];
        let dayOriginal = moment(day).format('YYYY-MM-DD')
        day = moment(day).add(6, "hours").format('YYYY-MM-DD')


        console.log(moment(day).format('YYYY-MM-DD HH:mm'))

        const dateIndex = allDates.indexOf(day);
        consumptionData[dateIndex] = dailyConsumption * consumptionPercentage;
        totalConsumption += dailyConsumption * consumptionPercentage;
      })

      console.log("consumptionData", consumptionData)

      //Let's calculate the totalAvoidedCo2 between start and end
      //For each day, find the co2 generated by the french energy mix
      //Then calculate the sum for each day of the energy matched by the asset
      barChartLabels.map((day, dayIndex) => {

        //Get the CO2 for that day
        let co2DataForDay = co2.find(item => {
          return item.date === day
        })
        let co2ForDay = 40; //use the average default value, 40kg of CO2
        if(co2DataForDay !== undefined && co2DataForDay.CO2 && !isNaN(co2DataForDay.CO2)) {
          co2ForDay = co2DataForDay.CO2
        } else {
          console.log("CO2 pb for date "+ day + ":");
          console.log(JSON.stringify(co2DataForDay))

        }
        //Initiate the energy matched for that day at 0, we will add the energy just after that
        let energyMatchedForDay = 0;

        //At this step, we have only the producing assets in barChartDataMatched
        //Let's calculate the energy matched for that day
        //For each prod asset, find the energy matched that day

        doughnutData[dayIndex] = [];
        let assetConsumption = consumptionData[dayIndex] ? consumptionData[dayIndex] : 0;



        barChartDataMatched.map((pAsset, pAssetIdx) => {

          const defaultValue = (pAssetIdx === 0) ? 100 : 0;

          let assetMatchedProdForDay = pAsset.data[dayIndex] ? pAsset.data[dayIndex] : 0;

          doughnutData[dayIndex].push(( assetConsumption > 0 ) ? (assetMatchedProdForDay/assetConsumption)*100 : false)
          energyMatchedForDay += assetMatchedProdForDay;
        })
        console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  assetConsumption = "+assetConsumption)
        console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  energyMatchedForDay = "+energyMatchedForDay)
        console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  dayIndex = "+dayIndex)
        console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  (assetConsumption - energyMatchedForDay) = "+(assetConsumption - energyMatchedForDay))
        assetConsumption > 0 ? console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  energyMixPercentageForDay = "+(((assetConsumption - energyMatchedForDay)/assetConsumption)*100)) : console.log("assetConsumption <= 0" , doughnutData[dayIndex])

        const energyMixPercentageForDay = ( assetConsumption > 0 && (assetConsumption - energyMatchedForDay) > 0) ? ((assetConsumption - energyMatchedForDay)/assetConsumption)*100 : 0

        //If the energyMixPercentageForDay is 0 for energy mix, we need to verify that the sum of each producing asset percentage is 100
        if(energyMixPercentageForDay === 0) {
          let totalPercentageInit = 0;
          doughnutData[dayIndex].map(dayPercentage => {
            totalPercentageInit += dayPercentage;
          })
          console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  totalPercentageInit = "+totalPercentageInit)

          doughnutData[dayIndex] = doughnutData[dayIndex].map(dayPercentage => {
            return totalPercentageInit > 0 ? 100*dayPercentage/totalPercentageInit : false
          })
          console.log("🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  doughnutData[dayIndex] = ", doughnutData[dayIndex])
        }

        doughnutData[dayIndex].push(energyMixPercentageForDay);

        totalAvoidedCo2 += energyMatchedForDay*co2ForDay


      })

      let doughnutDataCleaned = doughnutData.filter(percentagesDayData => {
        const hasNoData = percentagesDayData.find(percentage => {
          return percentage === false
        })
        console.log("percentagesDayData, hasNoData", percentagesDayData, hasNoData)
        return (hasNoData === false) ? false : true
      })

      //Add the consumption assets data :
      barChartData.push({
        data: consumptionData,
        label: asset.name,
        type: 'line'
      });

      barChartDataMatched.push({
        data: consumptionData,
        label: asset.name,
        type: 'line'
      });



      let doughnutChartLabels = asset.producingAssets.map(prodAsset => {
        let currentProdAsset = allRealProducingAssets.find(p => {
          return prodAsset.id === p.id
        })
        return currentProdAsset.displayedName;
      });

      doughnutChartLabels.push('Energy Mix')

      let totalPercentage = [];
      console.log("😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍😍")
      console.log("😍😍😍😍😍😍 doughnutData original" , doughnutData)
      console.log("😍😍😍😍😍😍 doughnutData cleaned" , doughnutDataCleaned)
      for (var i = 0; i < doughnutDataCleaned.length; i++) {
        doughnutDataCleaned[i].map((dataPercentage, index) => {
            totalPercentage[index] = isNaN(totalPercentage[index]) ? dataPercentage : totalPercentage[index]+dataPercentage;
        })
      }
      console.log("😍😍😍😍😍😍 doughnutData after mapping dataPercentage" , doughnutData)
      console.log("😍😍😍😍😍😍 doughnutDataCleaned" , doughnutDataCleaned)
      console.log("😍😍😍😍😍😍 totalPercentage" , totalPercentage)

      let doughnutChartData = totalPercentage.map(sitePercentage => {
        return parseInt(sitePercentage*10/doughnutDataCleaned.length, 10)/10;
      });

      //let doughnutChartLastDayData = doughnutDataCleaned[doughnutDataCleaned.length - 1].map(data => data);
      //If the last day doesn't has no certificates, we will display the last day with certificates :
      const doughnutDataTemp = doughnutDataCleaned.map(data => data);
      const dates = barChartLabels.map(data => data);

      //Since the "find" function can't work from the end of the array, we reverse the array.
      doughnutData.reverse();
      //If wo do that for the production data, we need to reverse the dates array, it will be more convenient later
      dates.reverse()

      //Find the value of the last day that has certificates
      const lastDayValues = doughnutData.find(dataOfTheDay => {
        let hasCertificateThatDay = false;
        //remove the consumption data
        let dataOfTheDayTemp = dataOfTheDay.map(d => d);
        dataOfTheDayTemp.splice(dataOfTheDay.length - 1, 1);

        dataOfTheDayTemp.map(energyProduced => {
          if(energyProduced > 0) hasCertificateThatDay = true
        })

        return hasCertificateThatDay;
      })

      //Find the index of the last day that has certificates (use it to find the day in the "dates" array)
      const lastDayIndex = doughnutData.findIndex(dataOfTheDay => {
        let hasCertificateThatDay = false;
        //remove the consumption data
        let dataOfTheDayTemp = dataOfTheDay.map(d => d);
        dataOfTheDayTemp.splice(dataOfTheDay.length - 1, 1);

        dataOfTheDayTemp.map(energyProduced => {
          if(energyProduced > 0) hasCertificateThatDay = true
        })

        return hasCertificateThatDay;
      })

      const lastDayDate = dates[lastDayIndex];
      let doughnutChartLastDayData = lastDayValues;

      ///////////////////////////////////////////
      console.log("doughnutChartData")
      console.log(doughnutChartData)

      console.log("barChartLabels");
      console.log(barChartLabels);

      console.log("barChartData");
      console.log(barChartData);

      console.log("barChartDataMatched");
      console.log(barChartDataMatched);

      console.log("totalAvoidedCo2");
      console.log(totalAvoidedCo2);

      console.log("totalConsumption");
      console.log(totalConsumption);

      console.log("totalEnergyMatched");
      console.log(totalEnergyMatched);

      console.log("allCertificates.totalEnergyMatchedByAsset");
      console.log(totalEnergyMatchedByAsset);

      //console.log("doughnutData");
      //console.log(doughnutData);

      console.log("⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️⚡️😍")

      let graphData = {
        doughnutChartLabels: doughnutChartLabels,
        doughnutChartData: doughnutChartData,
        doughnutChartLastDayData: lastDayValues,
        doughnutChartLastDayDay: lastDayDate,
        doughnutData: doughnutDataCleaned,
        barChartData: barChartData,                           //Done
        barChartDataMatched: barChartDataMatched,             //Done
        barChartLabels: barChartLabels,                      //Done
        totalAvoidedCo2: totalAvoidedCo2,                    //Done
        totalConsumption: totalConsumption,                  //Done
        totalEnergyMatched: totalEnergyMatched,              //Done
        totalEnergyMatchedByAsset: totalEnergyMatchedByAsset //Done
      }

      res.status(201).send({
        data:graphData
      });


      /*
      let graphData = {
        doughnutChartLabels: doughnutChartLabels2,
        doughnutChartData: doughnutChartData2,
        doughnutChartLastDayData: doughnutChartLastDayData2,
        doughnutData: doughnutData2,
        barChartData: barChartData2,                          //Done
        barChartLabels: barChartLabels2,                      //Done
        totalAvoidedCo2: totalAvoidedCo22,                    //Done
        totalConsumption: totalConsumption2,                  //Done
        totalEnergyMatched: totalEnergyMatched2,              //Done
        totalEnergyMatchedByAsset: totalEnergyMatchedByAsset2 //Done
      }
      */


    });
  },



  getUsers:function(needEndPoint=true){
    var getUsers={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getUsers;
    }
    this.use('/getUsers',async (req,res) => {
      const request = require("request");
      const options = {
        method: 'GET',
        url: 'https://weechain.eu.auth0.com/api/v2/users',
        qs: { search_engine: 'v3' },
        headers: { authorization: 'Bearer '+auth0ApiToken.token }
      };

      request(options, async function (error, response, body) {
        if (error) throw new Error(error);

        res.status(201).send({
          data:JSON.parse(body)
        });
      });
    });
  },

  getUserByEmail:function(needEndPoint=true){
    var getUserByEmail={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getUserByEmail;
    }
    this.use('/getUserByEmail',async (req,res) => {

      const email = req.body.email;

      const request = require("request");
      const options = {
        method: 'GET',
        url: 'https://weechain.eu.auth0.com/api/v2/users',
        qs: { q: 'email:"'+email+'"', search_engine: 'v3' },
        headers: { authorization: 'Bearer '+auth0ApiToken.token }
      };

      request(options, async function (error, response, body) {
        if (error) throw new Error(error);

        res.status(201).send({
          data:JSON.parse(body)
        });
      });
    });
  },

  setSiteCo2DataOffchain:function(needEndPoint=true){
    var setSiteDataOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        console.log(data);
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return setSiteCo2DataOffchain;
    }

    this.use('/setSiteCo2DataOffchain',async (req, res, next) => {
      //check the data
      const consumingSiteCheck = consumingSitesData.find(site => {
        return parseInt(req.body.asset.id) === parseInt(site.id);
      })

      if(!consumingSiteCheck) {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

      if(!req.body.asset.avoidedCo2) {
        res.status(403).send({ error: "avoidedCo2 is undefined" });
        res.end();
        throw "avoidedCo2 id undefined";
      }

      if(isNaN(parseInt(req.body.asset.avoidedCo2))) {
        res.status(403).send({ error: "avoidedCo2 is not a number" });
        res.end();
        throw "avoidedCo2 is not a number";
      }

      let consumingSiteUpdated = {}
      const consumingSitesUpdated = consumingSitesData.map(site => {
        if(parseInt(req.body.asset.id) === parseInt(site.id) && req.body.asset.avoidedCo2){
          site.avoidedCo2 = parseInt(req.body.asset.avoidedCo2);
          consumingSiteUpdated = site;
        }
        return site;
      })

      let consumingSitesUpdatedData = JSON.stringify(consumingSitesUpdated, null, 4);

      fs.writeFile(path.join(__dirname, '../../db/json') + '/consumingSites.json', consumingSitesUpdatedData, (error) => {
        if (error) {
          res.status(403).send({ error });
          res.end();
          throw error;
        }
        res.status(201).send({
          site:consumingSiteUpdated
        });
      });
    });
  },

  getProducingAssetsOffchain:function(needEndPoint=true){
    var getProducingAssetsOffchain={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getProducingAssetsOffchain;
    }
    this.use('/getProducingAssetsOffchain',async (req,res) => {

      readJson('../../db/json/producingAssets.json', (err, producingAssets) => {
        //console.log('producingAssets')
        producingAssets.map(cs => {
          //console.log(cs)
        })
        res.status(201).send({
          producingAssets:producingAssets
        });
      });

    });
  },

  getProducingAssetOffchain:function(needEndPoint=true){
    var getProducingAssetOffchain={
      find: function(params,callback){
        callback(null,this.procedure);
      },
      get:function(params,callback){
        callback(null,this.procedure);
      },
      create :function(data, params, callback){
        console.log(data);
        return data;
      },
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };

    if(needEndPoint == false){
      return getProducingAssetOffchain;
    }

    this.use('/getProducingAssetOffchain',async (req, res, next) => {
      const myAssets = await getAssets();
      const consumingSitesData = myAssets.consumingSites;
      const producingAssetsData = myAssets.producingAssets;

      const producingAsset = producingAssetsData.find(site => {
        return parseInt(req.body.assetId) === parseInt(site.id);
      })
      if(producingAsset) {
        res.status(201).send(
          producingAsset
        );
      } else {
        res.status(404).send({
          error:"Bad asset id",
        });
        res.end();
      }

    });
  },

  getCo2Offchain:function(needEndPoint=true){
    var getCo2Offchain={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getCo2Offchain;
    }
    this.use('/getCo2Offchain',async (req,res) => {
      res.status(201).send({
        co2:co2
      });
    });
  },


  getTranportCertificatesOffchain:function(needEndPoint=true){
    var getTranportCertificatesOffchain={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return getTranportCertificatesOffchain;
    }
    this.use('/getTranportCertificatesOffchain',async (req,res) => {
      res.status(201).send({
        data:transportCertificates
      });
    });
  },

  updateCacheData:function(needEndPoint=true){
    var updateCacheData={
      async find(params){},
      async get(id, params){},
      async create(data, params){},
      async update(id, data, params) {},
      async patch(id, data, params) {},
      async remove(id, params) {},
      setup(app, path) {}
    };
    if(needEndPoint==false){
      return updateCacheData;
    }
    this.use('/updateCacheData',async (req,res) => {

      console.log("updateCacheData start")
      let startDate = moment().subtract(30, 'days').format('YYYY-MM-DD'); //By default, yesterday
      let endDate = moment().format('YYYY-MM-DD'); //By default, today



      let url = process.env.API_URL + '/getConsumingSiteTimeSeriesOffchain';

      readJson('../../db/json/consumingSites.json', async (err, consumingSitesOffchain) => {
        console.log('consumingSitesOffchain')
        await consumingSitesOffchain.map( async (cs, idx) => {

          const data30Days = {
            asset: { id: cs.id },
            start: startDate,
            end: endDate
          };

          const dataYearToDate = {
            asset: { id: cs.id  },
            start: '2018-01-01',
            end: endDate
          };

          const json30Days = JSON.stringify(data30Days);
          const jsonYearToDate = JSON.stringify(dataYearToDate);

          console.log("updateCacheData data30Days "+cs.name, data30Days)
          console.log("updateCacheData dataYearToDate "+cs.name, dataYearToDate)
          const result30Days = await fetch(url, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json; charset=utf-8'
            },
            body: json30Days
          })
            .then(handleErrors)
            .then(res => res.json())
            .then(data => {
              console.log("updateCacheData data30Days retrieved for "+cs.name)
              return data
            })
            .catch(error => {
              console.log("error in updateCacheData : ");
              console.log(error);
            });

          const resultYearToDate = await fetch(url, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json; charset=utf-8'
            },
            body: jsonYearToDate
          })
            .then(handleErrors)
            .then(res => res.json())
            .then(data => {
              console.log("updateCacheData dataYearToDate retrieved for "+cs.name)
              return data
            })
            .catch(error => {
              console.log("error in updateCacheData : ");
              console.log(error);
            });

          const reqSiteTimeSeries30Days = result30Days.data;
          const reqSiteTimeSeriesYearToDate = resultYearToDate.data;

          console.log("updateCacheData timeseries ready to save for "+cs.name)

          let timeSeries30Days = JSON.stringify(reqSiteTimeSeries30Days, null, 4);
          let timeSeriesYearToDate = JSON.stringify(reqSiteTimeSeriesYearToDate, null, 4);

          fs.writeFileSync(path.join(__dirname, '../../db/json') + '/'+cs.smireName+'_'+cs.id+'30Days.json', timeSeries30Days);
          fs.writeFileSync(path.join(__dirname, '../../db/json') + '/'+cs.smireName+'_'+cs.id+'YearToDate.json', timeSeriesYearToDate);

          console.log("updateCacheData done for "+cs.name)
          if(idx === (consumingSitesOffchain.length - 1)){
            console.log("updateCacheData finished")
            res.status(201).send({
              status:"ok"
            });
          }

        })

      });
    });
  },


};
