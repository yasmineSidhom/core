// eslint-disable-next-line no-unused-vars
module.exports = function () {

  const app = this; // eslint-disable-line no-unused-vars
  const accountsService=require('./tobalaba/accountsService');
  const userLogicService=require('./tobalaba/userLogicService');
  const demandLogicService=require('./tobalaba/demandLogicService');
  const certificateLogicService=require('./tobalaba/certificateLogicService');
  const assetConsumingRegistryLogicService=require('./tobalaba/assetConsumingRegistryLogicService');
  const assetProducingRegistryLogicService=require('./tobalaba/assetProducingRegistryLogicService');
  const retrieveDataFromTobalaba=require('./tobalaba/retrieveDataFromTobalaba');
  const retrieveDataOffchain=require('./offchain/retrieveDataOffchain');
  //const assetConsumingController=require('./offchain/AssetConsumingController');
  const testController=require('./offchain/TestController');
  const eCO2mix=require('./tobalaba/eCO2mix');
  const parserPdf=require('./tobalaba/parserPdf');
  const updateSmartContract=require('./tobalaba/updateSmartContract');

  //All EndPoint of Accounts
  app.configure(accountsService.createAccount);
  app.configure(accountsService.decrypt);
  app.configure(accountsService.encrypt);
  app.configure(accountsService.getAllAccounts);

  //All EndPoint of UserLogic contract
  app.configure(userLogicService.addAdminRole);
  app.configure(userLogicService.addAssetManager);
  app.configure(userLogicService.addMatcherRole);
  app.configure(userLogicService.addTraderRole);
  app.configure(userLogicService.deactivateUser);
  app.configure(userLogicService.doesUserExist);
  app.configure(userLogicService.getAllUserAddress);
  app.configure(userLogicService.getFullUser);
  app.configure(userLogicService.getAllUsers);

  app.configure(userLogicService.getRolesRights);
  app.configure(userLogicService.getUserListLength);
  app.configure(userLogicService.removeAdminRole);
  app.configure(userLogicService.removeAssetManagerRole);
  app.configure(userLogicService.removeTraderRole);
  app.configure(userLogicService.setOrganization);
  app.configure(userLogicService.setOrganizationAddress);
  app.configure(userLogicService.setRoles);
  app.configure(userLogicService.setUser);
  app.configure(userLogicService.setUserName);

  //All EndPoint of demandLogic contract
  app.configure(demandLogicService.checkDemandCoupling);
  app.configure(demandLogicService.checkDemandGeneral);
  app.configure(demandLogicService.checkMatcher);
  app.configure(demandLogicService.checkPriceDriving);
  app.configure(demandLogicService.createDemand);
  app.configure(demandLogicService.createDemandCoupling);
  app.configure(demandLogicService.createDemandGeneralInfo);
  app.configure(demandLogicService.demandGetCurrentPeriod);
  app.configure(demandLogicService.demandLocation);
  app.configure(demandLogicService.demandMatcherProperties);
  app.configure(demandLogicService.demandPriceDriving);
  app.configure(demandLogicService.matching);
  app.configure(demandLogicService.QueryDemand);
  app.configure(demandLogicService.matchingForOneAsset);

  //All EndPoint of certificateLogic contract
  app.configure(certificateLogicService.changeCertificateOwner);
  app.configure(certificateLogicService.createCertificate);
  app.configure(certificateLogicService.createCertificateForAssetOwner);
  app.configure(certificateLogicService.getAllCertificates);
  //app.configure(certificateLogicService.getAllCertificatesOfOwner);
  app.configure(certificateLogicService.getCertificate);
  app.configure(certificateLogicService.getCertificateListLength);
  app.configure(certificateLogicService.getAllCertificatesFromServer);
  app.configure(certificateLogicService.getCertificateOwner);
  app.configure(certificateLogicService.isRetired);
  app.configure(certificateLogicService.retireCertificate);
  app.configure(certificateLogicService.transferOwnershipByEscrow);
  app.configure(certificateLogicService.setCertificateTxHash);
  app.configure(certificateLogicService.setCertificateHash);
  app.configure(certificateLogicService.transferOwnershipByEscrow);
  app.configure(certificateLogicService.getAllCertificatesFromCertificateDB);
  app.configure(certificateLogicService.getSmartContractVersion);//Test to call a function from certififcateDB

  //All EndPoint of assetConsumingRegistryLogic contract
  app.configure(assetConsumingRegistryLogicService.assetInitLocationConsuming);
  app.configure(assetConsumingRegistryLogicService.assetSaveSmartMeterReadConsuming);
  app.configure(assetConsumingRegistryLogicService.createAssetConsuming);
  app.configure(assetConsumingRegistryLogicService.getAssetDataLogConsuming);
  app.configure(assetConsumingRegistryLogicService.getAssetGeneralConsuming);
  app.configure(assetConsumingRegistryLogicService.getAssetListLengthConsuming);
  app.configure(assetConsumingRegistryLogicService.getAssetLocationConsuming);
  app.configure(assetConsumingRegistryLogicService.getConsumingProperies);
  app.configure(assetConsumingRegistryLogicService.initGeneralAssetConsuming);
  app.configure(assetConsumingRegistryLogicService.setConsumptionForPeriode);
  app.configure(assetConsumingRegistryLogicService.updateSmartMeterConsuming);

  //All EndPoint of assetProducingRegistryLogic contract
  app.configure(assetProducingRegistryLogicService.assetInitLocationProducing);
  app.configure(assetProducingRegistryLogicService.assetInitProducingProperties);
  app.configure(assetProducingRegistryLogicService.assetSaveSmartMeterReadProducing);
  app.configure(assetProducingRegistryLogicService.createAssetProducing);
  app.configure(assetProducingRegistryLogicService.getAssetCoSavedProducing);
  app.configure(assetProducingRegistryLogicService.getAssetDataLogProducing);
  app.configure(assetProducingRegistryLogicService.getAssetListLengthProducing);
  app.configure(assetProducingRegistryLogicService.getAssetLocationProducing);
  app.configure(assetProducingRegistryLogicService.getAssetProducing);
  app.configure(assetProducingRegistryLogicService.getAssetProducingPropertiesProducing);
  app.configure(assetProducingRegistryLogicService.initGeneralAssetProducing);
  app.configure(assetProducingRegistryLogicService.updateSmartMeterProducing);
  app.configure(assetProducingRegistryLogicService.getPastDataSmartMeter);

  //All EndPoint for retrieve data from TOBALABA
  app.configure(retrieveDataFromTobalaba.getAllCertificatesFromOrigin);
  app.configure(retrieveDataFromTobalaba.getAllCertificatesWithOtherSmartContract);
  app.configure(retrieveDataFromTobalaba.getGeneralAssetFromOrigin);
  app.configure(retrieveDataFromTobalaba.getLocationAssetFromOrigin);
  app.configure(retrieveDataFromTobalaba.getProducingPropertiesAssetFromOrigin);
  app.configure(retrieveDataFromTobalaba.getTypeAssetFromOrigin);

  //Endpoints for offchain data
  app.configure(retrieveDataOffchain.getConsumingSitesOffchain);
  app.configure(retrieveDataOffchain.getConsumingSiteOffchain);
  app.configure(retrieveDataOffchain.setSiteCo2DataOffchain);
  app.configure(retrieveDataOffchain.getConsumingSiteTimeSeriesOffchain);
  app.configure(retrieveDataOffchain.getConsumingSiteLightTimeSeriesOffchain);

  app.configure(retrieveDataOffchain.getConsumingSiteCalculatedDataOffchain);
  app.configure(retrieveDataOffchain.getConsumingSiteCalculatedDataOnchain);
  app.configure(retrieveDataOffchain.getAllConsumingSitesConsumptionOffchain);
  app.configure(retrieveDataOffchain.getAllProducingAssestProductionOffchain);

  app.configure(retrieveDataOffchain.getUsers);
  app.configure(retrieveDataOffchain.getUserByEmail);

  app.configure(retrieveDataOffchain.getProducingAssetsOffchain);
  app.configure(retrieveDataOffchain.getProducingAssetOffchain);

  app.configure(retrieveDataOffchain.getCo2Offchain);
  app.configure(retrieveDataOffchain.getTranportCertificatesOffchain);

  app.configure(retrieveDataOffchain.updateCacheData);

  //DB Endpoints
  //app.configure(assetConsumingController.create);
  app.configure(testController.createTest);
  app.configure(testController.getTests);
  app.configure(testController.myTest);

  //EndPoint emission CO2 from RTE
  app.configure(eCO2mix.emissionCO2History);

  //EndPoint pdf parser
  app.configure(parserPdf.uploadsPdfParser);

  //EndPoint for update smart contract
  app.configure(updateSmartContract.retrieveCertificateWithoutHash);
  app.configure(updateSmartContract.retrieveCertificate);
  app.configure(updateSmartContract.updateCertificateLogic);
  //app.configure(updateSmartContract.test);
  app.configure(updateSmartContract.addressCertificate);

};
