const express = require('express')
const bodyParser = require("body-parser");
const upload = require('express-fileupload');
const routes = require("./routes/routes.js");
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const logger = require('winston');
const fetch = require('node-fetch');
const cron = require('node-cron');
const moment = require('moment');
const exec = require('child_process').exec;
const path = require('path')

const app = express()

global.Headers = fetch.Headers;

app.use(cors());
app.use(helmet());
app.use(compress());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//
app.use('/static', express.static(__dirname + '/uploads'));

app.use(upload());

routes(app);

const getAssetsProducingLength = async () => {

  let url = process.env.API_URL + '/producing-assets-list-length?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async assetProducingListLength => {
      return assetProducingListLength;
    })
    .catch(error => {
      console.log('An error occured during getAssetsProducingLength : ', error);
      return false;
    });
}

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

const monitorParity = async () =>  {
	//check if the blockchain api respond
	console.log('Start checking Parity');
	assetProducingNb = await getAssetsProducingLength();
	console.log('assetProducingNb', assetProducingNb);
	if(!(assetProducingNb && assetProducingNb > 0)) {
		//the api is not responding correctly,
		console.log('Parity is not responding correctly, restarting...');
		var killAndRestartParity = exec('sh /home/weechain/www/api/kill_restart_parity.sh',
        (error, stdout, stderr) => {
            console.log(`${stdout}`);
            console.log(`${stderr}`);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
	} else {
		//Parity is up
		console.log('Parity is ok')
	}
}


//Monitoring Parity job
cron.schedule('*/2 * * * *', async function(){
  //console.log('monitoring parity (every 2min)');
	monitorParity();

});

//Updating cache data job
cron.schedule('*/5 * * * *', async function(){

  console.log('updateCo2 for cache (every 20min)');
  //updateCo2Data();
  let urlUpdateCo2 = process.env.API_URL + '/update-co2';

  await fetch(urlUpdateCo2, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  console.log('updateCo2 finished');
  /*
  console.log('updateCacheData for cache (every 20min)');
  //updateCacheData();
  let urlUpdateCacheData = process.env.API_URL + '/update-cache';
  await fetch(urlUpdateCacheData, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  console.log('updateCacheData finished');
  */

});


//Updating db data daily

cron.schedule('*/2 * * * *', async function(){

  /*console.log('Daily database update...');
  let urlUpdateDb = process.env.API_URL + '/update-db?save=false';

  await fetch(urlUpdateDb, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  console.log('update DB daily finished');
  */
});

/*
cron.schedule('30 10 * * *', async function(){
  console.log('Checking balance account...');
  const sender = '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  //Here, we have the coo owner and the smart meter addresses of the production assets

  const accountsAddresses = [
    "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", //coo owner
    "0x004A064a9AAf2fCD9687D82Fc5a1116e9325000A", //Eget
    "0x00c3B44DBeA6f103dd30D7e90D59787175FeA185", //Fontanelles
    "0x00CB17ea4DfA8578582C00A79Dd92d6111c28c54"  //Marèges
  ];

  for (let address of accountsAddresses) {
    let urlCheckBalance = process.env.API_URL + '/accounts/'+address+'/balance?_sender='+sender;
    let accountBalance = await fetch(urlCheckBalance, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(handleErrors).then(res => res.json()) .then(data => { return data })
    .catch(error => { console.log(error); });

    console.log(address+" has "+accountBalance.balance+" ethers")

    if(accountBalance.balance < 20) {
      console.log(address+" needs more ether...")
      const dataBalance = {
        address: address,
        email: "test@test.com",
        firstname: "",
        lastname: "",
        organisation: "",
        timestamp: (Math.round(new Date().getTime() / 1000))
      };

      const jsonBalance = JSON.stringify(dataBalance);

      await fetch('http://185.183.159.109:8888/api/getMoney', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: jsonBalance
      })
      .then(handleErrors).then(res => {console.log(res);res.json()}) .then(data => { console.log(data);console.log(address+" received 20 ethers");return data })
      .catch(error => { console.log(error); });
    }
  }
});
*/

//Matching job
cron.schedule('0 10 * * *', async function(){
	console.log('Running matching task every day at 11AM Paris time');


  let matchingDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
  //matchingDate = '2018-10-27';
  /*
  const data = {
    nbRetry: 0,
    date: matchingDate,
    generateCertificates: true
  };

  const json = JSON.stringify(data);

  let urlMatching = process.env.API_URL + '/certificates-management/matching';
  await fetch(urlMatching, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  */
  console.log('Matching task finished for day '+matchingDate);

});

let mongoDbProdPath = 'mongodb://teoDbAdmin:teoDb1234ENGIE@ds253343-a0.mlab.com:53343,ds253343-a1.mlab.com:53343/weechain_prod?replicaSet=rs-ds253343';
let mongoDbTestPath = 'mongodb://teoDbTestAdmin:teoTestDb1234ENGIE@ds157064.mlab.com:57064/weechain_test'


process.env.PORT = process.env.PORT || 3030;
process.env.BUILD_FOLDER  = process.env.BUILD_FOLDER || 'build';
process.env.DB_PATH = process.env.DB_PATH || 'mongodb://teoDbAdmin:teoDb1234ENGIE@ds253343-a0.mlab.com:53343,ds253343-a1.mlab.com:53343/weechain_prod?replicaSet=rs-ds253343';
//process.env.DB_PATH = process.env.DB === 'prod' ? mongoDbProdPath : mongoDbTestPath;

//console.log(process.env);
//const DB_PATH = (DB !== 'prod') ?  mongoDbTestPath : mongoDbProdPath;
//rocess.env.DB_PATH = (DB !== 'prod') ?  mongoDbTestPath : mongoDbProdPath;


var server = app.listen(process.env.PORT, function () {
    console.log("TEO app backend running on port ", server.address().port);
    console.log("BUILD_FOLDER = "+process.env.BUILD_FOLDER)
    console.log("DB = ", process.env.DB_PATH === mongoDbProdPath ? 'weechain_prod' : 'weechain_test')
});
