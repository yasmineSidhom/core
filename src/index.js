/* eslint-disable no-console */
let cron = require('node-cron');
let fetch = require('node-fetch');
let base64 = require('base-64');
let moment = require('moment');
let exec = require('child_process').exec;
const fs = require('fs');
const path = require('path');

const retrieveDataOffchain= require('./services/offchain/retrieveDataOffchain');

global.Headers = fetch.Headers;

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

const monitorParity = async () =>  {
	//check if the blockchain api respond
	console.log('Start checking Parity');
	assetProducingNb = await getAssetsProducingLength();
	console.log('assetProducingNb', assetProducingNb);
	if(!(assetProducingNb && assetProducingNb > 0)) {
		//the api is not responding correctly,
		console.log('Parity is not responding correctly, restarting...');
		var killAndRestartParity = exec('sh /home/weechain/www/api/kill_restart_parity.sh',
        (error, stdout, stderr) => {
            console.log(`${stdout}`);
            console.log(`${stderr}`);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });


	} else {
		//Parity is up
		console.log('Parity is ok')
	}

}

const fetchSmireData = async (date) => {
  try {
    let day = moment().subtract(1, 'days').format('YYYY-MM-DD'); //By default, yesterday
    let end_date = moment().format('YYYY-MM-DD'); //By default, today
    if(date) {
      day = date;
      end_date = moment(date, "YYYY-MM-DD").add(1, 'days').format('YYYY-MM-DD');
    }
    console.log('---------- trying fetchSmireData...')
    console.log('---------- fetching data for the '+day)


    let username = 'system.weechain';
    let password = '3OetAPO8D03XIYs2WRuj';
    let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site=plt_mareges,eget,fontanelles,lida_rte&start='+day+'&end='+end_date+'&specific_timezone=Europe%2FParis';

    const response = await fetch(url, {method:'GET'});

    console.log('---------- has retrieved fetchSmireData...')

    const json = await response.json();

    if(json.error) {
      console.log('fetchSmireData error :', json.error+' : '+json.description);
      return { status: 'error', error: json.error+' : '+json.description, res: null }
    }

    console.log('fetchSmireData finished');
    return { status: 'ok', error: null, res: json };
  }
  catch(error) {
    console.log('fetchSmireData error :', error);
    return { status: 'error', error, res: null }
  }
}

const updateCo2Data = async () => {

  const url_cons_def = 'https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=eco2mix-national-cons-def&q=taux_co2>0&rows=-1&facet=nature&facet=date_heure&refine.date_heure=2018';
  const url_national_tr = 'https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=eco2mix-national-tr&q=taux_co2 > 0&rows=-1&sort=-date_heure&refine.date_heure=2018';

  return await fetch(url_national_tr)
    //.then(handleErrors)
    .then(res => res.json())
    .then(async res => {
      //console.log('yooo co2res', res);
      const data = res.records;

      const resRte = await fetch(url_cons_def).then(resRte => resRte.json()).then(resRte => { return resRte });
      //console.log('yooo resRte', resRte);

      const dataRte = resRte.records;
      const fullData = data.concat(dataRte);

      const resWithCo2 = fullData.filter( record => {
          return record.fields.taux_co2
      });

      const co2Dates = resWithCo2.map( record => {
          return record.fields.date
      });
      //console.log('yooo co2Dates', co2Dates);

      const co2DatesUnique = co2Dates.filter((date, pos) => {
        return co2Dates.indexOf(date) === pos && date !== false;
      });

      co2DatesUnique.sort();

      //console.log('yooo co2DatesUnique', co2DatesUnique);

      const tab = co2DatesUnique.map(date => {
        let sumCo2 = 0;
        const resWithCo2AndDate = resWithCo2.filter( record => {
            return record.fields.date === date
        });

        resWithCo2AndDate.map( record => {
            sumCo2 += record.fields.taux_co2;
            return true;
        })

        return {
          date: date,
          CO2: resWithCo2AndDate.length ? sumCo2 / resWithCo2AndDate.length : 0
        }
      })

      //console.log('yooo resWithCo2', resWithCo2);
      //console.log('yooo co2 tab', JSON.stringify(tab));

      let isCo2FormatOk = true;
      if(tab.length < 2) isCo2FormatOk = false;

      tab.map(data => {
        if(!moment( data.date, 'YYYY-MM-DD').isValid()) isCo2FormatOk = false;
        if(isNaN(data.CO2)) isCo2FormatOk = false;

      })

      if(isCo2FormatOk) {
        let co2DataUpdated = JSON.stringify(tab, null, 4);
        fs.writeFileSync(path.join(__dirname, './db/json') + '/co2.json', co2DataUpdated);
        console.log("co2.json file has been correctly updated");
      } else {

        console.log("Error on tab : ")
        console.log(tab)
        throw "Error : A problem has occured with the data retrieved for CO2"
      }
    })
    .catch(error => {
      console.log(error)
    });
}

const updateCacheData = async () => {
  console.log("updateCacheData started...")

  let url = process.env.API_URL + '/updateCacheData';
  const resultCacheData = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in updateCacheData : ");
      console.log(error);
    });

  console.log("updateCacheData finished")
}


const createCertificate = async (assetId, powerInW, productionDate) => {

  const data={
    _assetId:parseInt(assetId, 10),
    _powerInW: parseInt(powerInW, 10),
    _productionDate: parseInt(productionDate, 10),
    _sender: '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747'
  };
  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/createCertificateForAssetOwner", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      console.log('New certificate created for asset id = '+assetId+' powerInW = '+powerInW+' _certificateId = '+data.logs[0]["result"]._certificateId);
      return data;
    })
    .catch(error => error);
}


const setCertificateTxHash = async (certificateId, txHash) => {

  console.log('setCertificateTxHash Start');

  const data = {
    _certificateId: certificateId,
    _txHash: txHash,
    _sender: '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747'

  };
  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/setCertificateTxHash", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async result => {
      console.log('setCertificateTxHash success');
      //console.log(result);
    })
    .catch(error => {
      console.log('setCertificateTxHash error');
      console.log(error);
    });
}

const transferCertificate = async (certificateId, newOwner) => {

  console.log('transferCertificate Start');

  const data = {
    _certificateId: certificateId,
    _newOwner: newOwner,
    _sender: '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747'

  };
  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/changeCertificateOwner", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async result => {
      console.log('transferCertificate success');
      //console.log(result);
    })
    .catch(error => {
      console.log('transferCertificate error');
      console.log(error);
    });
}

const getAssetsProducingLength = async (sender) => {

  const data = { _sender: sender ? sender : '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747' };
  const json = JSON.stringify(data);

  let url = process.env.API_URL + '/getAssetListLength/producer';
  //let url = process.env.API_URL + '/getAssetListLength/producer';

  return await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async assetProducingListLength => {
      console.log('assetProducingListLength = '+ assetProducingListLength);
      return assetProducingListLength;
    })
    .catch(error => {
      console.log('An error occured during getAssetsProducingLength : ', error);
      return false;
    });
}

const getAllCertificates = async () => {

	const data = {
	"_owner":null,
	"_sender":"0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747"
	}
  	const json = JSON.stringify(data);

	return await fetch(process.env.API_URL + "/getAllCertificates", {
		method: 'POST',
		headers: {
	  	'Content-Type': 'application/json; charset=utf-8'
		},
		body: json
	})
	.then(handleErrors)
	.then(async res => res.json())
	.then(async certificates => {
	  //console.log('certificatesLength = '+ certificates.length);
	  //console.log(certificates);
	  return certificates;
	})
	.catch(error => {
	  console.log(error);
	});
}

const getAssetProducing = async (assetProducingId) =>  {

  const sender = '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  const data = {
    _sender: sender,
    _assetId: assetProducingId
  };
  const json = JSON.stringify(data);
  const assetProducingGeneralData = await getAssetProducingGeneralData(sender, assetProducingId);
  const assetProducingProperties = await getAssetProducingProperties(sender, assetProducingId);
  const assetLocation = await getAssetProducingLocation(sender, assetProducingId);

  const asset = {};
  asset.id= assetProducingId;
  asset.smartMeter= assetProducingGeneralData[0];
  asset.owner= assetProducingGeneralData[1];
  asset.operationalSince=assetProducingGeneralData[2];
  asset.lastSmartMeterReadWh= assetProducingGeneralData[3];
  asset.active = assetProducingGeneralData[4];
  asset.lastSmartMeterReadFileHash = assetProducingGeneralData[5];

  asset.type=assetProducingProperties[0];
  asset.capacity=assetProducingProperties[1];
  asset.certificatesCreatedForWh=assetProducingProperties[2];
  asset.lastSmartMeterCO2OffsetRead=assetProducingProperties[3];
  asset.cO2UsedForCertificate=assetProducingProperties[4];
  asset.registryCompliance=assetProducingProperties[5];
  asset.otherGreenAttributes=assetProducingProperties[6];
  asset.typeOfPublicSupport=assetProducingProperties[7];

  asset.country= assetLocation[0];
  asset.region= assetLocation[1];
  asset.zip= assetLocation[2];
  asset.city= assetLocation[3];
  asset.street= assetLocation[4];
  asset.houseNumber= assetLocation[5];
  asset.gpsLatitude= assetLocation[6];
  asset.gpsLongitude= assetLocation[7];

  return(asset)
}

const getAssetProducingGeneralData = async (sender, assetId) =>  {

  const data = {
    _sender: sender ? sender : '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747',
    _assetId: assetId
  };
  const json = JSON.stringify(data);
  return await fetch(process.env.API_URL + "/getAsset/producer", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log('getAssetProducingGeneralData');
      return data;
    })
    .catch(error => {
      console.log(error);
    });
}

const getAssetProducingProperties = async (sender, assetId) =>  {

  const data = {
    _sender: sender ? sender : '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747',
    _assetId: assetId
  };
  const json = JSON.stringify(data);
  return await fetch(process.env.API_URL + "/getAssetProperties/producer", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log('getAssetProducingProperties');
      return data;
    })
    .catch(error => {
      console.log(error);
    });
}

const getAssetProducingLocation = async (sender, assetId) => {

  const data = {
    _sender: sender ? sender : '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747',
    _assetId: assetId
  };
  const json = JSON.stringify(data);
  return await fetch(process.env.API_URL + "/getAssetLocation/producer", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log('getAssetProducingLocation');
      return data;
    })
    .catch(error => {
      console.log(error);
    });
}

const saveProducingSmartMeterRead = async (
  assetProducingId,
  newMeterRead,
  smartMeterDown,
  lastSmartMeterReadFileHash,
  cO2OffsetMeterRead,
  cO2OffsetServiceDown,
  sender
  ) => {
  var data={
    _assetId:assetProducingId,
    _newMeterRead:newMeterRead,
    _smartMeterDown:smartMeterDown,
    _lastSmartMeterReadFileHash:lastSmartMeterReadFileHash,
    _CO2OffsetMeterRead:cO2OffsetMeterRead,
    _CO2OffsetServiceDown:cO2OffsetServiceDown,
    _sender:sender,
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/assetSaveSmartMeterRead/producer", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => {
      return error;
    });
}

const weechainMatching = async (nbRetry, date) => {
  let retry = nbRetry;
  const frequency = 30000;  //every 30s
  const maxRetry = 3;       //retry 5 times (don't retry during the last hour of the day)
  if(!retry) {
    retry = maxRetry;
  } else {
    retry -= 1;
  }

  let jobDay = moment().subtract(1, 'days').format('YYYY-MM-DD'); //By default, yesterday
  if(date) {
    jobDay = date;
  }

  console.log("weechainMatching - Starting... ("+(maxRetry-retry+1)+"/"+(maxRetry)+")");

  let smireData = await fetchSmireData(date);
  console.log("weechainMatching - smireData : ",  smireData);

  if( smireData.status === 'ok') {
    console.log("weechainMatching - Smire is Up");
  } else {
    console.log('weechainMatching - An error occured during fetchSmireData : ', smireData.error)
    sendSlackBotMessage("☝️ "+ jobDay+" weechainMatching - An error occured during fetchSmireData : "+smireData.error);
  }

  if( smireData.res && smireData.res.data.length > 0) {
    console.log("weechainMatching - Smire.data is not an empty array");
  } else {
    console.log('weechainMatching - An error occured during fetchSmireData with  result.data : ', smireData.res, smireData.res.data.length);
    console.log('weechainMatching - smireData.res test : ', smireData.res ? 'smire.data is true' : 'smire.data is false');
    sendSlackBotMessage("☝️ "+ jobDay+" weechainMatching - An error occured during fetchSmireData (call ok but wrong data)");
  }

  const nbOfProducingAssets = await getAssetsProducingLength();
  if( !nbOfProducingAssets ) {
    console.log('weechainMatching - An error occured during getAssetsProducingLength : ', nbOfProducingAssets)
    sendSlackBotMessage("☝️ "+ jobDay+" weechainMatching - An error occured during getAssetsProducingLength");
  } else {
    console.log("weechainMatching - Parity is Up");
  }

  let reqSiteTimeSeries = [];

  if( smireData.status === 'ok' && smireData.res && smireData.res.data && smireData.res.data.length > 0 && nbOfProducingAssets ) {
    console.log("weechainMatching - all the services are ok, launching the matching");
    reqSiteTimeSeries = smireData.res;
    let totalMatchingProductionFontanelles  = 0;
    let totalMatchingProductionEget = 0;
    let totalMatchingProductionMareges = 0;
    let totalResidualConsumption = 0;
    for (let i = 0; i < reqSiteTimeSeries.data.length; i++) {
        //Let's build the matching data

        //Priority 1 : Production Wind park W (kwh) Fontanelles
        //Priority 2 : Production Hydro Site 1 H1 (kWh) Eget
        //Priority 3 : Production Hydro Site 2 H2 (kwh) Marèges

        console.log('************************* New day : ', reqSiteTimeSeries.index[i].substr(0, 10) );
        //console.log('************************* New day moment unix : ', moment(reqSiteTimeSeries.index[i]).format('X') );

        let matchingProductionMareges = 0;
        let realProductionMareges = reqSiteTimeSeries.data[i][0]*1000;// *1000 : kWh -> Wh for the blockchain
        let matchingProductionEget = 0;
        let realProductionEget = reqSiteTimeSeries.data[i][1]*1000;
        let matchingProductionFontanelles = 0;
        let realProductionFontanelles = reqSiteTimeSeries.data[i][2]*1000;
        let residualConsumption = 0;
        let realConsumption = reqSiteTimeSeries.data[i][3]*1000*57/100;// We only take 57% of lida's consumption
        console.log('--------------------------------------------------')
        console.log(reqSiteTimeSeries.index[i]);
        //const productionDate = moment(reqSiteTimeSeries.index[i]).format('X');
        //console.log(productionDate)

        if(
          realProductionMareges === 0 &&
          realProductionEget === 0 &&
          realProductionFontanelles === 0 &&
          realConsumption === 0
        ) {
          console.log("weechainMatching - Error : Data are all 0");
          return false;
        }

        if(
          isNaN(realProductionMareges) ||
          isNaN(realProductionEget) ||
          isNaN(realProductionFontanelles) ||
          isNaN(realConsumption)
        ) {
          console.log("weechainMatching - Error : Some data are not number");
          sendSlackBotMessage('weechainMatching - Error : Some data are not number ');
          return false;
        }

        if( realProductionFontanelles < realConsumption) {
          residualConsumption = realConsumption - realProductionFontanelles;
          matchingProductionFontanelles = realProductionFontanelles;

          if( realProductionEget < residualConsumption) {
            residualConsumption = residualConsumption - realProductionEget;
            matchingProductionEget = realProductionEget;

            if( realProductionMareges < residualConsumption) {
              residualConsumption = residualConsumption - realProductionMareges;
              matchingProductionMareges = realProductionMareges;
              console.log('***** Consumption not fully absorbed by renewable assets', residualConsumption, realConsumption, residualConsumption*100/realConsumption);
            } else {
              matchingProductionMareges = residualConsumption;
              residualConsumption = 0;
              console.log('Consumption fully absorbed by Wind from fontanelles, Hydro from Eget and Hydro from Mareges');
            }

          } else {
            matchingProductionEget = residualConsumption;
            residualConsumption = 0;
            console.log('Consumption fully absorbed by Wind from fontanelles and Hydro from Eget');
          }

        } else {
          matchingProductionFontanelles = realConsumption;
          residualConsumption = 0;
          console.log('Consumption fully absorbed by Wind from fontanelles');
        }

        console.log('Real consumption', realConsumption);
        console.log('Residual consumption', residualConsumption);
        console.log('Real fontanelles production', realProductionFontanelles);
        console.log('Matching fontanelles production', matchingProductionFontanelles);
        console.log('Real Eget production', realProductionEget);
        console.log('Matching Eget production', matchingProductionEget);
        console.log('Real Mareges production', realProductionMareges);
        console.log('Matching Mareges production', matchingProductionMareges);


        totalMatchingProductionFontanelles += matchingProductionFontanelles;
        totalMatchingProductionEget += matchingProductionEget;
        totalMatchingProductionMareges += matchingProductionMareges;
        totalResidualConsumption += residualConsumption;

        let messageText = '😎 weechainMatching is ok for day '+jobDay+' ✅\n';
        messageText +=    'Real consumption = '+realConsumption+'\n';
        messageText +=    'Residual consumption = '+residualConsumption+'\n';
        messageText +=    'Fontanelles production = '+realProductionFontanelles+'\n';
        messageText +=    'Matching Fontanelles production = '+matchingProductionFontanelles+'\n';
        messageText +=    'Eget production = '+realProductionEget+'\n';
        messageText +=    'Matching Eget production = '+matchingProductionEget+'\n';
        messageText +=    'Mareges production = '+realProductionMareges+'\n';
        messageText +=    'Matching Mareges production = '+matchingProductionMareges+'\n';

        sendSlackBotMessage(messageText);

        console.log('######### weechainMatching finished');
    }
  } else {
    if(retry > 1) {
      console.log("weechainMatching - Attempt "+(maxRetry-retry+1)+"/"+(maxRetry)+" failed, one of the service is KO, retrying in "+frequency+"ms... retry = "+retry);
      sendSlackBotMessage("❌ "+ jobDay+" weechainMatching - Attempt "+(maxRetry-retry+1)+"/"+(maxRetry)+" failed, one of the service is KO, retrying in "+frequency+"ms... ");
      setTimeout(async function() {
        weechainMatching(retry, date)
      }, frequency);
    } else {
      console.log("weechainMatching - Fail after "+maxRetry+" attempts (every  "+frequency+"ms since the initial job launch for the day)");
      sendSlackBotMessage("❌❌❌ "+ jobDay+" weechainMatching - Fail after "+maxRetry+" attempts (every  "+frequency+"ms since the initial job launch for the day)");
    }

  }
}

const getConsumingSitesOffchain = async () => {

  let url = process.env.API_URL + '/getConsumingSitesOffchain';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      //console.log(result)
      return result.consumingSites
    })
    .catch(error => {
      console.log(error);
    });
}

const getProducingAssetsOffchain = async () => {

  let url = process.env.API_URL + '/getProducingAssetsOffchain';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      //console.log(result)
      return result.producingAssets
    })
    .catch(error => {
      console.log(error);
    });
}

const getAllConsumingSitesConsumptionOffchain = async (start, end) => {

  let url = process.env.API_URL + '/getAllConsumingSitesConsumptionOffchain';

  const data = {
    start,
    end
  };

  const json = JSON.stringify(data);
  return await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}

const getAllProducingAssestProductionOffchain = async (start, end) => {

  let url = process.env.API_URL + '/getAllProducingAssestProductionOffchain';

  const data = {
    start,
    end
  };

  const json = JSON.stringify(data);
  return await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}

const getConsumingSiteCalculatedDataOffchain = async (asset, start, end) => {

  let url = process.env.API_URL + '/getConsumingSiteCalculatedDataOffchain';

  const data = {
    asset,
    start,
    end
  };

  const json = JSON.stringify(data);

  return await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      return result.data
    })
    .catch(error => {
      console.log(error);
    });
}



const getConsumingSiteTimeSeriesOffchain = async (consumingAsset, start, end) => {

  let url = process.env.API_URL + '/getConsumingSiteTimeSeriesOffchain';

  const data = {
    "asset": consumingAsset,
    start,
    end
  };

  const json = JSON.stringify(data);
  return await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}


const weechainMatchingV2 = async (nbRetry, date, generateCertificates) => {

  //Get all the consuming assets
  console.log('weechainMatchingV2 starting...');
  const consumingSitesNotFiltered = await getConsumingSitesOffchain();
  const producingAssetsNotFiltered = await getProducingAssetsOffchain();


  //Use only the real assets, remove the fake one
  const consumingSites = consumingSitesNotFiltered.filter(c => {
    return c.isFake !== true
  });
  const producingAssets = producingAssetsNotFiltered.filter(p => {
    return p.isFake !== true
  });

  //If no date in arguments, use the previous day as start and current day as end
  const start =  date ? date : moment().subtract(1, 'days').format('YYYY-MM-DD')
  const end   =  date ? moment(date, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')
  const productionDate = moment(start, 'YYYY-MM-DD').add(2, 'hours').format('X');
  const productionDateTxt = moment.unix(productionDate).format('dddd, MMMM Do, YYYY h:mm:ss A')

  console.log("weechainMatchingV2 - getting consumption for date : ", productionDateTxt, start, end)

  const sitesConsumption = await getAllConsumingSitesConsumptionOffchain(start, end);
  const assetsProduction = await getAllProducingAssestProductionOffchain(start, end);

  let consSitesWithConsumption = consumingSites.map(site => {
    //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
    //reqSiteTimeSeries2
    let siteIndex = null;

    let consumptionPercentage = site.consumptionPercentage;
    if(!consumptionPercentage) {
      consumptionPercentage = 1;
    }

    sitesConsumption.data.ids.map((consSiteId, idx) => {
      if(consSiteId === site.id) {
        siteIndex = idx;
      }
    })

    site.consumption = sitesConsumption.data.data[0][siteIndex]*consumptionPercentage*1000;//Convert to Wh in the blockchain
    if(isNaN(site.consumption)) {
      //TODO manage this case
      site.consumption = 0
    }

    site.energyMatched = 0;

    return site;
  })

  let consSitesWithConsumptionOriginal = JSON.parse(JSON.stringify(consumingSites))
  let producingAssetsWithProduction = producingAssets.map(prodAsset => {
    //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
    //reqSiteTimeSeries2
    let prodAssetIndex = null;

    assetsProduction.data.ids.map((prodSiteId, idx) => {
      if(prodSiteId === prodAsset.id) {
        prodAssetIndex = idx;
      }
    })

    prodAsset.production = assetsProduction.data.data[0][prodAssetIndex]*1000;//Convert to Wh in the blockchain
    if(isNaN(prodAsset.production)) {
      //TODO manage this case
      prodAsset.production = 0
    }
    prodAsset.energyMatched = 0;

    /*
    if(prodAsset.id === 0) {
      prodAsset.production = 1000000;
    } else if(prodAsset.id === 1) {
      prodAsset.production = 0;
    } else if (prodAsset.id === 2) {
      prodAsset.production = 0;
    } else {
      prodAsset.production = 0;
    }
    */

    //console.log("⚠️ ⚠️ ⚠️ Using a fake prodAsset.production for "+prodAsset.name+" : "+prodAsset.production+" kWh")

    return prodAsset;
  })

  const matchingResult = retrieveDataOffchain.matching(consSitesWithConsumption, producingAssetsWithProduction, 0, 0);
  /*console.log(" ");
  console.log(" ");
  console.log(" ");
  console.log("matchingResult");
  console.log(matchingResult);*/

  //console.log('weechainMatchingV2 matching result '+assetIndex+'/'+consumingSites.length - 1);
  //if(assetIndex >= (consumingSites.length - 1)) {
    //console.log('weechainMatchingV2 last matching');
  //}
  console.log('weechainMatchingV2 matching result obtained ⚠️⚠️⚠️⚠️⚠️⚠️');

  let messageText = '\n';
  messageText += 'weechainMatchingV2 results for day '+productionDateTxt+'\n';
  messageText += '(no certificate generated)\n';

  matchingResult.consumingSites.map(async consumingSite => {


    messageText += '\n';
    console.log("    "+consumingSite.name+" matching ");
    messageText += "    *"+consumingSite.name+" matching* \n";

    console.log("        Consumption = "+consumingSite.consumption);
    messageText += "        Consumption = "+  Math.round(consumingSite.consumption)+" kWh \n";

    console.log("        Energy matched = "+consumingSite.energyMatched);
    messageText += "        Energy matched = "+Math.round(consumingSite.energyMatched)+" kWh "+"("+( consumingSite.consumption ? parseInt(100*consumingSite.energyMatched/consumingSite.consumption) : 0)+"%) \n";

    console.log("        Energy mix consumption = "+(consumingSite.consumption-consumingSite.energyMatched));
    messageText += "        Energy mix consumption = "+Math.round(consumingSite.consumption-consumingSite.energyMatched)+" kWh \n";

    consumingSite.producingAssets.map(pA => {
      const currentProdAssetWithConsumption = producingAssetsWithProduction.find(pAWC => {
        return pAWC.id === pA.id;
      })

      const initialConsumptionSite = consSitesWithConsumptionOriginal.find(cS => {
        return consumingSite.id === cS.id;
      })

      const initialProdSite = initialConsumptionSite.producingAssets.find(pS => {
        return pS.id === pA.id;
      })

      //console.log('******  initialProdSite ', initialProdSite)
      console.log("            "+currentProdAssetWithConsumption.displayedName+" production = "+currentProdAssetWithConsumption.production);
      messageText += "            "+currentProdAssetWithConsumption.displayedName+" production = "+Math.round(currentProdAssetWithConsumption.production)+" kWh \n";

      console.log("            "+currentProdAssetWithConsumption.displayedName+" energyMatched = "+pA.energyMatched);
      messageText += "            "+currentProdAssetWithConsumption.displayedName+" energyMatched = "+Math.round(pA.energyMatched)+" kWh ("+initialProdSite.percentage+'% minMatched = '+Math.round((initialProdSite.percentage/100)*currentProdAssetWithConsumption.production)  +" kWh) \n";
    })
  })


  sendSlackBotMessage(messageText);

  console.log('weechainMatchingV2 -  Entering the certificate generation phase... ');


  console.log('Update the smart meter of each producing asset :')

  //Update the smart meter of each producing asset :
  for (let pA of matchingResult.producingAssets) {

    const currentProducingAsset = await getAssetProducing(pA.id);
    console.log("weechainMatchingV2 - Asset retrieved : "+pA.id+' '+pA.name);
    //pA : name, displayedName, production, energyMatched, productionAvailable
    //currentProducingAsset : smartMeter, owner, lastSmartMeterReadWh, lastSmartMeterReadFileHash,
    //certificatesCreatedForWh, cO2UsedForCertificate


    const prodAssetLastMeterRead = parseInt(currentProducingAsset.lastSmartMeterReadWh, 10);
    const prodAssetTotalProduction = parseInt(pA.production*1000, 10);//kWh to Wh

    const prodAssetLastMeterReadFileHash = Date.now().toString()+currentProducingAsset.name;
    const prodAssetNewMeterRead = prodAssetLastMeterRead + prodAssetTotalProduction;


    if(generateCertificates) {
      console.log(" ⚠️  saveProducingSmartMeterRead will launch");
      const saveProducingSmartMeterReadResult = await saveProducingSmartMeterRead (
        currentProducingAsset.id,         //assetProducingId
        prodAssetNewMeterRead,            //newMeterRead
        false,                            //smartMeterDown
        prodAssetLastMeterReadFileHash,   //lastSmartMeterReadFileHash
        0,                                //cO2OffsetMeterRead
        false,                            //cO2OffsetServiceDown
        currentProducingAsset.smartMeter  //sender, must be the address of the asset's smart meter as '0x00c3B44DBeA6f103dd30D7e90D59787175FeA185'
      );
    }

    //For each consuming site, create the certificates related to the energy matched with THIS production asset
    for (let cA of matchingResult.consumingSites) {

       //let result = await getData(num);
       //console.log(result);

       const currentProdAssetCertif = cA.producingAssets.find( pAsset => {
         return pAsset.id === pA.id
       })

       if(currentProdAssetCertif) {

         if(currentProdAssetCertif.energyMatched > 0) {

           let matchingProdAssetCertifData = {
             assetName: pA.name,
             assetId: pA.id,
             production: Math.round(currentProdAssetCertif.energyMatched)+"/"+Math.round(pA.production),
             productionDate
           }

           if(generateCertificates) {

             console.log(" ⚠️  createCertificate will launch");
             matchingProdAssetCertifData = await createCertificate(pA.id, currentProdAssetCertif.energyMatched, productionDate);
           }

           console.log("weechainMatchingV2 - createCertificate params ⚠️⚠️⚠️⚠️⚠️")
           console.log(matchingProdAssetCertifData)

           let prodAssetCertificateId = matchingProdAssetCertifData.logs ? matchingProdAssetCertifData.logs[0]["result"]._certificateId : 'unknown certifId in test mode';
           /*console.log("weechainMatchingV2 - setCertificateTxHash params")
           console.log({
             certificateId: prodAssetCertificateId,
             certificateTxHash: matchingProdAssetCertifData.logs ? matchingProdAssetCertifData.logs[0].transactionHash : 'unknown txHash in test mode'
           })*/

           if(generateCertificates) {
             console.log(" ⚠️  setCertificateTxHash will launch");
             await setCertificateTxHash(prodAssetCertificateId, matchingProdAssetCertifData.logs[0].transactionHash);
           }

           console.log("weechainMatchingV2 - matchingProduction transferCertificate start for prod asset "+pA.name+" consumption of "+cA.name);
           //newOwner is the client : the consuming asset
           //const newOwner;
           // Airproducts, La Boulangère ou Barilla blockchain address
           //const newOwner = '0x0051CcCBCFF51788dDa9614b12dcea0D57208C03'; // <-- this is Airproducts Address Account
           const newOwner = cA.owner;

            console.log("weechainMatchingV2 - transferCertificate params")
            console.log({
              certificateId: prodAssetCertificateId,
              certificateOwner: newOwner
            })
           if(generateCertificates) {
             console.log(" ⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️  transferCertificate will launch CertificateId = "+prodAssetCertificateId+ " new Owner = "+newOwner);
             console.log(" ⚠️  Air Products = 0x0051CcCBCFF51788dDa9614b12dcea0D57208C03 ");
             console.log(" ⚠️  La Boulangère = 0x00d0F0e2AF12fec0C16c6D595a9554b99B6dE045 ");

             await transferCertificate(prodAssetCertificateId, newOwner)
           }

           //console.log("weechainMatchingV2 - matchingProduction transferCertificate finished for prod asset "+pA.name+" consumption of "+cA.name);

         }
       }

    }


    //create the certificate related to the energy NOT matched with THIS production asset

    const residualProduction = pA.production - pA.energyMatched;

    if(residualProduction > 0) {
      //console.log("weechainMatchingV2 - residualProduction createCertificate start for asset "+pA.name);
      let residualProdAssetCertifData = {
        prodAssetName: pA.name,
        prodAssetId: pA.id,
        residualProduction: Math.round(residualProduction)+"/"+Math.round(pA.production),
        productionDate
      }

      if(generateCertificates) {
        console.log(" ⚠️  createCertificate will launch (residual)");
        residualProdAssetCertifData = await createCertificate(pA.id, residualProduction, productionDate);
      }

      console.log("weechainMatchingV2 - createCertificate params ⚠️⚠️⚠️⚠️⚠️");

      console.log(residualProdAssetCertifData);

      const prodAssetResCertificateId = residualProdAssetCertifData.logs ? residualProdAssetCertifData.logs[0]["result"]._certificateId : 'unknown certifId in test mode';
      const prodAssetResCertificateTxHash = residualProdAssetCertifData.logs ? residualProdAssetCertifData.logs[0].transactionHash : 'unknown TxHash in test mode';

      /*console.log("weechainMatchingV2 - residualProduction setCertificateTxHash params ");
      console.log({
        prodAssetResCertificateId,
        prodAssetResCertificateTxHash
      })
      */

      if(generateCertificates) {
        console.log(" ⚠️  setCertificateTxHash will launch (residual)");
        await setCertificateTxHash(prodAssetResCertificateId, residualProdAssetCertifData.logs[0].transactionHash);
      }
      console.log("weechainMatchingV2 - residualProduction createCertificate finished for asset "+pA.name);
    }
  }

  console.log('weechainMatchingV2 finished ');
}


const checkGooMaxLimit = async () => {
  //For each asset, check if the yearly contract limit has been reached
  //If yes, set the % of the asset to  0 in the database

  //Get the assets

  const consumingSitesNotFiltered = await getConsumingSitesOffchain();
  const producingAssetsNotFiltered = await getProducingAssetsOffchain();

  const consumingSites = consumingSitesNotFiltered.filter(c => {
    return c.isFake !== true
  });
  const producingAssets = producingAssetsNotFiltered.filter(p => {
    return p.isFake !== true
  });

  const currentYear = moment().format('YYYY');
  const start = currentYear+'-01-01';
  const end = moment().subtract(1, 'days').format('YYYY-MM-DD')

  let messageText = '*Max Goo Limits* \n';
  console.log('⚡️⚡️⚡️⚡️ checkGooMaxLimit results : ');

  const checks = await consumingSites.map(async cs => {
    const csData = await getConsumingSiteCalculatedDataOffchain(cs, start, end);
    const totalEnergyMatchedByAsset = csData.totalEnergyMatchedByAsset;

    messageText +=    '  '+cs.name+' : \n';
    console.log('⚡️⚡️⚡️⚡️  '+cs.name+' : ');

    let assetContractsCheck = cs.maxGooContractByAsset.map( (contract, index) => {

      //For this contract related to the contract.producingAssets, what is the current energy matched ?
      let energyMatched = 0;

      const updatedProducingAssets = contract.producingAssets.map((prodAssetId, indexProducingAssets) => {

        return  totalEnergyMatchedByAsset.find((a,j) => {
          return a.id === prodAssetId
        });
      })

      return {
        maxGoo: contract.maxGoo,
        producingAssets: updatedProducingAssets,
        energyMatchedContract: energyMatched,
        year: contract.year
      }
    })

    return assetContractsCheck.map(contract => {
      let assetsNames = '';
      let energyMatchedContract = 0;

      contract.producingAssets.map((prodAsset, idx) => {
        energyMatchedContract += prodAsset.energyMatched;

        if(idx >= contract.producingAssets.length - 1 ) {
          assetsNames += prodAsset.label;
        } else {
          assetsNames += prodAsset.label+', ';
        }

      })

      energyMatchedContract = parseInt(energyMatchedContract/1000, 10);
      const maxGoo = parseInt(contract.maxGoo/1000000, 10);
      console.log('⚡️⚡️⚡️⚡️     '+assetsNames+' : '+energyMatchedContract+'/'+maxGoo+' MWh');
      messageText +=    '     '+assetsNames+' : '+energyMatchedContract+'/'+maxGoo+' MWh';

      if( energyMatchedContract > maxGoo ) {
        console.log('⚡️⚡️⚡️⚡️     Limit reached for the group '+assetsNames);
        messageText +=    ' ⚠️ Limit reached \n';
      } else {
        messageText +=    '\n';
      }

      return assetsNames
    });
  })

  Promise.all(checks).then((completed) => sendSlackBotMessage(messageText));

}

const sendSlackBotMessage = async (message) => {

  const slackBotData={
    text: message
  };

  const slackBotJson = JSON.stringify(slackBotData);
  //This is a test bot webhook url
  //https://api.slack.com/apps/ACW6RL88H/incoming-webhooks?

  const slackBotRes = await fetch("https://hooks.slack.com/services/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", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: slackBotJson
  })
}

const sendSlackBotMessageTest = async (message) => {

  const slackBotData={
    text: message
  };

  const slackBotJson = JSON.stringify(slackBotData);

  const slackBotRes = await fetch("https://hooks.slack.com/services/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", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: slackBotJson
  })
}

const weechainDailyJob = async (nbRetry, date) => {
  let retry = nbRetry;
  const frequency = 3600000;  //every 3600s = every hour
  const maxRetry = 12;       //retry 12 times (don't retry during the last hour of the day)
  if(!retry) {
    retry = maxRetry;
  } else {
    retry -= 1;
  }

  let jobDay = moment().subtract(1, 'days').format('YYYY-MM-DD'); //By default, yesterday
  if(date) {
    jobDay = date;
  }


  console.log("weechainDailyJob - Starting... ("+(maxRetry-retry+1)+"/"+(maxRetry)+")");

  let smireData = await fetchSmireData(date);
  console.log("weechainDailyJob - smireData : ",  smireData);

  if( smireData.status === 'ok') {
    console.log("weechainDailyJob - Smire is Up");
  } else {
    console.log('weechainDailyJob - An error occured during fetchSmireData : ', smireData.error)
    sendSlackBotMessage("☝️ "+ jobDay+" weechainDailyJob - An error occured during fetchSmireData : "+smireData.error);
  }

  if( smireData.res && smireData.res.data && smireData.res.data.length > 0) {
    console.log("weechainDailyJob - Smire.data is not an empty array");
  } else {
    console.log('weechainDailyJob - An error occured during fetchSmireData with  result.data : ', smireData.res, smireData.res.data.length);
    console.log('weechainDailyJob - smireData.res test : ', smireData.res ? 'smire.data is true' : 'smire.data is false');
    sendSlackBotMessage("☝️ "+ jobDay+" weechainDailyJob - An error occured during fetchSmireData (call ok but wrong data)");
  }

  const nbOfProducingAssets = await getAssetsProducingLength();
  if( !nbOfProducingAssets ) {
    console.log('weechainDailyJob - An error occured during getAssetsProducingLength : ', nbOfProducingAssets)
    sendSlackBotMessage("☝️ "+ jobDay+" weechainDailyJob - An error occured during getAssetsProducingLength");
  } else {
    console.log("weechainDailyJob - Parity is Up");
  }

  let reqSiteTimeSeries = [];

  if( smireData.status === 'ok' && smireData.res && smireData.res.data && smireData.res.data.length > 0 && nbOfProducingAssets ) {
    console.log("weechainDailyJob - all the services are ok, launching the matching");
    reqSiteTimeSeries = smireData.res;

    let totalMatchingProductionFontanelles  = 0;
  	let totalMatchingProductionEget = 0;
  	let totalMatchingProductionMareges = 0;
  	let totalResidualConsumption = 0;
  	for (let i = 0; i < reqSiteTimeSeries.data.length; i++) {
  	    //Let's build the matching data

  	    //Priority 1 : Production Wind park W (kwh) Fontanelles
  	    //Priority 2 : Production Hydro Site 1 H1 (kWh) Eget
  	    //Priority 3 : Production Hydro Site 2 H2 (kwh) Marèges

  	    console.log('weechainDailyJob - New day : ', reqSiteTimeSeries.index[i].substr(0, 10) );

  	    let matchingProductionMareges = 0;
  	    let realProductionMareges = reqSiteTimeSeries.data[i][0]*1000;// *1000 : kWh -> Wh for the blockchain
  	    let matchingProductionEget = 0;
  	    let realProductionEget = reqSiteTimeSeries.data[i][1]*1000;
  	    let matchingProductionFontanelles = 0;
  	    let realProductionFontanelles = reqSiteTimeSeries.data[i][2]*1000;
  	    let residualConsumption = 0;
  	    let realConsumption = reqSiteTimeSeries.data[i][3]*1000*57/100;// We only take 57% of lida's consumption
  	    console.log('weechainDailyJob - --------------------------------------------------')
  	    console.log(reqSiteTimeSeries.index[i]);
        const productionDate = moment(reqSiteTimeSeries.index[i]).format('X');
        console.log(productionDate)

        if(
          realProductionMareges === 0 &&
          realProductionEget === 0 &&
          realProductionFontanelles === 0 &&
          realConsumption === 0
        ) {
          console.log("weechainDailyJob - Error : Data are all 0");
          sendSlackBotMessage('weechainDailyJob - Error : Data are all 0 🤦');
          return false;
        }

        if(
          isNaN(realProductionMareges) ||
          isNaN(realProductionEget) ||
          isNaN(realProductionFontanelles) ||
          isNaN(realConsumption)
        ) {
          console.log("weechainDailyJob - Error : Some data are not number");
          sendSlackBotMessage('weechainDailyJob - Error : Some data are not number ');
          return false;
        }

  	    if( realProductionFontanelles < realConsumption) {
  	      residualConsumption = realConsumption - realProductionFontanelles;
  	      matchingProductionFontanelles = realProductionFontanelles;

  	      if( realProductionEget < residualConsumption) {
  	        residualConsumption = residualConsumption - realProductionEget;
  	        matchingProductionEget = realProductionEget;

  	        if( realProductionMareges < residualConsumption) {
  	          residualConsumption = residualConsumption - realProductionMareges;
  	          matchingProductionMareges = realProductionMareges;
  	          console.log('weechainDailyJob - ***** Consumption not fully absorbed by renewable assets', residualConsumption, realConsumption, residualConsumption*100/realConsumption);
  	        } else {
  	          matchingProductionMareges = residualConsumption;
  	          residualConsumption = 0;
  	          console.log('weechainDailyJob - Consumption fully absorbed by Wind from fontanelles, Hydro from Eget and Hydro from Mareges');
  	        }

  	      } else {
  	        matchingProductionEget = residualConsumption;
  	        residualConsumption = 0;
  	        console.log('weechainDailyJob - Consumption fully absorbed by Wind from fontanelles and Hydro from Eget');
  	      }

  	    } else {
  	      matchingProductionFontanelles = realConsumption;
  	      residualConsumption = 0;
  	      console.log('weechainDailyJob - Consumption fully absorbed by Wind from fontanelles');
  	    }

  	    console.log('weechainDailyJob - Real consumption', realConsumption);
  	    console.log('weechainDailyJob - Residual consumption', residualConsumption);
  	    console.log('weechainDailyJob - Real fontanelles production', realProductionFontanelles);
  	    console.log('weechainDailyJob - Matching fontanelles production', matchingProductionFontanelles);
  	    console.log('weechainDailyJob - Real Eget production', realProductionEget);
  	    console.log('weechainDailyJob - Matching Eget production', matchingProductionEget);
  	    console.log('weechainDailyJob - Real Mareges production', realProductionMareges);
  	    console.log('weechainDailyJob - Matching Mareges production', matchingProductionMareges);


  	    totalMatchingProductionFontanelles += matchingProductionFontanelles;
  	    totalMatchingProductionEget += matchingProductionEget;
  	    totalMatchingProductionMareges += matchingProductionMareges;
  	    totalResidualConsumption += residualConsumption;

  	    console.log('weechainDailyJob -      ###       ###        ###');
        console.log('weechainDailyJob - ###### Ready to generate certificates');


  	    //Fontanelles
  	    console.log('weechainDailyJob - Fontanelles', matchingProductionFontanelles, realProductionFontanelles);
  	    const newOwner = '0x0051CcCBCFF51788dDa9614b12dcea0D57208C03'; //Airproducts
  	    const fontanellesAssetId = 1; //Fontanelles assetId in the blockchain
  	    const fontanellesAsset = await getAssetProducing(fontanellesAssetId);
  	    console.log("weechainDailyJob - fontanellesAsset retrieved");
  	    const fontanellesLastMeterRead = parseInt(fontanellesAsset.lastSmartMeterReadWh, 10);
  	    console.log("weechainDailyJob - fontanellesLastMeterRead retrieved : "+fontanellesLastMeterRead);
  	    realProductionFontanelles = parseInt(realProductionFontanelles, 10);
  	    const fontanellesNewMeterRead = fontanellesLastMeterRead + realProductionFontanelles;
  	    const fontanellesLastMeterReadFileHash = Date.now().toString()+'fontanelles';
  	    console.log("weechainDailyJob - fontanellesLastMeterReadFileHash set : "+fontanellesLastMeterReadFileHash);
  	    const saveProducingSmartMeterReadResult = await saveProducingSmartMeterRead (
  				  fontanellesAssetId,
  				  fontanellesNewMeterRead,
  				  false,
  				  fontanellesLastMeterReadFileHash,
  				  0,
  				  false,
  				  '0x00c3B44DBeA6f103dd30D7e90D59787175FeA185'
  			  );

  	    console.log(saveProducingSmartMeterReadResult);
  	    console.log("weechainDailyJob - saveProducingSmartMeterRead finished");
  	    if(matchingProductionFontanelles > 0) {
  	    	console.log("weechainDailyJob - matchingProductionFontanelles createCertificate start");
  	    	const matchingFontanellesCertifData = await createCertificate(fontanellesAssetId, matchingProductionFontanelles, productionDate);
  	    	console.log("weechainDailyJob - matchingProductionFontanelles createCertificate finished");
  	    	const fontanellesCertificateId = matchingFontanellesCertifData.logs[0]["result"]._certificateId;
          await setCertificateTxHash(fontanellesCertificateId, matchingFontanellesCertifData.logs[0].transactionHash);
  	    	console.log("weechainDailyJob - matchingProductionFontanelles transferCertificate start");
  	    	await transferCertificate(fontanellesCertificateId, newOwner)
  	    	console.log("weechainDailyJob - matchingProductionFontanelles transferCertificate finished");
  	    }
  	    const residualProductionFontanelles = realProductionFontanelles - matchingProductionFontanelles;
  	    if(residualProductionFontanelles > 0) {
  	    	console.log("weechainDailyJob - residualProductionFontanelles createCertificate start");
  	    	const residualFontanellesCertifData = await createCertificate(fontanellesAssetId, residualProductionFontanelles, productionDate);
          const fontanellesResCertificateId = residualFontanellesCertifData.logs[0]["result"]._certificateId;
          await setCertificateTxHash(fontanellesResCertificateId, residualFontanellesCertifData.logs[0].transactionHash);
  	    	console.log("weechainDailyJob - residualProductionFontanelles createCertificate finished");
  	    }

  	    //Eget
  	    console.log('weechainDailyJob - Eget', matchingProductionEget, realProductionEget);
  	    const egetAssetId = 2; //Eget assetId in the blockchain
  	    const egetAsset = await getAssetProducing(egetAssetId);
  	    const egetLastMeterRead = parseInt(egetAsset.lastSmartMeterReadWh, 10);
  	    realProductionEget = parseInt(realProductionEget, 10);
  	    const egetNewMeterRead = egetLastMeterRead + realProductionEget;
  	    const egetLastMeterReadFileHash = Date.now().toString()+'eget';
  	    await saveProducingSmartMeterRead (
  				  egetAssetId,
  				  egetNewMeterRead,
  				  false,
  				  egetLastMeterReadFileHash,
  				  0,
  				  false,
  				  '0x004A064a9AAf2fCD9687D82Fc5a1116e9325000A'
  			  );
  	    if(matchingProductionEget > 0) {

          const matchingEgetCertifData = await createCertificate(egetAssetId, matchingProductionEget, productionDate);
          console.log("weechainDailyJob - matchingProductionEget createCertificate finished");
          const egetCertificateId = matchingEgetCertifData.logs[0]["result"]._certificateId;
          await setCertificateTxHash(egetCertificateId, matchingEgetCertifData.logs[0].transactionHash);
  	    	await transferCertificate(egetCertificateId, newOwner)
  	    }
  	    const residualProductionEget = realProductionEget - matchingProductionEget;
  	    if(residualProductionEget > 0) {

          const residualEgetCertifData = await createCertificate(egetAssetId, residualProductionEget, productionDate);
          console.log("weechainDailyJob - residualProductionEget createCertificate finished");
          const egetResCertificateId = residualEgetCertifData.logs[0]["result"]._certificateId;
          await setCertificateTxHash(egetResCertificateId, residualEgetCertifData.logs[0].transactionHash);
  	    }


  	    console.log('weechainDailyJob - Mareges', matchingProductionMareges, realProductionMareges);
  	    const maregesAssetId = 0; //Mareges assetId in the blockchain
  	    const maregesAsset = await getAssetProducing(maregesAssetId);
  	    const maregesLastMeterRead = parseInt(maregesAsset.lastSmartMeterReadWh, 10);
  	    realProductionMareges = parseInt(realProductionMareges, 10);
  	    const maregesNewMeterRead = maregesLastMeterRead + realProductionMareges;
  	    const maregesLastMeterReadFileHash = Date.now().toString()+'mareges';
  	    await saveProducingSmartMeterRead (
  				  maregesAssetId,
  				  maregesNewMeterRead,
  				  false,
  				  maregesLastMeterReadFileHash,
  				  0,
  				  false,
  				  '0x00CB17ea4DfA8578582C00A79Dd92d6111c28c54'
  			  );
  	    if(matchingProductionMareges > 0) {

          const matchingMaregesCertifData = await createCertificate(maregesAssetId, matchingProductionMareges, productionDate);
          console.log("weechainDailyJob - matchingProductionMareges createCertificate finished");
          const maregesCertificateId = matchingMaregesCertifData.logs[0]["result"]._certificateId;
          await setCertificateTxHash(maregesCertificateId, matchingMaregesCertifData.logs[0].transactionHash);
  	    	await transferCertificate(maregesCertificateId, newOwner)
  	    }
  	    const residualProductionMareges = realProductionMareges - matchingProductionMareges;
  	    if(residualProductionMareges > 0) {
          const residualMaregesCertifData = await createCertificate(maregesAssetId, residualProductionMareges, productionDate);
          console.log("weechainDailyJob - residualProductionMareges createCertificate finished");
          const maregesResCertificateId = residualMaregesCertifData.logs[0]["result"]._certificateId;
          await setCertificateTxHash(maregesResCertificateId, residualMaregesCertifData.logs[0].transactionHash);
  	    }

  	    console.log('weechainDailyJob - Consumption', residualConsumption, realConsumption);
  	    console.log('weechainDailyJob - End for this day *************************');

        let messageText = '😎 weechainDailyJob is ok for day '+jobDay+' ✅\n';
        messageText +=    'Real consumption = '+realConsumption+'\n';
        messageText +=    'Residual consumption = '+residualConsumption+'\n';
        messageText +=    'Fontanelles production = '+realProductionFontanelles+'\n';
        messageText +=    'Matching Fontanelles production = '+matchingProductionFontanelles+'\n';
        messageText +=    'Eget production = '+realProductionEget+'\n';
        messageText +=    'Matching Eget production = '+matchingProductionEget+'\n';
        messageText +=    'Mareges production = '+realProductionMareges+'\n';
        messageText +=    'Matching Mareges production = '+matchingProductionMareges+'\n';

        sendSlackBotMessage(messageText);


  	}
  } else {
    if(retry > 1) {
      console.log("weechainDailyJob - Attempt "+(maxRetry-retry+1)+"/"+(maxRetry)+" failed, one of the service is KO, retrying in "+frequency+"ms... ");
      sendSlackBotMessage("❌ "+ jobDay+" weechainDailyJob - Attempt "+(maxRetry-retry+1)+"/"+(maxRetry)+" failed, one of the service is KO, retrying in "+frequency+"ms... ");
      setTimeout(async function() {
        weechainDailyJob(retry, date)
      }, frequency);
    } else {
      console.log("weechainDailyJob - Fail after "+maxRetry+" attempts (every  "+frequency+"ms since the initial job launch for the day)");
      sendSlackBotMessage("❌❌❌ "+ jobDay+" weechainDailyJob - Fail after "+maxRetry+" attempts (every  "+frequency+"ms since the initial job launch for the day)");
    }

  }
}

cron.schedule('*/2 * * * *', async function(){
  console.log('monitoring parity (every 2min)');
	monitorParity();

});

cron.schedule('*/20 * * * *', async function(){
  console.log('updateCo2Data for cache (every 20min)');
  //updateCo2Data();

  console.log('updateCacheData for cache (every 20min)');
  //updateCacheData();

});

cron.schedule('0 3 * * *', async function(){
	console.log('running weechainDailyJob task every day at 3AM ');
	//weechainDailyJob();
  setTimeout(async function() {
    let matchingDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    //matchingDate = '2018-09-14';

    //weechainMatchingV2(0, matchingDate, false);
    //checkGooMaxLimit()
  }, 180000);

});


//console.log('Environnement variables');
//console.log(process.env);
/*
let selectedDay = moment().subtract(1, 'days').format('YYYY-MM-DD'); //By default, yesterday
//selectedDay = '2018-08-15';
console.log('30s before weechainMatching script launch (day = '+selectedDay+')...');
//console.log('30s before weechainDailyJob script launch (day = '+selectedDay+')...');
setTimeout(async function() {
    weechainMatching(null, selectedDay);
    //weechainDailyJob(null, selectedDay);
}, 30000);
*/


const logger = require('winston');
const app = require('./app');
const port = app.get('port');
const server = app.listen(port);


process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () => {
  logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
  setTimeout(async function() {
    //updateCo2Data();
    //updateCacheData();
    let matchingDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    matchingDate = '2018-09-15';
    //weechainMatching(null, matchingDate);
    //weechainDailyJob(null, matchingDate);
    //weechainMatchingV2(0, matchingDate, true);// use the third parameter to force (true) or not the generation of the certificate
  }, 5000);
}


);
