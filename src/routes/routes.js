const jwt = require("express-jwt");
const jwtAuthz = require("express-jwt-authz");
const jwksRsa = require("jwks-rsa");

var privateRoutes = require("./privateRoutes");
var certificatesService = require("./certificatesService");
var certificatesServiceLight = require("./certificatesServiceLight");
var usersService = require("./usersService");
var accountsService = require("./accountsService");
var producingAssetsServices = require("./producingAssetsServices");
var consumingAssetsServices = require("./consumingAssetsServices");
var offchainServices = require("./offchainServices");
var matchingServices = require("./matchingServices");
var initService = require("./initService");
var dbService = require("./dbService");
var meteringService = require("./meteringService");

// Authentication middleware. When used, the
// Access Token must exist and be verified against
// the Auth0 JSON Web Key Set
const checkJwt = jwt({
  // Dynamically provide a signing key
  // based on the kid in the header and
  // the signing keys provided by the JWKS endpoint.
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: process.env.AUTH_URL + `/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: process.env.AUTH_URL + "/userinfo",
  aud: process.env.AUTH_URL + "/userinfo",
  issuer: process.env.AUTH_URL + `/`,
  algorithms: ["RS256"]
});

const appRouter = function(app) {
  // These routes doesn't need authentication

  //Users (blockchain)

  app.get("/accounts", accountsService.getAllAccounts);
  app.get("/accounts/:accountAddress/balance", accountsService.getAccountBalance);
  app.get("/users-addresses", usersService.getAllUserAddress);
  app.get("/users-list-length", usersService.getUserListLength);
  app.get("/users", usersService.getAllUsers);
  app.get("/users/:userAddress/exist", usersService.doesUserExist);
  app.post("/users", usersService.setUser);
  app.put("/users/:userAddress", usersService.setUser);
  app.put("/users/:userAddress/v2", usersService.setUserV2);
  app.get("/users/:userAddress", usersService.getFullUser);
  app.put("/users/:userAddress/roles", usersService.setRoles);
  app.get("/users/:userAddress/roles/check", usersService.checkRoles);
  app.put("/users/:userAddress/organization/address", usersService.setOrganizationAddress);
  app.put("/users/:userAddress/organization", usersService.setOrganization);

  //To test :
  app.put("/users/:userAddress/username", usersService.setUserName);
  app.put("/users/:userAddress/add-admin-role", usersService.addAdminRole); //Not properly tested
  app.put("/users/:userAddress/remove-admin-role", usersService.removeAdminRole); //Not properly tested
  app.put("/users/:userAddress/add-asset-manager-role", usersService.addAssetManagerRole); //Not properly tested
  app.put("/users/:userAddress/remove-asset-manager-role", usersService.removeAssetManagerRole); //Not properly tested
  app.put("/users/:userAddress/add-matcher-role", usersService.addMatcherRole); //Not properly tested
  //app.put("/users/:userAddress/remove-matcher-role", usersService.removeMatcherRole);
  app.put("/users/:userAddress/add-trader-role", usersService.addTraderRole); //Not properly tested
  app.put("/users/:userAddress/remove-trader-role", usersService.removeTraderRole); //Not properly tested
  app.put("/users/:userAddress/deactivate", usersService.deactivateUser);

  //Users (offchain)
  app.get("/users-offchain", (req, res) => {
    if (req.query.email !== undefined) {
      offchainServices.getUserByEmail(req, res);
    } else if (req.query.address !== undefined) {
      offchainServices.getUserByAddress(req, res);
    } else {
      offchainServices.getUsers(req, res);
    }
  });

  app.put("/users-offchain/:userAddress", (req, res) => {
    offchainServices.setUserByAddress(req, res);
  });
  /*** NEW : using the new  version of the Smart Contract ***/
  app.post("/certificates-light/issue/issuingQuantity/issuingUnit/issuingName", certificatesServiceLight.issueCertificate);
  app.post("/certificates-light/transfer/transferedQuantity/consumerName", certificatesServiceLight.transferCertificate);
  app.post("/certificates-light/administrator/administrator/isAdmin", certificatesServiceLight.setAdministrator);
  app.get("/certificates-light/Ids", certificatesServiceLight.getCertificateIds);
  app.get("/certificates-light/children", certificatesServiceLight.getCertificateChildren);

  app.get("/certificates-light/certificateOld", certificatesServiceLight.getCertificateOld);
  app.put("/certificates-light/cancel", certificatesServiceLight.cancelCertificate);
  /*** END NEW ***/
  //Certificates Light (blockchain)
  app.post("/certificates-light", certificatesServiceLight.issueCertificate);
  // /!\/!\ LB - Commenté car fait crasher le serveur /!\/!\
  // app.post("/certificates-light/transfer", certificatesServiceLight.transferCertificate);

  //Certificates Light (blockchain)
  app.get("/certificates-list-length", certificatesService.getCertificateListLength);
  app.get("/certificates", certificatesService.getAllCertificates);
  app.get("/certificates-list", certificatesService.getAllCertificatesFromCertificateDB);

  app.post("/certificates", certificatesService.createCertificateForAssetOwner);
  app.get("/certificates/:certificateId", certificatesService.getCertificate);
  app.get("/certificates/:certificateId/owner", certificatesService.getCertificateOwner);
  app.put("/certificates/:certificateId/owner", certificatesService.changeCertificateOwner);
  app.put("/certificates/:certificateId/owner-by-escrow", certificatesService.transferOwnershipByEscrow); //Not tested (not used yet)
  app.put("/certificates/:certificateId/txhash", certificatesService.setCertificateTxHash);
  app.put("/certificates/:certificateId/hash", certificatesService.setCertificateHash); //Not tested (not used yet)
  app.get("/certificates/:certificateId/retired", certificatesService.isRetired);
  app.put("/certificates/:certificateId/retired", certificatesService.retireCertificate);

  app.post("/certificates-management/check", certificatesService.checkCertificateFile); //Not tested (test it with the frontend directly)
  app.put("/certificates-management/retire", certificatesService.retireCertificatesForDay);
  app.post("/certificates-management/matching", matchingServices.weechainMatching);
  app.post("/certificates-management/teomatching", matchingServices.teoMatching);

  //Contract version (in certificatesService.js...)
  app.get("/contract/version", certificatesService.getSmartContractVersion); //Not tested (not used yet)

  //Producing assets (blockchain)
  app.get("/producing-assets-list-length", producingAssetsServices.getAssetListLength);

  app.post("/producing-assets", producingAssetsServices.createAsset);
  app.put("/producing-assets/:assetId", producingAssetsServices.setGeneralProperties);
  app.put("/producing-assets/:assetId/properties", producingAssetsServices.setProducingProperties);
  app.put("/producing-assets/:assetId/location", producingAssetsServices.setLocation);

  app.put("/producing-assets/:assetId/smartmeter", producingAssetsServices.setSmartMeter);
  app.put("/producing-assets/:assetId/smartmeter/read", producingAssetsServices.setSmartMeterRead);

  app.get("/producing-assets/:assetId", producingAssetsServices.getAssetGeneral);
  app.get("/producing-assets/:assetId/properties", producingAssetsServices.getAssetProducingProperties);
  app.get("/producing-assets/:assetId/location", producingAssetsServices.getAssetLocation);
  app.get("/producing-assets/:assetId/co-saved/:wh", producingAssetsServices.getAssetCoSaved); //Not really tested (not used yet)
  app.get("/producing-assets/:assetId/datalog", producingAssetsServices.getAssetDataLog); //Not tested (not used yet)
  app.get("/producing-assets/:assetId/production", producingAssetsServices.getProduction);

  app.get("/producing-assets/:assetId/full", producingAssetsServices.getAssetFull);

  //Producing assets (offchain)
  app.get("/producing-assets-offchain", offchainServices.getProducingAssets);
  app.get("/producing-assets-offchain/:assetId", offchainServices.getProducingAsset);
  app.get("/production-offchain", offchainServices.getProducingAssetsProduction);
  app.get("/production-db", offchainServices.getProducingAssetsProductionDb);

  //Consuming assets (blockchain)
  app.get("/consuming-assets-list-length", consumingAssetsServices.getAssetListLength);
  app.post("/consuming-assets", consumingAssetsServices.createAsset);
  app.put("/consuming-assets/:assetId", consumingAssetsServices.setGeneralProperties);
  app.get("/consuming-assets/:assetId", consumingAssetsServices.getAssetGeneral);
  app.get("/consuming-assets/:assetId/properties", consumingAssetsServices.getConsumingProperties);
  app.put("/consuming-assets/:assetId/location", consumingAssetsServices.setLocation);
  app.get("/consuming-assets/:assetId/location", consumingAssetsServices.getAssetLocation);
  app.get("/consuming-assets/:assetId/datalog", consumingAssetsServices.getAssetDataLog);
  app.put("/consuming-assets/:assetId/smartmeter", consumingAssetsServices.setSmartMeter);
  app.put("/consuming-assets/:assetId/smartmeter/read", consumingAssetsServices.setSmartMeterRead);

  //Consuming assets (offchain)
  app.get("/consumption-offchain", offchainServices.getConsumingSitesConsumption);
  app.get("/consumption-db", offchainServices.getConsumingAssetsConsumptionDb);
  app.get("/consuming-assets-offchain", offchainServices.getConsumingSites);

  app.post("/consuming-assets-offchain-db", offchainServices.createConsumingAssetsDb);
  app.get("/consuming-assets-offchain-db", offchainServices.getConsumingAssetsDb);
  app.get("/consuming-assets-offchain-db/:assetId", offchainServices.getConsumingAssetDb);
  app.put("/consuming-assets-offchain-db/:assetId", offchainServices.setConsumingAssetDb);

  app.post("/producing-assets-offchain-db", offchainServices.createProducingAssetsDb);
  app.get("/producing-assets-offchain-db", offchainServices.getProducingAssetsDb);
  app.get("/producing-assets-offchain-db/:assetId", offchainServices.getProducingAssetDb);
  app.put("/producing-assets-offchain-db/:assetId", offchainServices.setProducingAssetDb);
  app.post("/producing-assets-offchain-db/:assetId/picture", offchainServices.uploadProducingAssetPicture);
  app.get("/producing-assets-offchain-db/:assetId/picture", offchainServices.getProducingAssetPicture);
  app.get("/producing-assets-offchain-db/:assetId/production", offchainServices.getProducingAssetProductionDb);
  app.get("/producing-assets-offchain-db/:assetId/production-certificates", offchainServices.getProducingAssetProductionCertificatesDb);

  app.get("/meters-db", offchainServices.getMetersDb);
  app.get("/meters-db/:meterId", offchainServices.getMeterDb);
  app.post("/meters-db", offchainServices.createMeterDb);
  app.put("/meters-db/:meterId", offchainServices.setMeterDb);
  app.get("/meters-assets-db", offchainServices.getAssetsMeteringDb);

  app.get("/certificates-db", offchainServices.getCertificatesDb);
  app.get("/certificates-db/:certificateId", offchainServices.getCertificateDb);
  app.post("/certificates-db", offchainServices.createCertificateDb);
  app.put("/certificates-db/:certificateId", offchainServices.setCertificateDb);
  app.get("/certificates-production-asset-db", offchainServices.getCertificatesByProductionAssetDb);
  app.get("/certificates-consumption-asset-db", offchainServices.getCertificatesByConsumptionAssetDb);
  app.get("/certificates-owner-db", offchainServices.getCertificatesByOwnerDb);

  app.get("/consuming-assets-offchain/:assetId", offchainServices.getConsumingSite);
  app.get("/consuming-assets-offchain/:assetId/consumption", (req, res) => {
    if (req.query.withProduction === "true") {
      offchainServices.getConsumingSiteTimeSeries(req, res);
    } else {
      offchainServices.getConsumingSiteLightTimeSeriesDb(req, res);
      //offchainServices.getConsumingSiteLightTimeSeries(req, res)
    }
  });

  app.get("/consuming-assets-offchain/:assetId/consumptiondb", (req, res) => {
    if (req.query.withProduction === "true") {
      offchainServices.getConsumingSiteTimeSeriesDb(req, res);
    } else {
      offchainServices.getConsumingSiteLightTimeSeriesDb(req, res);
    }
  });

  app.get("/consuming-assets-offchain/:assetId/graph-data", offchainServices.getConsumingSiteCalculatedData);
  app.get("/consuming-assets-offchain/:assetId/graph-data-db", offchainServices.getConsumingSiteCalculatedDataDb);
  app.get("/consuming-assets-offchain/:assetId/graph-data-cached", offchainServices.getConsumingSiteCachedData);
  //app.put("/consuming-assets-offchain/:assetId/avoided-co2", offchainServices.setSiteCo2Data);

  app.get("/co2", offchainServices.getCo2);
  app.get("/deliveries/certificates", offchainServices.getDeliveriesCertificates);

  app.get("/update-cache", offchainServices.updateCache);
  app.get("/update-co2", offchainServices.updateCo2Data);
  app.get("/update-deliveries-with-city", offchainServices.updateDeliveriesWithCity);

  app.post("/init", initService.initialization);

  app.post("/init-db", dbService.initialization);
  app.post("/update-db", dbService.dailyUpdate);
  app.post("/update-db-tourbillon", dbService.dailyUpdateFromTourbillon);
  app.post("/init-certificates-db", dbService.certificatesInitialization);
  app.post("/update-db-darwin", meteringService.hourlyUpdateFromDarwin);

  // This route need authentication
  app.get("/private", checkJwt, privateRoutes.test);

  const checkScopes = jwtAuthz(["profile"]);

  app.get("/private-scoped", checkJwt, checkScopes, (req, res) => {
    res.json({
      message: 'Hello from a private endpoint! You need to be authenticated and have a scope of "profile" to see this.'
    });
  });

  app.get("/private-roles", checkJwt, (req, res) => {
    let token = null;
    if (req.headers.authorization && req.headers.authorization.split(" ")[0] === "Bearer") {
      // Authorization: Bearer g1jipjgi1ifjioj
      // Handle token presented as a Bearer token in the Authorization header
      token = req.headers.authorization.split(" ")[1];
    }

    console.log("token provided", token);
    console.log("req.user", req.user);

    const roles = req.user["https://weeechain.app/user_authorization"].roles;

    if (roles.indexOf("admin") >= 0) {
      res.json({
        message: 'Hello from a private role endpoint! You need to be authenticated and have a scope of "profile" to see this.'
      });
    } else {
      res.json({
        message: "You don't have access to this route"
      });
    }
  });
};

module.exports = appRouter;
