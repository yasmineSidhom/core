const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');
var moment = require('moment');
var fetch = require('node-fetch');

var offchainServices = require('./offchainServices');

let AssetProducingRegistry_artefact = require('../../'+process.env.BUILD_FOLDER+'/contracts/AssetProducingRegistryLogic.json');
const abiAssetProducing = AssetProducingRegistry_artefact.abi;
const addressAssetProducing = AssetProducingRegistry_artefact.networks[401697].address;
const myContractAssetProducing = new web3.eth.Contract(abiAssetProducing, addressAssetProducing);

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
/////************************************************************* CREATE ASSET ****************************************************************************************///////

//Create asset producing registry
const createAsset = async (req, res) => {

  const sender = req.query._sender;        //address

  var openDoorDataPart = myContractAssetProducing.methods.createAsset().encodeABI();

  const nonce= await web3.eth.getTransactionCount(sender);

  var rawTx = {
    nonce: web3.utils.toHex(nonce),
    value: 0x00,
    gasPrice: web3.utils.toHex(4000000000),
    gasLimit: web3.utils.toHex(1000000),
    data: openDoorDataPart,
    to: addressAssetProducing,
    from: sender
  };

  var tx = new Tx(rawTx);
  var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
  await tx.sign(privateKey);
  const serializedTx=await tx.serialize();
  const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

  const results = await Promise.resolve(ret);
  res.json(results);
};
exports.createAsset = createAsset;

//Asset producing registry
const setGeneralProperties = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  var smartMeter = req.body._smartMeter;                //address
  var owner = req.body._owner;                          //address
  var operationalSince = req.body._operationalSince;    //uint
  var active = req.body._active;                        //bool

  var openDoorDataPart = myContractAssetProducing.methods.initGeneral(
    assetId,
    smartMeter,
    owner,
    operationalSince,
    active
  ).encodeABI();

  const nonce = await web3.eth.getTransactionCount(sender);

  var rawTx = {
    nonce: web3.utils.toHex(nonce),
    value: 0x00,
    gasPrice: web3.utils.toHex(4000000000),
    gasLimit: web3.utils.toHex(1000000),
    data: openDoorDataPart,
    to: addressAssetProducing,
    from: sender
  };

  var tx = new Tx(rawTx);
  var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
  await tx.sign(privateKey);
  const serializedTx = await tx.serialize();
  const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

  const results = await Promise.resolve(ret);
  res.json(results);
};
exports.setGeneralProperties = setGeneralProperties;

//init producing properties with the smart contract assetProducingRegistryLogic
const setProducingProperties = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId=req.params.assetId ;                             //uint

  var assetType = req.body._assetType ;                           //AssetType
  var capacityWh = req.body._capacityWh ;                         //uint
  var registryCompliance = req.body._registryCompliance ;         //Compliance
  var otherGreenAttributes = req.body._otherGreenAttributes ;     //bytes32
  var typeOfPublicSupport = req.body._typeOfPublicSupport ;       //bytes32

  var openDoorDataPart = myContractAssetProducing.methods.initProducingProperties(
    assetId,
    assetType,
    capacityWh,
    registryCompliance,
    web3.utils.fromAscii(otherGreenAttributes),
    web3.utils.fromAscii(typeOfPublicSupport)
  ).encodeABI();

  const nonce = await web3.eth.getTransactionCount(sender);

  var rawTx = {
    nonce: web3.utils.toHex(nonce),
    value: 0x00,
    gasPrice: web3.utils.toHex(4000000000),
    gasLimit: web3.utils.toHex(1000000),
    data: openDoorDataPart,
    to: addressAssetProducing,
    from: sender
  };

  var tx = new Tx(rawTx);
  var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
  await tx.sign(privateKey);
  const serializedTx=await tx.serialize();
  const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

  const results = await Promise.resolve(ret);
  res.json(results);
};
exports.setProducingProperties = setProducingProperties;

//init Location producing properties with the smart contract assetProducingRegistryLogic
const setLocation = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId ;            //uint

  var country = req.body._country ;            //bytes32
  var region = req.body._region ;              //bytes32
  var zip = req.body._zip ;                    //bytes32
  var city = req.body._city ;                  //bytes32
  var street = req.body._street ;              //bytes32
  var houseNumber = req.body._houseNumber ;    //bytes32
  var gpsLatitude = req.body._gpsLatitude ;    //bytes32
  var gpsLongitude = req.body._gpsLongitude ;  //bytes32

  var openDoorDataPart = myContractAssetProducing.methods.initLocation(
    parseInt(assetId, 10),
    web3.utils.fromAscii(country),
    web3.utils.fromAscii(region),
    web3.utils.fromAscii(zip),
    web3.utils.fromAscii(city),
    web3.utils.fromAscii(street),
    web3.utils.fromAscii(houseNumber),
    web3.utils.fromAscii(gpsLatitude),
    web3.utils.fromAscii(gpsLongitude)
  ).encodeABI();

  const nonce= await web3.eth.getTransactionCount(sender);

  var rawTx = {
    nonce: web3.utils.toHex(nonce),
    value: 0x00,
    gasPrice: web3.utils.toHex(4000000000),
    gasLimit: web3.utils.toHex(1000000),
    data: openDoorDataPart,
    to: addressAssetProducing,
    from: sender
  };

  var tx = new Tx(rawTx);
  var privateKey = require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
  await tx.sign(privateKey);
  const serializedTx = await tx.serialize();
  const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

  const results = await Promise.resolve(ret);
  res.json(results);
};
exports.setLocation = setLocation;

//asset Save Smart Meter Read from producing registry contract
const setSmartMeterRead = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId ;            //uint

  var newMeterRead = req.body._newMeterRead;                               //uint
  var smartMeterDown = req.body._smartMeterDown;                           //bool
  var lastSmartMeterReadFileHash =  web3.utils.fromAscii(req.body._lastSmartMeterReadFileHash); //bytes32
  var CO2OffsetMeterRead = req.body._CO2OffsetMeterRead;                   //uint
  var CO2OffsetServiceDown = req.body._CO2OffsetServiceDown;              //bool

  var openDoorDataPart = myContractAssetProducing.methods.saveSmartMeterRead(
    assetId,
    newMeterRead,
    smartMeterDown,
    lastSmartMeterReadFileHash,
    CO2OffsetMeterRead,
    CO2OffsetServiceDown
  ).encodeABI();

  const nonce= await web3.eth.getTransactionCount(sender);

  var rawTx = {
    nonce: web3.utils.toHex(nonce),
    value: 0x00,
    gasPrice: web3.utils.toHex(4000000000),
    gasLimit: web3.utils.toHex(1000000),
    data: openDoorDataPart,
    to: addressAssetProducing,
    from: sender
  };

  var tx = new Tx(rawTx);
  var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
  await tx.sign(privateKey);
  const serializedTx=await tx.serialize();
  const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

  const results = await Promise.resolve(ret);
  res.json(results);
};
exports.setSmartMeterRead = setSmartMeterRead;

//update Smart Meter from producing registry contract
const setSmartMeter = async (req, res) => {

  const sender = req.query._sender;             //address

  var assetId = req.params.assetId;            //uint

  var newSmartMeter = req.body._newSmartMeter;  //address

  var openDoorDataPart = myContractAssetProducing.methods.updateSmartMeter(
    assetId,
    newSmartMeter,
  ).encodeABI();

  const nonce = await web3.eth.getTransactionCount(sender);

  var rawTx = {
    nonce: web3.utils.toHex(nonce),
    value: 0x00,
    gasPrice: web3.utils.toHex(4000000000),
    gasLimit: web3.utils.toHex(1000000),
    data: openDoorDataPart,
    to: addressAssetProducing,
    from: sender
  };

  var tx = new Tx(rawTx);
  var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
  await tx.sign(privateKey);
  const serializedTx=await tx.serialize();
  const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

  const results = await Promise.resolve(ret);
  res.json(results);
};
exports.setSmartMeter = setSmartMeter;

//*****************************************Get Information Of Asset****************************************//
//Get Asset producing registry
const getAssetGeneral = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;            //uint

  const result = await myContractAssetProducing.methods.getAssetGeneral(assetId).call({ from:sender });

  const results = await Promise.resolve(result);
  res.json(results);
};
exports.getAssetGeneral = getAssetGeneral;

//get Asset Producing Properties producing registry
const getAssetProducingProperties = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;            //uint

  const result= await myContractAssetProducing.methods.getAssetProducingProperties(assetId).call({ from:sender });
  result[6] = web3.utils.toAscii(result[6]).replace(/\0/g, '');
  result[7] = web3.utils.toAscii(result[7]).replace(/\0/g, '');
  result['otherGreenAttributes'] = web3.utils.toAscii(result['otherGreenAttributes']).replace(/\0/g, '');
  result['typeOfPublicSupport'] = web3.utils.toAscii(result['typeOfPublicSupport']).replace(/\0/g, '');

  const results = await Promise.resolve(result);
  res.json(results);
};
exports.getAssetProducingProperties = getAssetProducingProperties;

//get Asset Location
const getAssetLocation = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;            //uint

  try {
    const result= await myContractAssetProducing.methods.getAssetLocation(assetId).call({ from:sender });

    result[0] = web3.utils.toAscii(result[0]).replace(/\0/g, '');
    result[1] = web3.utils.toAscii(result[1]).replace(/\0/g, '');
    result[2] = web3.utils.toAscii(result[2]).replace(/\0/g, '');
    result[3] = web3.utils.toAscii(result[3]).replace(/\0/g, '');
    result[4] = web3.utils.toAscii(result[4]).replace(/\0/g, '');
    result[5] = web3.utils.toAscii(result[5]).replace(/\0/g, '');
    result[6] = web3.utils.toAscii(result[6]).replace(/\0/g, '');
    result[7] = web3.utils.toAscii(result[7]).replace(/\0/g, '');
    result["country"]=result[0]
    result["region"]=result[1]
    result["zip"]=result[2]
    result["city"]=result[3]
    result["street"]=result[4]
    result["houseNumber"]=result[5]
    result["gpsLatitude"]=result[6]
    result["gpsLongitude"]=result[7]

    const results = await Promise.resolve(result);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};
exports.getAssetLocation = getAssetLocation;

//getCoSaved asset
const getAssetCoSaved = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;            //uint
  var wh = req.params.wh;           //uint

  const result= await myContractAssetProducing.methods.getCoSaved(assetId, wh).call({ from:sender });

  const results = await Promise.resolve(result);
  res.json(results);
};
exports.getAssetCoSaved = getAssetCoSaved;

//getAssetDataLog asset
const getAssetDataLog = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;            //uint

  const result= await myContractAssetProducing.methods.getAssetDataLog(assetId).call({ from:sender });
  console.log('getAssetDataLogProducing: '+JSON.stringify(result));

  const results = await Promise.resolve(result);
  res.json(results);
};
exports.getAssetDataLog = getAssetDataLog;

//get asset list lenght producing registry
const getAssetListLength = async (req, res) => {

  const sender = req.query._sender;        //address
  console.log('prod getAssetListLength sender = '+sender)

  const result = await myContractAssetProducing.methods.getAssetListLength().call({ from:sender });

  const results = await Promise.resolve(result);
  res.json(results);
};
exports.getAssetListLength = getAssetListLength;

const getAssetFull = async (req, res) =>  {

  const sender = req.query._sender;        //address
  const assetId = req.params.assetId;      //uint

  const assetGeneralData = await myContractAssetProducing.methods.getAssetGeneral(assetId).call({ from:sender });

  const assetProducingProperties = await myContractAssetProducing.methods.getAssetProducingProperties(assetId).call({ from:sender });
  assetProducingProperties[6] = web3.utils.toAscii(assetProducingProperties[6]).replace(/\0/g, '');
  assetProducingProperties[7] = web3.utils.toAscii(assetProducingProperties[7]).replace(/\0/g, '');
  assetProducingProperties['otherGreenAttributes'] = web3.utils.toAscii(assetProducingProperties['otherGreenAttributes']).replace(/\0/g, '');
  assetProducingProperties['typeOfPublicSupport'] = web3.utils.toAscii(assetProducingProperties['typeOfPublicSupport']).replace(/\0/g, '');

  const assetLocation = await myContractAssetProducing.methods.getAssetLocation(assetId).call({ from:sender });

  assetLocation[0] = web3.utils.toAscii(assetLocation[0]).replace(/\0/g, '');
  assetLocation[1] = web3.utils.toAscii(assetLocation[1]).replace(/\0/g, '');
  assetLocation[2] = web3.utils.toAscii(assetLocation[2]).replace(/\0/g, '');
  assetLocation[3] = web3.utils.toAscii(assetLocation[3]).replace(/\0/g, '');
  assetLocation[4] = web3.utils.toAscii(assetLocation[4]).replace(/\0/g, '');
  assetLocation[5] = web3.utils.toAscii(assetLocation[5]).replace(/\0/g, '');
  assetLocation[6] = web3.utils.toAscii(assetLocation[6]).replace(/\0/g, '');
  assetLocation[7] = web3.utils.toAscii(assetLocation[7]).replace(/\0/g, '');
  assetLocation["country"] = assetLocation[0]
  assetLocation["region"] = assetLocation[1]
  assetLocation["zip"] = assetLocation[2]
  assetLocation["city"] = assetLocation[3]
  assetLocation["street"] = assetLocation[4]
  assetLocation["houseNumber"] = assetLocation[5]
  assetLocation["gpsLatitude"] = assetLocation[6]
  assetLocation["gpsLongitude"] = assetLocation[7]

  const asset = {};
  asset.id= assetId;

  /*
  assetGeneralData returns(
    address _smartMeter,
    address _owner,
    uint _operationalSince,
    uint _lastSmartMeterReadWh,
    bool _active,
    bytes32 _lastSmartMeterReadFileHash
  )
  */
  asset.smartMeter = assetGeneralData[0];
  asset.owner = assetGeneralData[1];
  asset.operationalSince = assetGeneralData[2];
  asset.lastSmartMeterReadWh = assetGeneralData[3];
  asset.active = assetGeneralData[4];
  asset.lastSmartMeterReadFileHash = assetGeneralData[5];
  /*
  assetProducingProperties returns(
      uint assetType,
      uint capacityWh,
      uint certificatesCreatedForWh,
      uint lastSmartMeterCO2OffsetRead,
      uint cO2UsedForCertificate,
      uint registryCompliance,
      bytes32 otherGreenAttributes,
      bytes32 typeOfPublicSupport
  )
  */
  asset.type = assetProducingProperties[0];
  asset.capacity = assetProducingProperties[1];
  asset.certificatesCreatedForWh = assetProducingProperties[2];
  asset.lastSmartMeterCO2OffsetRead = assetProducingProperties[3];
  asset.cO2UsedForCertificate = assetProducingProperties[4];
  asset.registryCompliance = assetProducingProperties[5];
  asset.otherGreenAttributes = assetProducingProperties[6];
  asset.typeOfPublicSupport = assetProducingProperties[7];

  asset.country = assetLocation[0];
  asset.region = assetLocation[1];
  asset.zip = assetLocation[2];
  asset.city = assetLocation[3];
  asset.street = assetLocation[4];
  asset.houseNumber = assetLocation[5];
  asset.gpsLatitude = assetLocation[6];
  asset.gpsLongitude = assetLocation[7];

  res.json(asset);
}
exports.getAssetFull = getAssetFull;

const getProduction = async (req, res) =>  {

  const sender = req.query._sender;        //address
  const prodAssetId = req.params.assetId;      //uint
  const withMatching = req.query.withMatching;      //boolean
  const start = req.query.start;        //YYYY-MM-DD
  const end = req.query.end;        //YYYY-MM-DD


  //const myAssets = await offchainServices.getProducingAssetsDb();

  let assetsUrl = process.env.API_URL + '/producing-assets-offchain-db';

  const myAssets =  await fetch(assetsUrl, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      return result
    })
    .catch(error => {
      console.log(error);
    });

  ///producing-assets-offchain-db
  console.log(myAssets);

  //const myAssetsOld = await offchainServices.getAssets();

  let prodAsset = myAssets.find(pA => parseInt(pA.id, 10) === parseInt(prodAssetId, 10));
  prodAsset = prodAsset.isFake ? myAssets.find(pA => parseInt(pA.id, 10) === parseInt(prodAsset.duplicateOfAssetId, 10)) : prodAsset

  const minDateRange = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('X')
  const maxDateRange = end ? moment(end, 'YYYY-MM-DD').add(1, "days").set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

  let allDates = offchainServices.getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

  //Change the format of the date to display it in the charts
  allDates = allDates.map(date => {
    return moment(date).format('YYYY-MM-DD')
  })

  //Just to be sure to have the dates in the correct order
  allDates.sort((dateA, dateB) => {
    const unixDateA =  moment(dateA, 'YYYY-MM-DD').format('X')
    const unixDateB =  moment(dateB, 'YYYY-MM-DD').format('X')
    return unixDateA - unixDateB;
  });

  let url = process.env.API_URL + '/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  const certificates = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in getConsumingSiteCalculatedDataOffchain : ");
      console.log(error);
    });



  /////////////////////
  let assetCertificates = certificates.filter(certificate => {
    return parseInt(certificate._assetId, 10) === parseInt(prodAsset.id, 10) && !certificate._retired
  })

  let matchedCertificates = assetCertificates.filter(certificate => {
    return certificate._owner !== "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747"
  })

  let productionData = [];
  let matchedProductionData = [];

  console.log('assetCertificates size : '+assetCertificates.length)
  console.log('matchedCertificates size : '+matchedCertificates.length)

  allDates.map(date => {
    //Find the certificates for that day
    const assetCertificatesForDay = assetCertificates.filter(c => {
      const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
      return date === certifDate
    })

    //Let's calculate the quantity of energy produced with the sum of this certificates
    let energyProducedForDay = 0;
    assetCertificatesForDay.map(c => {
      energyProducedForDay += parseFloat(c._powerInW)/1000;
    })
    productionData.push(energyProducedForDay);

    //Do the same for the energy matched
    const assetMatchedCertificatesForDay = matchedCertificates.filter(c => {
      const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
      return date === certifDate
    })
    let energyMatchedForDay = 0;
    assetMatchedCertificatesForDay.map(c => {
      energyMatchedForDay += parseFloat(c._powerInW)/1000;
    })
    matchedProductionData.push(energyMatchedForDay);
  })

  res.json({
    asset:prodAsset,
    production: productionData,
    matchedProduction: matchedProductionData,
    dates:allDates
  });

}
exports.getProduction = getProduction;
