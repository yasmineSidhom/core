const fs = require('fs');
const path = require('path');
const moment = require('moment');
const fetch = require('node-fetch');
const cron = require('node-cron');
const exec = require('child_process').exec;

const co2 = JSON.parse(fs.readFileSync(require.resolve('../db/json/co2.json'), 'utf8'))


const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

const matching = (consumingSitesMatching, producingAssetsMatching, nbMaxIterations, currentIteration) => {
  console.log('******* entering matching function *********')
  //Enrich the data of the producing assets : set the number of consuming sites related to this asset
  let consumingSitesData = JSON.parse(fs.readFileSync(require.resolve('../db/json/consumingSites.json'), 'utf8'))
  let producingAssetsData = JSON.parse(fs.readFileSync(require.resolve('../db/json/producingAssets.json'), 'utf8'))
  console.log('matching, iteration '+currentIteration+'/'+nbMaxIterations)
  producingAssetsMatchingDetailed = producingAssetsMatching.map(producingAsset => {
    if(currentIteration === 0) {
      producingAsset.productionAvailable = producingAsset.production;
    }
    /*console.log('  '+producingAsset.name)
    console.log('    production :' + Number.parseFloat(producingAsset.production/1000).toPrecision(4)+'MWh')
    console.log('    productionAvailable :' + Number.parseFloat(producingAsset.productionAvailable/1000).toPrecision(4)+'MWh')
    console.log('    energyMatched :' + Number.parseFloat(producingAsset.energyMatched/1000).toPrecision(4)+'MWh')
    */

    return producingAsset;
  })

  consumingSitesMatchingDetailed = consumingSitesMatching.map(site => {
    //Sort the producings assets by priority
    site.producingAssets.sort((assetA, assetB) => {
      return assetB.priority - assetA.priority;
    });


    /*console.log('')
    console.log('-------------------------')
    console.log('  Consumption site : '+site.name)
    console.log('  Conso: '+Number.parseFloat(site.consumption/1000).toPrecision(4))
    console.log('  Energy matched: '+Number.parseFloat(site.energyMatched/1000).toPrecision(4))*/


    //For this consuming site, we can now run the first attempt to match the energy with the renewables assets regarding the priority
    //site.energyMatched = 0;
    site.producingAssets = site.producingAssets.map( asset => {

      //For this asset (the producingAssets are sorted by priority), let's calculate the energy matched for the site
      let prodAsset = producingAssetsMatching.find(producingAsset => {
        return producingAsset.id === asset.id //get the asset production
      })
      /*console.log('   Production site : '+prodAsset.name)*/

      let energyProducedReservedForSite = prodAsset.productionAvailable  * asset.percentage / 100;
      let energyNotMatchedForSite = site.consumption - site.energyMatched;
      asset.energyMatched = (asset.energyMatched && currentIteration !== 0) ? asset.energyMatched : 0;

      if(energyNotMatchedForSite > 0){
        if( energyProducedReservedForSite < energyNotMatchedForSite ) {
          site.energyMatched      += energyProducedReservedForSite;
          prodAsset.energyMatched += energyProducedReservedForSite;
          asset.energyMatched += energyProducedReservedForSite;
        } else {
          site.energyMatched += energyNotMatchedForSite;
          prodAsset.energyMatched += energyNotMatchedForSite;
          asset.energyMatched += energyNotMatchedForSite;
        }
      }
      /*

      console.log('     production : '+Number.parseFloat(prodAsset.production/1000).toPrecision(4)+'MWh')
      console.log('     productionAvailable : '+Number.parseFloat(prodAsset.productionAvailable/1000).toPrecision(4)+'MWh')
      console.log('     prodAsset.energyMatched : '+Number.parseFloat(prodAsset.energyMatched/1000).toPrecision(4)+'MWh')
      console.log('     energyProducedReservedForSite : '+asset.percentage+'% '+Number.parseFloat(energyProducedReservedForSite/1000).toPrecision(4)+'MWh')
      console.log('     energyNotMatchedForSite : '+Number.parseFloat(energyNotMatchedForSite/1000).toPrecision(4)+'MWh')
      */


      producingAssetsMatching = producingAssetsMatching.map( loopAsset => {
        if(prodAsset.id === loopAsset.id){
          return prodAsset
        } else {
          return loopAsset
        }
      })

      return asset;
    })
    return site;
  });




  /*console.log('')
  console.log('')
  console.log('consumingSitesMatchingDetailed')*/
  consumingSitesMatchingDetailed.map(consumingSiteMatching => {
    /*console.log('')
    console.log('  ' + consumingSiteMatching.smireName+' ' + parseInt(consumingSiteMatching.consumptionPercentage*100)+'%')
    console.log('    consumption : ' + parseInt(consumingSiteMatching.consumption/100)/10+'MWh')
    console.log('    energyMatched :' + parseInt(consumingSiteMatching.energyMatched/100)/10+'MWh')
    */
    consumingSiteMatching.producingAssets.map(producingAsset => {
      let producingAssetDetails = producingAssetsData.find(pAsset => {
        return pAsset.id === producingAsset.id //get the asset production
      })
      //console.log('        '+producingAssetDetails.name+' - energyMatched : ' + parseInt(producingAsset.energyMatched/1000)+'MWh '+producingAsset.percentage+'%')


    })
  })


  //Let's remove the percentage for the consuming sites that have all of their energy already matched
  const refreshedConsumingSitesMatching = consumingSitesMatchingDetailed.map( site => {
    if(site.consumption - site.energyMatched === 0) {
      //Put the percentage of all of its prod assets to 0
      const producingAssetsAtNullPercentage = site.producingAssets.map(prodAsset => {
        prodAsset.percentage = 0;
        return prodAsset;
      })
      site.producingAssets = producingAssetsAtNullPercentage
    }
    return site;
  })

  /*
  console.log('')
  console.log('')
  console.log('    refreshedConsumingSitesMatching')
  console.log(refreshedConsumingSitesMatching)
  */

  //Let's update the percentage of each producing assets for each consuming site
  //As we have removed some sites, the proportions need to be updated
  const updatedConsumingSitesMatching = refreshedConsumingSitesMatching.map( site => {
    const newSiteProducingAssets = site.producingAssets.map(prodAsset => {

      const oldPercentage = prodAsset.percentage;
      let totalForDivision = 0;

      //Find this asset among all the refreshed consuming sites (without the one with energy already matched )
      refreshedConsumingSitesMatching.map( siteCons => {
        siteCons.producingAssets.map(prodAssetBis => {
          if(prodAssetBis.id === prodAsset.id) {
            totalForDivision+= prodAssetBis.percentage;
          }
        })

      })

      let newPercentage = 100;
      if( totalForDivision > 0) {
        newPercentage = 100*oldPercentage/totalForDivision;
      } else {
        newPercentage = 0;
        //console.error('NtoN Error during totalForDivision...', totalForDivision, oldPercentage)
      }

      prodAsset.percentage = newPercentage;
      return prodAsset;

    })

    site.producingAssets = newSiteProducingAssets;
    return site;
  })

  //Update the available production for the next round
  producingAssetsMatchingDetailed2 = producingAssetsMatchingDetailed.map( pAsset => {
    pAsset.productionAvailable =  pAsset.production - pAsset.energyMatched
    //console.log('   Prod asset '+pAsset.name+' prod ='+pAsset.production+' matched ='+pAsset.energyMatched+' )')
    return pAsset
  })

  /*console.log('')
  console.log('')
  console.log('    producingAssetsMatchingDetailed2')
  console.log(producingAssetsMatchingDetailed2)
  */


  /*console.log('')
  console.log('')
  console.log('    updatedConsumingSitesMatching')
  console.log(updatedConsumingSitesMatching)
  */
  //Get the total needed equivalent to 100%
  //Apply this total to each consuming site
  let energyWaitingToMatch = 0;

  updatedConsumingSitesMatching.map( cs => {
    energyWaitingToMatch += (cs.consumption - cs.energyMatched)
  })

  let energyStillAvailable = 0;

  producingAssetsMatchingDetailed2.map( p => {
    energyStillAvailable += (p.production - p.energyMatched)
    //console.log('Adding '+(p.production - p.energyMatched)+' from asset '+p.name+' ( prod ='+p.production+' , matched ='+p.energyMatched+' )')
  })

  let iterateAgain = true;
  if(currentIteration >= nbMaxIterations || energyWaitingToMatch === 0 || energyStillAvailable === 0) {
    iterateAgain = false;
  }
  //console.log('energyWaitingToMatch = '+energyWaitingToMatch)
  //console.log('energyStillAvailable = '+energyStillAvailable)

  if(iterateAgain) {
    return matching(updatedConsumingSitesMatching, producingAssetsMatchingDetailed2, nbMaxIterations, currentIteration + 1)
  } else {
    return {consumingSites:updatedConsumingSitesMatching, producingAssets:producingAssetsMatchingDetailed2}
  }
}

const createCertificate = async (assetId, powerInW, productionDate) => {

  const data={
    _assetId:parseInt(assetId, 10),
    _powerInW: parseInt(powerInW, 10),
    _productionDate: parseInt(productionDate, 10)
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      console.log('New certificate created for asset id = '+assetId+' powerInW = '+powerInW+' _certificateId = '+data.logs[0]["result"]._certificateId);
      return data;
    })
    .catch(error => error);
}

const setCertificateTxHash = async (certificateId, txHash) => {

  console.log('setCertificateTxHash Start');

  const data = {
    _txHash: txHash

  };
  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/certificates/"+certificateId+"/txhash?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async result => {
      console.log('setCertificateTxHash success');
    })
    .catch(error => {
      console.log('setCertificateTxHash error');
      console.log(error);
    });
}

const transferCertificate = async (certificateId, newOwner, newAssetId) => {


  const data = {
    _newOwner: newOwner,
    _newConsumptionAssetId: newAssetId
  };
  console.log('transferCertificate Start with newOwner = '+newOwner +' newConsumptionAssetId = '+newAssetId);
  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/certificates/"+certificateId+"/owner?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async result => {
      console.log('transferCertificate success');
    })
    .catch(error => {
      console.log('transferCertificate error');
      console.log(error);
    });
}

const getAssetsProducingLength = async () => {

  let url = process.env.API_URL + '/producing-assets-list-length?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async assetProducingListLength => {
      console.log('assetProducingListLength = '+ assetProducingListLength);
      return assetProducingListLength;
    })
    .catch(error => {
      console.log('An error occured during getAssetsProducingLength : ', error);
      return false;
    });
}

const getAllCertificatesForDay = async (day) => {

	return await fetch(process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_date="+day, {
		method: 'GET',
		headers: {
	  	'Content-Type': 'application/json; charset=utf-8'
		}
	})
	.then(handleErrors)
	.then(async res => res.json())
	.then(async certificates => {
	  return certificates;
	})
	.catch(error => {
	  console.log(error);
	});
}

const getAssetProducing = async (assetId) =>  {

  const assetProducingGeneralData = await getAssetProducingGeneralData(assetId);
  const assetProducingProperties = await getAssetProducingProperties(assetId);
  const assetLocation = await getAssetProducingLocation(assetId);

  const asset = {};
  asset.id= assetId;
  asset.smartMeter= assetProducingGeneralData[0];
  asset.owner= assetProducingGeneralData[1];
  asset.operationalSince=assetProducingGeneralData[2];
  asset.lastSmartMeterReadWh= assetProducingGeneralData[3];
  asset.active = assetProducingGeneralData[4];
  asset.lastSmartMeterReadFileHash = assetProducingGeneralData[5];

  asset.type=assetProducingProperties[0];
  asset.capacity=assetProducingProperties[1];
  asset.certificatesCreatedForWh=assetProducingProperties[2];
  asset.lastSmartMeterCO2OffsetRead=assetProducingProperties[3];
  asset.cO2UsedForCertificate=assetProducingProperties[4];
  asset.registryCompliance=assetProducingProperties[5];
  asset.otherGreenAttributes=assetProducingProperties[6];
  asset.typeOfPublicSupport=assetProducingProperties[7];

  asset.country= assetLocation[0];
  asset.region= assetLocation[1];
  asset.zip= assetLocation[2];
  asset.city= assetLocation[3];
  asset.street= assetLocation[4];
  asset.houseNumber= assetLocation[5];
  asset.gpsLatitude= assetLocation[6];
  asset.gpsLongitude= assetLocation[7];

  return(asset)
}

const getAssetProducingGeneralData = async (assetId) =>  {

  return await fetch(process.env.API_URL + "/producing-assets/"+assetId+"?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log('getAssetProducingGeneralData');
      return data;
    })
    .catch(error => {
      console.log(error);
    });
}

const getAssetProducingProperties = async (assetId) =>  {


  return await fetch(process.env.API_URL + "/producing-assets/"+assetId+"/properties?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log('getAssetProducingProperties');
      return data;
    })
    .catch(error => {
      console.log(error);
    });
}

const getAssetProducingLocation = async (assetId) => {

  return await fetch(process.env.API_URL + "/producing-assets/"+assetId+"/location?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      console.log('getAssetProducingLocation');
      return data;
    })
    .catch(error => {
      console.log(error);
    });
}

const saveProducingSmartMeterRead = async (
  assetId,
  newMeterRead,
  smartMeterDown,
  lastSmartMeterReadFileHash,
  cO2OffsetMeterRead,
  cO2OffsetServiceDown,
  sender
  ) => {

  var data = {
    _newMeterRead:newMeterRead,
    _smartMeterDown:smartMeterDown,
    _lastSmartMeterReadFileHash:lastSmartMeterReadFileHash,
    _CO2OffsetMeterRead:cO2OffsetMeterRead,
    _CO2OffsetServiceDown:cO2OffsetServiceDown
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/producing-assets/"+assetId+"/smartmeter/read?_sender="+sender, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => {
      return error;
    });
}


const getConsumingSitesOffchain = async () => {

  let url = process.env.API_URL + '/consuming-assets-offchain';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      //console.log(result)
      return result.consumingSites
    })
    .catch(error => {
      console.log(error);
    });
}

const getProducingAssetsOffchain = async () => {

  let url = process.env.API_URL + '/producing-assets-offchain';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      //console.log(result)
      return result.producingAssets
    })
    .catch(error => {
      console.log(error);
    });
}

const getConsumingSitesDb = async () => {

  let url = process.env.API_URL + '/consuming-assets-offchain-db';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      return result
    })
    .catch(error => {
      console.log(error);
    });
}

const getProducingAssetsDb = async () => {

  let url = process.env.API_URL + '/producing-assets-offchain-db';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      return result
    })
    .catch(error => {
      console.log(error);
    });
}




const getAllConsumingSitesConsumptionOffchain = async (start, end) => {

  let url = process.env.API_URL + '/consumption-offchain?start='+start+'&end='+end;
  /*
  {
    "data": {
        "index": [
            "2018-09-24T00:00:00+02:00",
            "2018-09-25T00:00:00+02:00",
            "2018-09-26T00:00:00+02:00"
        ],
        "data": [
            [
                358878.3333333332,
                358878.3333333332,
                57162
            ],
            [
                373002.6666666667,
                373002.6666666667,
                59039
            ],
            [
                373536.8333333333,
                373536.8333333333,
                60453
            ]
        ],
        "columns": [
            "lida_rte",
            "lida_rte",
            "laboulangere"
        ],
        "labels": [
            "Lida",
            "My Green Company HQ",
            "Pain Concept"
        ],
        "ids": [
            0,
            1,
            2
        ]
    }
}
  */
  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}

const getAllProducingAssestProductionOffchain = async (start, end) => {

  let url = process.env.API_URL + '/production-offchain?start='+start+'&end='+end;
  console.log("getAllProducingAssestProductionOffchain url", url);
  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}

const getAllConsumingSitesConsumptionDb = async (start, end) => {

  let url = process.env.API_URL + '/consumption-db?start='+start+'&end='+end;

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}

const getAllProducingAssestProductionDb = async (start, end) => {

  let url = process.env.API_URL + '/production-db?start='+start+'&end='+end;
  console.log("getAllProducingAssestProductionDb url", url);
  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}



const getConsumingSiteCalculatedDataOffchain = async (asset, start, end) => {

  let url = process.env.API_URL + '/consuming-assets-offchain/'+asset.id+'/graph-data?start='+start+'&end='+end;

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      return result.data
    })
    .catch(error => {
      console.log(error);
    });
}



const getConsumingSiteTimeSeriesOffchain = async (consumingAsset, start, end) => {

  let url = process.env.API_URL + '/consuming-assets-offchain/'+consumingAsset.id+'/consumption?withProduction=true&start='+start+'&end='+end;

  return await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log(error);
    });
}

exports.teoMatching = async (req, res) => {

  const nbRetry = parseInt(req.body.nbRetry, 10);
  const date = req.body.date;
  const forceIfExistingCertificates = req.body.forceIfExistingCertificates === true || req.body.forceIfExistingCertificates === "true" ? true : false;
  const generateCertificates = req.body.generateCertificates === true || req.body.generateCertificates === "true" ? true : false;

  if(isNaN(nbRetry)) {
    res.status(404).send({
      error:"Bad nbRetry parameter",
    });
    res.end();
    throw Error ("Bad nbRetry parameter");
  } else if(!date) {
    res.status(400).send({
      error:"Empty date parameter",
    });
    res.end();
    throw Error ("Empty date parameter");
  } else if(!moment(date, 'YYYY-MM-DD').isValid()) {
    res.status(400).send({
      error:"Bad date format (YYYY-MM-DD)",
    });
    res.end();
    throw Error ("Bad date format (YYYY-MM-DD)");
  } else {
    try {
      //Get all the consuming assets
      console.log('teoMatching starting...');

      const certificatesForDate = await fetch(process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_date="+date, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      })
        .then(handleErrors)
        .then(async res => {
          return res.json();
        })
        .then(result => {
          console.log(result)
          return result;
        })

      if(generateCertificates && !forceIfExistingCertificates && certificatesForDate.length > 0) {
        res.status(400).send({
          error:certificatesForDate.length+" certificates have already been generated for the date "+date,
        });
        res.end();
        throw Error (certificatesForDate.length+" certificates have already been generated for the date "+date);
      }
      if(!generateCertificates && certificatesForDate.length > 0) {
        console.log(certificatesForDate.length+" certificates have already been generated for the date "+date)
      }

      const consumingSitesNotFilteredOld = await getConsumingSitesOffchain();
      const consumingSitesNotFiltered = await getConsumingSitesDb();
      const producingAssetsNotFilteredOld = await getProducingAssetsOffchain();
      const producingAssetsNotFiltered = await getProducingAssetsDb();

      //console.log('consumingSitesDbNotFiltered')
      //console.log(consumingSitesNotFiltered)
      console.log('producingAssetsDbNotFiltered')
      console.log(producingAssetsNotFiltered)

      /*res.json({
        producingAssets: producingAssetsDbNotFiltered,
        producingAssetsOld: producingAssetsNotFilteredOld,
        consumingAssets: consumingSitesDbNotFiltered,
        consumingAssetsOld: consumingSitesNotFilteredOld
      });

      return;*/

      //Use only the real assets, remove the fake one
      const consumingSites = consumingSitesNotFiltered.filter(c => {
        return c.isFake !== true
      });
      const producingAssets = producingAssetsNotFiltered.filter(p => {
        return p.isFake !== true
      });

      //If no date in arguments, use the previous day as start and current day as end
      const start =  date ? date : moment().subtract(1, 'days').format('YYYY-MM-DD')
      const end   =  date ? moment(date, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')
      const productionDate = moment(start, 'YYYY-MM-DD').add(2, 'hours').format('X');
      const productionDateTxt = moment.unix(productionDate).format('dddd, MMMM Do, YYYY h:mm:ss A')

      console.log("teoMatching - getting consumption for date : ", productionDateTxt, start, end)

      const sitesConsumption = await getAllConsumingSitesConsumptionDb(start, end);

      const assetsProduction = await getAllProducingAssestProductionDb(start, end);
      console.log("assetsProduction", assetsProduction.columns, assetsProduction.data);
      console.log("sitesConsumption", sitesConsumption.labels, sitesConsumption.data);

      let consSitesWithConsumption = consumingSites.map(site => {
        //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
        //reqSiteTimeSeries2
        let siteIndex = null;

        let consumptionPercentage = site.consumptionPercentage;
        if(!consumptionPercentage) {
          consumptionPercentage = 1;
        }

        console.log('sitesConsumption')
        console.log(sitesConsumption)

        sitesConsumption.ids.map((consSiteId, idx) => {
          if(consSiteId === site.id) {
            siteIndex = idx;
          }
        })

        site.consumption = sitesConsumption.data[0][siteIndex]*consumptionPercentage*1000;//Convert to Wh in the blockchain
        if(isNaN(site.consumption)) {
          //TODO manage this case
          site.consumption = 0
          throw Error ("Consumption is NaN for asset Id "+ site.name +" - Id "+site.id);
        }

        site.energyMatched = 0;

        return site;
      })

      let consSitesWithConsumptionOriginal = JSON.parse(JSON.stringify(consumingSites))

      let producingAssetsWithProduction = producingAssets.map(prodAsset => {
        //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
        let prodAssetIndex = null;

        assetsProduction.ids.map((prodSiteId, idx) => {
          if(prodSiteId === prodAsset.id) {
            prodAssetIndex = idx;
          }
        })
        console.log(JSON.stringify(assetsProduction))

        prodAsset.production = assetsProduction.data[0][prodAssetIndex]*1000;//Convert to Wh in the blockchain
        if(isNaN(prodAsset.production)) {
          //TODO manage this case
          prodAsset.production = 0
          //throw Error ("Production is NaN for asset "+ prodAsset.name +" - Id "+prodAsset.id);
        }
        prodAsset.energyMatched = 0;
        return prodAsset;
      })

      const matchingResult = matching(consSitesWithConsumption, producingAssetsWithProduction, nbRetry, 0);
      console.log('teoMatching matching result obtained ⚠️⚠️⚠️⚠️⚠️⚠️');

      let messageText = '✅ Matching finished without error\n';
      messageText += 'Results for day '+productionDateTxt+'\n';
      messageText += generateCertificates ? '(has generated certificates)\n' : '(no certificate generated)\n';

      matchingResult.consumingSites.map(async consumingSite => {


        messageText += '\n';
        console.log("    "+consumingSite.name+" matching ");
        messageText += "    *"+consumingSite.name+" matching* \n";

        console.log("        Consumption = "+consumingSite.consumption);
        messageText += "        Consumption = "+  Math.round(consumingSite.consumption)+" kWh \n";

        console.log("        Energy matched = "+consumingSite.energyMatched);
        let displayedPercentage = '';
        if(!consumingSite.consumption) {
          //A consumption which is 0 is suspect, just add a warning
          displayedPercentage = '⚠️ (0%)'
        } else {
          let energyMatchedPercentage = parseInt(100*consumingSite.energyMatched/consumingSite.consumption);

          //The consumption is not fully matched, this could be suspect, need to check
          if(energyMatchedPercentage < 100 ) {
            displayedPercentage = '(⚠️'+ energyMatchedPercentage +'%)'
          } else {
            displayedPercentage = '('+ energyMatchedPercentage +'%)'
          }
        }

        consumingSite.consumption ? "("+parseInt(100*consumingSite.energyMatched/consumingSite.consumption)+"%)" : '⚠️ (0%)'
        messageText += "        Energy matched = "+Math.round(consumingSite.energyMatched)+" kWh "+ displayedPercentage +"\n";

        console.log("        Energy mix consumption = "+(consumingSite.consumption-consumingSite.energyMatched));
        messageText += "        Energy mix consumption = "+Math.round(consumingSite.consumption-consumingSite.energyMatched)+" kWh \n";

        consumingSite.producingAssets.map(pA => {
          const currentProdAssetWithConsumption = producingAssetsWithProduction.find(pAWC => {
            return pAWC.id === pA.id;
          })

          const initialConsumptionSite = consSitesWithConsumptionOriginal.find(cS => {
            return consumingSite.id === cS.id;
          })

          const initialProdSite = initialConsumptionSite.producingAssets.find(pS => {
            return pS.id === pA.id;
          })

          //console.log('******  initialProdSite ', initialProdSite)
          console.log("            "+currentProdAssetWithConsumption.displayedName+" production = "+currentProdAssetWithConsumption.production);
          messageText += "            "+currentProdAssetWithConsumption.displayedName+" production = "+currentProdAssetWithConsumption.production+" kWh \n";

          console.log("            "+currentProdAssetWithConsumption.displayedName+" energyMatched = "+pA.energyMatched);
          messageText += "            "+currentProdAssetWithConsumption.displayedName+" energyMatched = "+pA.energyMatched+" kWh ("+initialProdSite.percentage+'% minMatched = '+(initialProdSite.percentage/100)*currentProdAssetWithConsumption.production  +" kWh) \n";
        })
      })


      console.log('teoMatching -  Entering the certificate generation phase... ');


      console.log('Update the smart meter of each producing asset :')

      //Update the smart meter of each producing asset :
      for (let pA of matchingResult.producingAssets) {

        const currentProducingAsset = await getAssetProducing(pA.id);
        console.log("teoMatching - Asset retrieved : "+pA.id+' '+pA.name);
        //pA : name, displayedName, production, energyMatched, productionAvailable
        //currentProducingAsset : smartMeter, owner, lastSmartMeterReadWh, lastSmartMeterReadFileHash,
        //certificatesCreatedForWh, cO2UsedForCertificate


        const prodAssetLastMeterRead = parseInt(currentProducingAsset.lastSmartMeterReadWh, 10);
        const prodAssetTotalProduction = parseInt(pA.production*1000, 10);//kWh to Wh

        const prodAssetLastMeterReadFileHash = Date.now().toString()+currentProducingAsset.name;
        const prodAssetNewMeterRead = prodAssetLastMeterRead + prodAssetTotalProduction;


        if(generateCertificates) {
          console.log(" ⚠️  saveProducingSmartMeterRead will launch");
          const saveProducingSmartMeterReadResult = await saveProducingSmartMeterRead (
            currentProducingAsset.id,         //assetProducingId
            prodAssetNewMeterRead,            //newMeterRead
            false,                            //smartMeterDown
            prodAssetLastMeterReadFileHash,   //lastSmartMeterReadFileHash
            0,                                //cO2OffsetMeterRead
            false,                            //cO2OffsetServiceDown
            currentProducingAsset.smartMeter  //sender, must be the address of the asset's smart meter as '0x00c3B44DBeA6f103dd30D7e90D59787175FeA185'
          );
        }

        //For each consuming site, create the certificates related to the energy matched with THIS production asset
        for (let cA of matchingResult.consumingSites) {

           //let result = await getData(num);
           //console.log(result);

           const currentProdAssetCertif = cA.producingAssets.find( pAsset => {
             return pAsset.id === pA.id
           })

           if(currentProdAssetCertif) {

             if(currentProdAssetCertif.energyMatched > 0) {

               let matchingProdAssetCertifData = {
                 assetName: pA.name,
                 assetId: pA.id,
                 production: Math.round(currentProdAssetCertif.energyMatched)+"/"+Math.round(pA.production),
                 productionDate
               }

               if(generateCertificates) {

                 console.log(" ⚠️  createCertificate will launch");
                 matchingProdAssetCertifData = await createCertificate(pA.id, currentProdAssetCertif.energyMatched, productionDate);
               } else {
                 console.log(" ⚠️  createCertificate will launch with currentProdAssetCertif.energyMatched = ", currentProdAssetCertif.energyMatched, parseInt(currentProdAssetCertif.energyMatched, 10), currentProdAssetCertif.energyMatched - parseInt(currentProdAssetCertif.energyMatched, 10));
               }

               console.log("teoMatching - createCertificate params ⚠️⚠️⚠️⚠️⚠️")
               console.log(matchingProdAssetCertifData)

               let prodAssetCertificateId = matchingProdAssetCertifData.logs ? matchingProdAssetCertifData.logs[0]["result"]._certificateId : 'unknown certifId in test mode';


               if(generateCertificates) {
                 console.log(" ⚠️  setCertificateTxHash will launch");
                 await setCertificateTxHash(prodAssetCertificateId, matchingProdAssetCertifData.logs[0].transactionHash);
               }

               console.log("teoMatching - matchingProduction transferCertificate start for prod asset "+pA.name+" consumption of "+cA.name+" ("+ cA.id+")");
               //newOwner is the client : the consuming asset
               //const newOwner;
               // Airproducts, La Boulangère ou Barilla blockchain address
               //const newOwner = '0x0051CcCBCFF51788dDa9614b12dcea0D57208C03'; // <-- this is Airproducts Address Account
               const newOwner = cA.owner;

                console.log("teoMatching - transferCertificate params")
                console.log({
                  certificateId: prodAssetCertificateId,
                  certificateOwner: newOwner,
                  consumtionassetId: cA.id
                })
               if(generateCertificates) {
                 console.log(" ⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️  transferCertificate will launch CertificateId = "+prodAssetCertificateId+ " new Owner = "+newOwner+ " consumptionAssetId = "+cA.id);
                 console.log(" ⚠️  Air Products = 0x0051CcCBCFF51788dDa9614b12dcea0D57208C03 ");
                 console.log(" ⚠️  La Boulangère = 0x00d0F0e2AF12fec0C16c6D595a9554b99B6dE045 ");
                 //console.log(cA);


                 await transferCertificate(prodAssetCertificateId, newOwner, cA.id)
               }

               //console.log("weechainMatchingV3 - matchingProduction transferCertificate finished for prod asset "+pA.name+" consumption of "+cA.name);

             }
           }

        }


        //create the certificate related to the energy NOT matched with THIS production asset

        const residualProduction = pA.production - pA.energyMatched;

        if(residualProduction > 0) {
          //console.log("weechainMatchingV3 - residualProduction createCertificate start for asset "+pA.name);
          let residualProdAssetCertifData = {
            prodAssetName: pA.name,
            prodAssetId: pA.id,
            residualProduction: Math.round(residualProduction)+"/"+Math.round(pA.production),
            productionDate
          }

          if(generateCertificates) {
            console.log(" ⚠️  createCertificate will launch (residual)");
            residualProdAssetCertifData = await createCertificate(pA.id, residualProduction, productionDate);
          }

          console.log("teoMatching - createCertificate params ⚠️⚠️⚠️⚠️⚠️");

          console.log(residualProdAssetCertifData);

          const prodAssetResCertificateId = residualProdAssetCertifData.logs ? residualProdAssetCertifData.logs[0]["result"]._certificateId : 'unknown certifId in test mode';
          const prodAssetResCertificateTxHash = residualProdAssetCertifData.logs ? residualProdAssetCertifData.logs[0].transactionHash : 'unknown TxHash in test mode';

          if(generateCertificates) {
            console.log(" ⚠️  setCertificateTxHash will launch (residual)");
            await setCertificateTxHash(prodAssetResCertificateId, residualProdAssetCertifData.logs[0].transactionHash);
          }
          console.log("teoMatching - residualProduction createCertificate finished for asset "+pA.name);
        }
      }


      const generatedCertificates = await getAllCertificatesForDay(date);
      var results = await Promise.resolve(generatedCertificates);

      res.json(results);

      if(process.env.BUILD_FOLDER === 'build') {
        sendSlackBotMessage(messageText);
      } else {
        sendSlackBotMessageTest(messageText);
      }


      console.log('teoMatching finished ');
    }
    catch(error) {
      console.error(error);
      let errorMessage = "❌ An error occured during matching for date "+ date +" : \n"
      errorMessage += "```"+error.message+"\n";
      errorMessage += error.stack ? error.stack+"```" : "```";

      if(process.env.BUILD_FOLDER === 'build') {
        sendSlackBotMessage(errorMessage);
      } else {
        sendSlackBotMessageTest(errorMessage);
      }

      res.status(400).send({
        message:error.message
      });
      res.end();
    }
  }
}

exports.weechainMatching = async (req, res) => {

  const nbRetry = parseInt(req.body.nbRetry, 10);
  const date = req.body.date;
  const forceIfExistingCertificates = req.body.forceIfExistingCertificates === true || req.body.forceIfExistingCertificates === "true" ? true : false;
  const generateCertificates = req.body.generateCertificates === true || req.body.generateCertificates === "true" ? true : false;

  if(isNaN(nbRetry)) {
    res.status(404).send({
      error:"Bad nbRetry parameter",
    });
    res.end();
    throw Error ("Bad nbRetry parameter");
  } else if(!date) {
    res.status(400).send({
      error:"Empty date parameter",
    });
    res.end();
    throw Error ("Empty date parameter");
  } else if(!moment(date, 'YYYY-MM-DD').isValid()) {
    res.status(400).send({
      error:"Bad date format (YYYY-MM-DD)",
    });
    res.end();
    throw Error ("Bad date format (YYYY-MM-DD)");
  } else {
    try {
      //Get all the consuming assets
      console.log('weechainMatchingV3 starting...');

      const certificatesForDate = await fetch(process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_date="+date, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }
      })
        .then(handleErrors)
        .then(async res => {
          return res.json();
        })
        .then(result => {
          console.log(result)
          return result;
        })

      if(generateCertificates && !forceIfExistingCertificates && certificatesForDate.length > 0) {
        res.status(400).send({
          error:certificatesForDate.length+" certificates have already been generated for the date "+date,
        });
        res.end();
        console.log(certificatesForDate)
        throw Error (certificatesForDate.length+" certificates have already been generated for the date "+date);
      }
      if(!generateCertificates && certificatesForDate.length > 0) {
        console.log(certificatesForDate.length+" certificates have already been generated for the date "+date)
      }

      const consumingSitesNotFiltered = await getConsumingSitesOffchain();
      const producingAssetsNotFiltered = await getProducingAssetsOffchain();


      //Use only the real assets, remove the fake one
      const consumingSites = consumingSitesNotFiltered.filter(c => {
        return c.isFake !== true
      });
      const producingAssets = producingAssetsNotFiltered.filter(p => {
        return p.isFake !== true
      });

      //If no date in arguments, use the previous day as start and current day as end
      const start =  date ? date : moment().subtract(1, 'days').format('YYYY-MM-DD')
      const end   =  date ? moment(date, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')
      const productionDate = moment(start, 'YYYY-MM-DD').add(2, 'hours').format('X');
      const productionDateTxt = moment.unix(productionDate).format('dddd, MMMM Do, YYYY h:mm:ss A')

      console.log("weechainMatchingV3 - getting consumption for date : ", productionDateTxt, start, end)

      const sitesConsumption = await getAllConsumingSitesConsumptionOffchain(start, end);
      const assetsProduction = await getAllProducingAssestProductionOffchain(start, end);
      console.log("assetsProduction", assetsProduction.data.columns, assetsProduction.data.data);
      console.log("sitesConsumption", sitesConsumption.data.labels, sitesConsumption.data.data);

      let consSitesWithConsumption = consumingSites.map(site => {
        //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
        //reqSiteTimeSeries2
        let siteIndex = null;

        let consumptionPercentage = site.consumptionPercentage;
        if(!consumptionPercentage) {
          consumptionPercentage = 1;
        }

        sitesConsumption.data.ids.map((consSiteId, idx) => {
          if(consSiteId === site.id) {
            siteIndex = idx;
          }
        })

        site.consumption = sitesConsumption.data.data[0][siteIndex]*consumptionPercentage*1000;//Convert to Wh in the blockchain
        if(isNaN(site.consumption)) {
          //TODO manage this case
          site.consumption = 0
          throw Error ("Consumption is NaN for asset Id "+ site.name +" - Id "+site.id);
        }

        site.energyMatched = 0;

        return site;
      })

      let consSitesWithConsumptionOriginal = JSON.parse(JSON.stringify(consumingSites))
      let producingAssetsWithProduction = producingAssets.map(prodAsset => {
        //Here, we need the data for ALL sites, consumption AND production... TODO : find a better way
        let prodAssetIndex = null;

        assetsProduction.data.ids.map((prodSiteId, idx) => {
          if(prodSiteId === prodAsset.id) {
            prodAssetIndex = idx;
          }
        })

        console.log(JSON.stringify(assetsProduction))

        prodAsset.production = assetsProduction.data.data[0][prodAssetIndex]*1000;//Convert to Wh in the blockchain
        if(isNaN(prodAsset.production)) {
          //TODO manage this case
          prodAsset.production = 0
          throw Error ("Production is NaN for asset "+ prodAsset.name +" - Id "+prodAsset.id);
        }
        prodAsset.energyMatched = 0;
        return prodAsset;
      })

      const matchingResult = matching(consSitesWithConsumption, producingAssetsWithProduction, nbRetry, 0);
      console.log('weechainMatchingV3 matching result obtained ⚠️⚠️⚠️⚠️⚠️⚠️');

      let messageText = '✅ Matching finished without error\n';
      messageText += 'Results for day '+productionDateTxt+'\n';
      messageText += generateCertificates ? '(has generated certificates)\n' : '(no certificate generated)\n';

      matchingResult.consumingSites.map(async consumingSite => {


        messageText += '\n';
        console.log("    "+consumingSite.name+" matching ");
        messageText += "    *"+consumingSite.name+" matching* \n";

        console.log("        Consumption = "+consumingSite.consumption);
        messageText += "        Consumption = "+  Math.round(consumingSite.consumption)+" kWh \n";

        console.log("        Energy matched = "+consumingSite.energyMatched);
        let displayedPercentage = '';
        if(!consumingSite.consumption) {
          //A consumption which is 0 is suspect, just add a warning
          displayedPercentage = '⚠️ (0%)'
        } else {
          let energyMatchedPercentage = parseInt(100*consumingSite.energyMatched/consumingSite.consumption);

          //The consumption is not fully matched, this could be suspect, need to check
          if(energyMatchedPercentage < 100 ) {
            displayedPercentage = '(⚠️'+ energyMatchedPercentage +'%)'
          } else {
            displayedPercentage = '('+ energyMatchedPercentage +'%)'
          }
        }

        consumingSite.consumption ? "("+parseInt(100*consumingSite.energyMatched/consumingSite.consumption)+"%)" : '⚠️ (0%)'
        messageText += "        Energy matched = "+Math.round(consumingSite.energyMatched)+" kWh "+ displayedPercentage +"\n";

        console.log("        Energy mix consumption = "+(consumingSite.consumption-consumingSite.energyMatched));
        messageText += "        Energy mix consumption = "+Math.round(consumingSite.consumption-consumingSite.energyMatched)+" kWh \n";

        consumingSite.producingAssets.map(pA => {
          const currentProdAssetWithConsumption = producingAssetsWithProduction.find(pAWC => {
            return pAWC.id === pA.id;
          })

          const initialConsumptionSite = consSitesWithConsumptionOriginal.find(cS => {
            return consumingSite.id === cS.id;
          })

          const initialProdSite = initialConsumptionSite.producingAssets.find(pS => {
            return pS.id === pA.id;
          })

          //console.log('******  initialProdSite ', initialProdSite)
          console.log("            "+currentProdAssetWithConsumption.displayedName+" production = "+currentProdAssetWithConsumption.production);
          messageText += "            "+currentProdAssetWithConsumption.displayedName+" production = "+currentProdAssetWithConsumption.production+" kWh \n";

          console.log("            "+currentProdAssetWithConsumption.displayedName+" energyMatched = "+pA.energyMatched);
          messageText += "            "+currentProdAssetWithConsumption.displayedName+" energyMatched = "+pA.energyMatched+" kWh ("+initialProdSite.percentage+'% minMatched = '+(initialProdSite.percentage/100)*currentProdAssetWithConsumption.production  +" kWh) \n";
        })
      })


      console.log('weechainMatchingV3 -  Entering the certificate generation phase... ');


      console.log('Update the smart meter of each producing asset :')

      //Update the smart meter of each producing asset :
      for (let pA of matchingResult.producingAssets) {

        const currentProducingAsset = await getAssetProducing(pA.id);
        console.log("weechainMatchingV3 - Asset retrieved : "+pA.id+' '+pA.name);
        //pA : name, displayedName, production, energyMatched, productionAvailable
        //currentProducingAsset : smartMeter, owner, lastSmartMeterReadWh, lastSmartMeterReadFileHash,
        //certificatesCreatedForWh, cO2UsedForCertificate


        const prodAssetLastMeterRead = parseInt(currentProducingAsset.lastSmartMeterReadWh, 10);
        const prodAssetTotalProduction = parseInt(pA.production*1000, 10);//kWh to Wh

        const prodAssetLastMeterReadFileHash = Date.now().toString()+currentProducingAsset.name;
        const prodAssetNewMeterRead = prodAssetLastMeterRead + prodAssetTotalProduction;


        if(generateCertificates) {
          console.log(" ⚠️  saveProducingSmartMeterRead will launch");
          const saveProducingSmartMeterReadResult = await saveProducingSmartMeterRead (
            currentProducingAsset.id,         //assetProducingId
            prodAssetNewMeterRead,            //newMeterRead
            false,                            //smartMeterDown
            prodAssetLastMeterReadFileHash,   //lastSmartMeterReadFileHash
            0,                                //cO2OffsetMeterRead
            false,                            //cO2OffsetServiceDown
            currentProducingAsset.smartMeter  //sender, must be the address of the asset's smart meter as '0x00c3B44DBeA6f103dd30D7e90D59787175FeA185'
          );
        }

        //For each consuming site, create the certificates related to the energy matched with THIS production asset
        for (let cA of matchingResult.consumingSites) {

           //let result = await getData(num);
           //console.log(result);

           const currentProdAssetCertif = cA.producingAssets.find( pAsset => {
             return pAsset.id === pA.id
           })

           if(currentProdAssetCertif) {

             if(currentProdAssetCertif.energyMatched > 0) {

               let matchingProdAssetCertifData = {
                 assetName: pA.name,
                 assetId: pA.id,
                 production: Math.round(currentProdAssetCertif.energyMatched)+"/"+Math.round(pA.production),
                 productionDate
               }

               if(generateCertificates) {

                 console.log(" ⚠️  createCertificate will launch");
                 matchingProdAssetCertifData = await createCertificate(pA.id, currentProdAssetCertif.energyMatched, productionDate);
               } else {
                 console.log(" ⚠️  createCertificate will launch with currentProdAssetCertif.energyMatched = ", currentProdAssetCertif.energyMatched, parseInt(currentProdAssetCertif.energyMatched, 10), currentProdAssetCertif.energyMatched - parseInt(currentProdAssetCertif.energyMatched, 10));
               }

               console.log("weechainMatchingV3 - createCertificate params ⚠️⚠️⚠️⚠️⚠️")
               console.log(matchingProdAssetCertifData)

               let prodAssetCertificateId = matchingProdAssetCertifData.logs ? matchingProdAssetCertifData.logs[0]["result"]._certificateId : 'unknown certifId in test mode';
               /*console.log("weechainMatchingV3 - setCertificateTxHash params")
               console.log({
                 certificateId: prodAssetCertificateId,
                 certificateTxHash: matchingProdAssetCertifData.logs ? matchingProdAssetCertifData.logs[0].transactionHash : 'unknown txHash in test mode'
               })*/

               if(generateCertificates) {
                 console.log(" ⚠️  setCertificateTxHash will launch");
                 await setCertificateTxHash(prodAssetCertificateId, matchingProdAssetCertifData.logs[0].transactionHash);
               }

               console.log("weechainMatchingV3 - matchingProduction transferCertificate start for prod asset "+pA.name+" consumption of "+cA.name);
               //newOwner is the client : the consuming asset
               //const newOwner;
               // Airproducts, La Boulangère ou Barilla blockchain address
               //const newOwner = '0x0051CcCBCFF51788dDa9614b12dcea0D57208C03'; // <-- this is Airproducts Address Account
               const newOwner = cA.owner;

                console.log("weechainMatchingV3 - transferCertificate params")
                console.log({
                  certificateId: prodAssetCertificateId,
                  certificateOwner: newOwner
                })
               if(generateCertificates) {
                 console.log(" ⚠️⚠️⚠️⚠️⚠️⚠️⚠️⚠️  transferCertificate will launch CertificateId = "+prodAssetCertificateId+ " new Owner = "+newOwner);
                 console.log(" ⚠️  Air Products = 0x0051CcCBCFF51788dDa9614b12dcea0D57208C03 ");
                 console.log(" ⚠️  La Boulangère = 0x00d0F0e2AF12fec0C16c6D595a9554b99B6dE045 ");

                 await transferCertificate(prodAssetCertificateId, newOwner, cA.id)
               }

               //console.log("weechainMatchingV3 - matchingProduction transferCertificate finished for prod asset "+pA.name+" consumption of "+cA.name);

             }
           }

        }


        //create the certificate related to the energy NOT matched with THIS production asset

        const residualProduction = pA.production - pA.energyMatched;

        if(residualProduction > 0) {
          //console.log("weechainMatchingV3 - residualProduction createCertificate start for asset "+pA.name);
          let residualProdAssetCertifData = {
            prodAssetName: pA.name,
            prodAssetId: pA.id,
            residualProduction: Math.round(residualProduction)+"/"+Math.round(pA.production),
            productionDate
          }

          if(generateCertificates) {
            console.log(" ⚠️  createCertificate will launch (residual)");
            residualProdAssetCertifData = await createCertificate(pA.id, residualProduction, productionDate);
          }

          console.log("weechainMatchingV3 - createCertificate params ⚠️⚠️⚠️⚠️⚠️");

          console.log(residualProdAssetCertifData);

          const prodAssetResCertificateId = residualProdAssetCertifData.logs ? residualProdAssetCertifData.logs[0]["result"]._certificateId : 'unknown certifId in test mode';
          const prodAssetResCertificateTxHash = residualProdAssetCertifData.logs ? residualProdAssetCertifData.logs[0].transactionHash : 'unknown TxHash in test mode';

          if(generateCertificates) {
            console.log(" ⚠️  setCertificateTxHash will launch (residual)");
            await setCertificateTxHash(prodAssetResCertificateId, residualProdAssetCertifData.logs[0].transactionHash);
          }
          console.log("weechainMatchingV3 - residualProduction createCertificate finished for asset "+pA.name);
        }
      }


      const generatedCertificates = await getAllCertificatesForDay(date);
      var results = await Promise.resolve(generatedCertificates);

      res.json(results);

      sendSlackBotMessage(messageText);
      console.log('weechainMatchingV3 finished ');
    }
    catch(error) {
      console.error(error);
      let errorMessage = "❌ An error occured during matching for date "+ date +" : \n"
      errorMessage += "```"+error.message+"\n";
      errorMessage += error.stack ? error.stack+"```" : "```";

      sendSlackBotMessage(errorMessage);
      res.status(400).send({
        message:error.message
      });
      res.end();
    }
  }
}

const checkGooMaxLimit = async () => {
  //For each asset, check if the yearly contract limit has been reached
  //If yes, set the % of the asset to  0 in the database

  //Get the assets

  const consumingSitesNotFiltered = await getConsumingSitesOffchain();
  const producingAssetsNotFiltered = await getProducingAssetsOffchain();

  const consumingSites = consumingSitesNotFiltered.filter(c => {
    return c.isFake !== true
  });
  const producingAssets = producingAssetsNotFiltered.filter(p => {
    return p.isFake !== true
  });

  const currentYear = moment().format('YYYY');
  const start = currentYear+'-01-01';
  const end = moment().subtract(1, 'days').format('YYYY-MM-DD')

  let messageText = '*Max Goo Limits* \n';
  console.log('⚡️⚡️⚡️⚡️ checkGooMaxLimit results : ');

  const checks = await consumingSites.map(async cs => {
    const csData = await getConsumingSiteCalculatedDataOffchain(cs, start, end);
    const totalEnergyMatchedByAsset = csData.totalEnergyMatchedByAsset;

    messageText +=    '  '+cs.name+' : \n';
    console.log('⚡️⚡️⚡️⚡️  '+cs.name+' : ');

    let assetContractsCheck = cs.maxGooContractByAsset.map( (contract, index) => {

      //For this contract related to the contract.producingAssets, what is the current energy matched ?
      let energyMatched = 0;

      const updatedProducingAssets = contract.producingAssets.map((prodAssetId, indexProducingAssets) => {

        return  totalEnergyMatchedByAsset.find((a,j) => {
          return a.id === prodAssetId
        });
      })

      return {
        maxGoo: contract.maxGoo,
        producingAssets: updatedProducingAssets,
        energyMatchedContract: energyMatched,
        year: contract.year
      }
    })

    return assetContractsCheck.map(contract => {
      let assetsNames = '';
      let energyMatchedContract = 0;

      contract.producingAssets.map((prodAsset, idx) => {
        energyMatchedContract += prodAsset.energyMatched;

        if(idx >= contract.producingAssets.length - 1 ) {
          assetsNames += prodAsset.label;
        } else {
          assetsNames += prodAsset.label+', ';
        }

      })

      energyMatchedContract = parseInt(energyMatchedContract/1000, 10);
      const maxGoo = parseInt(contract.maxGoo/1000000, 10);
      console.log('⚡️⚡️⚡️⚡️     '+assetsNames+' : '+energyMatchedContract+'/'+maxGoo+' MWh');
      messageText +=    '     '+assetsNames+' : '+energyMatchedContract+'/'+maxGoo+' MWh';

      if( energyMatchedContract > maxGoo ) {
        console.log('⚡️⚡️⚡️⚡️     Limit reached for the group '+assetsNames);
        messageText +=    ' ⚠️ Limit reached \n';
      } else {
        messageText +=    '\n';
      }

      return assetsNames
    });
  })

  Promise.all(checks).then((completed) => sendSlackBotMessage(messageText));

}

const sendSlackBotMessageTest = async (message) => {

  const slackBotData={
    text: message
  };

  const slackBotJson = JSON.stringify(slackBotData);
  //This is a test bot webhook url
  //https://api.slack.com/apps/ACW6RL88H/incoming-webhooks?

  const slackBotRes = await fetch("https://hooks.slack.com/services/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", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: slackBotJson
  })
}

const sendSlackBotMessage = async (message) => {

  const slackBotData={
    text: message
  };

  const slackBotJson = JSON.stringify(slackBotData);
  //This is a weechain_status bot webhook url

  const slackBotRes = await fetch("https://hooks.slack.com/services/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", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: slackBotJson
  })
}
