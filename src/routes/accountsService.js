const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

//Get an array of the blockchain accounts
exports.getAllAccounts = async (req, res) => {

  try {
    Promise.resolve(web3.eth.getAccounts(function(err, accounts) {
      if (err != null) {
        console.log('There was an error fetching your accounts.');
        res.json({ error });
      }
      res.json(accounts);
    }));
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}


exports.getAccountBalance = async (req, res) => {

  var accountAddress = req.params.accountAddress;

  try {
    web3.eth.getBalance(accountAddress).then(function(result){
      console.log("Balance of account "+accountAddress+": "+result*0.000000000000000001+" ether");
      res.json({balance: result*0.000000000000000001});
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}
