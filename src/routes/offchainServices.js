const fs = require('fs');
const path = require('path');
const moment = require('moment');
const fetch = require('node-fetch');

const transportCertificates = require('../db/json/deliveriesWithCity.json');
const co2 = JSON.parse(fs.readFileSync(require.resolve('../db/json/co2.json'), 'utf8'))

const auth0ApiToken = require('../db/json/auth0ApiToken.json');

const dbService = require('./dbService');

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

const readJson = (path, cb) => {
  fs.readFile(require.resolve(path), (err, data) => {
    if (err)
      cb(err)
    else
      cb(null, JSON.parse(data))
  })
}

const getDates = (startDate, endDate) => {
  var dates = [],
      currentDate = startDate,
      addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
  while (currentDate < endDate) {
    dates.push(currentDate);
    currentDate = addDays.call(currentDate, 1);
  }
  return dates;
};
exports.getDates = getDates;

const getAssets = async () => {

  let myConsumingSites = JSON.parse(fs.readFileSync(require.resolve('../db/json/consumingSites.json'), 'utf8'))
  let myProducingAssets = JSON.parse(fs.readFileSync(require.resolve('../db/json/producingAssets.json'), 'utf8'))

  return {
    producingAssets: myProducingAssets,
    consumingSites: myConsumingSites
  }
}
exports.getAssets = getAssets;

const getAssetsDb = async () => {

  let myConsumingAssets = await fetchConsumingAssetsDb()
  let myProducingAssets = await fetchProducingAssetsDb()

  return {
    producingAssets: myProducingAssets,
    consumingAssets: myConsumingAssets
  }
}
exports.getAssetsDb = getAssetsDb;

fetchConsumingAssetsDb = async () => {
  //let url = process.env.API_URL + '/producing-assets-offchain-db';
  let url = process.env.API_URL + '/consuming-assets-offchain-db'

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      return result
    })
    .catch(error => {
      console.log(error);
    });


};

fetchProducingAssetsDb = async () => {
  let url = process.env.API_URL + '/producing-assets-offchain-db';
  //let url = process.env.API_URL + '/consuming-assets-offchain-db'

  return  await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      console.log('fetchProducingAssetsDb finished')
      return result
    })
    .catch(error => {
      console.log(error);
    });
};

fetchProducingAssetDb = async (assetId) => {
  let url = process.env.API_URL + '/producing-assets-offchain-db/'+assetId;

  return  await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      console.log('fetchProducingAssetDb finished')
      return result
    })
    .catch(error => {
      console.log(error);
    });
};

fetchProducingAssetCertificatesDb = async (assetId, start, end) => {
  ///certificates-production-asset-db?start=2019-01-01&end=2019-01-21&assetsIds=0
  let url = process.env.API_URL + '/certificates-production-asset-db?start=' + start + '&end=' + end + '&assetsIds='+assetId;

  return  await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      console.log('fetchProducingAssetCertificatesDb finished')
      return result
    })
    .catch(error => {
      console.log(error);
    });
};



fetchConsumingAssetDb = async (assetId) => {
  let url = process.env.API_URL + '/consuming-assets-offchain-db/'+assetId;

  return  await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(result => {
      console.log('fetchProducingAssetDb finished')
      return result
    })
    .catch(error => {
      console.log(error);
    });
};



const matching = (consumingSitesMatching, producingAssetsMatching, nbMaxIterations, currentIteration) => {

  //Enrich the data of the producing assets : set the number of consuming sites related to this asset
  let consumingSitesData = JSON.parse(fs.readFileSync(require.resolve('../db/json/consumingSites.json'), 'utf8'))
  let producingAssetsData = JSON.parse(fs.readFileSync(require.resolve('../db/json/producingAssets.json'), 'utf8'))

  producingAssetsMatchingDetailed = producingAssetsMatching.map(producingAsset => {
    if(currentIteration === 0) {
      producingAsset.productionAvailable = producingAsset.production;
    }
    /*console.log('  '+producingAsset.name)
    console.log('    production :' + Number.parseFloat(producingAsset.production/1000).toPrecision(4)+'MWh')
    console.log('    productionAvailable :' + Number.parseFloat(producingAsset.productionAvailable/1000).toPrecision(4)+'MWh')
    console.log('    energyMatched :' + Number.parseFloat(producingAsset.energyMatched/1000).toPrecision(4)+'MWh')
    */

    return producingAsset;
  })

  consumingSitesMatchingDetailed = consumingSitesMatching.map(site => {
    //Sort the producings assets by priority
    site.producingAssets.sort((assetA, assetB) => {
      return assetB.priority - assetA.priority;
    });


    /*console.log('')
    console.log('-------------------------')
    console.log('  Consumption site : '+site.name)
    console.log('  Conso: '+Number.parseFloat(site.consumption/1000).toPrecision(4))
    console.log('  Energy matched: '+Number.parseFloat(site.energyMatched/1000).toPrecision(4))*/


    //For this consuming site, we can now run the first attempt to match the energy with the renewables assets regarding the priority
    //site.energyMatched = 0;
    site.producingAssets = site.producingAssets.map( asset => {

      //For this asset (the producingAssets are sorted by priority), let's calculate the energy matched for the site
      let prodAsset = producingAssetsMatching.find(producingAsset => {
        return producingAsset.id === asset.id //get the asset production
      })
      /*console.log('   Production site : '+prodAsset.name)*/



      let energyProducedReservedForSite = prodAsset.productionAvailable  * asset.percentage / 100;
      let energyNotMatchedForSite = site.consumption - site.energyMatched;
      asset.energyMatched = (asset.energyMatched && currentIteration !== 0) ? asset.energyMatched : 0;

      if(energyNotMatchedForSite > 0){
        if( energyProducedReservedForSite < energyNotMatchedForSite ) {
          site.energyMatched      += energyProducedReservedForSite;
          prodAsset.energyMatched += energyProducedReservedForSite;
          asset.energyMatched += energyProducedReservedForSite;
        } else {
          site.energyMatched += energyNotMatchedForSite;
          prodAsset.energyMatched += energyNotMatchedForSite;
          asset.energyMatched += energyNotMatchedForSite;
        }
      }
      /*

      console.log('     production : '+Number.parseFloat(prodAsset.production/1000).toPrecision(4)+'MWh')
      console.log('     productionAvailable : '+Number.parseFloat(prodAsset.productionAvailable/1000).toPrecision(4)+'MWh')
      console.log('     prodAsset.energyMatched : '+Number.parseFloat(prodAsset.energyMatched/1000).toPrecision(4)+'MWh')
      console.log('     energyProducedReservedForSite : '+asset.percentage+'% '+Number.parseFloat(energyProducedReservedForSite/1000).toPrecision(4)+'MWh')
      console.log('     energyNotMatchedForSite : '+Number.parseFloat(energyNotMatchedForSite/1000).toPrecision(4)+'MWh')
      */


      producingAssetsMatching = producingAssetsMatching.map( loopAsset => {
        if(prodAsset.id === loopAsset.id){
          return prodAsset
        } else {
          return loopAsset
        }
      })

      return asset;
    })
    return site;
  });




  /*console.log('')
  console.log('')
  console.log('consumingSitesMatchingDetailed')*/
  consumingSitesMatchingDetailed.map(consumingSiteMatching => {
    /*console.log('')
    console.log('  ' + consumingSiteMatching.smireName+' ' + parseInt(consumingSiteMatching.consumptionPercentage*100)+'%')
    console.log('    consumption : ' + parseInt(consumingSiteMatching.consumption/100)/10+'MWh')
    console.log('    energyMatched :' + parseInt(consumingSiteMatching.energyMatched/100)/10+'MWh')
    */
    consumingSiteMatching.producingAssets.map(producingAsset => {
      let producingAssetDetails = producingAssetsData.find(pAsset => {
        return pAsset.id === producingAsset.id //get the asset production
      })
      //console.log('        '+producingAssetDetails.name+' - energyMatched : ' + parseInt(producingAsset.energyMatched/1000)+'MWh '+producingAsset.percentage+'%')


    })
  })


  //Let's remove the percentage for the consuming sites that have all of their energy already matched
  const refreshedConsumingSitesMatching = consumingSitesMatchingDetailed.map( site => {
    if(site.consumption - site.energyMatched === 0) {
      //Put the percentage of all of its prod assets to 0
      const producingAssetsAtNullPercentage = site.producingAssets.map(prodAsset => {
        prodAsset.percentage = 0;
        return prodAsset;
      })
      site.producingAssets = producingAssetsAtNullPercentage
    }
    return site;
  })

  /*
  console.log('')
  console.log('')
  console.log('    refreshedConsumingSitesMatching')
  console.log(refreshedConsumingSitesMatching)
  */

  //Let's update the percentage of each producing assets for each consuming site
  //As we have removed some sites, the proportions need to be updated
  const updatedConsumingSitesMatching = refreshedConsumingSitesMatching.map( site => {
    const newSiteProducingAssets = site.producingAssets.map(prodAsset => {

      const oldPercentage = prodAsset.percentage;
      let totalForDivision = 0;

      //Find this asset among all the refreshed consuming sites (without the one with energy already matched )
      refreshedConsumingSitesMatching.map( siteCons => {
        siteCons.producingAssets.map(prodAssetBis => {
          if(prodAssetBis.id === prodAsset.id) {
            totalForDivision+= prodAssetBis.percentage;
          }
        })

      })

      let newPercentage = 100;
      if( totalForDivision > 0) {
        newPercentage = 100*oldPercentage/totalForDivision;
      } else {
        //console.error('NtoN Error during totalForDivision...', totalForDivision, oldPercentage)
      }

      prodAsset.percentage = newPercentage;
      return prodAsset;

    })

    site.producingAssets = newSiteProducingAssets;
    return site;
  })

  //Update the available production for the next round
  producingAssetsMatchingDetailed2 = producingAssetsMatchingDetailed.map( pAsset => {
    pAsset.productionAvailable =  pAsset.production - pAsset.energyMatched
    //console.log('   Prod asset '+pAsset.name+' prod ='+pAsset.production+' matched ='+pAsset.energyMatched+' )')
    return pAsset
  })

  /*console.log('')
  console.log('')
  console.log('    producingAssetsMatchingDetailed2')
  console.log(producingAssetsMatchingDetailed2)
  */


  /*console.log('')
  console.log('')
  console.log('    updatedConsumingSitesMatching')
  console.log(updatedConsumingSitesMatching)
  */
  //Get the total needed equivalent to 100%
  //Apply this total to each consuming site
  let energyWaitingToMatch = 0;

  updatedConsumingSitesMatching.map( cs => {
    energyWaitingToMatch += (cs.consumption - cs.energyMatched)
  })

  let energyStillAvailable = 0;

  producingAssetsMatchingDetailed2.map( p => {
    energyStillAvailable += (p.production - p.energyMatched)
    //console.log('Adding '+(p.production - p.energyMatched)+' from asset '+p.name+' ( prod ='+p.production+' , matched ='+p.energyMatched+' )')
  })

  let iterateAgain = true;
  if(currentIteration >= nbMaxIterations || energyWaitingToMatch === 0 || energyStillAvailable === 0) {
    iterateAgain = false;
  }
  //console.log('energyWaitingToMatch = '+energyWaitingToMatch)
  //console.log('energyStillAvailable = '+energyStillAvailable)

  if(iterateAgain) {
    return module.exports.matching(updatedConsumingSitesMatching, producingAssetsMatchingDetailed2, nbMaxIterations, currentIteration + 1)
  } else {
    return {consumingSites:updatedConsumingSitesMatching, producingAssets:producingAssetsMatchingDetailed2}
  }
}


exports.getConsumingSites = async (req, res) => {
  try {
    readJson('../db/json/consumingSites.json', (err, consumingSitesOffchain) => {
      res.status(201).send({
        consumingSites:consumingSitesOffchain
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};

exports.getConsumingSite = async (req, res) => {

  try {
    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;
    const producingAssetsData = myAssets.producingAssets;

    const consumingSite = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })
    if(consumingSite) {
      res.status(201).send(
        consumingSite
      );
    } else {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getConsumingSiteLightTimeSeriesDb = async (req, res) => {

  try {

    let consumingSitesData = await fetchConsumingAssetsDb()

    const consumingAsset = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    const start = req.query.start;
    const end = req.query.end;

    if(!consumingAsset) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
      return
    }

    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const MeterModel = dbService.MeterModel;

    //Find
    MeterModel.find({
        asset: consumingAsset.smireName,
        date: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, meters) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {
          const index = meters.map(meter => {
            return moment(meter.date).format();
          })
          const quantities = meters.map(meter => {
            return [meter.quantity];
          })
          const columns = [consumingAsset.smireName]
          const labels = [consumingAsset.name]

          const data = {
            index,
            data:quantities,
            columns,
            labels
          }

          res.json({data});
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.uploadProducingAssetPicture = async (req, res) => {

  console.log('starting uploadProducingAssetsPicture...')
  try {
    //fetch Data from server
    const assetId = parseInt(req.params.assetId, 10)
    console.log("req.files ",req.files);
    console.log("assetId ", assetId);

    //console.log("before req.files.files.mimetype "+req.files.files.mimetype);
    //if(req.files && req.files.files.mimetype == "application/pdf"){
    if(req.files){
      var file = req.files.files,
        name = file.name,
        type = file.mimetype;
      var uploadpath = __dirname + '/../uploads/prod_asset_' + assetId + '.jpg';
      console.log("°°°°°°°° before mv ");
      file.mv(uploadpath,function(err){
        if(err){
          res.send("Error Occured!");
          res.end();
        }
        else {
          console.log("°°°°°°°° after mv ");
          res.json({result:"image uploaded"});
        }
      });
    }
    else {
      console.log("Bad files or mimetype",req.files);
      res.status(403).send({
        result:false
      });
      res.end();
    }
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

exports.getProducingAssetPicture = async (req, res) => {

  console.log('starting getProducingAssetPicture...')
  try {
    res.json({result:"image"});
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//Meters in DB
exports.getMetersDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Meters = dbService.MeterModel;
    Meters.find({}, function(error, meters) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        res.json(meters);
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getMeterDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Meters = dbService.MeterModel;
    Meters.find({
        _id: req.params.meterId,
    }, function(error, meters) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!meters[0]) {
          res.status(400).send({
            message:'Meter not found'
          });
          res.end();
        } else {
          res.json(meters[0]);
        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.createMeterDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Meters = dbService.MeterModel;

    const meter = new Meters()

    meter.asset = req.body.asset;
    meter.quantity = req.body.quantity;
    meter.date = req.body.date;

    console.log('creating this new meter : ')
    console.log(meter)

    console.log('saving this new meter : ')
    meter.save(function (err, createdMeter) {
      if (err) {
        console.log(err)
        return handleErrors(err);
      }
      console.log( '*********** createdMeter start ')
      console.log( meter )
      console.log( '***********')
      console.log( createdMeter )
      console.log( '*********** createdMeter end ')
      res.json(createdMeter);
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.setMeterDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Meters = dbService.MeterModel;
    console.log('req.params.meterId = '+req.params.meterId)
    Meters.find({
        _id: req.params.meterId,
    }, function(error, meters) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!meters[0]) {
          res.status(400).send({
            message:'Meter not found'
          });
          res.end();
        } else {

          let meter = meters[0];

          meter.asset = req.body.asset;
          meter.quantity = req.body.quantity;
          meter.date = req.body.date;



          meter.save(function (err, updatedMeter) {
            if (err) return handleErrors(err);
            console.log( '*********** updatedMeter start ')
            console.log( meter )
            console.log( '***********')
            console.log( updatedMeter )
            console.log( '*********** updatedMeter end ')
            res.json(updatedMeter);
          });

        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};



//Certificates in DB
exports.getCertificatesDb = async (req, res) => {

  try {

    const users = await fetch(process.env.API_URL + "/users-offchain", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(handleErrors)
    .then(res => res.json())
    .then(results => {
      return results.data
    })

    const ownerUser = users.find(user => {
      return user.app_metadata.address === req.query._owner;
    })

    console.log(' req.query._owner' ,  req.query._owner)
    console.log(' ownerUser' ,  ownerUser)

    const adminRole = ownerUser.app_metadata.roles.find(role => {
      return role === 'admin'
    })

    const isAdmin = adminRole ? true : false;

    console.log(' isAdmin' ,  isAdmin)
    const queryArgs = isAdmin ?  {} : { _owner: req.query._owner }
    console.log(' queryArgs' ,  queryArgs)

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));


    const Certificates = dbService.Certificate;


    Certificates.find(queryArgs, async function(error, certificates) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {


        const consumingAssets = await fetch(process.env.API_URL + "/consuming-assets-offchain-db", {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(handleErrors)
        .then(res => res.json())
        .then(data => {
          return data
        })


        const producingAssets = await fetch(process.env.API_URL + "/producing-assets-offchain-db", {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }).then(handleErrors)
        .then(res => res.json())
        .then(data => {
          return data
        })

        let certificatesFull = certificates.map(c => {

          const currentUser = users.find(user => {
            return user.app_metadata.address === c._owner;
          })

          const currentConsumingAsset = consumingAssets.find(cA => {
            return cA.id === c._consumptionAssetId;
          })

          const currentProducingAsset = producingAssets.find(pA => {
            return pA.id === c._assetId;
          })


          c = JSON.parse(JSON.stringify(c));

          c._ownerName = currentUser && currentUser.user_metadata.name ? currentUser.user_metadata.name : 'Unknown';
          c._resourceName = currentProducingAsset && currentProducingAsset.displayedName ? currentProducingAsset.displayedName: 'Unknown';
          c._assetConsumptionName = currentConsumingAsset && currentConsumingAsset.name ? currentConsumingAsset.name: '';
          c._productionDate = moment(c._productionDate).format('X')

          console.log(c._certificateId+ ' owner:'+c._owner+ ' _ownerName:'+c._ownerName+' _resourceName: '+c. _resourceName)

          return c;
        })

        console.log('certificatesFull START ************************')
        certificatesFull.map(c => {
          console.log(c._certificateId+ ' owner:'+c._owner+ ' _ownerName:'+c._ownerName+' _resourceName: '+c. _resourceName)
        })
        console.log('certificatesFull FINISHED ************************')

        console.log(certificatesFull)

        res.json(certificatesFull);
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getCertificateDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Certificates = dbService.Certificate;
    Certificates.find({
        _certificateId: req.params.certificateId,
    }, function(error, certificates) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!certificates[0]) {
          res.status(400).send({
            message:'Certificate not found'
          });
          res.end();
        } else {
          res.json(certificates[0]);
        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getCertificatesByProductionAssetDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;
    let assetsIds = req.query.assetsIds.split(',')
    assetsIds = assetsIds.map(id => { return parseInt( id, 10)})

    console.log(start, end, assetsIds)

    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Certificates = dbService.Certificate;

    //Find
    Certificates.find({
        _assetId: { $in: assetsIds },
        _productionDate: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, certificates) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          res.status(201).send(certificates);
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getCertificatesByConsumptionAssetDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;
    const assetsIds = req.query.assetsIds.split(',')

    //Then we build the query
    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Certificates = dbService.Certificate;

    //Find
    Certificates.find({
        _consumptionAssetId: { $in: assetsIds },
        _productionDate: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, certificates) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          res.status(201).send(certificates);
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getCertificatesByOwnerDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;
    const owners = req.query.owners.split(',')

    //Then we build the query
    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Certificates = dbService.Certificate;

    //Find
    Certificates.find({
        _owner: { $in: owners },
        _productionDate: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, certificates) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          res.status(201).send(certificates);
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.createCertificateDb = async (req, res) => {
  console.log('------------ createCertificateDb start ')
  try {

    console.log('------------ 0 ')
    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    console.log('------------ 1 ')
    const Certificates = dbService.Certificate;

    const certificate = new Certificates()

    console.log('------------ 2 ')
    certificate._certificateId = req.body._certificateId;
    certificate._assetId = req.body._assetId;
    certificate._consumptionAssetId = req.body._consumptionAssetId;
    certificate._owner = req.body._owner;
    certificate._powerInW = req.body._powerInW;
    certificate._retired = req.body._retired;
    certificate._dataLog = req.body._dataLog;
    certificate._coSaved = req.body._coSaved;
    certificate._escrow = req.body._escrow;
    console.log('------------ 3 ')
    certificate._creationTime = req.body._creationTime;
    certificate._txHash = req.body._txHash;
    console.log('------------ 4 ')
    certificate._productionDate = req.body._productionDate;
    console.log('------------ 5 ')
    certificate._certificateHash = req.body._certificateHash;
    certificate._info = req.body._info;

    console.log('creating this new certificate : ')
    console.log(certificate)

    console.log('saving this new certificate : ')
    certificate.save(function (err, createdCertificate) {
      if (err) {
        console.log(err)
        return handleErrors(err);
      }
      console.log( '*********** createdCertificate start ')
      console.log( certificate )
      console.log( '***********')
      console.log( createdCertificate )
      console.log( '*********** createdCertificate end ')
      res.json(createdCertificate);
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.setCertificateDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Certificates = dbService.Certificate;
    console.log('req.params.certificateId = '+req.params.certificateId)
    Certificates.find({
        _certificateId: req.params.certificateId,
    }, function(error, certificates) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!certificates[0]) {
          res.status(400).send({
            message:'Meter not found'
          });
          res.end();
        } else {

          let certificate = certificates[0];

          certificate._assetId = req.body._assetId;
          certificate._consumptionAssetId = req.body._consumptionAssetId;
          certificate._owner = req.body._owner;
          certificate._powerInW = req.body._powerInW;
          certificate._retired = req.body._retired;
          certificate._dataLog = req.body._dataLog;
          certificate._coSaved = req.body._coSaved;
          certificate._escrow = req.body._escrow;
          certificate._creationTime = req.body._creationTime;
          certificate._txHash = req.body._txHash;
          certificate._productionDate = req.body._productionDate;
          certificate._certificateHash = req.body._certificateHash;
          certificate._info = req.body._info;


          certificate.save(function (err, updatedCertificate) {
            if (err) return handleErrors(err);
            console.log( '*********** updatedCertificate start ')
            console.log( certificate )
            console.log( '***********')
            console.log( updatedCertificate )
            console.log( '*********** updatedCertificate end ')
            res.json(updatedCertificate);
          });

        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};




exports.getProducingAssetsDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ProducingAsset = dbService.ProducingAsset;
    ProducingAsset.find({ id: { $exists: true } }, function(error, producingAssets) {

      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        res.json(producingAssets);
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getProducingAssetDb = async (req, res) => {
  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    const ProducingAsset = dbService.ProducingAsset;
    ProducingAsset.find({
        id: parseInt(req.params.assetId, 10),
    }, function(error, producingAssets) {
      if (error) {
        console.log("Error in getProducingAssetDb : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!producingAssets[0]) {
          res.status(400).send({
            message:'Asset not found'
          });
          res.end();
        } else {
          res.json(producingAssets[0]);
        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.createProducingAssetsDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ProducingAsset = dbService.ProducingAsset;

    const producingAsset = new ProducingAsset()

    producingAsset.id = req.body.id;
    producingAsset.houseNumber = req.body.houseNumber;
    producingAsset.street = req.body.street;
    producingAsset.zip = req.body.zip;
    producingAsset.city = req.body.city;
    producingAsset.region = req.body.region;
    producingAsset.country = req.body.country;


    producingAsset.gpsLatitude = req.body.gpsLatitude;
    producingAsset.gpsLongitude = req.body.gpsLongitude;

    producingAsset.owner = req.body.owner;
    producingAsset.smartMeter = req.body.smartMeter;

    producingAsset.name = req.body.name;
    producingAsset.displayedName = req.body.displayedName;

    producingAsset.capacityWh = req.body.capacityWh;
    producingAsset.otherGreenAttributes = req.body.otherGreenAttributes;
    producingAsset.typeOfPublicSupport = req.body.typeOfPublicSupport;
    producingAsset.assetType = req.body.assetType;
    producingAsset.registryCompliance = req.body.registryCompliance;
    producingAsset.operationalSince = req.body.operationalSince;

    producingAsset.active = req.body.active;

    console.log('creating this new producing asset : ')
    console.log(producingAsset)

    console.log('saving this new producing asset : ')
    producingAsset.save(function (err, createdProducingAsset) {
      if (err) return handleErrors(err);
      console.log( '*********** createdProducingAsset start ')
      console.log( producingAsset )
      console.log( '***********')
      console.log( createdProducingAsset )
      console.log( '*********** createdProducingAsset end ')
      res.json(createdProducingAsset);
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};



exports.setProducingAssetDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ProducingAsset = dbService.ProducingAsset;
    ProducingAsset.find({
        id: parseInt(req.params.assetId, 10),
    }, function(error, producingAssets) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!producingAssets[0]) {
          res.status(400).send({
            message:'Asset not found'
          });
          res.end();
        } else {

          let producingAsset = producingAssets[0];

          producingAsset.houseNumber = req.body.houseNumber;
          producingAsset.street = req.body.street;
          producingAsset.zip = req.body.zip;
          producingAsset.city = req.body.city;
          producingAsset.region = req.body.region;
          producingAsset.country = req.body.country;

          producingAsset.gpsLatitude = req.body.gpsLatitude;
          producingAsset.gpsLongitude = req.body.gpsLongitude;

          producingAsset.owner = req.body.owner;
          producingAsset.smartMeter = req.body.smartMeter;

          producingAsset.name = req.body.name;
          producingAsset.displayedName = req.body.displayedName;

          producingAsset.capacityWh = req.body.capacityWh;
          producingAsset.otherGreenAttributes = req.body.otherGreenAttributes;
          producingAsset.typeOfPublicSupport = req.body.typeOfPublicSupport;
          producingAsset.assetType = req.body.assetType;
          producingAsset.registryCompliance = req.body.registryCompliance;
          producingAsset.operationalSince = req.body.operationalSince;

          producingAsset.active = req.body.active;


          /*console.log( '*********** producingAsset start ')
          console.log( producingAsset )
          console.log( '*********** ')

          console.log( req.body )
          console.log( '*********** producingAsset end ')
          res.json(producingAsset);
          */



          producingAsset.save(function (err, updatedProducingAsset) {
            if (err) return handleErrors(err);
            console.log( '*********** updatedProducingAsset start ')
            console.log( producingAsset )
            console.log( '***********')
            console.log( updatedProducingAsset )
            console.log( '*********** updatedProducingAsset end ')
            res.json(updatedProducingAsset);
          });

        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};




exports.getConsumingAssetsDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ConsumingAsset = dbService.ConsumingAsset;
    ConsumingAsset.find({}, function(error, consumingAssets) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        res.json(consumingAssets);
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getConsumingAssetDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ConsumingAsset = dbService.ConsumingAsset;
    ConsumingAsset.find({
        id: parseInt(req.params.assetId, 10),
    }, function(error, consumingAssets) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {

        if(!consumingAssets[0]) {
          res.status(400).send({
            message:'Asset not found'
          });
          res.end();
        } else {
          res.json(consumingAssets[0]);
        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.createConsumingAssetsDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ConsumingAsset = dbService.ConsumingAsset;

    const consumingAsset = new ConsumingAsset()

    consumingAsset.houseNumber = req.body.houseNumber;
    consumingAsset.street = req.body.street;
    consumingAsset.zip = req.body.zip;
    consumingAsset.city = req.body.city;
    consumingAsset.region = req.body.region;
    consumingAsset.country = req.body.country;

    consumingAsset.gpsLatitude = req.body.gpsLatitude;
    consumingAsset.gpsLongitude = req.body.gpsLongitude;

    consumingAsset.owner = req.body.owner;
    consumingAsset.smartMeter = req.body.smartMeter;

    consumingAsset.maxCapacitySet = req.body.maxCapacitySet;

    consumingAsset.name = req.body.name;
    consumingAsset.smireName = req.body.smireName;
    consumingAsset.displayedName = req.body.displayedName;
    consumingAsset.producingAssets = req.body.producingAssets.map( pAsset => {
      pAsset.priority = parseInt(pAsset.priority, 10)
      return pAsset;
    });

    consumingAsset.active = req.body.active;

    console.log('creating this new consuming asset : ')
    console.log(consumingAsset)

    console.log('saving this new consuming asset : ')
    consumingAsset.save(function (err, createdConsumingAsset) {
      if (err) return handleErrors(err);
      ConsumingAsset.count({}, function(err, c) {
           console.log('There is ' + c + ' asset(s)');
           createdConsumingAsset.id = c - 1;
           consumingAsset.save(function (err, createdConsumingAssetWithId) {
             if (err) return handleErrors(err);
             console.log( '*********** createdConsumingAsset start ')
             console.log( consumingAsset )
             console.log( '***********')
             console.log( createdConsumingAssetWithId )
             console.log( '*********** createdConsumingAsset end ')

             res.json(createdConsumingAsset);

           });
      });

    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};



exports.setConsumingAssetDb = async (req, res) => {

  try {

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ConsumingAsset = dbService.ConsumingAsset;
    ConsumingAsset.find({
        id: parseInt(req.params.assetId, 10),
    }, function(error, consumingAssets) {
      if (error) {
        console.log("Error : ", error);
        res.status(400).send({
          message:error.message
        });
        res.end();
      }
      else {
        if(!consumingAssets[0]) {
          res.status(400).send({
            message:'Asset not found'
          });
          res.end();
        } else {

          let consumingAsset = consumingAssets[0];

          consumingAsset.houseNumber = req.body.houseNumber;
          consumingAsset.street = req.body.street;
          consumingAsset.zip = req.body.zip;
          consumingAsset.city = req.body.city;
          consumingAsset.region = req.body.region;
          consumingAsset.country = req.body.country;

          consumingAsset.gpsLatitude = req.body.gpsLatitude;
          consumingAsset.gpsLongitude = req.body.gpsLongitude;

          consumingAsset.owner = req.body.owner;
          consumingAsset.smartMeter = req.body.smartMeter;

          consumingAsset.maxCapacitySet = req.body.maxCapacitySet;

          consumingAsset.name = req.body.name;
          consumingAsset.smireName = req.body.smireName;
          consumingAsset.displayedName = req.body.displayedName;
          consumingAsset.producingAssets = req.body.producingAssets.map( pAsset => {
            pAsset.priority = parseInt(pAsset.priority, 10)
            return pAsset;
          });

          consumingAsset.active = req.body.active;


          consumingAsset.save(function (err, updatedConsumingAsset) {
            if (err) return handleErrors(err);
            res.json(updatedConsumingAsset);
          });

        }
      }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};





exports.getConsumingSiteLightTimeSeries = async (req, res) => {
  //check the data
  /*
    const paramAfterUri = req.query._sender;
    const paramInUri = req.params._sender;
    const sender = req.body._sender;
  */

  try {
    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;

    const consumingAsset = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    const start = req.query.start;
    const end = req.query.end;

    if(!consumingAsset) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }

    //Then we build the query
    let dayCount = 30;
    sitesInUrlSmire = consumingAsset.smireName;

    let username = 'system.weechain';
    let password = '3OetAPO8D03XIYs2WRuj';

    let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

    const response = await fetch(url, {method:'GET'});

    const json = await response.json();
    json.labels = [consumingAsset.name];
    res.status(201).send({
      data:json
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.getConsumingSiteTimeSeries = async (req, res) => {

  try {
    //check the data
    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;
    const producingAssetsData = myAssets.producingAssets;

    const consumingAsset = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    const start = req.query.start;
    const end = req.query.end;

    if(!consumingAsset) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }

    //First, we get the producing assets related to this consuming asset

    const prodAssets = consumingAsset.producingAssets.map(producingAsset => {
      const tempAsset = producingAssetsData.find(prodAsset => {
        return producingAsset.id === prodAsset.id
      })
      return { ...tempAsset,  priority: producingAsset.priority,  percentage: producingAsset.percentage };
    })
    prodAssets.sort((assetA, assetB) => {
      return(assetB.priority -  assetA.priority )
    })

    //Then we build the query
    let dayCount = 30;
    let sitesInUrlSmire = '';
    prodAssets.map(prodAsset => {
      sitesInUrlSmire += prodAsset.name+'%2C';
    })
    sitesInUrlSmire += consumingAsset.smireName;

    let username = 'system.weechain';
    let password = '3OetAPO8D03XIYs2WRuj';

    let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

    const response = await fetch(url, {method:'GET'});

    const json = await response.json();
    json.labels = prodAssets.map(prodAsset => {
      return prodAsset.displayedName
    });
    json.labels.push(consumingAsset.name);
    res.status(201).send({
      data:json
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getConsumingSiteTimeSeriesDb = async (req, res) => {

  try {
    //check the data
    let consumingSitesData = await fetchConsumingAssetsDb()
    let producingAssetsData = await fetchProducingAssetsDb()

    const consumingAsset = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    const start = req.query.start;
    const end = req.query.end;

    if(!consumingAsset) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }

    //First, we get the producing assets related to this consuming asset

    const prodAssets = consumingAsset.producingAssets.map(producingAsset => {
      const tempAsset = producingAssetsData.find(prodAsset => {
        return producingAsset.id === prodAsset.id
      })
      return { ...tempAsset,  priority: producingAsset.priority,  percentage: producingAsset.percentage };
    })
    prodAssets.sort((assetA, assetB) => {
      return(assetB.priority -  assetA.priority )
    })

    //Then we build the query
    let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    let url = process.env.API_URL + '/production-db?start=' + startDate + '&end=' +endDate;
    const response = await fetch(url, {method:'GET'});



    const json = await response.json();
    json.labels = prodAssets.map(prodAsset => {
      return prodAsset.displayedName
    });
    json.labels.push(consumingAsset.name);
    res.status(201).send({
      data:json
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}



exports.getConsumingSitesConsumption = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;

    //Then we build the query
    let dayCount = 30;
    let sitesInUrlSmire = '';
    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;
    const producingAssetsData = myAssets.producingAssets;

    consumingSitesData.map((asset, idx) => {

      if(idx < consumingSitesData.length - 1) {
        sitesInUrlSmire += asset.smireName+'%2C';
      } else {
        sitesInUrlSmire += asset.smireName;
      }
    })

    let username = 'system.weechain';
    let password = '3OetAPO8D03XIYs2WRuj';

    let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

    const response = await fetch(url, {method:'GET'});

    const json = await response.json();
    json.labels = consumingSitesData.map(asset => {
      return asset.name
    })
    json.ids = consumingSitesData.map(asset => {
      return asset.id
    })

    res.status(201).send({
      data:json
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getProducingAssetsProduction = async (req, res) => {

  try {
    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;
    const producingAssetsData = myAssets.producingAssets;

    const start = req.query.start;
    const end = req.query.end;

    //Then we build the query
    let dayCount = 30;
    let sitesInUrlSmire = '';
    producingAssetsData.map((asset, idx) => {
      if(idx < producingAssetsData.length - 1) {
        sitesInUrlSmire += asset.name+'%2C';
      } else {
        sitesInUrlSmire += asset.name;
      }
    })

    let username = 'system.weechain';
    let password = '3OetAPO8D03XIYs2WRuj';

    let startDate = !start ? moment().subtract(dayCount, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    let url = 'https://' + username + ':' + password+ '@smire.bluesafire.io/api/daily_data_on_period?site='+sitesInUrlSmire+'&start='+startDate+'&end='+endDate+'&specific_timezone=Europe%2FParis';

    const response = await fetch(url, {method:'GET'});

    const json = await response.json();
    json.labels = producingAssetsData.map(asset => {
      return asset.displayedName
    })
    json.ids = producingAssetsData.map(asset => {
      return asset.id
    })

    res.status(201).send({
      data:json
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getConsumingAssetsConsumptionDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;

    //Then we build the query
    let dayCount = 30;
    let sitesInUrlSmire = '';

    let consumingAssets = await fetchConsumingAssetsDb()
    let producingAssets = await fetchProducingAssetsDb()

    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    let sitesIds = consumingAssets.filter(cA => {
      return !cA.isFake;
    }).map(cA => {
      return cA.smireName;
    })
    const sitesIdsQuery = [];
    sitesIds.map( siteName => {
      sitesIdsQuery.push({ asset: siteName })
    })

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const MeterModel = dbService.MeterModel;

    //Find
    MeterModel.find({
        asset: { $in: sitesIds },
        date: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, meters) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          //Begin with dates array
          //Get all the dates
          let indexInit = meters.map(meter => {
            return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ");
          })

          //Remove duplicates
          let index = indexInit.filter((date, pos, indexFull) => {
            return indexFull.indexOf(date) == pos;
          })

          //Sort dates
          let sortedIndex = index.sort((dateA, dateB) => {
            return moment(dateA).format('x') - moment(dateB).format('x')
          });

          //Then build the asset's name array
          //Get all the assets
          let assetsInit = meters.map(meter => {
            return meter.asset;
          })

          //Remove duplicates
          let assets = assetsInit.filter((name, pos) => {
            return assetsInit.indexOf(name) === pos;
          })

          //Sort names
          let sortedAssets = assets.sort((nameA, nameB) => {
            return nameA - nameB
          });

          let data = [];

          sortedIndex.map((date, index) => {

            data[index] = [];

            //For each date, find the meters
            let metersForDate = meters.filter(meter => {
              return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ") === date
            })

            //We can now push the measures in the data array for this date
            sortedAssets.map(name => {
              let metersForAssetAndDay = metersForDate.filter( meter => {
                return meter.asset === name
              });
              if( metersForAssetAndDay.length > 1 ) {
                throw new Error("There is several meters for asset " + name + " on date " + date);
              }
              if( metersForAssetAndDay.length < 1 ) {
                data[index].push("None")
              } else {
                data[index].push(metersForAssetAndDay[0].quantity)
              }
            })
          })

          let ids = [];
          let labels = [];
          sortedAssets.map(name => {
            let consAsset = consumingAssets.filter(cA => {
              return cA.smireName === name && !cA.isFake;
            })
            if( consAsset.length > 1 ) {
              console.log(consAsset)
              res.status(400).send({
                message:"There is several consuming asset for the name " + name
              });
              res.end();
              throw new Error("There is several consuming asset for the name " + name);

            }
            if( consAsset.length < 1 ) {
              res.status(400).send({
                message:"There is no consuming asset for the name " + name
              });
              res.end();
              throw new Error("There is no consuming asset for the name " + name);
            } else {
              ids.push(consAsset[0].id)
              labels.push(consAsset[0].name)
            }

          })


          res.status(201).send({
            index:sortedIndex,
            data: data,
            columns:sortedAssets,
            labels:labels,
            ids:ids
          });
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getAssetsMeteringDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;
    const sitesIds = req.query.sitesIds.split(',')

    //Then we build the query
    let dayCount = 30;

    let consumingAssets = await fetchConsumingAssetsDb()
    let producingAssets = await fetchProducingAssetsDb()

    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    const sitesIdsQuery = [];
    sitesIds.map( siteName => {
      sitesIdsQuery.push({ asset: siteName })
    })

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const MeterModel = dbService.MeterModel;

    //Find
    MeterModel.find({
        asset: { $in: sitesIds },
        date: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, meters) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          //Begin with dates array
          //Get all the dates
          let indexInit = meters.map(meter => {
            return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ");
          })

          //Remove duplicates
          let index = indexInit.filter((date, pos, indexFull) => {
            console.log(pos, indexFull.indexOf(date), typeof date)
            return indexFull.indexOf(date) == pos;
          })

          //Sort dates
          let sortedIndex = index.sort((dateA, dateB) => {
            return moment(dateA).format('x') - moment(dateB).format('x')
          });

          //Then build the asset's name array
          //Get all the assets
          let assetsInit = meters.map(meter => {
            return meter.asset;
          })

          //Remove duplicates
          let assets = assetsInit.filter((name, pos) => {
            return assetsInit.indexOf(name) === pos;
          })

          //Sort names
          let sortedAssets = assets.sort((nameA, nameB) => {
            return nameA - nameB
          });

          let data = [];

          sortedIndex.map((date, index) => {

            data[index] = [];

            //For each date, find the meters
            let metersForDate = meters.filter(meter => {
              return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ") === date
            })

            //We can now push the measures in the data array for this date
            sortedAssets.map(name => {
              let metersForAssetAndDay = metersForDate.filter( meter => {
                return meter.asset === name
              });
              if( metersForAssetAndDay.length > 1 ) {
                throw new Error("There is several meters for asset " + name + " on date " + date);
              }
              if( metersForAssetAndDay.length < 1 ) {
                data[index].push("None")
              } else {
                data[index].push(metersForAssetAndDay[0].quantity)
              }
            })
          })

          let ids = [];
          let labels = [];
          sortedAssets.map(name => {
            let consAsset = consumingAssets.filter(cA => {
              return cA.smireName === name && !cA.isFake;
            })

            let pAsset = producingAssets.filter(pA => {
              return pA.name === name && !pA.isFake;
            })

            if(consAsset.length < 1 && pAsset.length < 1) {
              throw new Error("There is no asset for the name " + name);
            }
            if( consAsset.length > 1 ||  pAsset.length > 1) {
              throw new Error("There is several asset for the name " + name);
            }

            if( consAsset.length < 1 && pAsset.length === 1) {
              ids.push(pAsset[0].id)
              labels.push(pAsset[0].displayedName)
            } else if( pAsset.length < 1 && consAsset.length === 1) {
              ids.push(consAsset[0].id)
              labels.push(consAsset[0].name)
            }
          })


          res.status(201).send({
            index:sortedIndex,
            data: data,
            columns:sortedAssets,
            labels:labels,
            ids:ids
          });
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}


exports.getProducingAssetsProductionDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;

    //Then we build the query
    let dayCount = 30;
    let sitesInUrlSmire = '';

    console.log('getProducingAssetsProductionDb step 1');
    let consumingAssets = await fetchConsumingAssetsDb()
    let producingAssets = await fetchProducingAssetsDb()

    console.log('getProducingAssetsProductionDb step 2');

    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    console.log('getProducingAssetsProductionDb step producingAssets', producingAssets);

    let sitesIds = producingAssets.filter(pA => {
      return !pA.isFake;
    }).map(pA => {
      return pA.name;
    })
    console.log('getProducingAssetsProductionDb step 3');

    const sitesIdsQuery = [];
    sitesIds.map( siteName => {
      sitesIdsQuery.push({ asset: siteName })
    })

    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const MeterModel = dbService.MeterModel;

    //Find
    MeterModel.find({
        asset: { $in: sitesIds },
        date: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, meters) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          //Begin with dates array
          //Get all the dates
          let indexInit = meters.map(meter => {
            return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ");
          })

          //Remove duplicates
          let index = indexInit.filter((date, pos, indexFull) => {
            console.log(pos, indexFull.indexOf(date), typeof date)
            return indexFull.indexOf(date) == pos;
          })

          //Sort dates
          let sortedIndex = index.sort((dateA, dateB) => {
            return moment(dateA).format('x') - moment(dateB).format('x')
          });

          //Then build the asset's name array
          //Get all the assets
          let assetsInit = meters.map(meter => {
            return meter.asset;
          })

          //Remove duplicates
          let assets = assetsInit.filter((name, pos) => {
            return assetsInit.indexOf(name) === pos;
          })

          //Sort names
          let sortedAssets = assets.sort((nameA, nameB) => {
            return nameA - nameB
          });

          let data = [];

          sortedIndex.map((date, index) => {

            data[index] = [];

            //For each date, find the meters
            let metersForDate = meters.filter(meter => {
              return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ") === date
            })

            //We can now push the measures in the data array for this date
            sortedAssets.map(name => {
              let metersForAssetAndDay = metersForDate.filter( meter => {
                return meter.asset === name
              });
              if( metersForAssetAndDay.length > 1 ) {
                throw new Error("There is several meters for asset " + name + " on date " + date);
              }
              if( metersForAssetAndDay.length < 1 ) {
                data[index].push("None")
              } else {
                data[index].push(metersForAssetAndDay[0].quantity)
              }
            })
          })

          let ids = [];
          let labels = [];
          sortedAssets.map(name => {
            let prodAssets = producingAssets.filter(pA => {
              return pA.name === name && !pA.isFake;
            })
            if( prodAssets.length > 1 ) {
              throw new Error("There is several producing asset for the name " + name);
            }
            if( prodAssets.length < 1 ) {
              throw new Error("There is no producing asset for the name " + name);
            } else {
              ids.push(prodAssets[0].id)
              labels.push(prodAssets[0].displayedName)
            }

          })


          res.status(201).send({
            index:sortedIndex,
            data: data,
            columns:sortedAssets,
            labels:labels,
            ids:ids
          });
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getProducingAssetProductionDb = async (req, res) => {

  try {

    const start = req.query.start;
    const end = req.query.end;
    const assetId = req.params.assetId;

    //Then we build the query
    let dayCount = 30;
    let sitesInUrlSmire = '';

    console.log('getProducingAssetProductionDb step 1');
    let producingAsset = await fetchProducingAssetDb(assetId)

    console.log('getProducingAssetProductionDb step 2');

    //By default, 1 month ago
    const startDate = !start ? moment().subtract(31, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(start, 'YYYY-MM-DD').subtract(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.startTime = 2016-09-25 00:00:00
    //By default, today
    const endDate   = !end ? moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ") : moment(end, 'YYYY-MM-DD').format("YYYY-MM-DDTHH:mm:ss.SSSZ"); //req.params.endTime = 2016-09-25 01:00:00

    console.log('getProducingAssetProductionDb step producingAssets', producingAsset);


    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const MeterModel = dbService.MeterModel;

    //Find
    MeterModel.find({
        asset: producingAsset.name,
        date: {
            $gt:  startDate,
            $lt:  endDate
        }
    }, function(error, meters) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {

          //Begin with dates array
          //Get all the dates
          let indexInit = meters.map(meter => {
            return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ");
          })

          //Remove duplicates
          let index = indexInit.filter((date, pos, indexFull) => {
            console.log(pos, indexFull.indexOf(date), typeof date)
            return indexFull.indexOf(date) == pos;
          })

          //Sort dates
          let sortedIndex = index.sort((dateA, dateB) => {
            return moment(dateA).format('x') - moment(dateB).format('x')
          });

          //Then build the asset's name array
          //Get all the assets
          let assetsInit = meters.map(meter => {
            return meter.asset;
          })

          //Remove duplicates
          let assets = assetsInit.filter((name, pos) => {
            return assetsInit.indexOf(name) === pos;
          })

          //Sort names
          let sortedAssets = assets.sort((nameA, nameB) => {
            return nameA - nameB
          });

          let data = [];

          sortedIndex.map((date, index) => {

            data[index] = [];

            //For each date, find the meters
            let metersForDate = meters.filter(meter => {
              return moment(meter.date).format("YYYY-MM-DDTHH:mm:ssZ") === date
            })

            //We can now push the measures in the data array for this date
            sortedAssets.map(name => {
              let metersForAssetAndDay = metersForDate.filter( meter => {
                return meter.asset === name
              });
              if( metersForAssetAndDay.length > 1 ) {
                throw new Error("There is several meters for asset " + name + " on date " + date);
              }
              if( metersForAssetAndDay.length < 1 ) {
                data[index].push("None")
              } else {
                data[index].push(metersForAssetAndDay[0].quantity)
              }
            })
          })

          let ids = [producingAsset.id];
          let labels = [producingAsset.displayedName];

          res.status(201).send({
            index:sortedIndex,
            data: data,
            columns:sortedAssets,
            labels:labels,
            ids:ids
          });
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

exports.getProducingAssetProductionCertificatesDb = async (req, res) => {

  try {

    const start = req.query.start ? req.query.start : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('YYYY-MM-DD');
    const end = req.query.end ? req.query.end : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('YYYY-MM-DD');
    const assetId = req.params.assetId;

    //Then we build the query

    let producingAsset = await fetchProducingAssetDb(assetId)
    let certificates = await fetchProducingAssetCertificatesDb(assetId, start, end);

    //Begin with dates array
    //Get all the dates

    const minDateRange = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('X')
    const maxDateRange = end ? moment(end, 'YYYY-MM-DD').add(1, "days").set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    //Change the format of the date to display it in the charts
    let index = allDates.map(date => {
      return moment(date).format("YYYY-MM-DDTHH:mm:ssZ");
    })

    let production = [];
    let matchedProduction = [];

    index.map((date, idx) => {

      production[idx] = 0;
      matchedProduction[idx] = 0;

      //For each date, find the certificates
      let certificatesForDate = certificates.filter(certificate => {
        //return moment(certificate._productionDate).format("YYYY-MM-DDTHH:mm:ssZ") === date
        return moment(certificate._productionDate).format("YYYY-MM-DD") === moment(date).format("YYYY-MM-DD")
      })

      certificatesForDate.map(c => {
        production[idx] += c._powerInW/1000; //kWh instead of Wh

        if( c._consumptionAssetId >= 0) {
          matchedProduction[idx] += c._powerInW/1000; //kWh instead of Wh
        }
      })
    })

    res.status(201).send({
      asset:producingAsset,
      production:production,
      matchedProduction:matchedProduction,
      dates: index.map(date => moment(date).format('YYYY-MM-DD'))
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}


exports.getConsumingSiteCalculatedDataDb = async (req, res) => {

  try {

    let consumingSitesData = await fetchConsumingAssetsDb()
    let producingAssetsData = await fetchProducingAssetsDb()

    const asset = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    const start = req.query.start;
    const end = req.query.end;

    let startDate = !start ? moment().subtract(30, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    if(!asset) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
      return
    }

    if(start && !moment(start, 'YYYY-MM-DD').isValid()) {
      res.status(400).send({
        error:"Bad start date format (YYYY-MM-DD)",
      });
      res.end();
      return
    }

    if(end && !moment(end, 'YYYY-MM-DD').isValid()) {
      res.status(400).send({
        error:"Bad end date format (YYYY-MM-DD)",
      });
      res.end();
      return
    }

    let url = process.env.API_URL + '/certificates';
    url += '?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';
    url += '&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

    let allCertificatesOld =  await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(result => {
        return result
      })
      .catch(error => {
        console.log(error);
      });

    let urlv2 = process.env.API_URL + '/certificates-consumption-asset-db?start=' + startDate + '&end=' + endDate + '&assetsIds='+ req.params.assetId;


    let allCertificates =  await fetch(urlv2, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(result => {
        return result
      })
      .catch(error => {
        console.log(error);
      });

    /*console.log('❤️********************************************************************** START')
    console.log(allCertificates)
    console.log('❤️❤️❤️❤️❤️ V2 certificates : ********************************************************************** ')
    console.log(allCertificatesv2)
    console.log('❤️********************************************************************** END')
    */

    let urlPa = process.env.API_URL + '/producing-assets-offchain-db';
    const responsePa = await fetch(urlPa, { method:'GET' });
    const allProducingAssets = await responsePa.json();

    //Get the current asset and its owner
    //Find the producingAssets of the current asset

    //_powerInW
    //_owner
    //_productionDate
    //_assetId
    //_certificateId

    allCertificates = allCertificates.filter(certificate => {
      return !certificate._retired;
    });

    allCertificates.sort((certificateA, certificateB) => {
      return certificateA._productionDate - certificateB._productionDate;
    });

    //Manage all the dates
    const minDate = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(31, 'days').format('X')
    const maxDate = end ? moment(end, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

    const minDateRange = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('X')
    const maxDateRange = end ? moment(end, 'YYYY-MM-DD').add(1, "days").set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

    //Filter to keep only the certificates that are in the range of dates

    //console.log('Before FILTER allCertificates.length '+allCertificates.length )
    /*allCertificates = allCertificates.filter((c, index) => {
      const certifDate = parseInt(c._productionDate)

      return (certifDate >= minDate && certifDate <= maxDate);
    });*/

    //console.log('AFTER FILTER allCertificates.length '+allCertificates.length )

    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    //Change the format of the date to display it in the charts
    allDates = allDates.map(date => {
      return moment(date).format('YYYY-MM-DD')
    })

    //Just to be sure to have the dates in the correct order
    allDates.sort((dateA, dateB) => {
      const unixDateA =  moment(dateA, 'YYYY-MM-DD').format('X')
      const unixDateB =  moment(dateB, 'YYYY-MM-DD').format('X')
      return unixDateA - unixDateB;
    });

    const barChartLabels = allDates;

    //Don't keep the fake / demo assets
    let allRealProducingAssets = allProducingAssets.filter(p => {
      return p.isFake !== true
    });

    allRealProducingAssets = allProducingAssets.filter(p => {
      return asset.producingAssets.find(pa => {
        return p.id === pa.id
      })
    });

    let totalAvoidedCo2 = 0;
    let totalEnergyMatched = 0;

    //We sort the producing assets by priority, used in the same order later in the charts
    allRealProducingAssets.sort((prodAssetA, prodAssetB) => {
      const prodAssetAWithPriority =  asset.producingAssets.find(p => {
        return p.id === prodAssetA.id
      })

      const prodAssetAPriority =  prodAssetAWithPriority.priority

      const prodAssetBWithPriority =  asset.producingAssets.find(p => {
        return p.id === prodAssetB.id
      })

      const prodAssetBPriority =  prodAssetBWithPriority.priority

      return prodAssetBPriority - prodAssetAPriority;
    });

    let barChartData = [];
    const doughnutData = [];
    let barChartDataMatched = [];

    const totalEnergyMatchedByAsset = []

    allRealProducingAssets.map((pA, index) => {

      const assetCertificates = allCertificates.filter(certificate => {
        //return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10) && certificate._owner === asset.owner
        if( pA.isFake !== true ) {
          return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10)
        }
        else {
          return parseInt(certificate._assetId, 10) === parseInt(pA.duplicateOfAssetId, 10)
        }

      })

      const ownerCertificates = allCertificates.filter(certificate => {
        if( pA.isFake !== true ) {
          return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10) && certificate._owner === asset.owner
        }
        else {
          return parseInt(certificate._assetId, 10) === parseInt(pA.duplicateOfAssetId, 10) && certificate._owner === asset.owner
        }
      })


      barChartData[index] = {
        data: [],
        label: pA.displayedName,
        type: 'bar',
        assetType: pA.assetType
      };

      barChartDataMatched[index] = {
        data: [],
        label: pA.displayedName,
        type: 'bar',
        assetType: pA.assetType
      };

      assetCertificates.map(c => {
        //What is the date of the certificate ?
        //const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
        const certifDate = moment(c._productionDate).add(6, "hours").format('YYYY-MM-DD');
        //6 hours added is a patch. Need to manage utc, timezone...

        //Find the index in allDates for this date
        const dateIndex = allDates.indexOf(certifDate);

        //is there already a data for this date : if no, create it, otherwise, add the value to the existing value
        if(barChartData[index].data[dateIndex]) {
          //Sum all the certificates for this asset for that day
          barChartData[index].data[dateIndex] += parseFloat(c._powerInW)/1000;
        } else {
          barChartData[index].data[dateIndex] = parseFloat(c._powerInW)/1000;
        }
      })


      let pAEnergyMatched = 0;

      ownerCertificates.map(c => {

        //const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
        const certifDate = moment(c._productionDate).add(6, "hours").format('YYYY-MM-DD');
        //6 hours added is a patch. Need to manage utc, timezone...

        //Find the index in allDates for this date
        const dateIndex = allDates.indexOf(certifDate);

        //is there already a data for this date : if no, create it, otherwise, add the value to the existing value
        if(barChartDataMatched[index].data[dateIndex]) {
          //Sum all the certificates for this asset for that day
          barChartDataMatched[index].data[dateIndex] += parseFloat(c._powerInW)/1000;
        } else {
          barChartDataMatched[index].data[dateIndex] = parseFloat(c._powerInW)/1000;
        }
        pAEnergyMatched += parseFloat(c._powerInW)/1000;
        totalEnergyMatched += parseFloat(c._powerInW)/1000;
      })


      totalEnergyMatchedByAsset.push({
        energyMatched: pAEnergyMatched,
        id:  pA.id,
        label: pA.displayedName
      });


    })


    //Now, retrieve the consumption data of the asset

    //console.log(startDate+' ----->  '+endDate)
    let consumingSiteDailyConsumption = []

    let urlConsumingSiteTimeSeries = process.env.API_URL + '/consuming-assets-offchain/'+asset.id+'/consumptiondb';
    urlConsumingSiteTimeSeries += '?withProduction=false';
    urlConsumingSiteTimeSeries += '&start='+startDate;
    urlConsumingSiteTimeSeries += '&end='+endDate;

    const consumingSiteDailyConsumptionData = await fetch(urlConsumingSiteTimeSeries, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(handleErrors).then(res => res.json()) .then(data => { return data })
    .catch(error => { console.log(error); });
    consumingSiteDailyConsumption = consumingSiteDailyConsumptionData.data;

    const consumptionData = [];
    const consumptionPercentage = asset.consumptionPercentage ? asset.consumptionPercentage : 1;
    let totalConsumption = 0;

    consumingSiteDailyConsumption.data.map((consumptionArray, idx) => {
      //The consumption value is the last item of the array
      const dailyConsumption = consumptionArray[consumptionArray.length - 1];
      let day = consumingSiteDailyConsumption.index[idx];
      let dayOriginal = moment(day).format('YYYY-MM-DD')
      day = moment(day).add(6, "hours").format('YYYY-MM-DD')

      const dateIndex = allDates.indexOf(day);
      consumptionData[dateIndex] = dailyConsumption * consumptionPercentage;
      totalConsumption += dailyConsumption * consumptionPercentage;
    })

    //Let's calculate the totalAvoidedCo2 between start and end
    //For each day, find the co2 generated by the french energy mix
    //Then calculate the sum for each day of the energy matched by the asset
    barChartLabels.map((day, dayIndex) => {

      //Get the CO2 for that day
      let co2DataForDay = co2.find(item => {
        return item.date === day
      })
      let co2ForDay = 40; //use the average default value, 40g of CO2/kWh
      if(co2DataForDay !== undefined && co2DataForDay.CO2 && !isNaN(co2DataForDay.CO2)) {
        co2ForDay = co2DataForDay.CO2
      } else {
        console.log("CO2 pb for date "+ day + ":");
        console.log(JSON.stringify(co2DataForDay))

      }
      //Initiate the energy matched for that day at 0, we will add the energy just after that
      let energyMatchedForDay = 0;

      //At this step, we have only the producing assets in barChartDataMatched
      //Let's calculate the energy matched for that day
      //For each prod asset, find the energy matched that day

      doughnutData[dayIndex] = [];
      let assetConsumption = consumptionData[dayIndex] ? consumptionData[dayIndex] : 0;



      barChartDataMatched.map((pAsset, pAssetIdx) => {

        const defaultValue = (pAssetIdx === 0) ? 100 : 0;

        let assetMatchedProdForDay = pAsset.data[dayIndex] ? pAsset.data[dayIndex] : 0;

        doughnutData[dayIndex].push(( assetConsumption > 0 ) ? (assetMatchedProdForDay/assetConsumption)*100 : false)
        energyMatchedForDay += assetMatchedProdForDay;

      })

      /*if ( (assetConsumption - energyMatchedForDay) > 0) {

        console.log('---------------------------------------------')
        console.log("Day "+ day +" ("+ dayIndex +") has energy mix "+(assetConsumption - energyMatchedForDay))
        console.log("            assetConsumption "+assetConsumption)
        console.log("            energyMatchedForDay "+energyMatchedForDay)
      }*/

      const energyMixPercentageForDay = ( assetConsumption > 0 && (assetConsumption - energyMatchedForDay) > 0) ? ((assetConsumption - energyMatchedForDay)/assetConsumption)*100 : 0

      //If the energyMixPercentageForDay is 0 for energy mix, we need to verify that the sum of each producing asset percentage is 100
      if(energyMixPercentageForDay === 0) {
        let totalPercentageInit = 0;
        doughnutData[dayIndex].map(dayPercentage => {
          totalPercentageInit += dayPercentage;
        })

        doughnutData[dayIndex] = doughnutData[dayIndex].map(dayPercentage => {
          return totalPercentageInit > 0 ? 100*dayPercentage/totalPercentageInit : false
        })
      }

      doughnutData[dayIndex].push(energyMixPercentageForDay);

      totalAvoidedCo2 += energyMatchedForDay*co2ForDay


    })



    let doughnutDataCleaned = doughnutData.filter(percentagesDayData => {
      const hasNoData = percentagesDayData.find(percentage => {
        return percentage === false
      })
      return (hasNoData === false) ? false : true
    })

    //Add the consumption assets data :
    barChartData.push({
      data: consumptionData,
      label: asset.name,
      type: 'line',
      assetType: 'consumption'
    });

    //console.log(barChartDataMatched)
    allDates.map((myCurrentDate, index) => {
      barChartDataMatched = barChartDataMatched.map( currentAsset => {
        if (!currentAsset.data[index]) {
          currentAsset.data[index] = 0; //No certificates found for taht date
        }
        return currentAsset;
      })
    });


    barChartDataMatched.push({
      data: consumptionData,
      label: asset.name,
      type: 'line',
      assetType: 'consumption'
    });

    let doughnutChartLabels = asset.producingAssets.map(prodAsset => {

      let currentProdAsset = allRealProducingAssets.find(p => {
        return prodAsset.id === p.id
      })
      return currentProdAsset.displayedName;
    });

    doughnutChartLabels.push('Energy Mix')

    let totalPercentage = [];
    for (var i = 0; i < doughnutDataCleaned.length; i++) {
      doughnutDataCleaned[i].map((dataPercentage, index) => {
          totalPercentage[index] = isNaN(totalPercentage[index]) ? dataPercentage : totalPercentage[index]+dataPercentage;
      })
    }

    let doughnutChartData = totalPercentage.map(sitePercentage => {
      return parseInt(sitePercentage*10/doughnutDataCleaned.length, 10)/10;
    });

    //let doughnutChartLastDayData = doughnutDataCleaned[doughnutDataCleaned.length - 1].map(data => data);
    //If the last day doesn't has no certificates, we will display the last day with certificates :
    const doughnutDataTemp = doughnutDataCleaned.map(data => data);
    const dates = barChartLabels.map(data => data);

    //Since the "find" function can't work from the end of the array, we reverse the array.
    doughnutData.reverse();
    //If wo do that for the production data, we need to reverse the dates array, it will be more convenient later
    dates.reverse()

    //Find the value of the last day that has certificates
    const lastDayValues = doughnutData.find(dataOfTheDay => {
      let hasCertificateThatDay = false;
      //remove the consumption data
      let dataOfTheDayTemp = dataOfTheDay.map(d => d);
      dataOfTheDayTemp.splice(dataOfTheDay.length - 1, 1);

      dataOfTheDayTemp.map(energyProduced => {
        if(energyProduced > 0) hasCertificateThatDay = true
      })

      return hasCertificateThatDay;
    })

    //Find the index of the last day that has certificates (use it to find the day in the "dates" array)
    const lastDayIndex = doughnutData.findIndex(dataOfTheDay => {
      let hasCertificateThatDay = false;
      //remove the consumption data
      let dataOfTheDayTemp = dataOfTheDay.map(d => d);
      dataOfTheDayTemp.splice(dataOfTheDay.length - 1, 1);

      dataOfTheDayTemp.map(energyProduced => {
        if(energyProduced > 0) hasCertificateThatDay = true
      })

      return hasCertificateThatDay;
    })

    const lastDayDate = dates[lastDayIndex];
    let doughnutChartLastDayData = lastDayValues;


    let graphData = {
      doughnutChartLabels: doughnutChartLabels,
      doughnutChartData: doughnutChartData,
      doughnutChartLastDayData: lastDayValues,
      doughnutChartLastDayDay: lastDayDate,
      doughnutData: doughnutDataCleaned,
      barChartData: barChartData,                           //Done
      barChartDataMatched: barChartDataMatched,             //Done
      barChartLabels: barChartLabels,                      //Done
      totalAvoidedCo2: totalAvoidedCo2,                    //Done
      totalConsumption: totalConsumption,                  //Done
      totalEnergyMatched: totalEnergyMatched,              //Done
      totalEnergyMatchedByAsset: totalEnergyMatchedByAsset //Done
    }

    res.status(201).send({
      data:graphData
    });


    /*
    let graphData = {
      doughnutChartLabels: doughnutChartLabels2,
      doughnutChartData: doughnutChartData2,
      doughnutChartLastDayData: doughnutChartLastDayData2,
      doughnutData: doughnutData2,
      barChartData: barChartData2,                          //Done
      barChartLabels: barChartLabels2,                      //Done
      totalAvoidedCo2: totalAvoidedCo22,                    //Done
      totalConsumption: totalConsumption2,                  //Done
      totalEnergyMatched: totalEnergyMatched2,              //Done
      totalEnergyMatchedByAsset: totalEnergyMatchedByAsset2 //Done
    }
    */

  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getConsumingSiteCachedData = async (req, res) => {

  try {
    var mongoose = require('mongoose');
    mongoose.connect(process.env.DB_PATH);

    mongoose.Promise = global.Promise;
    //Get the default connection
    var db = mongoose.connection;

    //Bind connection to error event (to get notification of connection errors)
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const ConsumingAssetsAggregation = dbService.ConsumingAssetsAggregation;

    //Find
    ConsumingAssetsAggregation.find({
        assetId: parseInt(req.params.assetId),
        range: req.query.range
    }, function(error, graphData) {
        if (error) {
          console.log("Error : ", error);
          res.status(400).send({
            message:error.message
          });
          res.end();
        }
        else {
          res.json(graphData[0]);
        }
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};



exports.getConsumingSiteCalculatedData = async (req, res) => {

  try {
    //check the data
    const startingAt = moment().format('X');

    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;
    const producingAssetsData = myAssets.producingAssets;

    const asset = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    const step1 = moment().format('X');
    console.log("Step 1 time = "+(step1 - startingAt)+"s");

    const start = req.query.start;
    const end = req.query.end;

    let startDate = !start ? moment().subtract(30, 'days').format('YYYY-MM-DD') : start; //By default, yesterday
    let endDate = !end ? moment().format('YYYY-MM-DD') : end; //By default, today

    if(!asset) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }

    if(start && !moment(start, 'YYYY-MM-DD').isValid()) {
      res.status(400).send({
        error:"Bad start date format (YYYY-MM-DD)",
      });
      res.end();
    }

    if(end && !moment(end, 'YYYY-MM-DD').isValid()) {
      res.status(400).send({
        error:"Bad end date format (YYYY-MM-DD)",
      });
      res.end();
    }

    let url = process.env.API_URL + '/certificates';
    url += '?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';
    url += '&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';


    const step2 = moment().format('X');
    console.log("Step 2 time = "+(step2 - step1)+"s");

    let allCertificates =  await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(result => {
        return result
      })
      .catch(error => {
        console.log(error);
      });

    const step3 = moment().format('X');
    console.log("Step 3 time = "+(step3 - step2)+"s");

    let urlPa = process.env.API_URL + '/producing-assets-offchain';
    const responsePa = await fetch(urlPa, { method:'GET' });
    const allPasResults = await responsePa.json();
    const allProducingAssets = allPasResults.producingAssets;

    const step4 = moment().format('X');
    console.log("Step 4 time = "+(step4 - step3)+"s");

    //Get the current asset and its owner
    //Find the producingAssets of the current asset

    //_powerInW
    //_owner
    //_productionDate
    //_assetId
    //_certificateId

    allCertificates = allCertificates.filter(certificate => {
      return !certificate._retired;
    });

    allCertificates.sort((certificateA, certificateB) => {
      return certificateA._productionDate - certificateB._productionDate;
    });

    //Manage all the dates
    const minDate = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(31, 'days').format('X')
    const maxDate = end ? moment(end, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

    const minDateRange = start ? moment(start, 'YYYY-MM-DD').set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('X')
    const maxDateRange = end ? moment(end, 'YYYY-MM-DD').add(1, "days").set({hour:0,minute:0,second:0,millisecond:0}).format('X') : moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

    //Filter to keep only the certificates that are in the range of dates
    allCertificates = allCertificates.filter((c, index) => {
      const certifDate = parseInt(c._productionDate)

      return (certifDate >= minDate && certifDate <= maxDate);
    });

    const step5 = moment().format('X');
    console.log("Step 5 time = "+(step5 - step4)+"s");

    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    //Change the format of the date to display it in the charts
    allDates = allDates.map(date => {
      return moment(date).format('YYYY-MM-DD')
    })

    //Just to be sure to have the dates in the correct order
    allDates.sort((dateA, dateB) => {
      const unixDateA =  moment(dateA, 'YYYY-MM-DD').format('X')
      const unixDateB =  moment(dateB, 'YYYY-MM-DD').format('X')
      return unixDateA - unixDateB;
    });

    const barChartLabels = allDates;

    //Don't keep the fake / demo assets
    let allRealProducingAssets = allProducingAssets.filter(p => {
      return p.isFake !== true
    });

    allRealProducingAssets = allProducingAssets.filter(p => {
      return asset.producingAssets.find(pa => {
        return p.id === pa.id
      })
    });

    const step6 = moment().format('X');
    console.log("Step 6 time = "+(step6 - step5)+"s");

    /*
    let graphData = {
      doughnutChartLabels: doughnutChartLabels2,
      doughnutChartData: doughnutChartData2,
      doughnutChartLastDayData: doughnutChartLastDayData2,
      doughnutData: doughnutData2,
      barChartData: barChartData2,
      barChartLabels: barChartLabels2,
      totalAvoidedCo2: totalAvoidedCo22,
      totalConsumption: totalConsumption2,
      totalEnergyMatched: totalEnergyMatched2,
      totalEnergyMatchedByAsset: totalEnergyMatchedByAsset2
    }
    */

    let totalAvoidedCo2 = 0;
    let totalEnergyMatched = 0;

    //We sort the producing assets by priority, used in the same order later in the charts
    allRealProducingAssets.sort((prodAssetA, prodAssetB) => {
      const prodAssetAWithPriority =  asset.producingAssets.find(p => {
        return p.id === prodAssetA.id
      })

      const prodAssetAPriority =  prodAssetAWithPriority.priority

      const prodAssetBWithPriority =  asset.producingAssets.find(p => {
        return p.id === prodAssetB.id
      })

      const prodAssetBPriority =  prodAssetBWithPriority.priority

      return prodAssetBPriority - prodAssetAPriority;
    });

    const step7 = moment().format('X');
    console.log("Step 7 time = "+(step7 - step6)+"s");

    const barChartData = [];
    const doughnutData = [];
    const barChartDataMatched = [];

    const totalEnergyMatchedByAsset = []

    allRealProducingAssets.map((pA, index) => {

      const assetCertificates = allCertificates.filter(certificate => {
        //return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10) && certificate._owner === asset.owner
        if( pA.isFake !== true ) {
          return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10)
        }
        else {
          return parseInt(certificate._assetId, 10) === parseInt(pA.duplicateOfAssetId, 10)
        }

      })

      const ownerCertificates = allCertificates.filter(certificate => {
        if( pA.isFake !== true ) {
          return parseInt(certificate._assetId, 10) === parseInt(pA.id, 10) && certificate._owner === asset.owner
        }
        else {
          return parseInt(certificate._assetId, 10) === parseInt(pA.duplicateOfAssetId, 10) && certificate._owner === asset.owner
        }
      })

      barChartData[index] = {
        data: [],
        label: pA.displayedName,
        type: 'bar'
      };


      barChartDataMatched[index] = {
        data: [],
        label: pA.displayedName,
        type: 'bar'
      };

      assetCertificates.map(c => {
        //What is the date of the certificate ?
        const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
        //6 hours added is a patch. Need to manage utc, timezone...

        //Find the index in allDates for this date
        const dateIndex = allDates.indexOf(certifDate);

        //is there already a data for this date : if no, create it, otherwise, add the value to the existing value
        if(barChartData[index].data[dateIndex]) {
          //Sum all the certificates for this asset for that day
          barChartData[index].data[dateIndex] += parseFloat(c._powerInW)/1000;
        } else {
          barChartData[index].data[dateIndex] = parseFloat(c._powerInW)/1000;
        }
      })


      let pAEnergyMatched = 0;

      ownerCertificates.map(c => {

        const certifDate = moment.unix(parseInt(c._productionDate)).add(6, "hours").format('YYYY-MM-DD');
        //6 hours added is a patch. Need to manage utc, timezone...

        //Find the index in allDates for this date
        const dateIndex = allDates.indexOf(certifDate);

        //is there already a data for this date : if no, create it, otherwise, add the value to the existing value
        if(barChartDataMatched[index].data[dateIndex]) {
          //Sum all the certificates for this asset for that day
          barChartDataMatched[index].data[dateIndex] += parseFloat(c._powerInW)/1000;
        } else {
          barChartDataMatched[index].data[dateIndex] = parseFloat(c._powerInW)/1000;
        }
        pAEnergyMatched += parseFloat(c._powerInW)/1000;
        totalEnergyMatched += parseFloat(c._powerInW)/1000;
      })


      totalEnergyMatchedByAsset.push({
        energyMatched: pAEnergyMatched,
        id:  pA.id,
        label: pA.displayedName
      });


    })


    const step8 = moment().format('X');
    console.log("Step 8 time = "+(step8 - step7)+"s");

    //Now, retrieve the consumption data of the asset

    //console.log(startDate+' ----->  '+endDate)
    let consumingSiteDailyConsumption = []

    let urlConsumingSiteTimeSeries = process.env.API_URL + '/consuming-assets-offchain/'+asset.id+'/consumption';
    urlConsumingSiteTimeSeries += '?withProduction=false';
    urlConsumingSiteTimeSeries += '&start='+startDate;
    urlConsumingSiteTimeSeries += '&end='+endDate;

    const consumingSiteDailyConsumptionData = await fetch(urlConsumingSiteTimeSeries, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(handleErrors).then(res => res.json()) .then(data => { return data })
    .catch(error => { console.log(error); });

    const step9 = moment().format('X');
    console.log("Step 9 time = "+(step9 - step8)+"s");

    consumingSiteDailyConsumption = consumingSiteDailyConsumptionData.data;

    const consumptionData = [];
    const consumptionPercentage = asset.consumptionPercentage ? asset.consumptionPercentage : 1;
    let totalConsumption = 0;

    consumingSiteDailyConsumption.data.map((consumptionArray, idx) => {
      //The consumption value is the last item of the array
      const dailyConsumption = consumptionArray[consumptionArray.length - 1];
      let day = consumingSiteDailyConsumption.index[idx];
      let dayOriginal = moment(day).format('YYYY-MM-DD')
      day = moment(day).add(6, "hours").format('YYYY-MM-DD')

      const dateIndex = allDates.indexOf(day);
      consumptionData[dateIndex] = dailyConsumption * consumptionPercentage;
      totalConsumption += dailyConsumption * consumptionPercentage;
    })

    const step10 = moment().format('X');
    console.log("Step 10 time = "+(step10 - step9)+"s");
    //Let's calculate the totalAvoidedCo2 between start and end
    //For each day, find the co2 generated by the french energy mix
    //Then calculate the sum for each day of the energy matched by the asset
    barChartLabels.map((day, dayIndex) => {

      //Get the CO2 for that day
      let co2DataForDay = co2.find(item => {
        return item.date === day
      })
      let co2ForDay = 40; //use the average default value, 40g of CO2/kWh
      if(co2DataForDay !== undefined && co2DataForDay.CO2 && !isNaN(co2DataForDay.CO2)) {
        co2ForDay = co2DataForDay.CO2
      } else {
        console.log("CO2 pb for date "+ day + ":");
        console.log(JSON.stringify(co2DataForDay))

      }
      //Initiate the energy matched for that day at 0, we will add the energy just after that
      let energyMatchedForDay = 0;

      //At this step, we have only the producing assets in barChartDataMatched
      //Let's calculate the energy matched for that day
      //For each prod asset, find the energy matched that day

      doughnutData[dayIndex] = [];
      let assetConsumption = consumptionData[dayIndex] ? consumptionData[dayIndex] : 0;



      barChartDataMatched.map((pAsset, pAssetIdx) => {

        const defaultValue = (pAssetIdx === 0) ? 100 : 0;

        let assetMatchedProdForDay = pAsset.data[dayIndex] ? pAsset.data[dayIndex] : 0;

        doughnutData[dayIndex].push(( assetConsumption > 0 ) ? (assetMatchedProdForDay/assetConsumption)*100 : false)
        energyMatchedForDay += assetMatchedProdForDay;

      })

      if ( (assetConsumption - energyMatchedForDay) > 0) {

        console.log('---------------------------------------------')
        console.log("Day "+ day +" ("+ dayIndex +") has energy mix "+(assetConsumption - energyMatchedForDay))
        console.log("            assetConsumption "+assetConsumption)
        console.log("            energyMatchedForDay "+energyMatchedForDay)
      }

      const energyMixPercentageForDay = ( assetConsumption > 0 && (assetConsumption - energyMatchedForDay) > 0) ? ((assetConsumption - energyMatchedForDay)/assetConsumption)*100 : 0

      //If the energyMixPercentageForDay is 0 for energy mix, we need to verify that the sum of each producing asset percentage is 100
      if(energyMixPercentageForDay === 0) {
        let totalPercentageInit = 0;
        doughnutData[dayIndex].map(dayPercentage => {
          totalPercentageInit += dayPercentage;
        })

        doughnutData[dayIndex] = doughnutData[dayIndex].map(dayPercentage => {
          return totalPercentageInit > 0 ? 100*dayPercentage/totalPercentageInit : false
        })
      }

      doughnutData[dayIndex].push(energyMixPercentageForDay);

      totalAvoidedCo2 += energyMatchedForDay*co2ForDay


    })


    let doughnutDataCleaned = doughnutData.filter(percentagesDayData => {
      const hasNoData = percentagesDayData.find(percentage => {
        return percentage === false
      })
      return (hasNoData === false) ? false : true
    })

    //Add the consumption assets data :
    barChartData.push({
      data: consumptionData,
      label: asset.name,
      type: 'line'
    });

    barChartDataMatched.push({
      data: consumptionData,
      label: asset.name,
      type: 'line'
    });

    const step11 = moment().format('X');
    console.log("Step 11 time = "+(step11 - step10)+"s");

    let doughnutChartLabels = asset.producingAssets.map(prodAsset => {
      let currentProdAsset = allRealProducingAssets.find(p => {
        return prodAsset.id === p.id
      })
      return currentProdAsset.displayedName;
    });

    doughnutChartLabels.push('Energy Mix')

    let totalPercentage = [];
    for (var i = 0; i < doughnutDataCleaned.length; i++) {
      doughnutDataCleaned[i].map((dataPercentage, index) => {
          totalPercentage[index] = isNaN(totalPercentage[index]) ? dataPercentage : totalPercentage[index]+dataPercentage;
      })
    }
    console.log('totalPercentage')
    console.log(totalPercentage)

    let doughnutChartData = totalPercentage.map(sitePercentage => {
      return parseInt(sitePercentage*10/doughnutDataCleaned.length, 10)/10;
    });

    //let doughnutChartLastDayData = doughnutDataCleaned[doughnutDataCleaned.length - 1].map(data => data);
    //If the last day doesn't has no certificates, we will display the last day with certificates :
    const doughnutDataTemp = doughnutDataCleaned.map(data => data);
    const dates = barChartLabels.map(data => data);

    //Since the "find" function can't work from the end of the array, we reverse the array.
    doughnutData.reverse();
    //If wo do that for the production data, we need to reverse the dates array, it will be more convenient later
    dates.reverse()

    //Find the value of the last day that has certificates
    const lastDayValues = doughnutData.find(dataOfTheDay => {
      let hasCertificateThatDay = false;
      //remove the consumption data
      let dataOfTheDayTemp = dataOfTheDay.map(d => d);
      dataOfTheDayTemp.splice(dataOfTheDay.length - 1, 1);

      dataOfTheDayTemp.map(energyProduced => {
        if(energyProduced > 0) hasCertificateThatDay = true
      })

      return hasCertificateThatDay;
    })

    const step12 = moment().format('X');
    console.log("Step 12 time = "+(step12 - step11)+"s");

    //Find the index of the last day that has certificates (use it to find the day in the "dates" array)
    const lastDayIndex = doughnutData.findIndex(dataOfTheDay => {
      let hasCertificateThatDay = false;
      //remove the consumption data
      let dataOfTheDayTemp = dataOfTheDay.map(d => d);
      dataOfTheDayTemp.splice(dataOfTheDay.length - 1, 1);

      dataOfTheDayTemp.map(energyProduced => {
        if(energyProduced > 0) hasCertificateThatDay = true
      })

      return hasCertificateThatDay;
    })

    const lastDayDate = dates[lastDayIndex];
    let doughnutChartLastDayData = lastDayValues;


    let graphData = {
      doughnutChartLabels: doughnutChartLabels,
      doughnutChartData: doughnutChartData,
      doughnutChartLastDayData: lastDayValues,
      doughnutChartLastDayDay: lastDayDate,
      doughnutData: doughnutDataCleaned,
      barChartData: barChartData,                           //Done
      barChartDataMatched: barChartDataMatched,             //Done
      barChartLabels: barChartLabels,                      //Done
      totalAvoidedCo2: totalAvoidedCo2,                    //Done
      totalConsumption: totalConsumption,                  //Done
      totalEnergyMatched: totalEnergyMatched,              //Done
      totalEnergyMatchedByAsset: totalEnergyMatchedByAsset //Done
    }

    res.status(201).send({
      data:graphData
    });


    /*
    let graphData = {
      doughnutChartLabels: doughnutChartLabels2,
      doughnutChartData: doughnutChartData2,
      doughnutChartLastDayData: doughnutChartLastDayData2,
      doughnutData: doughnutData2,
      barChartData: barChartData2,                          //Done
      barChartLabels: barChartLabels2,                      //Done
      totalAvoidedCo2: totalAvoidedCo22,                    //Done
      totalConsumption: totalConsumption2,                  //Done
      totalEnergyMatched: totalEnergyMatched2,              //Done
      totalEnergyMatchedByAsset: totalEnergyMatchedByAsset2 //Done
    }
    */

  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.getUsers = async (req, res) => {

  try {
    var request = require("request");

    const tokenRequestResult = await fetch(
      process.env.AUTH_URL+'/oauth/token',
      {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: '{"client_id": "'+process.env.AUTH_CLIENT_ID+'","client_secret":"'+process.env.AUTH_CLIENT_SECRET+'","audience":"'+process.env.AUTH_URL+'/api/v2/","grant_type":"client_credentials"}'
      }
    )
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in getUsers : ");
      console.log(error);
    });

    const options = {
      method: 'GET',
      url: process.env.AUTH_URL+'/api/v2/users',
      qs: { search_engine: 'v3' },
      headers: { authorization: 'Bearer ' + tokenRequestResult.access_token }
    };

    request(options, async function (error, response, body) {
      if (error) throw new Error(error);

      res.status(201).send({
        data:JSON.parse(body)
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getUserByEmail = async (req, res) => {
  try {
    const email = req.query.email;

    const request = require("request");

    const tokenRequestResult = await fetch(
      process.env.AUTH_URL+'/oauth/token',
      {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: '{"client_id": "'+process.env.AUTH_CLIENT_ID+'","client_secret":"'+process.env.AUTH_CLIENT_SECRET+'","audience":"'+process.env.AUTH_URL+'/api/v2/","grant_type":"client_credentials"}'
      }
    )
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in getUserByEmail : ");
      console.log(error);
    });

    const options = {
      method: 'GET',
      url: process.env.AUTH_URL+'/api/v2/users',
      qs: { q: 'email:"'+email+'"', search_engine: 'v3' },
      headers: { authorization: 'Bearer ' + tokenRequestResult.access_token }
    };

    request(options, async function (error, response, body) {
      if (error) throw new Error(error);

      res.status(201).send({
        data:JSON.parse(body)
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getUserByAddress = async (req, res) => {
  try {
    const address = req.query.address;

    const request = require("request");

    const tokenRequestResult = await fetch(
      process.env.AUTH_URL+'/oauth/token',
      {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: '{"client_id": "'+process.env.AUTH_CLIENT_ID+'","client_secret":"'+process.env.AUTH_CLIENT_SECRET+'","audience":"'+process.env.AUTH_URL+'/api/v2/","grant_type":"client_credentials"}',
      }
    )
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in getUserByAddress : ");
      console.log(error);
    });

    const options = {
      method: 'GET',
      url: process.env.AUTH_URL+'/api/v2/users',
      qs: { q: 'app_metadata.address:"'+address+'" AND app_metadata.realAccount:true', search_engine: 'v3' },
      headers: { authorization: 'Bearer ' + tokenRequestResult.access_token }
    };

    request(options, async function (error, response, body) {
      if (error) throw new Error(error);

      res.status(201).send({
        data:JSON.parse(body)
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.setUserByAddress = async (req, res) => {
  try {
    const address = req.params.userAddress

    const request = require("request");

    const tokenRequestResult = await fetch(
      process.env.AUTH_URL+'/oauth/token',
      {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: '{"client_id": "'+process.env.AUTH_CLIENT_ID+'","client_secret":"'+process.env.AUTH_CLIENT_SECRET+'","audience":"'+process.env.AUTH_URL+'/api/v2/","grant_type":"client_credentials"}'
      }
    )
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in getUserByAddress : ");
      console.log(error);
    });

    const options = {
      method: 'GET',
      url: process.env.AUTH_URL+'/api/v2/users',
      qs: { q: 'app_metadata.address:"'+address+'"', search_engine: 'v3' },
      headers: { authorization: 'Bearer ' + tokenRequestResult.access_token }
    };

    let userData = await request(options, async function (error, response, body) {
      if (error) throw new Error(error);
      const userData = JSON.parse(body);

      if( !userData ) {
        throw new Error('Unable to update the user : the user is undefined for address '+address);
      }
      else if(userData.length < 1) {
        throw new Error('Unable to update the user : no users retrieved with address '+address);
      }
      else if(userData.length > 1) {
        throw new Error('Unable to update the user : several users ('+userData.length+') retrieved with address '+address);
      } else {
        const userToUpdate = userData[0];


        console.log('userToUpdate.user_metadata', userToUpdate.user_metadata)
        const new_user_metadata = {...userToUpdate.user_metadata}
        console.log('userToUpdate.new_user_metadata', new_user_metadata)

        const updateOptions = {
          method: 'PATCH',
          url: process.env.AUTH_URL+'/api/v2/users/'+userToUpdate.user_id,
          body: { "user_metadata" : new_user_metadata},
          headers: { authorization: 'Bearer ' + tokenRequestResult.access_token },
          json: true
        };

        request(updateOptions, async function (error, response, body) {
          if (error) throw new Error(error);

          res.status(201).send({
            data:body
          });
        });


      }

    });

    //userData = JSON.parse(userData)



  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};



exports.setSiteCo2Data = async (req, res) => {

  try {
    //check the data
    const consumingSiteCheck = consumingSitesData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })

    if(!consumingSiteCheck) {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }

    if(!req.body.avoidedCo2) {
      res.status(403).send({ error: "avoidedCo2 is undefined" });
      res.end();
      throw "avoidedCo2 id undefined";
    }

    if(isNaN(parseInt(req.body.avoidedCo2))) {
      res.status(403).send({ error: "avoidedCo2 is not a number" });
      res.end();
      throw "avoidedCo2 is not a number";
    }

    let consumingSiteUpdated = {}
    const consumingSitesUpdated = consumingSitesData.map(site => {
      if(parseInt(req.params.assetId) === parseInt(site.id) && req.body.avoidedCo2){
        site.avoidedCo2 = parseInt(req.body.avoidedCo2);
        consumingSiteUpdated = site;
      }
      return site;
    })

    let consumingSitesUpdatedData = JSON.stringify(consumingSitesUpdated, null, 4);

    fs.writeFile(path.join(__dirname, '../db/json') + '/consumingSites.json', consumingSitesUpdatedData, (error) => {
      if (error) {
        res.status(403).send({ error });
        res.end();
        throw error;
      }
      res.status(201).send({
        site:consumingSiteUpdated
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getProducingAssets = async (req, res) => {
  try {
    readJson('../db/json/producingAssets.json', (err, producingAssets) => {
      //console.log('producingAssets')
      producingAssets.map(cs => {
        //console.log(cs)
      })
      res.status(201).send({
        producingAssets:producingAssets
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.getProducingAsset = async (req, res) => {

  try {
    const myAssets = await getAssets();
    const consumingSitesData = myAssets.consumingSites;
    const producingAssetsData = myAssets.producingAssets;

    const producingAsset = producingAssetsData.find(site => {
      return parseInt(req.params.assetId) === parseInt(site.id);
    })
    if(producingAsset) {
      res.status(201).send(
        producingAsset
      );
    } else {
      res.status(404).send({
        error:"Bad asset id",
      });
      res.end();
    }
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

exports.getCo2 = async (req, res) => {

  try {
    res.status(201).send({
      co2:co2
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.getDeliveriesCertificates = async (req, res) => {
  try {
    res.status(201).send({
      data:transportCertificates
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.updateCache = async (req, res) => {

  try {
    console.log("updateCache start")
    let startDate = moment().subtract(30, 'days').format('YYYY-MM-DD'); //By default, yesterday
    let endDate = moment().format('YYYY-MM-DD'); //By default, today

    readJson('../db/json/consumingSites.json', async (err, consumingSitesOffchain) => {
      console.log('consumingSitesOffchain')

      for (let cs of consumingSitesOffchain) {

        let url30Days = process.env.API_URL + '/consuming-assets-offchain/'+cs.id+'/consumption';
        url30Days += '?withProduction=true';
        url30Days += '&start='+startDate;
        url30Days += '&end='+endDate;


        let urlYearToDate = process.env.API_URL + '/consuming-assets-offchain/'+cs.id+'/consumption';
        urlYearToDate += '?withProduction=true';
        urlYearToDate += '&start=2018-01-01';
        urlYearToDate += '&end='+endDate;

        const result30Days = await fetch(url30Days, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          }
        })
          .then(handleErrors)
          .then(res => res.json())
          .then(data => {
            console.log("updateCacheData data30Days retrieved for "+cs.name)
            return data
          })
          .catch(error => {
            console.log("error in updateCacheData : ");
            console.log(error);
          });

        const resultYearToDate = await fetch(urlYearToDate, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json; charset=utf-8'
          }
        })
          .then(handleErrors)
          .then(res => res.json())
          .then(data => {
            console.log("updateCacheData dataYearToDate retrieved for "+cs.name)
            return data
          })
          .catch(error => {
            console.log("error in updateCacheData : ");
            console.log(error);
          });

        const reqSiteTimeSeries30Days = result30Days.data;
        const reqSiteTimeSeriesYearToDate = resultYearToDate.data;

        console.log("updateCacheData timeseries ready to save for "+cs.name)

        let timeSeries30Days = JSON.stringify(reqSiteTimeSeries30Days, null, 4);
        let timeSeriesYearToDate = JSON.stringify(reqSiteTimeSeriesYearToDate, null, 4);

        fs.writeFileSync(path.join(__dirname, '../db/json') + '/'+cs.smireName+'_'+cs.id+'30Days.json', timeSeries30Days);
        fs.writeFileSync(path.join(__dirname, '../db/json') + '/'+cs.smireName+'_'+cs.id+'YearToDate.json', timeSeriesYearToDate);

        console.log("updateCacheData done for "+cs.name)
      }

      console.log("updateCacheData finished")
      res.status(201).send({
        status:"ok"
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};


exports.updateCo2Data = async (req, resRequest) => {

  const url_cons_def = 'https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=eco2mix-national-cons-def&q=taux_co2>0&rows=-1&facet=nature&facet=date_heure&refine.date_heure=2018';
  const url_national_tr = 'https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=eco2mix-national-tr&q=taux_co2 > 0&rows=-1&sort=-date_heure&refine.date_heure=2018';

  fetch(url_national_tr)
    //.then(handleErrors)
    .then(res => res.json())
    .then(async res => {
      //console.log('yooo co2res', res);
      const data = res.records;

      const resRte = await fetch(url_cons_def).then(resRte => resRte.json()).then(resRte => { return resRte });
      //console.log('yooo resRte', resRte);

      const dataRte = resRte.records;
      const fullData = data.concat(dataRte);

      const resWithCo2 = fullData.filter( record => {
          return record.fields.taux_co2
      });

      const co2Dates = resWithCo2.map( record => {
          return record.fields.date
      });
      //console.log('yooo co2Dates', co2Dates);

      const co2DatesUnique = co2Dates.filter((date, pos) => {
        return co2Dates.indexOf(date) === pos && date !== false;
      });

      co2DatesUnique.sort();

      //console.log('yooo co2DatesUnique', co2DatesUnique);

      const tab = co2DatesUnique.map(date => {
        let sumCo2 = 0;
        const resWithCo2AndDate = resWithCo2.filter( record => {
            return record.fields.date === date
        });

        resWithCo2AndDate.map( record => {
            sumCo2 += record.fields.taux_co2;
            return true;
        })

        return {
          date: date,
          CO2: resWithCo2AndDate.length ? sumCo2 / resWithCo2AndDate.length : 0
        }
      })

      //console.log('yooo resWithCo2', resWithCo2);
      //console.log('yooo co2 tab', JSON.stringify(tab));

      let isCo2FormatOk = true;
      if(tab.length < 2) isCo2FormatOk = false;

      tab.map(data => {
        if(!moment( data.date, 'YYYY-MM-DD').isValid()) isCo2FormatOk = false;
        if(isNaN(data.CO2)) isCo2FormatOk = false;

      })

      if(isCo2FormatOk) {
        let co2DataUpdated = JSON.stringify(tab, null, 4);
        fs.writeFileSync(path.join(__dirname, '../db/json') + '/co2.json', co2DataUpdated);
        console.log("co2.json file has been correctly updated");
        console.log("updateCo2 finished")
        resRequest.status(201).send({
          status:"ok"
        });
      } else {

        console.log("Error on tab : ")
        console.log(tab)
        throw "Error : A problem has occured with the data retrieved for CO2"
      }
    })
    .catch(error => {
      console.error(error);
      res.status(400).send({
        message:error.message
      });
      res.end();
    });
}


exports.updateDeliveriesWithCity = async (req, res) => {

  try {
    console.log("updateDeliveriesWithCity start");

    readJson('../db/json/transportCertificates.json', async (err, deliveries) => {

      //console.log('deliveries', deliveries)
      readJson('../db/json/ap-villes-sh-co2.json', async (err2, sites) => {

        console.log('sites', typeof sites, sites.length)

        let numberOfDeliveriesWithoutCity = 0;
        let deliveriesWithCity = deliveries.map((delivery, index) => {
          //console.log(delivery.curstomerSiteId)
          //console.log("----------------")
          const deliveryCurstomerSite = sites.find( site => {
            //console.log(site.sh)
            return parseInt(site.sh, 10) === parseInt(delivery.curstomerSiteId, 10)
          })
          //console.log("--------------------------------------------------------------------------------")
          deliveryCurstomerSite === undefined ? numberOfDeliveriesWithoutCity++ : false;

          delivery.curstomerSiteCity =  deliveryCurstomerSite !== undefined ? deliveryCurstomerSite.city : 'unknown';
          return delivery;
        })

        fs.writeFileSync(path.join(__dirname, '../db/json') + '/deliveriesWithCity.json', JSON.stringify(deliveriesWithCity));
        console.log(numberOfDeliveriesWithoutCity+"/"+deliveries.length+" deliveries without city")

        console.log("updateDeliveriesWithCity finished")
        res.status(201).send({
          deliveriesWithCity,
          deliveries,
          sites
        });
      });
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};
