const Web3 = require("web3");
var web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"));
const Tx = require("ethereumjs-tx");

/*** Instanciate Contract */
const CertificateLogic_artefact = require("../../buildDev/contracts/TEO_dev.json"); //using buildDev Smart Contract
const abiCertificateLogic = CertificateLogic_artefact.abi;
const addressCertificateLogic = CertificateLogic_artefact.networks[401697].address;
const myContractCertificateLogic = new web3.eth.Contract(abiCertificateLogic, addressCertificateLogic);

/*** Constant Functions (Views) ***/

// Get Certificate Ids (sorted by owner, day, month & year)
exports.getCertificateIds = async (req, res) => {
  var sender = req.query.sender;
  var year = parseInt(req.query.year, 10); //uint
  var month = parseInt(req.query.month, 10); //uint
  var day = parseInt(req.query.day, 10); //uint
  var owner = req.query.owner; //address

  var input_error; //bool

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!owner) {
    res.status(400).send({
      error: {
        message: "Owner parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!Number.isInteger(year)) {
    res.status(400).send({
      error: {
        message: "Year parameter should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }
  if (!Number.isInteger(month)) {
    res.status(400).send({
      error: {
        message: "Month parameter should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }
  if (!Number.isInteger(day)) {
    res.status(400).send({
      error: {
        message: "Day parameter should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!input_error) {
    try {
      const result = await myContractCertificateLogic.methods.getCertificateIds(year, month, day, owner).call({ from: sender });

      let response = JSON.stringify(result);
      response = JSON.parse(response);
      res.json(response);
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};

// Get a certificate's children
exports.getCertificateChildren = async (req, res) => {
  var sender = req.query.sender;
  var id = req.query.id; //bytes32

  var input_error; //bool

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!id) {
    res.status(400).send({
      error: {
        message: "Id parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!input_error) {
    try {
      const result = await myContractCertificateLogic.methods.getCertificateChildren(id).call({ from: sender });

      const results = await Promise.resolve(result.toString());

      res.json(results);
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};

// Get certificate Old (returning the struct's elements individually)
exports.getCertificateOld = async (req, res) => {
  var sender = req.query.sender;
  var id = req.query.id; //bytes32

  var input_error; //bool

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!id) {
    res.status(400).send({
      error: {
        message: "Id parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!input_error) {
    try {
      const result = await myContractCertificateLogic.methods.getCertificateOld(id).call({ from: sender });

      const result_0 = await Promise.resolve(result[0]);
      const result_1 = await Promise.resolve(result[1]);
      const result_2 = await Promise.resolve(result[2]);
      const result_3 = await Promise.resolve(result[3]);
      const result_4 = await Promise.resolve(result[4]);
      const result_5 = await Promise.resolve(result[5]);
      const result_6 = await Promise.resolve(result[6]);
      const result_7 = await Promise.resolve(result[7]);
      const result_8 = await Promise.resolve(result[8]);
      const result_9 = await Promise.resolve(result[9]);
      const result_10 = await Promise.resolve(result[10]);
      const result_11 = await Promise.resolve(result[11]);

      var array = [result_0, result_1, result_2, result_3, result_4, result_5, result_6, result_7, result_8, result_9, result_10, result_11];
      res.json(array);
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};

/*** Non-Constant Functions  ***/

// Set an administrator
exports.setAdministrator = async (req, res) => {
  var sender = req.query.sender;
  var administrator = req.body.administrator; //address
  var isAdmin = String(req.body.isAdmin); //bool

  var input_error; //bool

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!administrator) {
    res.status(400).send({
      error: {
        message: "Administrator (in Body) should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (isAdmin.valueOf() != "true") {
    if (isAdmin.valueOf() != "false") {
      res.status(400).send({
        error: {
          message: "IsAdmin(in Body) should be Boolean (true/false)",
          type: "input_error",
          code: 400
        }
      });
      input_error = true;
      res.end();
    }
  }

  if (!input_error) {
    try {
      var openDoorDataPart = myContractCertificateLogic.methods.setAdministrator(administrator, isAdmin).encodeABI();

      const nonce = await web3.eth.getTransactionCount(sender);

      var params = require("./dbService")
        .getParams("all")
        .then(function(result) {
          return result;
        });

      var par = await Promise.resolve(params);
      var value_ = par[0].toObject().value;
      var gasPrice_ = par[0].toObject().gasPrice;
      var gasLimit_ = par[0].toObject().gasLimit;

      var rawTx = {
        nonce: web3.utils.toHex(nonce),
        value: value_,
        gasPrice: web3.utils.toHex(gasPrice_),
        gasLimit: web3.utils.toHex(gasLimit_),
        data: openDoorDataPart,
        to: addressCertificateLogic,
        from: sender
      };
      var tx = new Tx(rawTx);

      var privateK = require("../services/tobalaba/privateKey")
        .privateKeyByAddress2(sender)
        .then(function(result) {
          return result;
        });
      var privateKey = await Promise.resolve(privateK);

      await tx.sign(privateKey);
      const serializedTx = await tx.serialize();

      const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

      if (!ret.logs[0]) {
        res.status(405).send({
          error: {
            message: "CANNOT GET TRANSACTION LOGS. This problem is probably due to the fact that sender is not owner (check modifier onlyOwner)",
            type: "transaction_logs_error",
            code: 405
          }
        });
        res.end();
      }

      const results = await Promise.resolve(ret);

      res.json(results);
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};

// Transfer a certificate
exports.transferCertificate = async (req, res) => {
  var today = new Date();
  var year = JSON.stringify(today.getDate());
  var day = JSON.stringify(today.getMonth() + 1); //january is 0 !
  var month = JSON.stringify(today.getFullYear());
  var consumerName = req.body.consumerName;
  var input_error; //bool

  var secret;
  var secret_w;
  let secret_word;

  if (!consumerName) {
    res.status(400).send({
      error: {
        message: "ConsumerName (in body) should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  secret = require("./dbService")
    .getSecretWords(consumerName, year, month, day)
    .then(function(result) {
      return result;
    });

  secret_w = await Promise.resolve(secret);

  if (!secret_w[0]) {
    secret = require("./dbService")
      .setSecretWords(consumerName, year, month, day)
      .then(function(result) {
        return result;
      });
    secret_w = await Promise.resolve(secret);
    secret_word = secret_w.secret_word;
  } else {
    secret_word = secret_w[0].secret_word;
  }

  var assignedConsumer = Web3.utils.sha3(consumerName + secret_word); //bytes
  var sender = req.query.sender;
  var id = req.query.id; //bytes32
  var transferedQuantity = parseInt(req.body.transferedQuantity, 10); //uint

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!id) {
    res.status(400).send({
      error: {
        message: "Id parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!Number.isInteger(transferedQuantity)) {
    res.status(400).send({
      error: {
        message: "TransferedQuantity (in Body) should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!assignedConsumer) {
    res.status(400).send({
      error: {
        message: "AssignedConsumer should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }
  if (!input_error) {
    try {
      var openDoorDataPart = myContractCertificateLogic.methods.transferCertificate(id, assignedConsumer, transferedQuantity).encodeABI();

      const nonce = await web3.eth.getTransactionCount(sender);

      var params = require("./dbService")
        .getParams("all")
        .then(function(result) {
          return result;
        });

      var par = await Promise.resolve(params);
      var value_ = par[0].toObject().value;
      var gasPrice_ = par[0].toObject().gasPrice;
      var gasLimit_ = par[0].toObject().gasLimit;

      var rawTx = {
        nonce: web3.utils.toHex(nonce),
        value: value_,
        gasPrice: web3.utils.toHex(gasPrice_),
        gasLimit: web3.utils.toHex(gasLimit_),
        data: openDoorDataPart,
        to: addressCertificateLogic,
        from: sender
      };

      var tx = new Tx(rawTx);

      var privateK = require("../services/tobalaba/privateKey")
        .privateKeyByAddress2(sender)
        .then(function(result) {
          return result;
        });
      var privateKey = await Promise.resolve(privateK);

      await tx.sign(privateKey);
      const serializedTx = await tx.serialize();

      const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

      if (!ret.logs[0]) {
        res.status(405).send({
          error: {
            message: "CANNOT GET TRANSACTION LOGS. This problem is probably due to the fact that sender is not issuer (check modifier onlyIssuer), or that certificate has been canceled (check modifier notCancelled), or that the quantity remaining in the certificate is not sufficient, or that he certificate has already been transfered (check modifier verifiedAssetAndCustomer)",
            type: "transaction_logs_error",
            code: 405
          }
        });
        res.end();
      }

      const results = await Promise.resolve(ret);

      res.json(results);
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};

// Issue  a certificate
let issuingDateEnd_ = Date.parse("2019-03-03T00:00:00Z"); //bytes --test value

exports.issueCertificate = async (req, res) => {
  var today = new Date();
  var issuingDateStart_ = Date.parse(today); //bytes
  var year = JSON.stringify(today.getDate());
  var day = JSON.stringify(today.getMonth() + 1); //january is 0 !
  var month = JSON.stringify(today.getFullYear());
  var issuingName = req.body.issuingName;
  var input_error; //bool

  var secret;
  var secret_w;
  let secret_word;

  if (!issuingName) {
    res.status(400).send({
      error: {
        message: "IssuingName (in body) should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  secret = require("./dbService")
    .getSecretWords(issuingName, year, month, day)
    .then(function(result) {
      return result;
    });

  secret_w = await Promise.resolve(secret);

  if (!secret_w[0]) {
    secret = require("./dbService")
      .setSecretWords(issuingName, year, month, day)
      .then(function(result) {
        return result;
      });
    secret_w = await Promise.resolve(secret);
    secret_word = secret_w.secret_word;
  } else {
    secret_word = secret_w[0].secret_word;
  }

  console.log("===");
  console.log(secret_word);

  var issuingAsset = Web3.utils.sha3(issuingName + secret_word); //bytes
  var sender = req.query.sender;
  //var issuingAsset = issuingAsset_; //bytes
  var issuingDateStart = parseInt(issuingDateStart_, 10); //uint
  var issuingDateEnd = parseInt(issuingDateEnd_, 10); //uint
  var issuingQuantity = parseInt(req.body.issuingQuantity, 10); //uint
  var issuingUnit = req.body.issuingUnit; //string

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!issuingAsset) {
    res.status(400).send({
      error: {
        message: "IssuingAsset should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!Number.isInteger(issuingDateStart)) {
    res.status(400).send({
      error: {
        message: "IssuingDateStart should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!Number.isInteger(issuingQuantity)) {
    res.status(400).send({
      error: {
        message: "IssuingQuantity (in Body) should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!Number.isInteger(issuingDateEnd)) {
    res.status(400).send({
      error: {
        message: "IssuingDateEnd should be Int",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!issuingUnit) {
    res.status(400).send({
      error: {
        error: "IssuingUnit (in Body) should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!input_error) {
    try {
      var openDoorDataPart = myContractCertificateLogic.methods.issueCertificate(issuingAsset, issuingDateStart, issuingDateEnd, issuingQuantity, issuingUnit).encodeABI();

      const nonce = await web3.eth.getTransactionCount(sender);

      var params = require("./dbService")
        .getParams("all")
        .then(function(result) {
          return result;
        });

      var par = await Promise.resolve(params);

      var value_ = par[0].toObject().value;
      var gasPrice_ = par[0].toObject().gasPrice;
      var gasLimit_ = par[0].toObject().gasLimit;

      var rawTx = {
        nonce: web3.utils.toHex(nonce),
        value: value_,
        gasPrice: web3.utils.toHex(gasPrice_),
        gasLimit: web3.utils.toHex(gasLimit_),
        data: openDoorDataPart,
        to: addressCertificateLogic,
        from: sender
      };

      var tx = new Tx(rawTx);

      var privateK = require("../services/tobalaba/privateKey")
        .privateKeyByAddress2(sender)
        .then(function(result) {
          return result;
        });
      var privateKey = await Promise.resolve(privateK);

      await tx.sign(privateKey);
      const serializedTx = await tx.serialize();

      const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

      if (!ret.logs[0]) {
        res.status(405).send({
          error: {
            message: "CANNOT GET TRANSACTION LOGS. This problem is probably due to the fact that sender is not administrator (check modifier onlyAdministrator), or that issuingDateStart>issuingDateEnd",
            type: "transaction_logs_error",
            code: 405
          }
        });
        res.end();
      }

      const results = await Promise.resolve(ret);

      res.json(results);
    } catch (error) {
      console.error(error);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};

//Cancel a certificate

const cancel1 = Web3.utils.sha3("cancel1"); //bytes --test value
exports.cancelCertificate = async (req, res) => {
  var sender = req.query.sender;
  var id = req.query.id; //bytes32

  var input_error; //bool

  if (!sender) {
    res.status(400).send({
      error: {
        message: "Sender parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!id) {
    res.status(400).send({
      error: {
        message: "Id parameter should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!cancel1) {
    res.status(400).send({
      error: {
        error: "Cancel1 should NOT be empty",
        type: "input_error",
        code: 400
      }
    });
    input_error = true;
    res.end();
  }

  if (!input_error) {
    try {
      var openDoorDataPart = myContractCertificateLogic.methods.cancelCertificate(id, cancel1).encodeABI();

      const nonce = await web3.eth.getTransactionCount(sender);

      var params = require("./dbService")
        .getParams("all")
        .then(function(result) {
          return result;
        });

      var par = await Promise.resolve(params);
      var value_ = par[0].toObject().value;
      var gasPrice_ = par[0].toObject().gasPrice;
      var gasLimit_ = par[0].toObject().gasLimit;

      var rawTx = {
        nonce: web3.utils.toHex(nonce),
        value: value_,
        gasPrice: web3.utils.toHex(gasPrice_),
        gasLimit: web3.utils.toHex(gasLimit_),
        data: openDoorDataPart,
        to: addressCertificateLogic,
        from: sender
      };

      var tx = new Tx(rawTx);

      var privateK = require("../services/tobalaba/privateKey")
        .privateKeyByAddress2(sender)
        .then(function(result) {
          return result;
        });
      var privateKey = await Promise.resolve(privateK);

      await tx.sign(privateKey);
      const serializedTx = await tx.serialize();

      const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

      if (!ret.logs[0]) {
        res.status(405).send({
          error: {
            message: "CANNOT GET TRANSACTION LOGS. This problem is probably due to the fact that certificate has already been cancelled (Check modifier notCancelled) or that the sender is not issuer (check modifier onlyIssuer)",
            type: "transaction_logs_error",
            code: 405
          }
        });
        res.end();
      }

      const results = await Promise.resolve(ret);

      res.json(results);
    } catch (error) {
      console.error(error);
      console.log(id);
      res.status(500).send({
        error: {
          message: error.message,
          type: "blockchain_error",
          code: 500
        }
      });
      res.end();
    }
  }
};
