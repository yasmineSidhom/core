const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

var fetch =require('node-fetch');
var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');

let UserLogic_artefact = require('../../'+process.env.BUILD_FOLDER+'/contracts/UserLogic.json');
const abiUserLogic = UserLogic_artefact.abi;
const addressUserLogic = UserLogic_artefact.networks[401697].address;
const myContractUserLogic = new web3.eth.Contract(abiUserLogic, addressUserLogic);


const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

//Get an array of the blockchain addresses of the existing users
exports.getAllUserAddress = async (req, res) => {
  var sender = req.query._sender;
  const users=[];
  let user;
  try {
    const listLength= await myContractUserLogic.methods.getUserListLength().call({ from:sender });

    for(i = 0; i < listLength; i++){
      user = await myContractUserLogic.methods.getUserAddress(i).call({ from:sender });
      users.push(user);
    }
    const results = await Promise.resolve(users);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//get the number of users recorded in the blockchain
exports.getUserListLength = async (req, res) => {
  const sender = req.query._sender;
  try {
    const result = await myContractUserLogic.methods.getUserListLength().call({ from:sender });
    var response = JSON.stringify(result);
    response = JSON.parse(response);
    const results = await Promise.resolve(response);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }

}

exports.getAllUserAddress = async (req, res) => {
  var sender = req.query._sender;
  const users=[];
  let user;

  try {
    const listLength= await myContractUserLogic.methods.getUserListLength().call({ from:sender });

    for(i = 0; i < listLength; i++){
      user = await myContractUserLogic.methods.getUserAddress(i).call({ from:sender });
      users.push(user);
    }
    const results = await Promise.resolve(users);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}


exports.getAllUsers = async (req, res) => {
  var sender = req.query._sender;
  try {

    const usersAddresses = await fetch(process.env.API_URL + "/users-addresses?_sender="+sender, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })

    let users = [];

    for (let address of usersAddresses) {
      const userTemp = await myContractUserLogic.methods.getFullUser(address).call({ from:sender });


      userTemp[0]=web3.utils.toAscii(userTemp[0]).replace(/\0/g, '');
      userTemp[1]=web3.utils.toAscii(userTemp[1]).replace(/\0/g, '');
      userTemp[2]=web3.utils.toAscii(userTemp[2]).replace(/\0/g, '');
      userTemp[3]=web3.utils.toAscii(userTemp[3]).replace(/\0/g, '');
      userTemp[4]=web3.utils.toAscii(userTemp[4]).replace(/\0/g, '');
      userTemp[5]=web3.utils.toAscii(userTemp[5]).replace(/\0/g, '');
      userTemp[6]=web3.utils.toAscii(userTemp[6]).replace(/\0/g, '');
      userTemp[7]=web3.utils.toAscii(userTemp[7]).replace(/\0/g, '');
      userTemp[8]=web3.utils.toAscii(userTemp[8]).replace(/\0/g, '');
      userTemp["address"]=address;
      userTemp["firstName"]=web3.utils.toAscii(userTemp["firstName"]).replace(/\0/g, '');
      userTemp["surname"]=web3.utils.toAscii(userTemp["surname"]).replace(/\0/g, '');
      userTemp["organization"]=web3.utils.toAscii(userTemp["organization"]).replace(/\0/g, '');
      userTemp["street"]=web3.utils.toAscii(userTemp["street"]).replace(/\0/g, '');
      userTemp["number"]=web3.utils.toAscii(userTemp["number"]).replace(/\0/g, '');
      userTemp["zip"]=web3.utils.toAscii(userTemp["zip"]).replace(/\0/g, '');
      userTemp["city"]=web3.utils.toAscii(userTemp["city"]).replace(/\0/g, '');
      userTemp["country"]=web3.utils.toAscii(userTemp["country"]).replace(/\0/g, '');
      userTemp["state"]=web3.utils.toAscii(userTemp["state"]).replace(/\0/g, '');

      users.push(userTemp)
    }

    const offchainUsersData = await fetch(process.env.API_URL + "/users-offchain", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })

    const offchainUsers = offchainUsersData.data;

    users = users.map((user, index) => {

      const offchainUserToMerge = offchainUsers.find((offchainUser, idx) => {
        if(!offchainUser.app_metadata || !offchainUser.app_metadata.address) return false;
        return offchainUser.app_metadata.address === user.address && offchainUser.app_metadata.realAccount === true
      });

      if( !offchainUserToMerge ) return { ...user, offchainData: {
        email: 'unknown',
        nickname: 'unknown',
        user_metadata: {
          consumingSites: [],
          name: 'unknown',
          logo: 'laboulangere-square.png',
          username: 'unknown'
        },
        name: 'unknown',
        app_metadata: { roles: [],
            authorized: true,
            address: user.address,
            realAccount: false
          }
        }
      }

      return { ...user, offchainData: {
        email: offchainUserToMerge.email,
        nickname: offchainUserToMerge.nickname,
        user_metadata: offchainUserToMerge.user_metadata,
        name: offchainUserToMerge.name,
        app_metadata: offchainUserToMerge.app_metadata
      }}
    })

    const results = await Promise.resolve(users);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

//get Full User with contract logistic
exports.getFullUser = async (req, res) => {

  var userAddress = req.params.userAddress;
  var sender = req.query._sender;

  try {
    const result= await myContractUserLogic.methods.getFullUser(userAddress).call({ from:sender });
    result[0]=web3.utils.toAscii(result[0]).replace(/\0/g, '');
    result[1]=web3.utils.toAscii(result[1]).replace(/\0/g, '');
    result[2]=web3.utils.toAscii(result[2]).replace(/\0/g, '');
    result[3]=web3.utils.toAscii(result[3]).replace(/\0/g, '');
    result[4]=web3.utils.toAscii(result[4]).replace(/\0/g, '');
    result[5]=web3.utils.toAscii(result[5]).replace(/\0/g, '');
    result[6]=web3.utils.toAscii(result[6]).replace(/\0/g, '');
    result[7]=web3.utils.toAscii(result[7]).replace(/\0/g, '');
    result[8]=web3.utils.toAscii(result[8]).replace(/\0/g, '');
    result["firstName"]=web3.utils.toAscii(result["firstName"]).replace(/\0/g, '');
    result["surname"]=web3.utils.toAscii(result["surname"]).replace(/\0/g, '');
    result["organization"]=web3.utils.toAscii(result["organization"]).replace(/\0/g, '');
    result["street"]=web3.utils.toAscii(result["street"]).replace(/\0/g, '');
    result["number"]=web3.utils.toAscii(result["number"]).replace(/\0/g, '');
    result["zip"]=web3.utils.toAscii(result["zip"]).replace(/\0/g, '');
    result["city"]=web3.utils.toAscii(result["city"]).replace(/\0/g, '');
    result["country"]=web3.utils.toAscii(result["country"]).replace(/\0/g, '');
    result["state"]=web3.utils.toAscii(result["state"]).replace(/\0/g, '');
    var user = JSON.stringify(result);

    user = JSON.parse(user);

    const offchainUserData = await fetch(process.env.API_URL + "/users-offchain?address="+userAddress, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    console.log(offchainUserData)
    const offchainUser = offchainUserData.data[0];

    if( !offchainUser ) {
      user = { ...user, offchainData: {
        email: 'unknown',
        nickname: 'unknown',
        user_metadata: {
          consumingSites: [],
          name: 'unknown',
          logo: 'laboulangere-square.png',
          username: 'unknown'
        },
        name: 'unknown',
        app_metadata: { roles: [],
            authorized: true,
            address: user.address,
            realAccount: false
          }
        }
      }
    } else {
      user = { ...user, offchainData:{
        email: offchainUser.email,
        nickname: offchainUser.nickname,
        user_metadata: offchainUser.user_metadata,
        name: offchainUser.name,
        app_metadata: offchainUser.app_metadata
      }}
    }



    const results = await Promise.resolve(user);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

//create a new user
exports.setUser = async (req, res) => {

  var user = req.body._user;             //Address
  var sender = req.query._sender;        //Address

  var firstName = req.body._firstName;
  var surname = req.body._surname;
  var organization = req.body._organization;

  var street = req.body._street;
  var number = req.body._number;
  var zip = req.body._zip;
  var city = req.body._city;
  var country = req.body._country;
  var state = req.body._state;

  //Offchain data
  var app_metadata = {
    address: "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
    authorized: true,
    realAccount: true,
    roles: ["admin", "hasDeliveries"]
  }
  var email = "admin@weechain.app"
  var name = "admin@weechain.app"
  var nickname= "admin"

  var user_metadata = {
    consumingSites: [0, 1, 2],
    logo: "engie_logo_bleu-square.png",
    name: "Engie",
    username: "admin",
    firstname: "thierry",
    lastname: "mathieu",
    organization: "Engie",
    number,
    street,
    zip,
    city,
    region,
    country
  }



  try {

    const doesUserExist = await fetch(process.env.API_URL + "/users/"+user+"/exist?_sender="+sender, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data
    })
    .catch(error => {
      console.log("error in doesUserExist in setUser : ");
      console.log(error);
    });

    if(doesUserExist === true || doesUserExist === "true") {
      console.log("Error in setUser : A user with the same address already exists")
      res.json({error : "A user with the same address already exists"});
    } else {
      var openDoorDataPart = myContractUserLogic.methods.setUser(
        user,
        web3.utils.fromAscii(firstName),
        web3.utils.fromAscii(surname),
        web3.utils.fromAscii(organization),
        web3.utils.fromAscii(street),
        web3.utils.fromAscii(number),
        web3.utils.fromAscii(zip),
        web3.utils.fromAscii(city),
        web3.utils.fromAscii(country),
        web3.utils.fromAscii(state)
      ).encodeABI();

      const nonce = await web3.eth.getTransactionCount(sender);

      var rawTx = {
        nonce: web3.utils.toHex(nonce),
        value: 0x00,
        gasPrice: web3.utils.toHex(4000000000),
        gasLimit: web3.utils.toHex(1000000),
        data: openDoorDataPart,
        to: addressUserLogic,
        from: sender
      };

      var tx = new Tx(rawTx);
      var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
      await tx.sign(privateKey);
      const serializedTx = await tx.serialize();
      const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));


      const results = await Promise.resolve(ret);
      res.json(results);

    }
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}


exports.setUserV2 = async (req, res) => {

  var userAddress = req.body._user;             //Address
  var sender = req.query._sender;        //Address

  console.log(req.body)

  var firstname = req.body._firstName;
  var surname = req.body._surname;
  var organization = req.body._organization;

  var street = req.body._street;
  var number = req.body._number;
  var zip = req.body._zip;
  var city = req.body._city;
  var country = req.body._country;
  var state = req.body._state;

  //Offchain data
  var app_metadata = {
    address: userAddress,
    authorized: true,
    realAccount: true,
    roles: []
  }

  var user_metadata = {
    consumingSites: [0, 1, 2],
    logo: "engie_logo_bleu-square.png",
    name: "Engie",
    username: "admin",
    firstname,
    surname,
    organization,
    number,
    street,
    zip,
    city,
    region,
    country
  }

  res.json(req.body);

}


exports.checkRoles = async (req, res) => {
  try {
    var user = req.params.userAddress;     //Address
    var sender = req.query._sender;
    console.log('0')
    const resultAdmin = await myContractUserLogic.methods.isRole( 0, user ).call({ from:sender });
    var responseAdmin = JSON.stringify(resultAdmin);
    responseAdmin = JSON.parse(responseAdmin);
    const resultsAdmin = await Promise.resolve(responseAdmin);
    console.log('1')

    const resultUserAdmin = await myContractUserLogic.methods.isRole( 1, user ).call({ from:sender });
    var responseUserAdmin = JSON.stringify(resultUserAdmin);
    responseUserAdmin = JSON.parse(responseUserAdmin);
    const resultsUserAdmin = await Promise.resolve(responseUserAdmin);
    console.log('2')

    const resultAssetAdmin = await myContractUserLogic.methods.isRole( 2, user ).call({ from:sender });
    var responseAssetAdmin = JSON.stringify(resultAssetAdmin);
    responseAssetAdmin = JSON.parse(responseAssetAdmin);
    const resultsAssetAdmin = await Promise.resolve(responseAssetAdmin);
    console.log('3')

    const resultAgreementAdmin = await myContractUserLogic.methods.isRole( 3, user ).call({ from:sender });
    var responseAgreementAdmin = JSON.stringify(resultAgreementAdmin);
    responseAgreementAdmin = JSON.parse(responseAgreementAdmin);
    const resultsAgreementAdmin = await Promise.resolve(responseAgreementAdmin);
    console.log('4')

    const resultAssetManager = await myContractUserLogic.methods.isRole( 4, user ).call({ from:sender });
    var responseAssetManager = JSON.stringify(resultAssetManager);
    responseAssetManager = JSON.parse(responseAssetManager);
    const resultsAssetManager = await Promise.resolve(responseAssetManager);
    console.log('5')

    const resultTrader = await myContractUserLogic.methods.isRole( 5, user ).call({ from:sender });
    var responseTrader = JSON.stringify(resultTrader);
    responseTrader = JSON.parse(responseTrader);
    const resultsTrader = await Promise.resolve(responseTrader);
    console.log('6')

    const resultMatcher = await myContractUserLogic.methods.isRole( 6, user ).call({ from:sender });
    var responseMatcher = JSON.stringify(resultMatcher);
    responseMatcher = JSON.parse(responseMatcher);
    const resultsMatcher = await Promise.resolve(responseMatcher);
    console.log('7')

    res.json({
      hasAdminRole: resultsAdmin,
      hasUserAdminRole: resultsUserAdmin,
      hasAssetAdminRole: resultsAssetAdmin,
      hasAgreementAdminRole: resultsAgreementAdmin,
      hasAssetManagerRole: resultsAssetManager,
      hasTraderRole: resultsTrader,
      hasMatcherRole: resultsMatcher,
    });
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }

}

exports.setRoles = async (req, res) => {
  try {
    var user = req.params.userAddress;;     //Address
    var rights = req.body._rights;  //Unit
    var sender = req.query._sender;

    var openDoorDataPart = myContractUserLogic.methods.setRoles(
      user,
      parseInt(rights,10)
    ).encodeABI();
    console.log(openDoorDataPart);

    const nonce = await web3.eth.getTransactionCount(sender);
    console.log(nonce);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }

}

exports.setOrganizationAddress = async (req, res) => {
  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;        //Address

  var street = req.body._street;         //bytes32
  var number = req.body._number;        //bytes32
  var zip = req.body._zip;              //bytes32
  var city = req.body._city;            //bytes32
  var country = req.body._country;      //bytes32
  var state = req.body._state;          //bytes32

  try {
    var openDoorDataPart = myContractUserLogic.methods.setOrganizationAddress(
      user,
      web3.utils.fromAscii(street),
      web3.utils.fromAscii(number),
      web3.utils.fromAscii(zip),
      web3.utils.fromAscii(city),
      web3.utils.fromAscii(country),
      web3.utils.fromAscii(state)
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }

}

exports.setOrganization = async (req, res) => {
  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  var organization = req.body._organization;  //bytes32

  try {
    var openDoorDataPart = myContractUserLogic.methods.setOrganization(
      user,
      web3.utils.fromAscii(organization)
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}


/////// Start new here

exports.setUserName = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  var firstname = req.body._firstname;   //bytes32
  var surname = req.body._surname;         //bytes32

  console.log("setUserName firstname = "+firstname)
  console.log("setUserName surname = "+surname)
  console.log("setUserName user = "+user)

  try {
    var openDoorDataPart = myContractUserLogic.methods.setUserName(
      user,
      web3.utils.fromAscii(firstname),
      web3.utils.fromAscii(surname)
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//add AdminRole role with userlogic contract
exports.addAdminRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  /*

  no role:        0x0...000000     0
  TopAdmin:       0x0...-----1     1
  UserAdmin:      0x0...----1-     2
  AssetAdmin:     0x0...---1--     4
  AgreementAdmin: 0x0...--1---     8
  AssetManager:   0x0...-1----    16
  Trader:         0x0...1-----    32

  See /contracts/UserLogic.sol to a better understanding
  Have a look here about the shift manipulation if necessary
  https://medium.com/@imolfar/bitwise-operations-and-bit-manipulation-in-solidity-ethereum-1751f3d2e216

  newUserRoles = previousUserRoles + uint(2) ** uint(role)


  */

  var roleManagementRole = req.body._roleManagementRole;

  try {
    var openDoorDataPart = myContractUserLogic.methods.addAdminRole(
      user,
      parseInt(roleManagementRole,10)
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//add AssetManagerRole role with userlogic contract
exports.addAssetManagerRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    var openDoorDataPart = myContractUserLogic.methods.addAssetManagerRole(
      user
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//add matcherRole role with userlogic contract
exports.addMatcherRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    var openDoorDataPart = myContractUserLogic.methods.addMatcherRole(
      user
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

//add addTraderRole role with userlogic contract
exports.addTraderRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    var openDoorDataPart = myContractUserLogic.methods.addTraderRole(
      user
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//Deactivate User role with userlogic contract
exports.deactivateUser = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    var openDoorDataPart = myContractUserLogic.methods.deactivateUser(
      user
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//remove Admin Role  role with userlogic contract
exports.removeAdminRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  var roleManagementRole = req.body._roleManagementRole;

  try {
    var openDoorDataPart = myContractUserLogic.methods.removeAdminRole(
      user,
      parseInt(roleManagementRole,10)
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}

//remove Asset Manager Role with userlogic contract
exports.removeAssetManagerRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    var openDoorDataPart = myContractUserLogic.methods.removeAssetManagerRole(
      user
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//remove Trader Role with userlogic contract
exports.removeTraderRole = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    var openDoorDataPart = myContractUserLogic.methods.removeTraderRole(
      user
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressUserLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//get Roles Rights with userlogic contract
exports.getRolesRights = async (req, res) => {

  var user = req.params.userAddress;     //Address
  var sender = req.query._sender;       //Address

  try {
    const result = await myContractUserLogic.methods.getRolesRights(user).call({ from:sender });

    var response = JSON.stringify(result);
    response = JSON.parse(response);

    const results = await Promise.resolve(response);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


}

//does User Exist with userlogic contract
exports.doesUserExist = async (req, res) => {

  try {

    var user = req.params.userAddress;     //Address
    var sender = req.query._sender;       //Address

    const result = await myContractUserLogic.methods.doesUserExist(user).call({ from:sender });

    var response = JSON.stringify(result);
    response = JSON.parse(response);

    const results = await Promise.resolve(response);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}
