const fs = require('fs');
const path = require('path');
const moment = require('moment');
const fetch = require('node-fetch');
const cron = require('node-cron');
const exec = require('child_process').exec;

const co2 = JSON.parse(fs.readFileSync(require.resolve('../db/json/co2.json'), 'utf8'))

const myUsers = [
  {
      "_user":"0x0051CcCBCFF51788dDa9614b12dcea0D57208C03",
  		"_firstName": "Air Products",
      "_surname": "airproducts",
      "_organization": "Air Products",
      "_street": "Avenue Victor Hugo",
      "_number": "45",
      "_zip": "93300",
      "_city": "Aubervilliers",
      "_country": "France",
      "_state": "IDF",
      "_sender": "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747"
  },
  {
      "_user":"0x00d0F0e2AF12fec0C16c6D595a9554b99B6dE045",
      "_firstName": "La Boulangère",
      "_surname": "laboulangere",
      "_organization": "La Boulangère",
      "_street": "rue Olivier de Serres",
      "_number": "10",
      "_zip": "85500",
      "_city": "Les herbiers",
      "_country": "France",
      "_state": "Pays de la Loire",
      "_sender": "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747"
  }
]

const myProducingAssets = [
  {
    "_smartMeter":"0x00CB17ea4DfA8578582C00A79Dd92d6111c28c54",
    "_owner":"0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
    "_operationalSince":"1318781876",
    "_active":true,
  	"_assetType":2,
  	"_capacityWh":150000000,
  	"_registryCompliance":0,
  	"_otherGreenAttributes":"NA",
  	"_typeOfPublicSupport":"NA",
    "_country":"France",
  	"_region":"Nouvelle-Aquitaine",
  	"_zip":"19160",
  	"_city":"Liginiac",
  	"_street":"Barrage de Marèges",
  	"_houseNumber":"",
  	"_gpsLatitude":"45,391546",
  	"_gpsLongitude":"2,364349"
  },
  {
    "_smartMeter":"0x00c3B44DBeA6f103dd30D7e90D59787175FeA185",
    "_owner":"0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
    "_operationalSince":"1318781876",
    "_active":true,
    "_assetType":0,
  	"_capacityWh":7800000,
  	"_registryCompliance":0,
  	"_otherGreenAttributes":"NA",
  	"_typeOfPublicSupport":"NA",
    "_country":"France",
  	"_region":"Occitanie",
  	"_zip":"12360",
  	"_city":"Brusque",
  	"_street":"Merdelou-Fontanelles",
  	"_houseNumber":"",
  	"_gpsLatitude":"43,761234",
  	"_gpsLongitude":"2,902555"
  },
  {
    "_smartMeter":"0x004A064a9AAf2fCD9687D82Fc5a1116e9325000A",
    "_owner":"0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
    "_operationalSince":"1318781876",
    "_active":true,
    "_assetType":2,
  	"_capacityWh":39000000,
  	"_registryCompliance":0,
  	"_otherGreenAttributes":"NA",
  	"_typeOfPublicSupport":"NA",
    "_country":"France",
  	"_region":"Occitanie",
  	"_zip":"65170",
  	"_city":"Saint-Lary-Soulan",
  	"_street":"Réservoir de l'Oule",
  	"_houseNumber":"Eget",
  	"_gpsLatitude":"42,8331477",
  	"_gpsLongitude":"0,1998515000000225"
  },

]


const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

const getDates = (startDate, endDate) => {
  var dates = [],
      currentDate = startDate,
      addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
      };
  while (currentDate < endDate) {
    dates.push(currentDate);
    currentDate = addDays.call(currentDate, 1);
  }
  return dates;
};


createAsset = async () => {

  return await fetch(process.env.API_URL + "/producing-assets?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

initGeneral = async (_assetId, _smartMeter, _owner, _operationalSince, _lastSmartMeterReadWh, _active, _lastSmartMeterReadFileHash) => {

  const data = {
    _smartMeter,
    _owner,
    _operationalSince,
    _lastSmartMeterReadWh,
    _active,
    _lastSmartMeterReadFileHash
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/producing-assets/"+_assetId+"?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

initProperties = async (_assetId, _assetType, _capacityWh, _registryCompliance, _otherGreenAttributes, _typeOfPublicSupport) => {

  const data = {
    _assetType,
    _capacityWh,
    _registryCompliance,
    _otherGreenAttributes,
    _typeOfPublicSupport
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/producing-assets/"+_assetId+"/properties?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

initLocation = async (_assetId, _country, _region, _zip, _city, _street, _houseNumber, _gpsLatitude, _gpsLongitude) => {

  const data = {
    _country,
    _region,
    _zip,
    _city,
    _street,
    _houseNumber,
    _gpsLatitude,
    _gpsLongitude
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/producing-assets/"+_assetId+"/location?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

setUser = async (
    _user,
    _firstName,
    _surname,
    _organization,
    _street,
    _number,
    _zip,
    _city,
    _country,
    _state
  ) => {

  const data = {
    _user,
    _firstName,
    _surname,
    _organization,
    _street,
    _number,
    _zip,
    _city,
    _country,
    _state
  };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/users?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

setUserRoles = async (_userAddress, _rights) => {

  const data = { _rights };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/users/"+_userAddress+"/roles?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

getProdAssetListLength = async () => {
  return await fetch(process.env.API_URL + "/producing-assets-list-length?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

getProdAsset = async (_assetId) => {
  return await fetch(process.env.API_URL + "/producing-assets/"+_assetId+"/full?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}

launchMatchingForDay = async (nbRetry, date, generateCertificates) => {

  const data = { nbRetry, date, generateCertificates };

  const json = JSON.stringify(data);

  return await fetch(process.env.API_URL + "/certificates-management/matching", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
    .then(handleErrors)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(error => error);
}


exports.initialization = async (req, res) => {
  try {
    /*
    let index = 0;

    for (let asset of myProducingAssets) {

      await createAsset();
      const listLength = await getProdAssetListLength();
      console.log("initialization : listLength = "+listLength)
      const _assetId = listLength - 1;
      await initGeneral(_assetId, asset._smartMeter, asset._owner, asset._operationalSince, asset._lastSmartMeterReadWh, asset._active, asset._lastSmartMeterReadFileHash);
      await initProperties(_assetId, asset._assetType, asset._capacityWh, asset._registryCompliance, asset._otherGreenAttributes, asset._typeOfPublicSupport);
      await initLocation(_assetId, asset._country, asset._region, asset._zip, asset._city, asset._street, asset._houseNumber, asset._gpsLatitude, asset._gpsLongitude);

      const bcAsset = await getProdAsset(_assetId);
      console.log("initialization : new blockchain production asset created : ");
      console.log(bcAsset);
      index++;
    }

    for (let user of myUsers) {
      await setUser(
          user._user,
          user._firstName,
          user._surname,
          user._organization,
          user._street,
          user._number,
          user._zip,
          user._city,
          user._country,
          user._state
        )
      await setUserRoles( user._user, 4)
    }*/

    const minDateRange = moment().set({hour:0,minute:0,second:0,millisecond:0}).subtract(30, 'days').format('X')
    const maxDateRange = moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X')

    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    for (let date of allDates) {
      console.log("🤖🤖🤖 launchMatchingForDay for date "+moment(date).format('YYYY-MM-DD'));
      await launchMatchingForDay(0, moment(date).format('YYYY-MM-DD'), true)
    }
    res.json({status: "ok"});



  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
}
