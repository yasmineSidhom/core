const fs = require("fs");
const path = require("path");
const moment = require("moment");
const fetch = require("node-fetch");
const cron = require("node-cron");
const exec = require("child_process").exec;
var randomstring = require("randomstring");

const handleErrors = response => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};

const getDates = (startDate, endDate) => {
  var dates = [],
    currentDate = startDate,
    addDays = function(days) {
      var date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
    };
  while (currentDate < endDate) {
    dates.push(currentDate);
    currentDate = addDays.call(currentDate, 1);
  }
  return dates;
};

var mongoose = require("mongoose");
//  console.log('process.env in dBservice :', process.env)
mongoose.connect(process.env.DB_PATH);

mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind(console, "MongoDB connection error:"));

//Define a schema
var Schema = mongoose.Schema;

var MeterSchema = new Schema({
  asset: String,
  quantity: Number,
  date: Date
});

// Compile model from schema
const MeterModel = mongoose.model("MeterModel", MeterSchema);

exports.MeterModel = MeterModel;

//Cache the chart data to improve performance
var ConsumingAssetsAggregationSchema = new Schema({
  assetId: Number,
  range: String,
  data: {
    doughnutChartLabels: [String],
    doughnutChartData: [Number],
    doughnutChartLastDayData: [Number],
    doughnutChartLastDayDay: String,
    doughnutData: [[Number]],
    barChartData: [],
    barChartDataMatched: [],
    barChartLabels: [String],
    totalAvoidedCo2: Number,
    totalConsumption: Number,
    totalEnergyMatched: Number,
    totalEnergyMatchedByAsset: [{
      energyMatched: Number,
      id: Number,
      label: String
    }]
  }
});

// Compile model from schema
const ConsumingAssetsAggregation = mongoose.model("ConsumingAssetsAggregation", ConsumingAssetsAggregationSchema );

exports.ConsumingAssetsAggregation = ConsumingAssetsAggregation;

/****  YS - Retriving Blockchain related data from DB ****/
// retrieving the daily secret word corresponding to a specific Asset

const SecretWordsModel = mongoose.model(
  "secret_words",
  new Schema({
    asset_name: String,
    date: { year: String, month: String, day: String },
    secret_word: String
  })
);

async function getSecretWords(asset_name_, year_, month_, day_) {


  var promise3 = await SecretWordsModel.find(
    {
      $and: [
        {
          asset_name: asset_name_
        },
        {
          date: { year: year_, month: month_, day: day_ }
        }
      ]
    },
    async function(error, obj) {
      if (error) {
        console.log("Error : ", error);
        return false;
      } else return obj;
    }
  );



  return promise3;
}

module.exports.getSecretWords = getSecretWords;

// adding the daily secret word corresponding to a specific Asset to DB
async function setSecretWords(asset_name_, year_, month_, day_) {
  var secret = new SecretWordsModel({
    asset_name: asset_name_,
    date: { year: year_, month: month_, day: day_ },
    secret_word: randomstring.generate()
  });

  try {
    var obj = await secret.save();
    return obj;
  } catch (err) {
    console.log("Error : ", error);
    return false;
  }
}

module.exports.setSecretWords = setSecretWords;

// retrieving transaction params :value, gasPrice & gasLimit
const ParamsModel = mongoose.model(
  "blockchain_params",
  new Schema({
    function: String,
    value: String,
    gasPrice: Number,
    gasLimit: Number
  })
);

async function getParams(function_name) {
  var promise1 = ParamsModel.find(
    {
      function: function_name
    },
    function(error, obj) {
      if (error) {
        console.log("Error : ", error);
        return false;
      } else return obj;
    }
  );

  return promise1;
}

module.exports.getParams = getParams;

// retrieving private keys
const KeysModel = mongoose.model(
  "blockchain_keys",
  new Schema({
    account_name: String,
    pwd: String,
    key: {
      address: String,
      crypto: {
        cipher: String,
        cipherparams: {
          iv: String
        },
        ciphertext: String,
        kdf: String,
        kdfparams: {
          c: Number,
          dklen: Number,
          prf: String,
          salt: String
        },
        mac: String
      },
      id: String,
      meta: {
        description: String,
        passwordHint: String,
        timestamp: Number
      },
      name: String,
      version: Number
    }
  })
);
async function getKey(account_address) {
  var promise2 = KeysModel.find(
    {
      "Key.address": account_address
    },
    function(error, obj) {
      if (error) {
        console.log("Error : ", error);
        return false;
      }
    }
  );
  return promise2;
}

module.exports.getKey = getKey;

/****  END YS ****/

var AssetConsumingSchema = new Schema({
  id: {
    type: Number,
    unique: true
  },
  smartMeter: {
    type: String,
    unique: true
  },
  active: Boolean,
  cO2UsedForCertificate: Number,
  capacityWh: Number,
  maxCapacitySet: Boolean,
  certificatesCreatedForWh: Number,
  city: String,
  country: String,
  gpsLatitude: Number,
  gpsLongitude: Number,
  houseNumber: String,
  lastSmartMeterCO2OffsetRead: Number,
  lastSmartMeterReadFileHash: String,
  lastSmartMeterReadWh: Number,
  operationalSince: Date,
  owner: String,
  region: String,
  street: String,
  zip: String,
  smireName: String,
  name: String,
  displayedName: String,
  maxGooContract: Number,
  maxGooContractByAsset: [
    {
      producingAssets: [Number],
      maxGoo: Number,
      year: Number
    }
  ],
  producingAssets: [
    {
      id: Number,
      priority: Number,
      percentage: Number
    }
  ]
});

// Compile model from schema
const ConsumingAsset = mongoose.model("consumingAsset", AssetConsumingSchema);

exports.ConsumingAsset = ConsumingAsset;

var AssetProducingSchema = new Schema({
  id: {
    type: Number,
    unique: true
  },
  smartMeter: {
    type: String,
    unique: true
  },
  owner: String,
  operationalSince: Date,
  active: Boolean,
  capacityWh: Number,
  maxCapacitySet: Boolean,
  certificatesUsedForWh: Number,
  lastSmartMeterReadWh: Number,
  lastSmartMeterReadFileHash: String,
  houseNumber: String,
  street: String,
  zip: String,
  city: String,
  region: String,
  country: String,
  gpsLatitude: Number,
  gpsLongitude: Number,
  name: String,
  displayedName: String,
  charteFilename: String,
  isFake: Boolean,
  duplicateOfAssetId: Number,
  assetType: Number,
  typeOfPublicSupport: String,
  otherGreenAttributes: String,
  registryCompliance: Number
});

// Compile model from schema
const ProducingAsset = mongoose.model("producingAsset", AssetProducingSchema);

exports.ProducingAsset = ProducingAsset;

var CertificateSchema = new Schema({
  _certificateId: {
    type: Number,
    unique: true
  },
  _assetId: Number,
  _consumptionAssetId: Number,
  _owner: String,
  _powerInW: Number,
  _retired: Boolean,
  _dataLog: String,
  _coSaved: Number,
  _escrow: String,
  _creationTime: Date,
  _txHash: String,
  _productionDate: Date,
  _certificateHash: String,
  _info: String
});

// Compile model from schema
const Certificate = mongoose.model("certificate", CertificateSchema);

exports.Certificate = Certificate;

exports.initialization = async (req, res) => {
  try {
    const minDateRange = moment("2018-07-03", "YYYY-MM-DD")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("X");
    const maxDateRange = moment()
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("X");

    const smireAssets = ["laboulangere", "lida", "lida_rte", "eget", "plt_mareges", "fontanelles"];

    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    let allAssetsUrlString = "laboulangere,lida,lida_rte,eget,plt_mareges,fontanelles";

    let assetData = [];

    for (let date of allDates) {
      const startDate = moment(date)
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      const endDate = moment(date).format("YYYY-MM-DD");
      console.log("fetching Smire data from " + startDate + " to " + endDate + "...");

      let username = "system.weechain";
      let password = "3OetAPO8D03XIYs2WRuj";

      let url = "https://" + username + ":" + password + "@smire.bluesafire.io/api/daily_data_on_period?site=" + allAssetsUrlString + "&start=" + startDate + "&end=" + endDate + "&specific_timezone=Europe%2FParis";

      let headers = new Headers();
      /*
      console.log("fetching Tourbillon data from "+startDate+" to "+endDate+"...");

      let url = 'https://tourbillon.bluesafire.io/table/'
      url +=    'lida_from_rte/data?'
      url +=    'start='+startDate+'T00:00:00Z&end='+endDate+'T00:00:00Z&'
      url +=    'timezone=Europe/Paris&sort=asc&period=day&amount=1';

      console.log(url)

      headers.append('Authorization', 'Basic dXZ2Mm8xNnpmaXRwaW04dGpzeHViamd2ZjR2NGUxcHM6');
      */

      const meteringData = await fetch(url, { method: "GET", headers: headers })
        .then(response => response.json())
        .then(json => {
          return json;
        });

      console.log(meteringData);

      const dbDate = moment(meteringData.index[0]).toDate();

      for (i = 0; i < meteringData.columns.length; i++) {
        // Create an instance of model MeterModel
        var meterData = {
          asset: meteringData.columns[i],
          quantity: meteringData.data[0][i],
          date: dbDate
        };

        if (!isNaN(meterData.quantity)) {
          var meter = new MeterModel(meterData);

          // Save the new model instance, passing a callback
          /*
          await meter.save(function (err) {
            if (err) return console.error(err);
            console.log("saved!", meterData)
          });*/
        } else {
          console.log("Quantity not available for " + meterData.asset + " on date " + meterData.date);
        }
      }

      console.log(startDate, endDate, meteringData);
      console.log("-------------------------------------------------");
    }
    res.json({ status: "ok" });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

exports.certificatesInitialization = async (req, res) => {
  try {
    //Get all the existing certificates
    let url = process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747";

    let headers = new Headers();

    const bcCertificates = await fetch(url, { method: "GET", headers: headers })
      .then(response => response.json())
      .then(json => {
        return json;
      });

    const dBCertificates = [];

    for (const c of bcCertificates) {
      let dBcertificate = {};

      dBcertificate._certificateId = parseInt(c._certificateId, 10);
      dBcertificate._assetId = parseInt(c._assetId, 10);
      dBcertificate._owner = c._owner;
      dBcertificate._powerInW = parseInt(c._powerInW, 10);
      dBcertificate._retired = c._retired;
      dBcertificate._dataLog = c._dataLog;
      dBcertificate._coSaved = parseInt(c._coSaved, 10);
      dBcertificate._escrow = c._escrow;
      dBcertificate._creationTime = moment(c._creationTime, "X").toDate();
      dBcertificate._txHash = c._txHash;
      dBcertificate._productionDate = moment(c._productionDate, "X").toDate();
      dBcertificate._info = c._info;

      dBcertificate._consumptionAssetId = -1; //-1 is for a certificate that has not been consumed by a consumption asset

      switch (c._owner) {
      case "0x0051CcCBCFF51788dDa9614b12dcea0D57208C03":
        dBcertificate._consumptionAssetId = 0;
        break;
      case "0x00d0F0e2AF12fec0C16c6D595a9554b99B6dE045":
        dBcertificate._consumptionAssetId = 2;
        break;
      default:
        dBcertificate._consumptionAssetId = -1;
      }
      if (dBcertificate._certificateId !== 0 && !dBcertificate._certificateId) {
        console.log("This certificate has no certificateId");
        console.log(dBcertificate);
      }
      dBCertificates.push(dBcertificate);
    }

    //for inserting large batches of documents
    Certificate.insertMany(dBCertificates, function(err) {
      if (err) {
        console.log("An error occured saving certificates");
        console.log(err);
        res.json({ dBCertificates, bcCertificates });
      } else {
        console.log("certificates saved correctly  ");
        res.json({ dBCertificates, bcCertificates });
      }
    });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

exports.dailyUpdate = async (req, res) => {
  try {
    const saveData = req.query.save === true || req.query.save === "true" ? true : false;
    const start = req.query.start ? req.query.start : moment().format("YYYY-MM-DD");

    var date = moment(start, "YYYY-MM-DD");
    if (start && !moment(start, "YYYY-MM-DD").isValid()) {
      res.json({
        status: "ko",
        error: "Date is not on a valid format 'YYYY-MM-DD'"
      });
      return;
    }

    const minDateRange = moment(start, "YYYY-MM-DD")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("X");
    const maxDateRange = moment(start, "YYYY-MM-DD")
      .add(1, "days")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("X");

    const minDateRangeDate = moment(start, "YYYY-MM-DD")
      .subtract(1, "days")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("YYYY-MM-DD");
    const maxDateRangeDate = moment(start, "YYYY-MM-DD")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("YYYY-MM-DD");

    console.log("-------------------------------------------------");

    if (saveData) {
      console.log("Starting update-db (will save data)");
    } else {
      console.log("Starting update-db (without saving data)");
    }

    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    let allAssetsUrlString = "laboulangere,lida_rte,plt_mareges,eget,fontanelles";

    let assetData = [];

    for (let date of allDates) {
      const startDate = moment(date)
        .subtract(1, "days")
        .format("YYYY-MM-DD");
      const endDate = moment(date).format("YYYY-MM-DD");
      console.log("Fetching Smire data from " + startDate + " to " + endDate + "...");

      let username = "system.weechain";
      let password = "3OetAPO8D03XIYs2WRuj";

      let url = "https://" + username + ":" + password + "@smire.bluesafire.io/api/daily_data_on_period?site=" + allAssetsUrlString + "&start=" + startDate + "&end=" + endDate + "&specific_timezone=Europe%2FParis";

      let headers = new Headers();

      const meteringData = await fetch(url, { method: "GET", headers: headers })
        .then(response => response.json())
        .then(json => {
          return json;
        });

      console.log(meteringData);

      const dbDate = moment(meteringData.index[0]).toDate();

      for (i = 0; i < meteringData.columns.length; i++) {
        // Create an instance of model MeterModel
        var meterData = {
          asset: meteringData.columns[i],
          quantity: meteringData.data[0][i],
          date: dbDate
        };

        if (!isNaN(meterData.quantity)) {
          const meter = new MeterModel(meterData);

          const existingMetering = await MeterModel.find(
            {
              asset: meterData.asset,
              date: dbDate
            },
            async function(error, meters) {
              if (error) {
                console.log("Error : ", error);
                return false;
              }
            }
          );
          console.log("existingMetering");
          console.log(existingMetering);

          if (existingMetering.length === 0) {
            console.log("No existing record for " + meterData.asset + " on date " + dbDate);

            // Save the new model instance
            if (saveData) {
              console.log("Saving the data...");
              await meter.save(function(err) {
                if (err) {
                  console.log("An error occured saving " + meterData.asset + " on date " + dbDate + " : ");
                  console.log(err);
                } else {
                  console.log("meter saved correctly for " + meterData.asset + " on date " + dbDate + " : ");
                }
              });
            }
          } else {
            console.log("There is already data recorded for " + meterData.asset + " on date " + dbDate + " : ");
            //console.log(meters)
          }
        } else {
          console.log("Quantity not available for " + meterData.asset + " on date " + meterData.date);
        }
      }
    }

    console.log("update-db finished between " + minDateRangeDate + " and " + maxDateRangeDate + ".");
    console.log("-------------------------------------------------");

    res.json({ status: "ok" });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

exports.dailyUpdateFromTourbillon = async (req, res) => {
  try {
    const saveData = req.query.save === true || req.query.save === "true" ? true : false;
    const start = req.query.start
      ? req.query.start
      : moment()
        .subtract(1, "days")
        .format("YYYY-MM-DD");
    const end = req.query.end ? req.query.end : moment().format("YYYY-MM-DD");

    if (start && !moment(start, "YYYY-MM-DD").isValid()) {
      res.json({
        status: "ko",
        error: "Start date is not on a valid format 'YYYY-MM-DD'"
      });
      return;
    }

    if (end && !moment(end, "YYYY-MM-DD").isValid()) {
      res.json({
        status: "ko",
        error: "End date is not on a valid format 'YYYY-MM-DD'"
      });
      return;
    }

    const minDateRange = moment(start, "YYYY-MM-DD")
      .add(1, "days")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("X");
    const maxDateRange = moment(end, "YYYY-MM-DD")
      .add(2, "days")
      .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
      .format("X");

    console.log("***----------------------------------------------");

    if (saveData) {
      console.log("Starting update-db from tourbillon (will save data) ");
    } else {
      console.log("Starting update-db from tourbillon (without saving data)");
    }
    console.log("minDateRange " + moment(minDateRange, "X").toDate());
    console.log("maxDateRange " + moment(maxDateRange, "X").toDate());
    let allDates = getDates(moment.unix(minDateRange).toDate(), moment.unix(maxDateRange).toDate());

    let allAssets = [
      "lida_from_rte",
      "plt_mareges",
      "plt_eget",
      "fontanelles_fake_realtime", //Fontanelles
      "plt_coindre", // Coindre
      "plt_soulom_bc", // soulom bc
      "plt_soulom_hc", // soulom hc
      "teo_itron_30000210408399_ap_rt", //Data La Boulangère 1 - 30000210408399 - NOR"PAIN
      "teo_itron_30001430796549_ap_rt", //Data La Boulangère 2 - 30001430796549 - Pain Concept
      "teo_itron_30001430366805_ap_rt", //Data La Boulangère 3 - 30001430366805 - OUEST BOULANGERE Production
      "teo_itron_30001431024949_ap_rt", //Data La Boulangère 4 - 30001431024949 - OUEST BOULANGERE Logistique
      "teo_itron_30001220574230_ap_rt", //Data La Boulangère 5 - 30001220574230 - BEAUNE BRIOCHE
      "teo_itron_30001430767193_ap_rt", //Data La Boulangère 6 - 30001430767193 - VIENNOISERIE LIGERIENNE
      "teo_itron_30002212087578_ap_rt" //Data La Boulangère 7 - 30002212087578 - PANORIENT
    ];

    let allAssetsNames = [
      "lida_rte",
      "plt_mareges",
      "eget",
      "fontanelles", //Fontanelles
      "plt_coindre", // Coindre
      "plt_soulom_bc", // soulom bc
      "plt_soulom_hc", // soulom hc
      "laboulangere_norpain", //Data La Boulangère 1 - 30000210408399 - NOR"PAIN
      "laboulangere", //Data La Boulangère 2 - 30001430796549 - Pain Concept
      "laboulangere_ouest_boulangere_prod", //Data La Boulangère 3 - 30001430366805 - OUEST BOULANGERE Production
      "laboulangere_ouest_boulangere_logistique", //Data La Boulangère 4 - 30001431024949 - OUEST BOULANGERE Logistique
      "laboulangere_beaune_brioche", //Data La Boulangère 5 - 30001220574230 - BEAUNE BRIOCHE
      "laboulangere_viennoiserie_ligerienne", //Data La Boulangère 6 - 30001430767193 - VIENNOISERIE LIGERIENNE
      "laboulangere_panorient" //Data La Boulangère 7 - 30002212087578 - PANORIENT
    ];

    let allAssetsSmire = [
      "lida_rte",
      "plt_mareges",
      "eget",
      "fontanelles", //Fontanelles
      null, // Coindre
      null, // soulom bc
      null, // soulom hc
      null, //Data La Boulangère 1 - 30000210408399 - NOR'PAIN
      "laboulangere", //Data La Boulangère 2 - 30001430796549 - Pain Concept
      null, //Data La Boulangère 3 - 30001430366805 - OUEST BOULANGERE Production
      null, //Data La Boulangère 4 - 30001431024949 - OUEST BOULANGERE Logistique
      null, //Data La Boulangère 5 - 30001220574230 - BEAUNE BRIOCHE
      null, //Data La Boulangère 6 - 30001430767193 - VIENNOISERIE LIGERIENNE
      null //Data La Boulangère 7 - 30002212087578 - PANORIENT
    ];

    let assetData = [];
    let updateDbResults = [];
    let j = 0;
    for (let asset of allAssets) {
      for (let date of allDates) {
        const startDate = moment(date)
          .subtract(1, "days")
          .format("YYYY-MM-DDTHH:mm:ss.SSZ");
        const endDate = moment(date).format("YYYY-MM-DDTHH:mm:ss.SSZ");

        const newStartDate = moment(date)
          .subtract(1, "hours")
          .subtract(1, "days")
          .format("YYYY-MM-DDTHH:mm:ss.SSZ");
        const newEndDate = moment(date).format("YYYY-MM-DDTHH:mm:ss.SSZ");
        console.log("Fetching Tourbillon data from " + newStartDate + " to " + newEndDate + "...");

        let username = "13akuvc8qc52cv1bm5gqik7cmvclrl11";
        let password = "";

        let newTourbillonUrl = "https://" + username + ":" + password + "@tourbillon.bluesafire.io/table/";
        newTourbillonUrl += asset;
        newTourbillonUrl += "/data?";
        newTourbillonUrl += "start=" + newStartDate;
        newTourbillonUrl += "&end=" + newEndDate;
        newTourbillonUrl += "&timezone=Europe/Paris&sort=asc&period=hour&amount=1";

        let newHeaders = new Headers();

        console.log("Fetching " + asset + " between " + newStartDate + " and " + newEndDate);
        let newMeteringData = await fetch(newTourbillonUrl, {
          method: "GET",
          headers: newHeaders
        }).then(response => {
          console.log("newMeteringData response", JSON.stringify(response));
          return response.json();
        });

        //We need only the data for the dates after  newStartDate
        newMeteringData.data = newMeteringData.data.filter((power, idx) => {
          return idx >= 22 && idx <= 45;
        });

        newMeteringData.index.unix = newMeteringData.index.unix.filter((unixDate, idx) => {
          return idx >= 22 && idx <= 45;
        });

        const dbDate = moment(newMeteringData.index.unix[0], "X").toDate();

        let newQuantity = 0;
        newMeteringData.data.map(power => {
          newQuantity += power;
        });

        // Create an instance of model MeterModel
        var meterData = {
          asset: allAssetsNames[j],
          quantity: newQuantity,
          date: dbDate
        };

        if (!isNaN(meterData.quantity)) {
          const meter = new MeterModel(meterData);

          const existingMetering = await MeterModel.find(
            {
              asset: meterData.asset,
              date: dbDate
              //date: { $gte: dbDateStart, $lte: dbDateEnd }
            },
            async function(error, meters) {
              if (error) {
                console.log("Error : ", error);
                return false;
              }
            }
          );

          if (existingMetering.length === 0) {
            console.log("No existing record for " + meterData.asset + " on date " + dbDate);

            // Save the new model instance
            if (saveData) {
              console.log("Saving the data for asset " + meterData.asset + " on date " + dbDate);
              await meter.save(function(err) {
                if (err) {
                  console.log("An error occured saving " + meterData.asset + " on date " + dbDate + " : ");
                  console.log(err);
                } else {
                  console.log("meter saved correctly for " + meterData.asset + " on date " + dbDate + " : ");
                }
              });
            }
          } else {
            console.log("There is already data recorded for " + meterData.asset + " on date " + dbDate + " : ");
            console.log(existingMetering);
          }
        } else {
          console.log("Quantity not available for " + meterData.asset + " on date " + meterData.date);
        }
      }
      j++;
    }

    console.log("update-db-tourbillon finished between " + start + " and " + end + ".");
    console.log("-------------------------------------------------");

    res.json({ status: "ok" });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};
