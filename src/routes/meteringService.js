const mongoose = require('mongoose');
const soap = require('soap');
const moment = require('moment');
const fetch = require('node-fetch');

// -- DARWIN
const DARWIN_WSDL_PATH = "src/soap-wsdl/darwin.wsdl";
const DARWIN_API_USER = "TeoUser";
const DARWIN_API_PASSWORD = "jberPMEffFE3Jx4wmMGS";
const DARWIN_API_URL = "https://realtimeservice.darwin4res.com/v1.7/RealTimeService.svc";
const DARWIN_SOURCE_API = "DARWIN";

// -- CRIGEN
const CRIGEN_API_URL = "http://18.191.2.105:3001/graphql";
const CRIGEN_SOURCE_API = "CRIGEN";

// -BEGIN- Mongoose Init
mongoose.connect(process.env.DB_PATH);
mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;
// -END- Mongoose Init

// Asset metering
var AssetMeteringSchema = new Schema({
  asset_id: String,
  start_date: Date,
  end_date: Date,
  measure: Number,
  unit: String,
  measure_original: Number,
  unit_original: String,
  measure_date: Date,
  source: String,
  source_id: Number,
  valid: Boolean,
  creation_date: Date,
  source_api: String
});

// Compile model from schema
const AssetMetering = mongoose.model('asset_metering', AssetMeteringSchema);

exports.AssetMetering = AssetMetering;

// -BEGIN- Darwin update
async function getDarwinHistoricalData(_args) {
  return new Promise((resolve, reject) => {
    soap.createClient(DARWIN_WSDL_PATH, function(err, client) {
      if(err) reject(err);
      client.Get24HistoricalData(_args, function(err, result) {
        if(err) reject(err);
        resolve(result);
      });
    });
  })
}

async function updateDarwinAsset_Hourly(_asset) {
  try {
    let darwinURL = DARWIN_API_URL;

    let args = {
      userLogin: DARWIN_API_USER,
      userPassword: DARWIN_API_PASSWORD,
      location: _asset.asset_id,
      granularity: 60,
      measureCodes: [
        { string: "ActivePower" }
      ]
    }

    let meteringValueArray = null;

    try {
      let result = await getDarwinHistoricalData(args); 

      meteringValueArray = result.Get24HistoricalDataResult.ListeTags.DataTag;

      for(let meteringValue of meteringValueArray) {
        let valueHour = moment(meteringValue.TimeStamp);

        let twoHoursAgo = moment().subtract('2', 'hour');

        // Don't process values that are less than 2 hours old
        if(valueHour.isAfter(twoHoursAgo)) continue;

        let alreadyExistingMeteringValues = await new Promise((resolve, reject) => {
          AssetMetering.find({
            end_date: {
              $gte: valueHour.clone().subtract('1', 'minute'),
              $lt: valueHour.clone().add('1', 'minute')
            },
            source: DARWIN_SOURCE_API,
            source_id: _asset.source_id,
            asset_id: _asset.asset
          })
          .sort({ creation_date: -1 })
          .exec((err, result) => {
            if(err) reject(err);
            resolve(result);
          })
        });

        let create = false;

        if(alreadyExistingMeteringValues && alreadyExistingMeteringValues.length > 0) {
          if(alreadyExistingMeteringValues[0].measure_original != meteringValue.Value) {
            create = true;
          }
        } else {
          create = true;
        }

        if(meteringValue.Unit != "MW") {
          throw new Error(`Unknown unit in Darwin result for asset ${_asset} : ${meteringValue.Unit}`);
        }

        if(create) {
          let newMeteringData = new AssetMetering({
            asset_id: _asset.asset,
            start_date: valueHour.clone().subtract('1', 'hour').add('1', 'millisecond'),
            end_date: valueHour.clone(),
            measure: meteringValue.Value * 1000,
            unit: "kWh",
            measure_original: meteringValue.Value,
            unit_original: meteringValue.Unit,
            source: DARWIN_SOURCE_API,
            source_id: _asset.source_id,
            valid: true,
            creation_date: moment()
          }); 

          await newMeteringData.save();
        } 
      }
    } catch (error) {
      console.log(error);
    }
  } catch (error) {
    console.log(error);
  }
}
  
exports.hourlyUpdateFromDarwin = async (req, res) => {
  let assets = [
    {
      asset: "EGET",
      source_id: "PLT_EGET"
    },
    {
      asset: "COINDRE",
      source_id: "PLT_COINDRE"
    },
    {
      asset: "SOULOM-BC",
      source_id: "PLT_SOULOM-BC"
    },
    {
      asset: "SOULOM-HC",
      source_id: "PLT_SOULOM-HC"
    },
    {
      asset: "MAREGES",
      source_id: "PLT_MAREGES"
    }
  ];

  for(let asset of assets) {
    await updateDarwinAsset_Hourly(asset);
  }
}
// -END- Darwin update

// -BEGIN- Crigen update
exports.updateCrigenAsset = async(req, res) => {
  let day = moment();

  if(req.body.dayTimestampInMs) {
    day = moment(req.body.dayTimestampInMs);
  }

  try {
    updateCrigenAssetFromDay(day);
    return res.json({ success: true });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
}

let updateCrigenAssetFromDay = async (specifiedMoment) => {
  let payload = {
    "query": "query queryBetweenTimestamps($sts:String, $ets:String){queryBetweenTimestamps(sTimestamp:$sts, eTimestamp:$ets){Node_ID SQN Timestamp Power Energy Sign}}",
    "variables": {
        "sts": null,
        "ets": null
    }
  };

  let meteringValueArray = [];

  try {
    let lastDayStart = specifiedMoment.clone().subtract('1', 'day').startOf('day');
    let lastDayEnd = specifiedMoment.clone().subtract('1', 'day').endOf('day');

    payload.variables.sts = lastDayStart.valueOf();
    payload.variables.ets = lastDayEnd.valueOf();

    let response = await fetch(CRIGEN_API_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    });

    let responseBody = await response.json();

    if(!responseBody || !responseBody.data || !responseBody.data.queryBetweenTimestamps) throw new Error(`Cannot parse received json -BEGIN JSON-${responseBody}-END JSON-'`);

    meteringValueArray = responseBody.data.queryBetweenTimestamps;

    // Get unique sensor id set
    let sensorIdArray = [];

    for(let meteringValue of meteringValueArray) {
      if(sensorIdArray.indexOf(meteringValue.Node_ID) < 0) {
        sensorIdArray.push(meteringValue.Node_ID);
      }
    }

    // For each sensor
    for(let sensorId of sensorIdArray) {
      sensorMeteringValueArray = meteringValueArray.filter(m => m.Node_ID == sensorId);

      // Loop through each hour of the previous day
      for(let currentHour = 0; currentHour < 24; currentHour++) {
        lastDayStart.hour(currentHour);

        let currentHourBeginning = lastDayStart.clone().startOf('hour').add('1', 'millisecond');
        let currentHourEnding = lastDayStart.clone().endOf('hour').add('1', 'millisecond');

        // Filtering array of led ticks for this sensor on current hour period
        hourSensorMeteringValueArray = sensorMeteringValueArray.filter((m) => { 
          let valueMoment = moment(parseInt(m.Timestamp));
          return valueMoment.isSameOrAfter(currentHourBeginning) && valueMoment.isSameOrBefore(currentHourEnding);
        });

        // One tick equals 1 kWh, so we count the items in the array for this hour
        let currentHourEnergyIn_kWh = hourSensorMeteringValueArray.length;

        // Searching for existing record in DB
        let alreadyExistingMeteringValues = await new Promise((resolve, reject) => {
          AssetMetering.find({
            end_date: {
              $gte: currentHourEnding.clone().subtract('1', 'minute'),
              $lt: currentHourEnding.clone().add('1', 'minute')
            },
            source_id: sensorId,
            source: CRIGEN_SOURCE_API
          })
          .sort({ creation_date: -1 })
          .exec((err, result) => {
            if(err) reject(err);
            resolve(result);
          })
        });

        let create = false;

        if(alreadyExistingMeteringValues && alreadyExistingMeteringValues.length > 0) {
          if(alreadyExistingMeteringValues[0].measure_original != currentHourEnergyIn_kWh) {
            create = true;
          }
        } else {
          create = true;
        }
  
        if(create) {
          // Don't record value at 0
          if(currentHourEnergyIn_kWh == 0) continue;

          let newMeteringData = new AssetMetering({
            start_date: currentHourBeginning.clone(),
            end_date: currentHourEnding.clone(),
            measure: currentHourEnergyIn_kWh,
            unit: "kWh",
            measure_original: currentHourEnergyIn_kWh,
            unit_original: "kWh",
            valid: true,
            creation_date: moment(),
            source: CRIGEN_SOURCE_API,
            source_id: sensorId
          }); 
  
          await newMeteringData.save();
        } 
      }
    }
  } catch (error) {
    console.log(error);
    return res.status(500);
  }
}
// -END- Crigen update