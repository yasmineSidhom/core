const Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545'));

var keythereum = require('keythereum');
var Tx = require('ethereumjs-tx');

let AssetConsumingRegistry_artefact = require('../../'+process.env.BUILD_FOLDER+'/contracts/AssetConsumingRegistryLogic.json');
const abiAssetConsuming = AssetConsumingRegistry_artefact.abi;
const addressAssetConsuming = AssetConsumingRegistry_artefact.networks[401697].address;
const myContractAssetConsuming = new web3.eth.Contract(abiAssetConsuming, addressAssetConsuming);

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

//Create asset consuming registry
exports.createAsset = async (req, res) => {

  const sender = req.query._sender;        //address

  try {
    var openDoorDataPart = myContractAssetConsuming.methods.createAsset().encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressAssetConsuming,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};

//Init General Asset consuming registry
exports.setGeneralProperties = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  var smartMeter = req.body._smartMeter;               //address
  var owner = req.body._owner;                         //address
  var operationalSince = req.body._operationalSince;   //uint
  var capacityWh = req.body._capacityWh;               //uint
  var maxCapacitySet = req.body._maxCapacitySet;       //bool
  var active =  req.body._active;                       //bool

  try {
    var openDoorDataPart = myContractAssetConsuming.methods.initGeneral(
      assetId,
      smartMeter,
      owner,
      operationalSince,
      capacityWh,
      maxCapacitySet,
      active,
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressAssetConsuming,
      from: sender
    };
    var tx = new Tx(rawTx);
    var privateKey = require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};

//init Location producing properties with the smart contract assetProducingRegistryLogic
exports.setLocation = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  var country = req.body._country ;            //bytes32
  var region = req.body._region ;              //bytes32
  var zip = req.body._zip ;                    //bytes32
  var city = req.body._city ;                  //bytes32
  var street = req.body._street ;              //bytes32
  var houseNumber = req.body._houseNumber ;    //bytes32
  var gpsLatitude = req.body._gpsLatitude ;    //bytes32
  var gpsLongitude = req.body._gpsLongitude ;  //bytes32

  try {
    var openDoorDataPart = myContractAssetConsuming.methods.initLocation(
      parseInt(assetId, 10),
      web3.utils.fromAscii(country),
      web3.utils.fromAscii(region),
      web3.utils.fromAscii(zip),
      web3.utils.fromAscii(city),
      web3.utils.fromAscii(street),
      web3.utils.fromAscii(houseNumber),
      web3.utils.fromAscii(gpsLatitude),
      web3.utils.fromAscii(gpsLongitude)
    ).encodeABI();

    const nonce= await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressAssetConsuming,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

//Save Smart Meter Read consuming registry
exports.setSmartMeterRead = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  var newMeterRead = req.body._newMeterRead;                             //uint
  var lastSmartMeterReadFileHash = req.body._lastSmartMeterReadFileHash; //bytes32
  var smartMeterDown = req.body._smartMeterDown;                         //bool

  try {
    var openDoorDataPart = myContractAssetConsuming.methods.saveSmartMeterRead(
      assetId,
      newMeterRead,
      web3.utils.fromAscii(lastSmartMeterReadFileHash),
      smartMeterDown,
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressAssetConsuming,
      from: sender
    };
    var tx = new Tx(rawTx);
    var privateKey =require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx=await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

//set Consumption For Periode from consuming registry contract
exports.setConsumptionForPeriode = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  var consumed = req.body._consumed;           //uint

  try {
    var openDoorDataPart = myContractAssetConsuming.methods.setConsumptionForPeriode(
      assetId,
      consumed
    ).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressAssetConsuming,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

//update Smart Meter from consuming registry contract
exports.setSmartMeter = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  var newSmartMeter = req.body._newSmartMeter;                             //address

  try {
    var openDoorDataPart = myContractAssetConsuming.methods.updateSmartMeter(
      assetId,
      newSmartMeter,
    ).encodeABI();

    const nonce= await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressAssetConsuming,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require('../services/tobalaba/privateKey').privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await  web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex'));

    const results = await Promise.resolve(ret);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

///************************************************************************************GET INFORMATION CONSUMER********************************************************///

//get Asset General consuming registry
exports.getAssetGeneral = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  try {
    const result = await myContractAssetConsuming.methods.getAssetGeneral(assetId).call({ from:sender });

    const results = await Promise.resolve(result);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

//get Asset General consuming registry
exports.getConsumingProperties = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  try {
    const result = await myContractAssetConsuming.methods.getConsumingProperies(assetId).call({ from:sender });
    const results = await Promise.resolve(result);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }
};

//get Asset Location
exports.getAssetLocation = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint

  try {
    const result= await myContractAssetConsuming.methods.getAssetLocation(assetId).call({ from:sender });
    result[0]=web3.utils.toAscii(result[0]).replace(/\0/g, '');
    result[1]=web3.utils.toAscii(result[1]).replace(/\0/g, '');
    result[2]=web3.utils.toAscii(result[2]).replace(/\0/g, '');
    result[3]=web3.utils.toAscii(result[3]).replace(/\0/g, '');
    result[4]=web3.utils.toAscii(result[4]).replace(/\0/g, '');
    result[5]=web3.utils.toAscii(result[5]).replace(/\0/g, '');
    result[6]=web3.utils.toAscii(result[6]).replace(/\0/g, '');
    result[7]=web3.utils.toAscii(result[7]).replace(/\0/g, '');
    result["country"]=result[0]
    result["region"]=result[1]
    result["zip"]=result[2]
    result["city"]=result[3]
    result["street"]=result[4]
    result["houseNumber"]=result[5]
    result["gpsLatitude"]=result[6]
    result["gpsLongitude"]=result[7]

    const results = await Promise.resolve(result);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};

//getAssetDataLog asset
exports.getAssetDataLog = async (req, res) => {

  const sender = req.query._sender;        //address

  var assetId = req.params.assetId;                      //uint
  try {
    const result = await myContractAssetConsuming.methods.getAssetDataLog(assetId).call({ from:sender });

    const results = await Promise.resolve(result);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};

//get asset list lenght consuming registry
exports.getAssetListLength = async (req, res) => {

  const sender = req.query._sender;        //address
  try {
    const result = await myContractAssetConsuming.methods.getAssetListLength().call({ from:sender });

    const results = await Promise.resolve(result);
    res.json(results);
  }
  catch(error) {
    console.error(error);
    res.status(400).send({
      message:error.message
    });
    res.end();
  }


};
