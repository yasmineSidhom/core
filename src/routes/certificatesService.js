const Web3 = require("web3");
var web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545"));

const keythereum = require("keythereum");
const Tx = require("ethereumjs-tx");
const fetch = require("node-fetch");
const contract = require("truffle-contract");
const ethers = require("ethers");
const moment = require("moment");

let CertificateLogic_artefact = require("../../" + process.env.BUILD_FOLDER + "/contracts/CertificateLogic.json");
const abiCertificateLogic = CertificateLogic_artefact.abi;
const addressCertificateLogic = CertificateLogic_artefact.networks[401697].address;
const myContractCertificateLogic = new web3.eth.Contract(abiCertificateLogic, addressCertificateLogic);

let AssetProducingRegistry_artefact = require("../../" + process.env.BUILD_FOLDER + "/contracts/AssetProducingRegistryLogic.json");
const abiAssetProducing = AssetProducingRegistry_artefact.abi;
const addressAssetProducing = AssetProducingRegistry_artefact.networks[401697].address;
const myContractAssetProducing = new web3.eth.Contract(abiAssetProducing, addressAssetProducing);

let CertificateDB_artefact = require("../../" + process.env.BUILD_FOLDER + "/contracts/CertificateDB.json");
const abiCertificateDb = CertificateDB_artefact.abi;
const addressCertificateDb = CertificateDB_artefact.networks[401697].address;
const myContractCertificateDb = new web3.eth.Contract(abiCertificateDb, addressCertificateDb);

var Contract = new ethers.Contract(addressCertificateDb, abiCertificateDb, new ethers.providers.JsonRpcProvider("http://127.0.0.1:8545"));

const pdfParser = require("../libPdf/src/pdf2json/pdf2json.js");
const fs = require("fs");

const handleErrors = response => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};

exports.getCertificateListLength = async (req, res) => {
  const sender = req.query._sender; //address

  try {
    const result = await myContractCertificateLogic.methods.getCertificateListLength().call({ from: sender });
    let response = JSON.stringify(result);
    response = JSON.parse(response);
    res.json(response);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

exports.getAllCertificates = async (req, res) => {
  const owner = req.query._owner; //address
  const sender = req.query._sender; //address
  const date = req.query._date; //address

  if (date && !moment(date, "YYYY-MM-DD").isValid()) {
    console.log("Bad date format (YYYY-MM-DD)");
    res.status(400).send({
      error: "Bad date format (YYYY-MM-DD)"
    });
    res.end();
  } else {
    var certificates = [];
    var prodAssets = [];

    try {
      const certificatesListLength = await myContractCertificateLogic.methods.getCertificateListLength().call({ from: sender });

      //Use the offchain data, later we will use the data on chain, checking the roles
      const users = await fetch(process.env.API_URL + "/users?_sender=" + sender, {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
      })
        .then(handleErrors)
        .then(res => res.json())
        .then(data => {
          return data;
        });

      const prodAssetsListLength = await myContractAssetProducing.methods.getAssetListLength().call({ from: sender });

      for (i = 0; i < prodAssetsListLength; i++) {
        //Get each producing asset from the blockchain (but not the demo assets...)
        const prodAssetLocation = await myContractAssetProducing.methods.getAssetLocation(i).call({ from: sender });
        prodAssetLocation[0] = web3.utils.toAscii(prodAssetLocation[0]).replace(/\0/g, "");
        prodAssetLocation[1] = web3.utils.toAscii(prodAssetLocation[1]).replace(/\0/g, "");
        prodAssetLocation[2] = web3.utils.toAscii(prodAssetLocation[2]).replace(/\0/g, "");
        prodAssetLocation[3] = web3.utils.toAscii(prodAssetLocation[3]).replace(/\0/g, "");
        prodAssetLocation[4] = web3.utils.toAscii(prodAssetLocation[4]).replace(/\0/g, "");
        prodAssetLocation[5] = web3.utils.toAscii(prodAssetLocation[5]).replace(/\0/g, "");
        prodAssetLocation[6] = web3.utils.toAscii(prodAssetLocation[6]).replace(/\0/g, "");
        prodAssetLocation[7] = web3.utils.toAscii(prodAssetLocation[7]).replace(/\0/g, "");

        prodAssets.push({
          id: i,
          name: (prodAssetLocation[5] + " " + prodAssetLocation[4]).trim()
        });
      }

      const currentUser = users.find(user => {
        return user.address === owner;
      });

      //const isAdmin = currentUser && currentUser.roles <= 1 ? true : false;
      const isAdmin = currentUser && currentUser.organization === "ENGIE" ? true : false;

      for (i = 0; i < certificatesListLength; i++) {
        var certificate = await myContractCertificateLogic.methods.getCertificate(i).call({ from: sender });
        certificate._certificateId = i;

        const certificateOwner = users.find(user => {
          return user.address ? certificate[1] === user.address : false;
        });

        certificate._ownerName = certificateOwner ? certificateOwner.offchainData.user_metadata.name : "Unknown";

        const certificateResource = prodAssets.find(prodAsset => {
          return parseInt(certificate[0], 10) === parseInt(prodAsset.id, 10);
        });
        certificate._resourceName = certificateResource ? certificateResource.name : "Unknown";

        if (isAdmin || (owner && certificate[1] === owner && certificate._retired === false)) {
          const productionDate = moment.unix(certificate._productionDate).format("YYYY-MM-DD");
          if (!date) {
            certificates.push(certificate);
          } else {
            if (productionDate === date) {
              certificates.push(certificate);
            }
          }
        }
      }
      var results = await Promise.resolve(certificates);
      res.json(results);
    } catch (error) {
      console.log(error);
      res.status(400).send({
        message: error.message
      });
      res.end();
    }
  }
};

exports.getAllCertificatesFromCertificateDB = async (req, res) => {
  const owner = req.query._owner; //address
  const sender = req.query._sender; //address
  const date = req.query._date; //address

  if (date && !moment(date, "YYYY-MM-DD").isValid()) {
    console.log("Bad date format (YYYY-MM-DD)");
    res.status(400).send({
      error: "Bad date format (YYYY-MM-DD)"
    });
    res.end();
  } else {
    var prodAssets = [];
  }

  try {
    const step0 = moment().format("X");

    //Use the offchain data, later we will use the data on chain, checking the roles
    const users = await fetch(process.env.API_URL + "/users?_sender=" + sender, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        return data;
      });

    const step1 = moment().format("X");
    console.log("Certif Step 1 time = " + (step1 - step0) + "s");

    const prodAssetsListLength = await myContractAssetProducing.methods.getAssetListLength().call({ from: sender });

    const step2 = moment().format("X");
    console.log("Certif Step 2 time = " + (step2 - step1) + "s");

    for (i = 0; i < prodAssetsListLength; i++) {
      //Get each producing asset from the blockchain (but not the demo assets...)
      const prodAssetLocation = await myContractAssetProducing.methods.getAssetLocation(i).call({ from: sender });
      prodAssetLocation[0] = web3.utils.toAscii(prodAssetLocation[0]).replace(/\0/g, "");
      prodAssetLocation[1] = web3.utils.toAscii(prodAssetLocation[1]).replace(/\0/g, "");
      prodAssetLocation[2] = web3.utils.toAscii(prodAssetLocation[2]).replace(/\0/g, "");
      prodAssetLocation[3] = web3.utils.toAscii(prodAssetLocation[3]).replace(/\0/g, "");
      prodAssetLocation[4] = web3.utils.toAscii(prodAssetLocation[4]).replace(/\0/g, "");
      prodAssetLocation[5] = web3.utils.toAscii(prodAssetLocation[5]).replace(/\0/g, "");
      prodAssetLocation[6] = web3.utils.toAscii(prodAssetLocation[6]).replace(/\0/g, "");
      prodAssetLocation[7] = web3.utils.toAscii(prodAssetLocation[7]).replace(/\0/g, "");

      prodAssets.push({
        id: i,
        name: (prodAssetLocation[5] + " " + prodAssetLocation[4]).trim()
      });
    }

    const step3 = moment().format("X");
    console.log("Certif Step 3 time = " + (step3 - step2) + "s");

    const currentUser = users.find(user => {
      return user.address === owner;
    });

    //const isAdmin = currentUser && currentUser.roles <= 1 ? true : false;
    const isAdmin = currentUser && currentUser.organization === "ENGIE" ? true : false;

    var listCertificates = await Contract.getAllCertificates();
    /*

      0 uint _assetId,            0,
      1 address _owner,           "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
      2 uint _powerInW,           2471810000,
      3 bool _retired,            false,
      4 bytes32 _dataLog          "0x31353430353630303738363636756e646566696e656400000000000000000000",
      5 uint _coSaved,            0,
      6 address _escrow,          "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747",
      7 uint _creationTime,       1540560084,
      8 bytes32 _txHash,          "0x6d6ba8679a774c4d7e4bb8d988664d745cf24940bff184fdafa26762345a0115",
      9 uint _productionDate,     1537920000,
      10 bytes32 _certificateHash  "0x0000000000000000000000000000000000000000000000000000000000000000"

      */

    const listCertificatesObj = listCertificates.map((certificate, index) => {
      const certificateObj = {};

      for (i = 0; i < certificate.length; i++) {
        if (i === 0 || i === 2 || i === 5 || i === 7 || i === 9) {
          certificateObj[i] = certificate[i].toNumber();
        } else {
          certificateObj[i] = certificate[i];
        }
      }

      certificateObj._assetId = certificate[0].toNumber();
      certificateObj._owner = certificate[1];
      certificateObj._powerInW = certificate[2].toNumber();
      certificateObj._retired = certificate[3];
      certificateObj._dataLog = certificate[4];
      certificateObj._coSaved = certificate[5].toNumber();
      certificateObj._escrow = certificate[6];
      certificateObj._creationTime = certificate[7].toNumber();
      certificateObj._txHash = certificate[8];
      certificateObj._productionDate = certificate[9].toNumber();
      certificateObj._certificateHash = certificate[10];
      certificateObj._certificateId = index;

      const certificateOwner = users.find(user => {
        return user.address ? certificate[1] === user.address : false;
      });

      certificateObj._ownerName = certificateOwner ? certificateOwner.organization : "Unknown";

      const certificateResource = prodAssets.find(prodAsset => {
        return parseInt(certificate[0]) === parseInt(prodAsset.id);
      });

      certificateObj._resourceName = certificateResource ? certificateResource.name : "Unknown";

      return certificateObj;
    });

    const listCertificatesObjFiltered = listCertificatesObj.filter(certificate => {
      if (isAdmin || (owner && certificate[1] === owner && certificate._retired === false)) {
        const productionDate = moment.unix(certificate._productionDate).format("YYYY-MM-DD");
        if (!date) {
          return true;
        } else {
          if (productionDate === date) {
            return true;
          }
        }
      }
    });

    const results = await Promise.resolve(listCertificatesObjFiltered);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//Get the certificate details with logic certificate contract
exports.getCertificate = async (req, res) => {
  var sender = req.query._sender; //Address
  var certificateId = req.params.certificateId;

  try {
    const result = await myContractCertificateLogic.methods.getCertificate(certificateId).call({ from: sender });
    var response = JSON.stringify(result);
    response._certificateId = certificateId;
    result._certificateId = certificateId;

    const results = await Promise.resolve(result);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//Get the address owner of a certificate with logic certificate contract
exports.getCertificateOwner = async (req, res) => {
  var sender = req.query._sender; //Address
  var certificateId = req.params.certificateId;

  try {
    const result = await myContractCertificateLogic.methods.getCertificateOwner(certificateId).call({ from: sender });
    var response = JSON.stringify(result);
    response = JSON.parse(response);

    const results = await Promise.resolve(response);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//Create a certificate for asset owner with Logic Contract
exports.createCertificateForAssetOwner = async (req, res) => {
  var sender = req.query._sender; //Address

  var assetId = req.body._assetId; //uint
  var powerInW = req.body._powerInW; //uint
  var productionDate = req.body._productionDate; //uint
  var info = req.body._info; //bytes32

  try {
    var openDoorDataPart;

    if (process.env.BUILD_FOLDER === "build") {
      //prod only, small difference between the prod smart contract and the dev smart contract
      openDoorDataPart = myContractCertificateLogic.methods.createCertificateForAssetOwner(assetId, powerInW, productionDate, web3.utils.fromAscii(info)).encodeABI();
    } else {
      openDoorDataPart = myContractCertificateLogic.methods.createCertificateForAssetOwner(assetId, powerInW, productionDate).encodeABI();
    }

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();

    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));
    console.log("ret.logs[0]");
    console.log(JSON.stringify(ret));
    const result = await web3.eth.abi.decodeLog(
      [
        {
          type: "uint",
          name: "_certificateId",
          indexed: true
        },
        {
          type: "uint",
          name: "_powerInW"
        },
        {
          type: "address",
          name: "_owner"
        },
        {
          type: "address",
          name: "_escrow"
        },
        {
          type: "uint",
          name: "_productionDate"
        }
      ],
      ret.logs[0].data,
      ret.logs[0].topics[1]
    );
    ret.logs[0]["result"] = result;

    const results = await Promise.resolve(ret);

    const certificateId = result._certificateId;

    const certificateCreatedBc = await fetch(process.env.API_URL + "/certificates/" + certificateId + "?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log("Certificate N°" + certificateId + " retrieved from blockchain ");
        return data;
      });

    console.log("certificateCreatedBc : ");
    console.log(certificateCreatedBc);

    const certificateToCreateDb = {
      _certificateId: parseInt(certificateCreatedBc._certificateId, 10),
      _assetId: parseInt(certificateCreatedBc._assetId, 10),
      _consumptionAssetId: -1,
      _owner: certificateCreatedBc._owner,
      _powerInW: parseInt(certificateCreatedBc._powerInW, 10),
      _retired: certificateCreatedBc._retired === "true" || certificateCreatedBc._retired === true ? true : false,
      _dataLog: certificateCreatedBc._dataLog,
      _coSaved: parseInt(certificateCreatedBc._coSaved, 10),
      _escrow: certificateCreatedBc._escrow,
      _creationTime: moment(certificateCreatedBc._creationTime, "X").toDate(),
      _txHash: certificateCreatedBc._txHash,
      _productionDate: moment(certificateCreatedBc._productionDate, "X").toDate(),
      _certificateHash: certificateCreatedBc._certificateHash,
      _info: ""
    };

    console.log("certificateToCreateDb : ");
    console.log(certificateToCreateDb);

    const certificateDbResult = await fetch(process.env.API_URL + "/certificates-db", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(certificateToCreateDb)
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log("New certificate created in DB for asset id = " + assetId + " powerInW = " + powerInW + " _certificateId = " + data._certificateId);
        return data;
      });

    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//Create a certificate with Logic Contract
exports.createCertificate = async (req, res) => {
  var sender = req.query._sender; //Address

  var assetId = req.body._assetId; //uint
  var owner = req.body._owner; //address
  var powerInW = req.body._powerInW; //uint
  var productionDate = req.body._productionDate; //uint
  var info = req.body._info; //bytes32

  try {
    var openDoorDataPart = myContractCertificateLogic.methods.createCertificate(assetId, owner, powerInW, productionDate, web3.utils.fromAscii(info)).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));
    const result = await web3.eth.abi.decodeLog(
      [
        {
          type: "uint",
          name: "_certificateId",
          indexed: true
        },
        {
          type: "uint",
          name: "_powerInW"
        },
        {
          type: "address",
          name: "_owner"
        },
        {
          type: "address",
          name: "_escrow"
        },
        {
          type: "uint",
          name: "_productionDate"
        },
        {
          type: "bytes32",
          name: "_info"
        }
      ],
      ret.logs[0].data,
      ret.logs[0].topics[1]
    );
    ret.logs[0]["result"] = result;

    const results = await Promise.resolve(ret);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//set a transaction hash txHash of a certificate with Logic Contract
exports.setCertificateTxHash = async (req, res) => {
  var sender = req.query._sender; //Address

  var certificateId = req.params.certificateId; //uint

  var txHash = req.body._txHash; //bytes32

  try {
    var openDoorDataPart = myContractCertificateLogic.methods.setCertificateTxHash(certificateId, txHash).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

    const results = await Promise.resolve(ret);

    let certificateToUpdateDb = await fetch(process.env.API_URL + "/certificates-db/" + certificateId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log("Certificate N°" + certificateId + " retrieved from DB for _txHash update");
        return data;
      });

    certificateToUpdateDb = { ...certificateToUpdateDb, _txHash: txHash };

    const certificateDbResult = await fetch(process.env.API_URL + "/certificates-db/" + certificateId, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(certificateToUpdateDb)
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log("Certificate " + certificateId + " _txHash updated in DB");
        return data;
      });

    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//set an hash of the certificate with Logic Contract
exports.setCertificateHash = async (req, res) => {
  var sender = req.query._sender; //Address
  var certificateId = req.params.certificateId; //uint

  try {
    var openDoorDataPart = myContractCertificateLogic.methods.setCertificateHash(certificateId).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

    const results = await Promise.resolve(ret);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//Transfer certificate to another one with logic certificate contract
exports.changeCertificateOwner = async (req, res) => {
  var sender = req.query._sender; //Address

  var certificateId = req.params.certificateId;
  var newOwner = req.body._newOwner;
  var newConsumptionAssetId = req.body._newConsumptionAssetId;

  try {
    var openDoorDataPart = myContractCertificateLogic.methods.changeCertificateOwner(certificateId, newOwner).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

    const results = await Promise.resolve(ret);

    let certificateToUpdateDb = await fetch(process.env.API_URL + "/certificates-db/" + certificateId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log("Certificate N°" + certificateId + " retrieved from DB for _txHash update");
        return data;
      });

    certificateToUpdateDb = {
      ...certificateToUpdateDb,
      _owner: newOwner,
      _consumptionAssetId: newConsumptionAssetId || newConsumptionAssetId === 0 ? newConsumptionAssetId : certificateToUpdateDb._consumptionAssetId
    };

    const certificateDbResult = await fetch(process.env.API_URL + "/certificates-db/" + certificateId, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(certificateToUpdateDb)
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log("Certificate " + certificateId + " _owner & _consumptionAssetId updated in DB");
        return data;
      });

    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//is Certificate Retired with logic certificate contract
exports.isRetired = async (req, res) => {
  var sender = req.query._sender; //Address

  var certificateId = req.params.certificateId;

  try {
    const result = await myContractCertificateLogic.methods.isRetired(certificateId).call({ from: sender });
    var reponse = JSON.stringify(result);

    const results = await Promise.resolve(reponse);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//retire Certificate with logic certificate contract
exports.retireCertificate = async (req, res) => {
  var sender = req.query._sender; //Address
  var certificateId = req.params.certificateId;

  try {
    var openDoorDataPart = myContractCertificateLogic.methods.retireCertificate(certificateId).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

    const results = await Promise.resolve(ret);

    let certificateToUpdateDb = await fetch(process.env.API_URL + "/certificates-db/" + certificateId, {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log("Certificate N°" + certificateId + " retrieved from DB for retireCertificate update");
        return data;
      });

    certificateToUpdateDb = { ...certificateToUpdateDb, _retired: true };

    const certificateDbResult = await fetch(process.env.API_URL + "/certificates-db/" + certificateId, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(certificateToUpdateDb)
    })
      .then(handleErrors)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        console.log("Certificate " + certificateId + " _retire updated in DB");
        return data;
      });

    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

//retire Certificate with logic certificate contract
exports.retireCertificatesForDay = async (req, res) => {
  var sender = req.query._sender; //Address

  var date = req.body.date;

  if (!date) {
    res.status(400).send({
      error: "Empty date parameter"
    });
    res.end();
  } else if (!moment(date, "YYYY-MM-DD").isValid()) {
    res.status(400).send({
      error: "Bad date format (YYYY-MM-DD)"
    });
    res.end();
  } else {
    const certificatesToRetire = await fetch(process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
      .then(handleErrors)
      .then(async res => res.json())
      .then(async certificates => {
        return certificates;
      })
      .catch(error => {
        console.log(error);
      });

    var results = await Promise.resolve(certificatesToRetire);
    res.json(results);

    /*
      for (let certificate of certificatesToRetire) {

        app.put('/certificates/:certificateId/retired', certificatesService.retireCertificate);

        await fetch(process.env.API_URL + "/certificates/"++"/retired?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
      		method: 'PUT',
      		headers: {
      	  	'Content-Type': 'application/json; charset=utf-8'
      		}
      	})
      	.then(handleErrors)
      	.then(async res => res.json())
      	.then(async certificates => {
      	  return certificates;
      	})
      	.catch(error => {
      	  console.log(error);
      	});
      }*/
  }
};

//transfer Ownership By Escrow with logic certificate contract
exports.transferOwnershipByEscrow = async (req, res) => {
  var sender = req.query._sender; //Address

  var certificateId = req.params.certificateId; //uint
  var newOwner = req.body._newOwner; //address

  try {
    var openDoorDataPart = myContractCertificateLogic.methods.transferOwnershipByEscrow(certificateId, newOwner).encodeABI();

    const nonce = await web3.eth.getTransactionCount(sender);

    var rawTx = {
      nonce: web3.utils.toHex(nonce),
      value: 0x00,
      gasPrice: web3.utils.toHex(4000000000),
      gasLimit: web3.utils.toHex(1000000),
      data: openDoorDataPart,
      to: addressCertificateLogic,
      from: sender
    };

    var tx = new Tx(rawTx);
    var privateKey = require("../services/tobalaba/privateKey").privateKeyByAddress(sender);
    await tx.sign(privateKey);
    const serializedTx = await tx.serialize();
    const ret = await web3.eth.sendSignedTransaction("0x" + serializedTx.toString("hex"));

    const results = await Promise.resolve(ret);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

exports.checkCertificateFile = async (req, res) => {
  console.log("starting uploadsPdfParser...");
  try {
    //fetch Data from server
    var fetchCertificate = await fetch(process.env.API_URL + "/certificates?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747&_owner=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
    }).then(
      function(response) {
        return response.text();
      },
      function(error) {
        console.log("error: " + error);
        error.message; //=> String
      }
    );

    fetchCertificate = JSON.parse(fetchCertificate);

    console.log("req.files ", req.files);
    console.log("before req.files.files.mimetype " + req.files.files.mimetype);
    if (req.files && req.files.files.mimetype == "application/pdf") {
      var file = req.files.files,
        name = file.name,
        type = file.mimetype;
      var uploadpath = __dirname + "/../uploads/" + name;
      console.log("before mv ");
      file.mv(uploadpath, function(err) {
        if (err) {
          res.send("Error Occured!");
          res.end();
        } else {
          console.log("before pdf2json ");
          pdfParser.pdf2json(uploadpath, function(error, pdf) {
            fs.unlink(uploadpath, function(err) {
              if (err) throw err;
              // if no error, file has been deleted successfully
            });

            if (error != null) {
              res.status(404).send({
                result: false
              });
              res.end();
            } else {
              try {
                console.log("parsing PDF...");
                dataCertificate = {
                  powerInW: null,
                  coSaved: null,
                  assetId: null,
                  productionDate: null,
                  txHash: null
                };

                asset = pdf.pages[0].texts[4].text.split(" ");
                getLength = asset.length;
                dataCertificate.assetId = asset[getLength - 2] + " " + asset[getLength - 1];

                console.log("parsed coordinates : " + dataCertificate.assetId);

                //TODO : get the assets coordinates from the blockchain and compare each coordinnates with the document

                if (dataCertificate.assetId == "(45,391546N, 2,364349E)") {
                  dataCertificate.assetId = 0;
                }
                if (dataCertificate.assetId == "(43,761234N, 2,902555E)") {
                  dataCertificate.assetId = 1;
                }
                if (dataCertificate.assetId == "(42,8331477N, 0,1998515000000225E)") {
                  dataCertificate.assetId = 2;
                }

                dataCertificate.powerInW = pdf.pages[0].texts[2].text;
                if (dataCertificate.powerInW.split(" ")[0]) {
                  dataCertificate.powerInW = parseFloat(dataCertificate.powerInW.split(" ")[0].replace(",", ".")) * 1000;
                }

                if (
                  pdf.pages[0].texts[2].text.split(" ")[4].split("(")[1] &&
                  !isNaN(
                    parseFloat(
                      pdf.pages[0].texts[2].text
                        .split(" ")[4]
                        .split("(")[1]
                        .replace(",", ".")
                    )
                  )
                ) {
                  //Manage older certificate with "Watt Hour" instead of "Watt-Hour"
                  dataCertificate.coSaved = parseFloat(
                    pdf.pages[0].texts[2].text
                      .split(" ")[4]
                      .split("(")[1]
                      .replace(",", ".")
                  );
                } else if (
                  pdf.pages[0].texts[2].text.split(" ")[3].split("(")[1] &&
                  !isNaN(
                    parseFloat(
                      pdf.pages[0].texts[2].text
                        .split(" ")[3]
                        .split("(")[1]
                        .replace(",", ".")
                    )
                  )
                ) {
                  dataCertificate.coSaved = parseFloat(
                    pdf.pages[0].texts[2].text
                      .split(" ")[3]
                      .split("(")[1]
                      .replace(",", ".")
                  );
                } else {
                  dataCertificate.coSaved = "undefined";
                }

                var tps = pdf.pages[0].texts[7].text.split(" ");
                var time = tps[0] + "T" + tps[1];
                var productionDateTimeStamp = new Date(time).getTime();
                dataCertificate.productionDate = productionDateTimeStamp / 1000;
                dataCertificate.txHash = pdf.pages[0].texts[13].text.split("/")[4];

                console.log("Parsed powerInW", dataCertificate.powerInW);
                console.log("Parsed assetId", dataCertificate.assetId);
                console.log("Parsed productionDate", dataCertificate.productionDate);
                console.log("Parsed txHash", dataCertificate.txHash);

                if (dataCertificate.powerInW == null || dataCertificate.assetId == null || dataCertificate.productionDate == null || dataCertificate.txHash == null) {
                  res.status(403).send({ result: false });
                  res.end();
                }

                const realCertificate = fetchCertificate.find(certificate => {
                  return (
                    dataCertificate.powerInW === parseInt(parseInt(certificate._powerInW) / 1000) &&
                    //dataCertificate.assetId === parseInt(certificate._assetId) &&
                    //dataCertificate.productionDate === parseInt(certificate._productionDate) &&
                    dataCertificate.txHash === certificate._txHash
                  );
                });

                if (realCertificate !== undefined) {
                  const fullCertificate = { ...realCertificate, _coSaved: dataCertificate.coSaved };

                  res.status(201).send({
                    dataCertificate: fullCertificate,
                    result: true
                  });
                } else {
                  res.status(201).send({
                    result: false
                  });
                }

                //This part is used to verify the hash of certificate with smart contract
                /* myContractCertificateLogic.methods.verifydHash(
                    dataCertificate.powerInW,
                    dataCertificate.assetId,
                    dataCertificate.productionDate,
                    dataCertificate.txHash
                  ).call({ from:'0x002cbE31Ed26DA610f742f1EE2B957229e57a8e4' }).then(function(result){
                    if(result == true){
                      dataCertificate.productionDate=dataCertificate.productionDate*1000;
                      res.status(201).send({
                        dataCertificate:dataCertificate,
                        result:result
                      });
                      res.end();
                    }else{
                      //console.log("here");
                      res.status(403).send({
                        result:result
                      });
                      res.end();
                    }
                  }); */
              } catch (error) {
                console.log(error);
                res.status(403).send({
                  result: false
                });
                res.end();
              }
            }
          });
        }
      });
    } else {
      console.log("Bad files or mimetype", req.files);
      res.status(403).send({
        result: false
      });
      res.end();
    }
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};

exports.getSmartContractVersion = async (req, res) => {
  var sender = req.query._sender; //Address

  try {
    //Put the CertificatesLogic's address
    var version = await myContractCertificateLogic.methods.getSmartContractVersion().call({ from: "0x002cbE31Ed26DA610f742f1EE2B957229e57a8e4" });

    const results = await Promise.resolve(version);
    res.json(results);
  } catch (error) {
    console.error(error);
    res.status(400).send({
      message: error.message
    });
    res.end();
  }
};
