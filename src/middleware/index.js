// eslint-disable-next-line no-unused-vars
const upload = require('express-fileupload');
module.exports = function (app) {
  // Add your custom middleware here. Remember, that
  // in Express the order matters
  app.use(upload()); // configure middleware
};
