const express = require('express')
const bodyParser = require("body-parser");
const upload = require('express-fileupload');
const routes = require("./routes/routes.js");
const dbService = require("./routes/dbService.js");
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const logger = require('winston');
const fetch = require('node-fetch');
const cron = require('node-cron');
const moment = require('moment');
const exec = require('child_process').exec;
const path = require('path');
const fs = require('fs');
const https = require('https');
const http = require('http');

const httpApp = express();
const httpsApp = express();

global.Headers = fetch.Headers;

// Désactiver les cron en production
if(process.env.NODE_ENV != "production") {
  require("./crons.js");
}

// -- BEGIN ENV VAR --
let mongoDbProdPath = 'mongodb://teoDbAdmin:teoDb1234ENGIE@ds253343-a0.mlab.com:53343,ds253343-a1.mlab.com:53343/weechain_prod?replicaSet=rs-ds253343';
let mongoDbTestPath = 'mongodb://teoDbTestAdmin:teoTestDb1234ENGIE@ds157064.mlab.com:57064/weechain_test'

process.env.AUTH_CLIENT_ID = process.env.AUTH_CLIENT_ID || 'ZoZlt77cSxL4XjMsz2y6o6UuwGf7Yjs6';
process.env.AUTH_CLIENT_SECRET = process.env.AUTH_CLIENT_SECRET || 'KHC37YnJBuxNdbTl9kjiIy7pj9FL4h12zuHoR-TlSMwRAicdWMnQCEXCQ2dd1tti';
process.env.AUTH_URL = process.env.AUTH_URL || 'https://weechain.eu.auth0.com';
process.env.BUILD_FOLDER  = process.env.BUILD_FOLDER || 'build';
process.env.DB_PATH = process.env.DB_PATH || 'mongodb://teoDbAdmin:teoDb1234ENGIE@ds253343-a0.mlab.com:53343,ds253343-a1.mlab.com:53343/weechain_prod?replicaSet=rs-ds253343';

const HTTP_PORT = process.env.HTTP_PORT || 5000;

process.env.API_URL = process.env.API_URL || "http://localhost:" + HTTP_PORT;

const HTTPS_PORT = process.env.HTTPS_PORT || 5443;
const PRIVATE_KEY_PATH = process.env.PRIVATE_KEY_PATH;
const SSL_CERT_PATH = process.env.SSL_CERT_PATH;

const ACTIVATE_HTTPS = process.env.ACTIVATE_HTTPS !== undefined ? process.env.ACTIVATE_HTTPS : true;
// -- END ENV VAR --

function setupExpressApp(_app) {
  _app.use(cors());
  _app.use(helmet());
  _app.use(compress());
  _app.use(bodyParser.json());
  _app.use(bodyParser.urlencoded({ extended: true }));
  _app.use('/static', express.static(__dirname + '/uploads'));
  _app.use(upload());

  routes(_app);
}

// -- BEGIN SERVER BOOT UP --
if(ACTIVATE_HTTPS == true || ACTIVATE_HTTPS == 'true' || ACTIVATE_HTTPS == 1 || ACTIVATE_HTTPS == '1') {
  // ---- HTTPS ----
  if(!fs.existsSync(PRIVATE_KEY_PATH)) throw new Error(`Cannot find SSL private key at path : ${PRIVATE_KEY_PATH}`);
  if(!fs.existsSync(SSL_CERT_PATH)) throw new Error(`Cannot find SSL certificate at path : ${SSL_CERT_PATH}`);

  const options = {
    key: fs.readFileSync(PRIVATE_KEY_PATH),
    cert: fs.readFileSync(SSL_CERT_PATH),
  };

  setupExpressApp(httpsApp);

  let httpsServer = https.createServer(options, httpsApp);

  httpsServer.listen(HTTPS_PORT, () => {
    console.log(`TEO core API listening on HTTPS : ${ HTTPS_PORT }`);
  });

  // ---- HTTP REDIRECT ONLY ----
  httpApp.use(function(req, res, next) {  
    res.redirect('https://' + req.headers.host + req.url);
  });

  let httpServer = http.createServer(httpApp);

  httpServer.listen(HTTP_PORT, () => {
    console.log(`HTTP server listens and redirects all to HTTPS, launched on HTTP port : ${ HTTP_PORT }`);
  });
} else {
  // ---- HTTP ONLY ----
  setupExpressApp(httpApp);

  let httpServer = http.createServer(httpApp);

  httpServer.listen(HTTP_PORT, () => {
    console.log(`TEO core API - HTTP server only, launched on HTTP port : ${ HTTP_PORT }`);
  });
}
// -- END SERVER BOOT UP --
