const express = require('express')
const bodyParser = require("body-parser");
const upload = require('express-fileupload');
const routes = require("./routes/routes.js");
const dbService = require("./routes/dbService.js");
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const logger = require('winston');
const fetch = require('node-fetch');
const cron = require('node-cron');
const moment = require('moment');
const exec = require('child_process').exec;
const path = require('path');
const fs = require('fs');

const getAssetsProducingLength = async () => {

  let url = process.env.API_URL + '/producing-assets-list-length?_sender=0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  return await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(handleErrors)
    .then(async res => res.json())
    .then(async assetProducingListLength => {
      return assetProducingListLength;
    })
    .catch(error => {
      console.log('An error occured during getAssetsProducingLength : ', error);
      return false;
    });
}

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

const monitorParity = async () =>  {
	//check if the blockchain api respond
	console.log('Start checking Parity');
	assetProducingNb = await getAssetsProducingLength();
	console.log('assetProducingNb', assetProducingNb);
	if(!(assetProducingNb && assetProducingNb > 0)) {
		//the api is not responding correctly,
		console.log('Parity is not responding correctly, restarting...');
		var killAndRestartParity = exec('sh /home/weechain/www/api/kill_restart_parity.sh',
        (error, stdout, stderr) => {
            console.log(`${stdout}`);
            console.log(`${stderr}`);
            if (error !== null) {
                console.log(`exec error: ${error}`);
            }
        });
	} else {
		//Parity is up
		console.log('Parity is ok')
	}
}

//Monitoring Parity job
cron.schedule('*/2 * * * *', async function(){
  console.log('monitoring parity (every 2min)');
	monitorParity();

});

const updateAssetGraphCache = async () =>  {
	//check if the blockchain api respond
	console.log('Start caching consuming assets graph data...');

  let urlConsumingAssets = process.env.API_URL + '/consuming-assets-offchain-db';

  let consumingAssets = await fetch(urlConsumingAssets, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });

  //console.log('Get consumingAssets result', consumingAssets);

	for (let cA of consumingAssets) {
    console.log('************************ Caching data for asset '+ cA.smireName +' ************************')
    //console.log(cA)
    //console.log('************************')

    const defaultMonthStart = moment().subtract(30, 'days').format('YYYY-MM-DD');
    const defaultMonthEnd = moment().format('YYYY-MM-DD');

    let urlMonth = process.env.API_URL+'/consuming-assets-offchain/'+cA.id+'/graph-data-db?';
    urlMonth += '&start='+defaultMonthStart;
    urlMonth += '&end='+defaultMonthEnd;

    let currentYear = moment().format('YYYY')
    let defaultYtdStart = moment(currentYear+"-01-01", "YYYY-MM-DD").format('YYYY-MM-DD');
    let defaultYtdEnd = moment().format('YYYY-MM-DD');

    let urlYtd = process.env.API_URL+'/consuming-assets-offchain/'+cA.id+'/graph-data-db?';
    urlYtd += '&start='+defaultYtdStart;
    urlYtd += '&end='+defaultYtdEnd;

    let resultMonth = await fetch(urlMonth, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(handleErrors).then(res => res.json()) .then(data => { return data })
    .catch(error => { console.log(error); });


    let resultYtd = await fetch(urlYtd, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(handleErrors).then(res => res.json()) .then(data => { return data })
    .catch(error => { console.log(error); });

    /*
    console.log('resultYtd')
    console.log(resultYtd)
    console.log('resultMonth')
    console.log(resultMonth)
    */

    let consumingAssetsAggregationDataMonth = {
      assetId: cA.id,
      range: "month",
      data: resultMonth.data,
    }
    let consumingAssetsAggregationDataYtd = {
      assetId: cA.id,
      range: "ytd",
      data: resultYtd.data,
    }

    const existingConsumingAssetsAggregationMonth = await dbService.ConsumingAssetsAggregation.find({
        assetId: consumingAssetsAggregationDataMonth.assetId,
        range: consumingAssetsAggregationDataMonth.range
    }, async function (err) {
      if (err) {
        console.log("Error : ", error);
        return false;
      }
    });

    const existingConsumingAssetsAggregationYtd = await dbService.ConsumingAssetsAggregation.find({
        assetId: consumingAssetsAggregationDataYtd.assetId,
        range: consumingAssetsAggregationDataYtd.range
    }, async function (err) {
      if (err) {
        console.log("Error : ", err);
        return false;
      }
    });

    if(existingConsumingAssetsAggregationMonth.length === 0) {
      //Then we create a new one

      const consumingAssetsAggregationMonth = new dbService.ConsumingAssetsAggregation(consumingAssetsAggregationDataMonth);
      console.log("Creating a new month ConsumingAssetsAggregation object in the DB for asset "+cA.smireName)
      await consumingAssetsAggregationMonth.save(function (err) {
        if (err) {
          console.log("An error occured saving 30 days aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation 30 days data saved correctly for asset "+cA.smireName)
        }

      });
    }

    if(existingConsumingAssetsAggregationMonth.length === 1) {
      //Then we update the one we find
      existingConsumingAssetsAggregationMonth[0].data = resultMonth.data;

      console.log("Updating a new month ConsumingAssetsAggregation object in the DB for asset "+cA.smireName)

      await existingConsumingAssetsAggregationMonth[0].save(function (err) {
        if (err) {
          console.log("An error occured saving 30 days aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation 30 days data saved correctly for asset "+cA.smireName)
        }

      });

    }

    if(existingConsumingAssetsAggregationMonth.length > 1) {
      //Then we delete all and create a new one
      console.log("Deleting month ConsumingAssetsAggregation objects in the DB for asset "+cA.smireName)
      await dbService.ConsumingAssetsAggregation.deleteMany({
          assetId: consumingAssetsAggregationDataMonth.assetId,
          range: consumingAssetsAggregationDataMonth.range
      }, async function (err) {
        if (err) {
          console.log("An error occured deleting 30 days aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation 30 days data correctly deleted for asset "+cA.smireName)
        }
      });
      const consumingAssetsAggregationMonth = new dbService.ConsumingAssetsAggregation(consumingAssetsAggregationDataMonth);
      console.log("Creating a new month ConsumingAssetsAggregation object (after delete) in the DB for asset "+cA.smireName)
      await consumingAssetsAggregationMonth.save(function (err) {
        if (err) {
          console.log("An error occured saving 30 days aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation 30 days data saved correctly for asset "+cA.smireName)
        }

      });

    }


    if(existingConsumingAssetsAggregationYtd.length === 0) {
      //Then we create a new one

      const consumingAssetsAggregationYtd = new dbService.ConsumingAssetsAggregation(consumingAssetsAggregationDataYtd);
      console.log("Creating a new year to date ConsumingAssetsAggregation object in the DB for asset "+cA.smireName)
      await consumingAssetsAggregationYtd.save(function (err) {
        if (err) {
          console.log("An error occured saving year to date aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation year to date data saved correctly for asset "+cA.smireName)
        }

      });
    }

    if(existingConsumingAssetsAggregationYtd.length === 1) {
      //Then we update the one we find
      existingConsumingAssetsAggregationYtd[0].data = resultYtd.data;

      console.log("Updating a new year to date ConsumingAssetsAggregation object in the DB for asset "+cA.smireName)

      await existingConsumingAssetsAggregationYtd[0].save(function (err) {
        if (err) {
          console.log("An error occured saving year to date aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation year to date data saved correctly for asset "+cA.smireName)
        }

      });

    }

    if(existingConsumingAssetsAggregationYtd.length > 1) {
      //Then we delete all and create a new one
      console.log("Deleting  year to date ConsumingAssetsAggregation objects in the DB for asset "+cA.smireName)
      await dbService.ConsumingAssetsAggregation.deleteMany({
          assetId: consumingAssetsAggregationDataYtd.assetId,
          range: consumingAssetsAggregationDataYtd.range
      }, async function (err) {
        if (err) {
          console.log("An error occured deleting year to date aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation year to date data correctly deleted for asset "+cA.smireName)
        }
      });
      const consumingAssetsAggregationYtd = new dbService.ConsumingAssetsAggregation(consumingAssetsAggregationDataYtd);
      console.log("Creating a new year to date ConsumingAssetsAggregation object (after delete) in the DB for asset "+cA.smireName)
      await consumingAssetsAggregationYtd.save(function (err) {
        if (err) {
          console.log("An error occured saving year to date aggregation data for asset "+cA.smireName)
          console.log(err)
        }
        else {
          console.log("Aggregation year to date data saved correctly for asset "+cA.smireName)
        }

      });

    }
  }
}

//Cache the data related to the consuming asset chart
cron.schedule('*/2 * * * *', async function(){
  //console.log('updating cached data (every 2min)');
  //await updateAssetGraphCache()
});

//Updating db data daily
/*cron.schedule('10 10 * * *', async function(){
  console.log('Daily database update at 09h50 Paris time...');
  let urlUpdateDb = process.env.API_URL + '/update-db?save=true';

  await fetch(urlUpdateDb, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  console.log('update DB daily finished');
});
*/

cron.schedule('30 10 * * *', async function(){
  console.log('Checking balance account...');
  const sender = '0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747';

  //Here, we have the coo owner and the smart meter addresses of the production assets

  const accountsAddresses = [
    "0x0029ab4DFCEf587Bb80e8b66d89aE44F4827F747", //coo owner
    "0x004A064a9AAf2fCD9687D82Fc5a1116e9325000A", //Eget
    "0x00c3B44DBeA6f103dd30D7e90D59787175FeA185", //Fontanelles
    "0x00CB17ea4DfA8578582C00A79Dd92d6111c28c54"  //Marèges
  ];

  for (let address of accountsAddresses) {
    let urlCheckBalance = process.env.API_URL + '/accounts/'+address+'/balance?_sender='+sender;
    let accountBalance = await fetch(urlCheckBalance, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })
    .then(handleErrors).then(res => res.json()) .then(data => { return data })
    .catch(error => { console.log(error); });

    console.log(address+" has "+accountBalance.balance+" ethers")

    if(accountBalance.balance < 20) {
      console.log(address+" needs more ether...")
      const dataBalance = {
        address: address,
        email: "test@test.com",
        firstname: "",
        lastname: "",
        organisation: "",
        timestamp: (Math.round(new Date().getTime() / 1000))
      };

      const jsonBalance = JSON.stringify(dataBalance);

      await fetch('http://185.183.159.109:8888/api/getMoney', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: jsonBalance
      })
      .then(handleErrors).then(res => {console.log(res);res.json()}) .then(data => { console.log(data);console.log(address+" received 20 ethers");return data })
      .catch(error => { console.log(error); });
    }
  }
});

//Updating cache data job
cron.schedule('*/5 * * * *', async function(){
  /*console.log('updateCo2 for cache (every 20min)');
  //updateCo2Data();
  let urlUpdateCo2 = process.env.API_URL + '/update-co2';

  await fetch(urlUpdateCo2, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  console.log('updateCo2 finished');


  */

  //updateCacheData();
  /*
  console.log('updateCacheData for cache (every 20min)');
  let urlUpdateCacheData = process.env.API_URL + '/update-cache';
  await fetch(urlUpdateCacheData, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });
  console.log('updateCacheData finished');*/

});

// ---- Update Darwin DB
async function updateDarwin() {
  console.log('Update Darwin Metering Asset every hour');

  let URL = process.env.API_URL + '/update-db-darwin';

  console.log(URL)

  try {
    let response = await fetch(URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    })

    let data = await response.json();

    console.log(data);

  } catch (error) {
    console.log("Error during : Update Darwin Metering Asset")
    console.log(error);
  }

  console.log('Update Darwin Metering Asset finished');
}

cron.schedule('5 * * * *', async function(){
  await updateDarwin();
});

// ---- Update Crigen DB
async function updateCrigen(specifiedMoment) {
  console.log('Update Crigen Metering Asset');

  let URL = process.env.API_URL + '/update-db-crigen';

  let body = {
    dayTimestampInMs: moment().valueOf()
  }

  if(specifiedMoment) {
    body.dayTimestampInMs = specifiedMoment.valueOf()
  }

  try {
    let response = await fetch(URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },
      body: JSON.stringify(body)
    })

    let data = await response.json();
  } catch (error) {
    console.log("Error during : Update Crigen Metering Asset")
    console.log(error);
  }

  console.log('Update Crigen Metering Asset finished');
}

// Run Crigen update each day at 01:00
cron.schedule('0 1 * * *', async function(){
  await updateCrigen();
});

//Matching job
/*
cron.schedule('0 10 * * *', async function(){
	console.log('Running matching task every day at 11AM Paris time');

  let matchingDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
  //matchingDate = '2018-10-27';

  const data = {
    nbRetry: 2,
    date: matchingDate,
    generateCertificates: true
  };

  const json = JSON.stringify(data);

  let urlMatching = process.env.API_URL + '/certificates-management/matching';
  await fetch(urlMatching, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    body: json
  })
  .then(handleErrors).then(res => res.json()) .then(data => { return data })
  .catch(error => { console.log(error); });

  console.log('Matching task finished for day '+matchingDate);

});
*/