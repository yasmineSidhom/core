var knex=require('./connection.js');
module.exports=function()
{
  
  knex.schema.createTable('demand', function (table) {
    table.increments('demandId');
    table.integer('demandMask');
    table.integer('created');
    table.boolean('enabled');
  }).then(function() {
    console.log("demand table created");
  }).catch(function(error) {
    console.error(error);
    console.log("demand table has already created");
  });
  
  knex.schema.createTable('demandGeneralInfo', function (table) {
    table.increments('demandId');
    table.string('originator');
    table.string('buyer');
    table.integer('startTime');
    table.integer('endTime');
    table.integer('timeframe');
    table.integer('pricePerCertifiedKWh');
    table.integer('currency');
  
  }).then(function() {
    console.log("demandGeneralInfo table created");
  }).catch(function(error) {
    console.error(error);
    console.log("demandGeneralInfo table has already created");
  });
  
  knex.schema.createTable('demandCoupling', function (table) {
    table.increments('demandId');
    table.integer('producingAssets');
    table.integer('consumingAssets');
  }).then(function() {
    console.log("demandCoupling table created");
  }).catch(function(error) {
    console.error(error);
    console.log("demandCoupling table has already created");
  });
  
  knex.schema.createTable('demandPriceDriving', function (table) {
    table.increments('demandId');
    table.integer('registryCompliance');
    table.integer('assetType');
    table.integer('minCO2Offset');
    table.string('otherGreenAttributes');
    table.string('typeOfPublicSupport');
    table.boolean('isInitialized');
  }).then(function() {
    console.log("demandPriceDriving table created");
  }).catch(function(error) {
    console.error(error);
    console.log("demandPriceDriving table has already created");
  });
  
  knex.schema.createTable('demandLocation', function (table) {
    table.increments('demandId');
    table.string('country');
    table.string('region');
    table.string('city');
    table.string('street');
    table.string('zip');
    table.string('houseNumber');
    table.string('gpsLatitude');
    table.string('gpsLongitude');
    table.boolean('exists');
  }).then(function() {
    console.log("demandLocation table created");
  }).catch(function(error) {
    console.error(error);
    console.log("demandLocation table has already created");
  });
  
  knex.schema.createTable('demandMatcherProperties', function (table) {
    table.increments('demandId');
    table.integer('targetWhPerperiod');
    table.integer('currentWhPerperiod');
    table.integer('certInCurrentperiod');
    table.integer('productionLastSetInPeriod');
    table.string('matcher');
  }).then(function() {
    console.log("demandMatcherProperties table created");
  }).catch(function(error) {
    console.error(error);
    console.log("demandMatcherProperties table has already created");
  });
};
