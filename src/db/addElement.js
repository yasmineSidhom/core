var knex=require('./connection');

exports.demand = function(_demandMask,_created,_enabled) {
  return (knex('demand').insert({
    demandMask: _demandMask, //int
    created: _created,  //int
    enabled: _enabled}) //bool
    .then( function (result) {
      console.log(result); 
      return result;   
    }));
};
  
exports.demandGeneralInfo = function(_originator,_buyer,_startTime,_endTime,_timeframe,_pricePerCertifiedKWh,_currency) {
  return(knex('demandGeneralInfo').insert({
    originator: _originator, //address
    buyer: _buyer,           //address
    startTime: _startTime, //int
    endTime:_endTime,      //int
    timeframe: _timeframe,  //int
    pricePerCertifiedKWh:_pricePerCertifiedKWh,  //int
    currency:_currency})    //int
    .then( function (result) {
      console.log(result);  
      return result;  
    }));
};  
 
exports.demandCoupling = function(_producingAssets,_consumingAssets) {
  return(knex('demandCoupling').insert({
    producingAssets:_producingAssets, //int
    consumingAssets:_consumingAssets})  //int
    .then( function (result) {
      console.log(result);
      return result;    
    }));
};

exports.demandPriceDriving = function(_registryCompliance,_assettype,_minCO2Offset,_otherGreenAttributes,_typeOfPublicSupport,_isInitialized) {
  return(knex('demandPriceDriving').insert({
    registryCompliance: _registryCompliance, //int
    assettype: _assettype, //int
    minCO2Offset: _minCO2Offset,//int
    otherGreenAttributes:_otherGreenAttributes,//bytes32
    typeOfPublicSupport:_typeOfPublicSupport, //bytes32
    isInitialized:_isInitialized})   //bool
    .then( function (result) {
      console.log(result);
      return result;    
    }));
};

exports.demandLocation = function(_country,_region,_city,_street,_zip,_houseNumber,_gpsLatitude,_gpsLongitude,_exists) {
  return(knex('demandLocation').insert({
    country: _country, //bytes32
    region: _region, //bytes32
    city: _city,//bytes32
    street:_street,//bytes32
    zip: _zip,//bytes32
    houseNumber:_houseNumber,//bytes32
    gpsLatitude:_gpsLatitude,//bytes32
    gpsLongitude:_gpsLongitude,//bytes32
    exists:_exists}) //bool
    .then( function (result) {
      console.log(result); 
      return result;  
    }));
}; 
  
exports.demandMatcherProperties = function(_targetWhPerperiod,_currentWhPerperiod,_certInCurrentperiod,_productionLastSetInPeriod,_matcher) {
  return(knex('demandMatcherProperties').insert({
    targetWhPerperiod: _targetWhPerperiod, //int
    currentWhPerperiod: _currentWhPerperiod, //int
    certInCurrentperiod: _certInCurrentperiod,//int
    productionLastSetInPeriod:_productionLastSetInPeriod,//int
    matcher: _matcher,}) //address
    .then( function (result) {
      console.log(result); 
      return result;  
    }));
};
