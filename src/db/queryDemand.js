var knex=require('./connection.js');

// Get all information for demandId
exports.infodemand=function(id){
  return (knex('demand')
    .join('demandGeneralInfo', 'demand.demandId', '=', 'demandGeneralInfo.demandId')
    .join('demandCoupling', 'demand.demandId', '=', 'demandCoupling.demandId')
    .join('demandPriceDriving', 'demand.demandId', '=', 'demandPriceDriving.demandId')
    .join('demandLocation', 'demand.demandId', '=', 'demandLocation.demandId')
    .join('demandMatcherProperties', 'demand.demandId', '=', 'demandMatcherProperties.demandId')
    .whereRaw('demand.demandId = ?', [id])
    .select('*'));
};

