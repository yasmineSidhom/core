// Test.js
var mongoose = require('mongoose');

var TestSchema = new mongoose.Schema({
  name: String,
});
mongoose.model('Test', TestSchema);
module.exports = mongoose.model('Test');
