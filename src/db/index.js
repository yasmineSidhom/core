var mongoose = require('mongoose');
mongoose.connect(process.env.DB_PATH);

mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//Define a schema
var Schema = mongoose.Schema;

var ConsumptionSchema = new Schema({
    asset: String,
    consumption: Number,
    date: Date
});

// Compile model from schema
const ConsumptionModel = mongoose.model('ConsumptionModel', ConsumptionSchema );

exports.ConsumptionModel = ConsumptionModel;

// Create an instance of model SomeModel
var consumption = new ConsumptionModel({
    asset: "lida_rte",
    consumption: 12345,
    date: Date.now
});

// Save the new model instance, passing a callback
consumption.save(function (err) {
  if (err) return console.error(err);

  console.log("saved!")
});
