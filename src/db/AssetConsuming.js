// Asset.js
var mongoose = require('mongoose');
var AssetProducingItemSchema = new mongoose.Schema(
  {
    id: Number,
    priority: Number,
    percentage: { type: Number, min: 0, max: 100 }
  }
);

var AssetConsumingSchema = new mongoose.Schema({
  id: Number,
  ownerId: Number,
  smireName: String,
  name: String,
  consumptionPercentage: { type: Number, min: 0, max: 1 },
  producingAssets: [AssetProducingItemSchema],
  smartMeter: String,
  owner: String,
  operationalSince: Number,
  capacity: Number,
  maxGooContract: Number,
  maxCapacitySet: Boolean,
  lastSmartMeterReadWh: Number,
  certificatesUsedForWh: Number,
  active: Boolean,
  lastSmartMeterReadFileHash: String,
  country: String,
  region: String,
  zip: String,
  city: String,
  street: String,
  houseNumber: String,
  gpsLatitude: String,
  gpsLongitude: String,
  avoidedCo2: Number
});
mongoose.model('AssetConsuming', AssetConsumingSchema);
module.exports = mongoose.model('AssetConsuming');
