/*** YS - Added the following ***/
var TEO = artifacts.require("./TEO.sol");
module.exports = function(deployer) {
  deployer.deploy(TEO);
};

var TEO_dev = artifacts.require("TEO_dev");
module.exports = function(deployer) {
  deployer.deploy(TEO_dev);
};

/*** YS - Former Contracts -- we probably won't need them ***/
// commented this part because it is producing compilation errors (key work "constant" no longer supported  => switch to "view")

// Place your contract inputs here
// var CoO = artifacts.require("CoO");

// var UserLogic = artifacts.require("UserLogic");
// var UserDB = artifacts.require("UserDB");

// var AssetProducingRegistryLogic = artifacts.require(
//   "AssetProducingRegistryLogic"
// );
// var AssetProducingRegistryDB = artifacts.require("AssetProducingRegistryDB");

// var AssetConsumingRegistryLogic = artifacts.require(
//   "AssetConsumingRegistryLogic"
// );
// var AssetConsumingRegistryDB = artifacts.require("AssetConsumingRegistryDB");

// var CertificateLogic = artifacts.require("CertificateLogic");
// var CertificateDB = artifacts.require("CertificateDB");

// //Retrieve CoO smart contract address
// /*onst fetchCoO = require('../build/contracts/CoO');
// const CoOAddress = fetchCoO.networks[401697].address;
// */

// // This is the actual migration function. All deployment is happening here
// module.exports = async (deployer, network, accounts) => {
//   //deploying CoO contract
//   var cooInstance;
//   var certificateLogicInstance;
//   var userLogicInstance;
//   var assetProducingRegistryLogicInstance;
//   var assetConsumingLogicInstance;

//   await deployer
//     .deploy(CoO)
//     .then(async () => {
//       cooInstance = await CoO.deployed();
//     })
//     .then(async () => {
//       await deployer
//         .deploy(CertificateLogic, cooInstance.address, { gas: 10000000 })
//         .then(async () => {
//           certificateLogicInstance = await CertificateLogic.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(CertificateDB, CertificateLogic.address, { gas: 10000000 })
//         .then(async () => {
//           await CertificateDB.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(UserLogic, cooInstance.address, { gas: 10000000 })
//         .then(async () => {
//           userLogicInstance = await UserLogic.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(UserDB, UserLogic.address, { gas: 10000000 })
//         .then(async () => {
//           await UserDB.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(AssetProducingRegistryLogic, cooInstance.address, {
//           gas: 10000000
//         })
//         .then(async () => {
//           assetProducingRegistryLogicInstance = await AssetProducingRegistryLogic.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(
//           AssetProducingRegistryDB,
//           assetProducingRegistryLogicInstance.address,
//           { gas: 10000000 }
//         )
//         .then(async () => {
//           await AssetProducingRegistryDB.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(AssetConsumingRegistryLogic, cooInstance.address, {
//           gas: 10000000
//         })
//         .then(async () => {
//           assetConsumingRegistryLogicInstance = await AssetConsumingRegistryLogic.deployed();
//         });
//     })
//     .then(async () => {
//       await deployer
//         .deploy(
//           AssetConsumingRegistryDB,
//           assetConsumingRegistryLogicInstance.address,
//           { gas: 10000000 }
//         )
//         .then(async () => {
//           await AssetConsumingRegistryDB.deployed();
//         });
//     })
//     .then(async () => {
//       await userLogicInstance.init(UserDB.address);
//     })
//     .then(async () => {
//       await assetProducingRegistryLogicInstance.init(
//         AssetProducingRegistryDB.address
//       );
//     })
//     .then(async () => {
//       await certificateLogicInstance.init(CertificateDB.address);
//     })
//     .then(async () => {
//       await assetConsumingRegistryLogicInstance.init(
//         AssetConsumingRegistryDB.address
//       );
//     })
//     .then(async () => {
//       await cooInstance.init(
//         UserLogic.address,
//         AssetProducingRegistryLogic.address,
//         CertificateLogic.address,
//         AssetConsumingRegistryLogic.address
//       );
//     })
//     .then(async () => {
//       await userLogicInstance.setUser(
//         accounts[0],
//         web3.fromAscii("ENGIE"),
//         web3.fromAscii("ENGIE"),
//         web3.fromAscii("ENGIE"),
//         web3.fromAscii("Boulevard Simon Bolivar"),
//         web3.fromAscii("34"),
//         web3.fromAscii("1000"),
//         web3.fromAscii("Brussels"),
//         web3.fromAscii("Belgium"),
//         web3.fromAscii("AnyState")
//       );
//     }); /*.then(async () => {
//     await userLogicInstance.setUser(accounts[1],
//       web3.fromAscii('Erwan'),
//       web3.fromAscii('RESTES'),
//       web3.fromAscii('ENGIE'),
//       web3.fromAscii('rue stévin'),
//       web3.fromAscii('69'),
//       web3.fromAscii('1000'),
//       web3.fromAscii('Brussels'),
//       web3.fromAscii('Belgium'),
//       web3.fromAscii('AnyState'));
//   }).then(async () => {
//     //accounts[1] = userAdmin
//     await userLogicInstance.setRoles(accounts[1], 2);
//   });*/

//   //For Certificate Smart Contract Migration
//   /*
//   console.log("CoOAddress :"+CoOAddress);

//   await deployer.deploy(CertificateLogic, CoOAddress, { gas: 10000000 }).then(
//     async () => { certificateLogicInstance = await CertificateLogic.deployed();
//       await deployer.deploy(CertificateDB, CertificateLogic.address, { gas: 10000000 }).then(
//         async () => { await CertificateDB.deployed();
//           await certificateLogicInstance.init(CertificateDB.address);
//         }
//       );
//     }
//   );*/
// };
