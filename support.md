#TEO Platform support


##1 Accès aux log

Dans un terminal lancer la commande

`scp -r weechain@138.68.105.64:/home/weechain/.pm2/logs/ ./`

et utiliser le mot de passe

`weechain1234ENGIE`

##2 Génération des certificats

Dans un terminal, lancer la commande suivante:

`ssh weechain@138.68.105.64`

et utiliser le mot de passe

`weechain1234ENGIE`

Ouvrir le fichier de configuration

```
cd www/api/src/
nano index.js
```

Puis à la fin du fichier, index.js, retrouver le bloc :

```
server.on('listening', () => {
  logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
  setTimeout(async function() {
    //updateCo2Data();
    //updateCacheData();
    let matchingDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    //matchingDate = '2018-10-16';
    //weechainMatching(null, matchingDate);
    //weechainDailyJob(null, matchingDate);
    //weechainMatchingV2(0, matchingDate, false);// use the third parameter to force (true) or not the generation of the certificate
  }, 5000);
}
```

La fonction `weechainMatchingV2`contient 3 paramètres à mettre à jour éventuellement : 

`(nbRetry, date, generateCertificates)`

`nbRetry` nombre de fois où on itère pour le matching afin de combler si on le peut. Dans notre cas, on ne fait qu'une seule itération, donc on ne répète pas, donc à `0`

`date` la date pour laquelle on veut faire le matching au format `YYYY-MM-DD`
Le code ici par défaut prend la veille mais on peut décommenter la ligne en dessous pour mettre à jour le paramètre `matchingDate` avec une autre date

`generateCertificates` à mettre à `false` pour tester sans générer les certificats (résultats dans les logs du serveur et le bot Slack) ou à `true` pour lancer le process complet avec génération de certificats

Pour sortir du fichier faire `Ctrl+x`
puis `Y` pour confirmer le fait de sauvegarder
puis `Enter` pour sauvegarder avec le nom actuel de fichier

Pour redémarrer le serveur `pm2 restart api`

Pour suivre l'avancement dans les logs `pm2 logs api`

Si tout est s'est bien déroulé, retourner dans le fichier `nano index.js` , remettre à `false` le paramètre de la fonction `weechainMatchingV2` et recommenter les différentes lignes pour revenir à qqch qui ressemble à ça:

```server.on('listening', () => {
  logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
  setTimeout(async function() {
    //updateCo2Data();
    //updateCacheData();
    let matchingDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    //matchingDate = '2018-10-16';
    //weechainMatching(null, matchingDate);
    //weechainDailyJob(null, matchingDate);
    //weechainMatchingV2(0, matchingDate, false);// use the third parameter to force (true) or not the generation of the certificate
  }, 5000);
}
```

Ceci pour éviter que plus tard, un redémarrage intempestif soit fait en générant les certificats