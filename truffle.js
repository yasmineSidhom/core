// // Allows us to use ES6 in our migrations and tests.
// //require('babel-register')

module.exports = {
  deploy: [
    /*** YS - Former Contracts -- we probably won't need them ***/
    // commented this part because it is producing compilation errors (key work "constant" no longer supported  => switch to "view")
    //     "AssetConsumingRegistryDB.sol",
    //     "AssetConsumingRegistryLogic.sol",
    //     "AssetDbInterface.sol",
    //     "AssetGeneralDefinition.sol",
    //     "AssetLogic.sol",
    //     "AssetProducingRegistryDB.sol",
    //     "AssetProducingRegistryLogic.sol",
    //     "CertificateDB.sol",
    //     "CertificateLogic.sol",
    //     "CoO.sol",
    //     "DemandDB.sol",
    //     "DemandLogic.sol",
    //     "LocationDefinition.sol",
    //     "Migrations.sol",
    //     "Owned.sol",
    //     "RoleManagement.sol",
    //     "RolesInterface.sol",
    //     "Updatable.sol",
    //     "UserDB.sol",
    //     "UserLogic.sol",
    "TEO.sol",
    "TEO_dev.sol"
  ],
  networks: {
    development: {
      host: "127.0.0.1",
      //host:'52.232.116.178',
      port: "8545", //rpcport on geth
      //port: 8101,
      //network_id: '888', // Match any network id
      network_id: "401697",
      from: "0x00B091c0dc2AdDe00D9481D5B0a186a7005673D4" // YS - added this line (owner's address)
    }
  }
};
