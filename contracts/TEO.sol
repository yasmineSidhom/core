pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;
import "./Date_Time.sol";

/// @author TEO Team

/// @title The contract for certificate of green energy allocation
contract TEO {
    
    /// @dev Variables
    
    /// @notice instanciating DateTime contract, used to convert timestamp to human readable date
    DateTime d = new DateTime();

    address public owner;                                           //Owner of the smartcontract (ie TEO)
    mapping (address => bool) public administrators;                //Adminstrators than can issue certificates (ie TEO Clients)
    mapping (address => mapping(uint16 => mapping(uint8=>mapping(uint8=>bytes32[])))) certificates;  //certificate gathered by Adminstrators and Date (year, month and day)
    mapping (bytes32 => Certificate) certificatesDB;                //Certificates Databse reference
    mapping (bytes32=>bytes32[]) certificatesChildren;              //Tree of certificate (Parent->Children)


    struct Certificate {
        address owner;              //public adress of the administrator who issued the certificate
        bytes32 id;                 //unique id of the certificate, will be automatically generated
        bytes issuingAsset;         //id of the issuing asset
        bytes assignedConsumer;     //id of the assigned consumer 
        uint issuingDateStart;      //start of period taken into account for energy measurement
        uint issuingDateEnd;        //end of period taken into account for energy measurement
        uint issuingQuantity;       //quantity issued
        uint actualQuantity;        //quantity not yet transfered
        string issuingUnit;         //Unit
        uint creationDate;          //Creation date of the certificate (block time)
        bool cancelled;             //if the certificate has been cancelled
        bytes cancelComment;        //comment for cancellation
        bytes32 parent;             //id of the parent certificate
        bool exist;                 //flag to check unicity of id
    }

    /// @dev Events 
    
    /// @notice A certificate has been cancelled
    event CertificateCancelled(bytes32 _id, bytes _cancelComment);
    
    /// @notice A certificate has been transfered
    event CertificateTransfered(bytes32 _parentId,uint256 timestamp,bytes32 _newId,bytes _assignedConsumer, uint _transferedQuantity);
    
    /// @notice A certificate has been issued
    event NewCertificateIssued(address _administrator,bytes32  _id, uint16 year, uint8 month, uint8 day, bytes _issuingAsset,uint _issuingDateStart,uint _issuingDateEnd,uint _issuingQuantity, string  _issuingUnit);
    
    /// @notice An administrator has been updated
    event NewAdministrator(address _administrator, bool _isAdmin);


    /// @dev Constructor
    /// @notice Set the sender as owner and as administrator
    constructor() public {
        owner = msg.sender;
        administrators[msg.sender] = true;
    }


    /// @dev Function modifiers 

    /// @notice Verify that the certificate is not cancelled
    /// @param _id Id of the certificate
    modifier notCancelled(bytes32 _id) {
        require(certificatesDB[_id].cancelled == false, "The certificate is canceled");
        _;
    }

    /// @notice Verify that the sender is administrator
    modifier onlyAdministrator()  {
        require(administrators[msg.sender] == true,"Sender is not administrator");
        _;
    }

    /// @notice Verify that the sender owns the smart contract
    modifier onlyOwner()  {
        require(msg.sender==owner,"Sender is not the owner of the smart contract");
        _;
    }

    /// @notice Verify that the sender has issued the certificate
    /// @param _id Id of the certificate
    modifier onlyIssuer(bytes32  _id) {
        require(certificatesDB[_id].owner == msg.sender,"The sender is not the owner of the certificate");
        _;
    }

    /// @notice Verify that the certificate has not been transfered
    /// @param _issuingAsset issuingAsset of the certificate
    /// @param _issuingAsset issuingAsset of the certificate
    modifier verifiedAssetAndCustomer(bytes memory _issuingAsset, bytes memory _assignedConsumer) {
    require(keccak256(_issuingAsset) == keccak256(_assignedConsumer), "The certificate has already been transfered");
        _;
    }


    /// @dev Public funtions

    /// @notice Get list of id of certificates that are children of the specified one
    /// @param _id Id of the parent
    /// @return List of ids of children certificates
    function getCertificateChildren(bytes32 _id) public view returns(bytes32[] memory){
        return certificatesChildren[_id];
    }
  
    /// @notice Get list of certificate ids for an owner, based on an owner address and a date (year, month and day)
    /// @param _owner Address of the owner of the certificate  
    /// @return List of ids of certificates
    function getCertificateIds(uint16 _year, uint8 _month, uint8 _day, address _owner) public view returns(bytes32[] memory){
        return certificates[_owner][_year][_month][_day];
    }
    // function getCertificateIds(uint16 _year, uint8 _month, uint8 _day, address _owner) public view returns(uint){
    // bytes32[] storage tab = certificates[_owner][_year][_month][_day];
    // return (tab.length);
    // }


    

    /// @notice Get a certificate
    /// @param _id Id the requested certificate
    /// @return The certificate
    function getCertificate(bytes32  _id) public view 
    returns(Certificate memory) {
        return (certificatesDB[_id]);
    }

    /// @notice Get certificate values (struct members)
    /// @notice We probably won't be using this function, since we can read structs via Web3
    function getCertificateOld(bytes32  _id) public view 
    returns(address, bytes32,bytes memory,bytes memory,uint,uint,uint,uint, string memory, uint, bool, bytes memory ){
        Certificate memory certif = certificatesDB[_id];
        return (certif.owner,certif.id,certif.issuingAsset,certif.assignedConsumer,certif.issuingDateStart,certif.issuingDateEnd,certif.issuingQuantity,certif.actualQuantity,certif.issuingUnit,certif.creationDate,certif.cancelled,certif.cancelComment);
    }

    
    /// @notice Issue a new certificate
    /// @param _issuingAsset Production asset that generates the certificate
    /// @param _issuingDateStart Start of the generation period as timestamp
    /// @param _issuingDateEnd End of the generation period as timestamp
    /// @param _issuingQuantity Quantity of energy
    /// @param _issuingUnit Unit of energy (ex: "KWH")
    function issueCertificate(bytes memory  _issuingAsset,uint _issuingDateStart,uint _issuingDateEnd,uint _issuingQuantity, string memory _issuingUnit) public onlyAdministrator() returns(bytes32)
    {
        require(_issuingDateStart <= _issuingDateEnd, "DateStart is not <= to DateEnd");
        
        //Create the certificate
        Certificate memory certificate = createCertificate(_issuingAsset,_issuingAsset,_issuingDateStart,_issuingDateEnd,_issuingQuantity,_issuingUnit,0);
        
        //Retrieve human readable date from timestamp (creationate)
        uint16 year = getYear(certificate.creationDate);
        uint8 month = getMonth(certificate.creationDate);
        uint8 day = getDay(certificate.creationDate);  

        //Add the certificate to the DB and to its owner
        addCertificate(certificate,year,month,day);

        emit NewCertificateIssued(msg.sender,certificate.id,year,month,day,_issuingAsset,_issuingDateStart,_issuingDateEnd,_issuingQuantity,_issuingUnit);   
        return (certificate.id);
    }

    /// @notice Transfer a certificate: decrease actualQuantity of the certificate and create a new one for the assignedConsumer 
    /// @param _id Id of the parent certificate
    /// @param _assignedConsumer Id of the consumer
    /// @param _transferedQuantity Quantity
    function transferCertificate(bytes32 _id,bytes memory _assignedConsumer,uint _transferedQuantity) public  onlyIssuer(_id) notCancelled(_id) verifiedAssetAndCustomer(certificatesDB[_id].issuingAsset,certificatesDB[_id].assignedConsumer) {
        require(certificatesDB[_id].actualQuantity >= _transferedQuantity, "The quantity remaining in the certificate is not sufficient");
        
        
        certificatesDB[_id].actualQuantity -= _transferedQuantity;

        //Create the certificate
        Certificate memory certificate = createCertificate(certificatesDB[_id].issuingAsset,_assignedConsumer,certificatesDB[_id].issuingDateStart,certificatesDB[_id].issuingDateEnd,_transferedQuantity,certificatesDB[_id].issuingUnit,_id);
        
        //Retrieve human readable date from timestamp (creationate)
        uint16 year = getYear(certificate.creationDate);
        uint8 month = getMonth(certificate.creationDate);
        uint8 day = getDay(certificate.creationDate);
        
        //Add the certificate to the DB and to its owner to the children list
        addCertificate(certificate,year,month,day);
        
        //Add the certificate to the children list 
        certificatesChildren[_id].push(certificate.id);

        emit CertificateTransfered(_id,certificate.creationDate,certificate.id,_assignedConsumer,_transferedQuantity);   
    }

    /// @notice Cancel a certificate and its children
    /// @param _id Id of the certificate to be cancelled
    /// @param _cancelComment Comment of the cancellation
    function cancelCertificate(bytes32 _id,bytes memory _cancelComment) public onlyIssuer(_id) notCancelled(_id) {
        certificatesDB[_id].cancelled = true;
        certificatesDB[_id].cancelComment = _cancelComment;
        emit CertificateCancelled(_id,_cancelComment);

        if (certificatesDB[_id].parent==0) {

            bytes32[] memory children = certificatesChildren[_id];
            uint arrayLength = children.length;
                     
            for (uint i = 0; i < arrayLength; i++) {
                Certificate storage certif = certificatesDB[certificatesChildren[_id][i]];          

                if (!certif.cancelled) {
                    certif.cancelled = true;
                    certif.cancelComment = _cancelComment;
                    emit CertificateCancelled(certif.id,certif.cancelComment);
                }
            }
        }

        else {
            certificatesDB[certificatesDB[_id].parent].actualQuantity += certificatesDB[_id].issuingQuantity;
        }
    }

    /// @notice Add or Remove administrator of the smart contract
    /// @param _administrator The adress of administrator to be added or removed
    /// @param _isAdmin Add(true) or Remove(false) as administrator
    function setAdministrator(address _administrator, bool _isAdmin) public onlyOwner {
        administrators[_administrator] = _isAdmin;
        emit NewAdministrator(_administrator,_isAdmin);
    }


    /// @dev Private functions 

    function generateId(Certificate memory certif) private pure returns(bytes32) {
    return keccak256(abi.encode(certif.owner,certif.issuingAsset,certif.assignedConsumer,certif.issuingDateStart,certif.issuingDateEnd,certif.issuingQuantity,certif.actualQuantity,certif.issuingUnit,certif.creationDate,certif.cancelled,certif.parent,certif.cancelComment));
    }
    
    function getYear(uint timestamp) private view returns (uint16){
    return d.getYear(timestamp);
    }
    
    function getMonth(uint timestamp) private view returns (uint8){
    return  d.getMonth(timestamp);
    }
    
    function getDay(uint timestamp) private view returns (uint8){
    return d.getDay(timestamp);
    }
    
    function createCertificate(bytes memory  _issuingAsset,bytes memory _assignedConsumer,uint _issuingDateStart,uint _issuingDateEnd,uint _issuingQuantity, string memory _issuingUnit,bytes32 _parentId) private view returns (Certificate memory){
        Certificate memory certificate = Certificate({
            owner:msg.sender,
            id:0,
            issuingAsset:_issuingAsset,
            assignedConsumer:_assignedConsumer,
            issuingDateStart:_issuingDateStart,
            issuingDateEnd:_issuingDateEnd,
            issuingQuantity:_issuingQuantity,
            actualQuantity:_issuingQuantity,
            issuingUnit:_issuingUnit,
            creationDate:now,
            cancelled:false,
            cancelComment:"",
            parent: _parentId,
            exist:true
        });
        
        //Generate and assign the certificate's id 
        bytes32 _id = generateId(certificate);  
        certificate.id = _id;
        
        return certificate;
    }
    
    function addCertificate(Certificate  memory _certificate,uint16 _year,uint8 _month,uint8 _day) private {
        certificatesDB[_certificate.id] = _certificate;
        certificates[msg.sender][_year][_month][_day].push(_certificate.id);
    }

}


