module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'app',
      script: 'src/appv2.js',
      restart_delay: 10000,
      env_staging: {
        NODE_ENV: 'development',
        HTTP_PORT: 5000,
        HTTPS_PORT: 5443,
        API_URL: process.env.STAGING_API_URL,
        BUILD_FOLDER: process.env.STAGING_SMART_CONTRAT_BUILD_DIRECTORY,
        DB_PATH: process.env.STAGING_DB_URL,
        AUTH_CLIENT_ID: process.env.STAGING_AUTH_CLIENT_ID,
        AUTH_CLIENT_SECRET: process.env.STAGING_AUTH_CLIENT_SECRET,
        AUTH_URL: process.env.STAGING_AUTH_URL,
        PRIVATE_KEY_PATH: process.env.STAGING_PRIVATE_KEY_PATH,
        SSL_CERT_PATH: process.env.STAGING_SSL_CERT_PATH,
        ACTIVATE_HTTPS: process.env.STAGING_ACTIVATE_HTTPS
      },
      env_demo: {
        NODE_ENV: 'demo',
        HTTP_PORT: 5000,
        HTTPS_PORT: 5443,
        API_URL: process.env.DEMO_API_URL,
        BUILD_FOLDER: process.env.DEMO_SMART_CONTRAT_BUILD_DIRECTORY,
        DB_PATH: process.env.DEMO_DB_URL,
        AUTH_CLIENT_ID: process.env.DEMO_AUTH_CLIENT_ID,
        AUTH_CLIENT_SECRET: process.env.DEMO_AUTH_CLIENT_SECRET,
        AUTH_URL: process.env.DEMO_AUTH_URL,
        PRIVATE_KEY_PATH: process.env.DEMO_PRIVATE_KEY_PATH,
        SSL_CERT_PATH: process.env.DEMO_SSL_CERT_PATH,
        ACTIVATE_HTTPS: process.env.DEMO_ACTIVATE_HTTPS
      },
      env_production: {
        NODE_ENV: 'production',
        HTTP_PORT: 5000,
        HTTPS_PORT: 5443,
        API_URL: process.env.PROD_API_URL,
        BUILD_FOLDER: process.env.PROD_SMART_CONTRAT_BUILD_DIRECTORY,
        DB_PATH: process.env.PROD_DB_URL,
        AUTH_CLIENT_ID: process.env.PROD_AUTH_CLIENT_ID,
        AUTH_CLIENT_SECRET: process.env.PROD_AUTH_CLIENT_SECRET,
        AUTH_URL: process.env.PROD_AUTH_URL,
        PRIVATE_KEY_PATH: process.env.PROD_PRIVATE_KEY_PATH,
        SSL_CERT_PATH: process.env.PROD_SSL_CERT_PATH,
        ACTIVATE_HTTPS: process.env.PROD_ACTIVATE_HTTPS
      }
    }
  ]
}