# TEO core

## About

This headless application uses nodejs with a parity client running on the tobalaba blockchain.

## Getting Started

### Setup parity

```
bash <(curl https://get.parity.io -L) -r stable
```

Création d’un service pour lancer parity en background
sudo nano /etc/systemd/system/teo-parity.service
[Unit]
Description=Parity Ethereum client - Launched on Tobalaba for TEO
Documentation=https://wiki.parity.io
After=network.target
[Service]
Type=simple
ExecStart=/usr/bin/parity --chain /home/ubuntu/core/parity-config/tobalaba.json --reserved-peers /home/ubuntu/core/parity-config/parityTobalabaPeersPrivateNetwork.txt
Restart=on-failure

# SIGHUP gives parity time to exit cleanly before SIGKILL

KillSignal=SIGHUP
[Install]
WantedBy=multi-user.target
Alias=parity.service
Activation du service
sudo systemctl enable teo-parity.service
Lancement du service
sudo systemctl start teo-parity.service

# YS - How to make it work :

### Install software

1. Truffle framework : `npm install -g truffle`
2. Energy Web UI (parity client) 0.4.3 : [See Release History](https://github.com/energywebfoundation/energyweb-ui/releases)
3. Postman (API Development Environment): [click here](https://www.getpostman.com/downloads/)

### Pull git repo

1. Clone the repo :
   `git clone git@gitlab.com:the-energy-origin/core.git`
2. Go into core folder : type `cd core`
3. Switch to staging branch in order to pull the latest changes from there
   `git checkout staging`
4. Download the latest changes made inside the branch
   `git pull`

### Install depencencies

1. Install required packages for building npm modules

- install python 2.7.15 (globally):

  - _for mac :_

    run the following command :`brew install python@2`.

    _NOTE :_ In case you don't have Homebrew installed, open a terminal and type : `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

  - _for windows :_

    [click here ](https://www.python.org/downloads/release/python-2715/) to download python 2
    and then add python to the windows path : [see instructions here](https://stackoverflow.com/questions/3701646/how-to-add-to-the-pythonpath-in-windows)

- under core, install sha3 : `npm i sha3`
- install pm2 globally : `sudo npm i -g pm2`

2. Install npm modules :
   under core, type `npm install`

### Launch Energy Web UI

You need to have Energy Web UI launched (and a few peers)

_Note :_ if you have problems connecting to peers, please check the following [possible solutions](https://energyweb.atlassian.net/wiki/spaces/EWF/pages/530808833/Problems+Connecting+to+Peers)

### Migrate contract using truffle (one-time setup)

1. In core/truffle.js, paste your account address in the "from" section

2. Go into core and open terminal, type the following commands :

   `truffle compile`, and then

   `truffle migrate --reset`

3. On Energy WEB UI, you will be asked to sign transactions (needed to migrate the Smart Contract). To do so, provide your key file & your password
4. In core, move TEO_dev.json from core/build/contracts to core/buildDev/contracts

### Configure private key (one-time setup)

1. Go to Enery Web UI and export your account in the form of a json file

2. Add your private key to DB under weechain_test>blockchain_keys by adding a new document to the collection, containing the following fields :

- "account_name" : "INSERT_HERE_THE_NAME_OF_YOUR_ACCOUNT"
- "pwd" : "PASTE_HERE_YOUR_ACCOUNT_PASSWORD"
- "Key" : "PASTE_HERE_THE_CONTENT_OF_THE_JSON_FILE_OBTAINED_BY_EXPORTING_YOUR_ACCOUNT_ON_ENERY WEB UI"

_NOTE :_ you'll need to apply necessary changes to field "meta" (inside filed "Key", please follow the structure of the document with the account_name "YASMINE_ACCOUNT" available in DB).

### Run the server

- _for mac :_

  Go into core folder, open a terminal & run directly the following command :
  `BUILD_FOLDER='buildDev' DB='test' DB_PATH='mongodb://teoDbTestAdmin:teoTestDb1234ENGIE@ds157064.mlab.com:57064/weechain_test' node src/appv2.js`

- _for windows :_

  you must first define the follwing environment variables : BUILD_FOLDER, DB and DB_PATH.

  To do so :

  - _using cmd.exe :_

    `set BUILD_FOLDER = “buildDev”`

    `set DB = “test”`

    `set DB_PATH = “mongodb://teoDbTestAdmin:teoTestDb1234ENGIE@ds157064.mlab.com:57064/weechain_test”`

  - _using PowerShell :_

    `$env:BUILD_FOLDER= “buildDev”`

    `$env:DB= “test”`

    `$env:DB_PATH = “mongodb://teoDbTestAdmin:teoTestDb1234ENGIE@ds157064.mlab.com:57064/weechain_test”`

You can then run the server : under core, run the following command : `node src/appv2.js`

### Test routes using postman

1. Import "TEO" query collection in postman (collection available on slack : on "plateform" channel)
2. Be sure to replace value of the "sender" parameter in each query by your own account address
3. You can now start testing the routes in postman

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Changelog

**0.1.0**

- Initial release

## License

Copyright (c) 2016

Licensed under the [MIT license](LICENSE).

## Password Metamask

usage grass trick debris angry coral canoe provide budget clump skirt similar
